package org.oly.game.cache;

public class MapHashes {
	
	public int hash;
	public int id;
	
	public MapHashes(int hash, int id) {
		this.hash = hash;
		this.id = id;
	}

}
