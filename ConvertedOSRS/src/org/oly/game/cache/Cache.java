package org.oly.game.cache;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.oly.game.cache.stream.OutputStream;

import com.runecore.network.io.MessageBuilder;


public final class Cache {

	private static CacheFileManager[] cacheFileManagers;
	private static CacheFile containersInformCacheFile;
	public static final String DIRECTORY = "./data/cache62/";
	
	/**
	 * Creates RandomAccessFile instances to each cache file.
	 * Also sets the UpdateServers' CRCs (byte[] array) as 520 * 8 in size - but an empty array...
	 */
	public static final void init() throws IOException {
		byte[] cacheFileBuffer = new byte[520];
		MessageBuilder crcPacketGen = new MessageBuilder().writeByte((byte)0).writeInt(cacheFileBuffer.length * 8);
		RandomAccessFile containersInformFile = new RandomAccessFile(DIRECTORY+"/main_file_cache.idx255", "r");
		RandomAccessFile dataFile =	new RandomAccessFile(DIRECTORY+"/main_file_cache.dat2", "r");
		containersInformCacheFile = new CacheFile(255, containersInformFile, dataFile, 500000, cacheFileBuffer);
		
		int length = (int) (containersInformFile.length() / 6);//16 (96/6)
		cacheFileManagers = new CacheFileManager[length];//array with 16 components
		
		for(int i = 0; i < length; i++) {//created a new manager instance for idx 0-15
			File f = new File(DIRECTORY+"/main_file_cache.idx" + i);
			if(f.exists() && f.length() > 0) {
				cacheFileManagers[i] = new CacheFileManager(new CacheFile(i, new RandomAccessFile(f, "r"), dataFile, 1000000, cacheFileBuffer), true);
			}
		}
		UpdateServer.setCRC(crcPacketGen.toMessage().getBuffer().array());
	}
	
	public static final byte[] generateUkeysContainer() {
		OutputStream stream = new OutputStream(cacheFileManagers.length * 8);
		for(int index = 0; index < cacheFileManagers.length; index++) {
			if(cacheFileManagers[index] == null) {
				stream.writeInt(0);
				stream.writeInt(0);
				continue;
			}
			stream.writeInt(cacheFileManagers[index].getInformation().getInformationContainer().getCrc());
			stream.writeInt(cacheFileManagers[index].getInformation().getRevision());
		}
		byte[] ukeysContainer = new byte[stream.getOffset()];
		stream.setOffset(0);
		stream.getBytes(ukeysContainer, 0, ukeysContainer.length);
		return ukeysContainer;
	}
	
	public static final CacheFileManager[] getCacheFileManagers() {
		return cacheFileManagers;
	}
	
	public static final CacheFile getConstainersInformCacheFile() {
		return containersInformCacheFile;
	}
}