package org.oly.game.cache;

import com.runecore.network.io.Message;
import com.runecore.network.io.MessageBuilder;

public class UpdateServer {

	/**
	 * the CRC file as a byte array
	 */
	private static byte[] crcfile;
	
    public static Message getPacketRequest(int cacheId, int id) {
		try {
			MessageBuilder packet = new MessageBuilder().writeByte((byte) cacheId).writeShort(id);
			byte cache[] = getFile(cacheId, id);
			if (cache == null) {
				return null;
			}
			int len = (((cache[1] & 0xff) << 24) + ((cache[2] & 0xff) << 16) + ((cache[3] & 0xff) << 8) + (cache[4] & 0xff)) + 9; //ERROR?
			if (cache[0] == 0) {
				len -= 4;
			}
			int c = 3;//starting at the 4th byte
			for (int i = 0; i < len; i++) {
				if (c == 512) {
					packet.writeByte((byte) 0xFF);
					c = 1;
				}
				packet.writeByte(cache[i]);
				c++;
			}
			return packet.toMessage();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static synchronized byte[] getFile(int cache, int id) {
		try {
			if (cache == 255 && id == 255) {
				return crcfile;
			}
			return FileManager.getFile(cache, id);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static void setCRC(byte[] bytes) {
		crcfile = bytes;
	}
}