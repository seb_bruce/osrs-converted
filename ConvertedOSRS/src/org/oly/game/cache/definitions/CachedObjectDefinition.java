package org.oly.game.cache.definitions;

import org.oly.game.cache.Cache;
import org.oly.game.cache.stream.InputStream;
/**
 * revision #464 client object definition
 * @author Jak
 *
 */
@SuppressWarnings("unused")
public final class CachedObjectDefinition {

	public static final CachedObjectDefinition[] objectDefinitions = new CachedObjectDefinition[Cache.getCacheFileManagers()[2].getFilesSize(6)];
	
	public static final int MAX_OBJECT_DEFINITIONS_SIZE = objectDefinitions.length;//defs-1 as index starts at 0

	private int anInt1641 = -1;
    private int anInt1642;
    private int shading;
    private int anInt1644 = 0;
    private int[] childrenIDs;
    private String name;
    private int groundDecorationSprite;
    private boolean aBoolean1648;
    private int clipType = 2;
    private int id;
    private boolean isSolid;
    private int sizeX = 1;
    private int hasActions;
    private boolean projectileCliped;
    private boolean aBoolean1654;
    private int scaleX;
    private int[] originalModelColors;
    private int anInt1658;
    private String[] actions;
	private boolean aBoolean1660;
    private int animationId;
    private int[] modifiedModelColors;
    private int mapSceneSprite;
    private int sizeY;
    private boolean aBoolean1665;
    private boolean aBoolean1666;
    private int walkToData;
    private int anInt1670;
    private int anInt1671;
    private int scaleY;
    private int anInt1673;
    private boolean notCliped;
    private int[] types;
    private int scaleZ;
    private boolean aBoolean1677;
    private int[] models;
    private int lightness;
    
    public String name(){ return name; };
    public int id(){ return id; };
    public int sizeX(){ return sizeX; };
    public int sizeY(){ return sizeY; };
    public int clipType() { return clipType; };
    public int walkToData(){ return walkToData; };
    public boolean isSolid(){ return isSolid; };
    public String[] actions() { return actions; };
    
    final void checkOptions() {
    	if (hasActions == -1) {
    		hasActions = 0;
    		if (models != null && (types == null || types[0] == 10))
    			hasActions = 1;
		    for (int i_26_ = 0; i_26_ < 5; i_26_++) {
		    	if (actions[i_26_] != null)
		    		hasActions = 1;
		    }
    	}
    	if (anInt1671 == -1)
    		anInt1671 = isSolid ? 1 : 0;
    }
    
    public int actionCount() {
    	/*int count = 0;
    	for(int i = 0; i < actions.length; i++) {
    		if(actions[i] == null)
    			continue;
    		if(!actions[i].equalsIgnoreCase("null") || !actions[i].equalsIgnoreCase("hidden"))
    			count++;
    	}*/
    	return hasActions;
    }
    
    public boolean hasActions() {
    	for(int i = 0; i < actions.length; i++) {
    		if(actions[i] == null)
    			continue;
    		if(!actions[i].equalsIgnoreCase("null") || !actions[i].equalsIgnoreCase("hidden"))
    			return true;
    	}
    	return false;
    }
    
    public String[] getActions() {
    	String[] allActions = new String[actions.length];
    	for(int i = 0; i < actions.length; i++) {
    		if(actions[i] == null)
    			continue;
    		allActions[i] = actions[i];
    	}
    	return allActions;
    }
    /**
     * If a definition is not cached, its definition is read from the cache ON DEMAND (so when player needs it) (def only loaded once cos it wont we null after its loaded)
     * ENTIRE CACHE NOT LOADED ON SERVER STARTUP - proof: println when a definition is instanced 
     * if you then login you'll get a huge pritnln from all objects being loaded in regions + surrounding regions
     */
	public static final CachedObjectDefinition forId(int objId) {
		//TODO if rev==474 && id >= 464 length <= 474 end length then LOAD CUSTOM FROM PI
		if (objId < 0 || objId >= objectDefinitions.length)
			return null;
		CachedObjectDefinition def = objectDefinitions[objId];
		if (def == null)
			objectDefinitions[objId] = def = new CachedObjectDefinition(objId);
		if ((def.name != null && (def.name.equalsIgnoreCase("bank booth") || def.name.equalsIgnoreCase("counter")))) {
			def.notCliped = false;
			def.projectileCliped = true;
			if (def.clipType == 0)
				def.clipType = 1;
		}
		if (def.notCliped) {
			def.projectileCliped = false;
			def.clipType = 0;
		}
		if (def.id == 14432 || def.id == 2292) {
			def.clipType = 1;
		}
		if (def.id == 14435 || def.id == 2311) {
			def.clipType = 1;
		}
		return def;
	}

	/**
	 * creates an INSTANCE of an OBJECT DEFINITION
	 * @param id
	 */
	private CachedObjectDefinition(int id) {
		//System.out.println("Loading object definition from cache... (id "+id+")");
		this.id = id;
		setDefaultsVariableValues();
		loadObjectDefinitions();
	}
	
	/*public static final CachedObjectDefinition464 getHardCodedDef(int id) {
		CachedObjectDefinition464 def = new CachedObjectDefinition464(id);
		def.setDefaultsVariableValues();
		return def;
	}*/

	private final void loadObjectDefinitions() {
		byte[] data = Cache.getCacheFileManagers()[2].getFileData(6, id);
		if (data == null) {
			//System.out.println("No object definition in cache for id: " + id + ".");
			return;
		}
		readOpcodes(new InputStream(data));
		checkOptions();
	}

	private void setDefaultsVariableValues() {
		groundDecorationSprite = -1;
		scaleX = 128;
		animationId = -1;
		name = "null";
		aBoolean1654 = false;
		mapSceneSprite = -1;
		aBoolean1660 = false;
		isSolid = true;
		aBoolean1665 = false;
		aBoolean1666 = true;
		hasActions = -1;
		anInt1642 = 0;
		scaleY = 128;
		shading = 0;
		anInt1658 = -1;
		aBoolean1648 = false;
		sizeY = 1;
		notCliped = false;
		anInt1671 = -1;
		walkToData = 0;
		actions = new String[5];
		anInt1670 = 0;
		scaleZ = 128;
		anInt1673 = 16;
		aBoolean1677 = false;
		lightness = 0;
		projectileCliped = true;
	}

	private final void readOpcodes(InputStream buffer, int opcode) {
		if (opcode != 1) {
			if ((opcode ^ 0xffffffff) == -3)
				name = buffer.readString();
			else if (opcode == 5) {
				int size = buffer.readUnsignedByte();
	    		if (size > 0) {
	    			if (models == null) {
	    				types = null;
	    				models = new int[size];
	    				for (int id = 0; id < size; id++)
	    					models[id] = buffer.readUnsignedShort();
	    			} else
	    				buffer.offset += size * 2;
	    		}
			} else if (opcode == 14)
				sizeX = buffer.readUnsignedByte();
			else if ((opcode ^ 0xffffffff) == -16)
				sizeY = buffer.readUnsignedByte();
			else if ((opcode ^ 0xffffffff) != -18) {
				if ((opcode ^ 0xffffffff) == -19)
					projectileCliped = false;
				else if (opcode == 19)
					hasActions = buffer.readUnsignedByte();
				else if (opcode == 21)
					aBoolean1654 = true;
				else if (opcode == 22)
					aBoolean1648 = true;
				else if ((opcode ^ 0xffffffff) != -24) {
					if (opcode != 24) {
						if (opcode == 27)
							clipType = 1;
						else if (opcode != 28) {
							if ((opcode ^ 0xffffffff) == -30)
								lightness = buffer.readByte();
							else if (opcode != 39) {
								if (opcode < 30 || (opcode ^ 0xffffffff) <= -36) {
									if (opcode != 40) {
										if (opcode != 60) {
											if (opcode != 62) {
												if ((opcode ^ 0xffffffff) == -65)
													aBoolean1666 = false;
												else if ((opcode ^ 0xffffffff) == -66)
													scaleX = (buffer
															.readUnsignedShort());
												else if (opcode != 66) {
													if (opcode != 67) {
														if (opcode == 68)
															mapSceneSprite = (buffer.readUnsignedShort());
														else if (opcode == 69)
															walkToData = (buffer.readUnsignedByte());
														else if ((opcode ^ 0xffffffff) == -71)
															anInt1644 = (buffer.readShort());
														else if (opcode == 71)
															anInt1670 = (buffer.readShort());
														else if (opcode != 72) {
															if (opcode == 73)
																aBoolean1665 = true;
															else if ((opcode ^ 0xffffffff) == -75)
																notCliped = true;
															else if (opcode != 75) {
																if (opcode == 77) {
																	anInt1641 = buffer.readUnsignedShort();
																	if (anInt1641 == 65535)
																		anInt1641 = -1;
																	anInt1658 = buffer.readUnsignedShort();
																	if ((anInt1658 ^ 0xffffffff) == -65536)
																		anInt1658 = -1;
																	int i = buffer.readUnsignedByte();
																	childrenIDs = new int[i + 1];
																	for (int i_2_ = 0; (i_2_ ^ 0xffffffff) >= (i ^ 0xffffffff); i_2_++) {
																		childrenIDs[i_2_] = buffer.readUnsignedShort();
																		if ((childrenIDs[i_2_] ^ 0xffffffff) == -65536)
																			childrenIDs[i_2_] = -1;
																	}
																} else if (opcode == 78) {
																	buffer.readUnsignedShort();
																	buffer.readUnsignedByte();
																} else if ((opcode ^ 0xffffffff) == -80) {
																	buffer.readUnsignedShort();
																	buffer.readUnsignedShort();
																	buffer.readUnsignedByte();
																	int paris = buffer.readUnsignedByte();
																	for(int i = 0; i < paris; i++)
																		buffer.readUnsignedShort();
																}
															} else
																anInt1671 = (buffer.readByte());
														} else
															anInt1642 = (buffer.readShort());
													} else
														scaleZ = (buffer.readUnsignedShort());
												} else
													scaleY = (buffer.readUnsignedShort());
											} else
												aBoolean1660 = true;
										} else
											groundDecorationSprite = (buffer.readUnsignedShort());
									} else {
										int i_34_ = buffer.readUnsignedByte();
							    		modifiedModelColors = new int[i_34_];
							    		originalModelColors = new int[i_34_];
							    		for (int i_35_ = 0; i_34_ > i_35_; i_35_++) {
							    			originalModelColors[i_35_] = buffer.readUnsignedShort();
							    			modifiedModelColors[i_35_] = buffer.readUnsignedShort();
							    		}
									}
								} else {
									actions[opcode - 30] = buffer.readString();
						    		if (actions[opcode - 30].equalsIgnoreCase("hidden"))
						    			actions[opcode - 30] = null;
								}
							} else
								shading = buffer.readByte() * 5;
						} else
							anInt1673 = buffer.readUnsignedByte();
					} else {
						animationId = buffer.readUnsignedShort();
						if (animationId == 65535)
							animationId = -1;
					}
				} else
					aBoolean1677 = true;
			} else {
				clipType = 0;
				projectileCliped = false;
			}
		} else {
			int i = buffer.readByte();
			if ((i ^ 0xffffffff) < -1) {
				if (models != null)
					buffer.offset += 3 * i;
				else {
					models = new int[i];
					types = new int[i];
					for (int i_5_ = 0; (i ^ 0xffffffff) < (i_5_ ^ 0xffffffff); i_5_++) {
						models[i_5_] = buffer.readUnsignedShort();
						types[i_5_] = buffer.readByte();
					}
				}
			}
		}
		/*if (opcode == 1) {
    		int size = stream.readUnsignedByte();
    		if (size > 0) {
    			if (models == null) {
    				models = new int[size];
    				types = new int[size];
    				for (int id = 0; id < size; id++) {
    					models[id] = stream.readUnsignedShort();
    					types[id] = stream.readUnsignedByte();
    				}
    			} else
    				stream.offset += size * 3;
    		}
		}
    	if (opcode == 2) {
    		name = stream.readString();
    	}
    	if (opcode == 5) {
    		int size = stream.readUnsignedByte();
    		if (size > 0) {
    			if (models == null) {
    				types = null;
    				models = new int[size];
    				for (int id = 0; id < size; id++)
    					models[id] = stream.readUnsignedShort();
    			} else
    				stream.offset += size * 2;
    		}
    	}
    	if (opcode == 14) {
    		sizeX = stream.readUnsignedByte();
    	}
    	if (opcode == 15) {
    		sizeY = stream.readUnsignedByte();
    	}
		if (opcode == 17) {
			clipType = 0;
			projectileCliped = false;
		    isSolid = false;
		}
		if (opcode == 18) {
			projectileCliped = false;
    	}
    	if (opcode == 19) {
		    hasActions = stream.readUnsignedByte();
    	}
    	if (opcode == 21) {
		    aBoolean1654 = true;
    	}
    	if (opcode == 22) {
		    aBoolean1648 = true;
    	}
    	if (opcode == 23) {
    		aBoolean1677 = true;
    	}
    	if (opcode == 24) {
			animationId = stream.readUnsignedShort();
			if (animationId == 65535) {
			    animationId = -1;
		    }
    	}
    	if (opcode == 27) {
    		clipType = 1;
    	}
    	if (opcode == 28) {
		    anInt1673 = stream.readUnsignedByte();
    	}
    	if (opcode == 29) {
			lightness = stream.readByte();
    	}
    	if (opcode == 39) {
    		shading = stream.readByte() * 5;
    	}
    	if (opcode >= 30 && opcode < 35) {
    		actions[opcode - 30] = stream.readString();
    		if (actions[opcode - 30].equalsIgnoreCase("hidden"))
    			actions[opcode - 30] = null;
    	}
    	if (opcode == 40) {
    		int i_34_ = stream.readUnsignedByte();
    		modifiedModelColors = new int[i_34_];
    		originalModelColors = new int[i_34_];
    		for (int i_35_ = 0; i_34_ > i_35_; i_35_++) {
    			originalModelColors[i_35_] = stream.readUnsignedShort();
    			modifiedModelColors[i_35_] = stream.readUnsignedShort();
    		}
    	}
    	if (opcode == 60) {
    		groundDecorationSprite = stream.readUnsignedShort();
    	}
    	if (opcode == 62) {
		    aBoolean1660 = true;
    	}
    	if (opcode == 64) {
			aBoolean1666 = false;
    	}
    	if (opcode == 65) {
    		scaleX = stream.readUnsignedShort();
    	}
    	if (opcode == 66) {
		    scaleY = stream.readUnsignedShort();
    	}
    	if (opcode == 67) {
    		scaleZ = stream.readUnsignedShort();
    	}
    	if (opcode == 68) {
			mapSceneSprite = stream.readUnsignedShort();
    	}	
    	if (opcode == 69) {
    		//anInt1668 = stream.readUnsignedByte();
    	}
    	if (opcode == 70) {
		    anInt1644 = stream.readShort();
    	}
    	if (opcode == 71) {
			anInt1670 = stream.readShort();
    	}
    	if (opcode == 72) {
		    anInt1642 = stream.readShort();
    	}
    	if (opcode == 73) {
			aBoolean1665 = true;
    	}	
		if (opcode == 74) {
			notCliped = true;
		}
		if (opcode == 75) {
			anInt1671 = stream.readUnsignedByte();
		}
		if (opcode == 77) {
			anInt1641 = stream.readUnsignedShort();
			if (anInt1641 == 65535)
				anInt1641 = -1;
			anInt1658 = stream.readUnsignedShort();
			if (anInt1658 == 65535)
				anInt1658 = -1;
			int i_36_ = stream.readUnsignedByte();
			childrenIDs = new int[i_36_+ 1];
			for (int i_37_ = 0; (i_37_ <= i_36_); i_37_++) {
				childrenIDs[i_37_] = stream.readUnsignedShort();
				if (childrenIDs[i_37_] == 65535)
					childrenIDs[i_37_] = -1;
			}
		}
		if(opcode == 78) {
			stream.readUnsignedShort();
			stream.readByte();
		}
		if(opcode == 79) {
			stream.readUnsignedShort();
			stream.readUnsignedShort();
			stream.readByte();
			int paris = stream.readByte();
			for(int i = 0; i < paris; i++)
				stream.readUnsignedShort();
		}*/
	}

	private final void readOpcodes(InputStream stream) {
		while (true) {
			int opcode = stream.readUnsignedByte();
			if (opcode == 0)
				break;
			readOpcodes(stream, opcode);
		}
	}
	
	public boolean rangableObject() {
        int[] rangableObjects = {26975, 26977, 26978, 26979, 23271, 11754, 3007, 980, 997, 4262, 14437, 14438, 4437, 4439, 3487, 23053};
        for (int i = 0; i < rangableObjects.length; i++) {
        	if (rangableObjects[i] == id) {
        		return true;
        	}
        }
        if (name != null) {
            final String name1 = name.toLowerCase();
            String[] rangables = {"fungus", "mushroom", "sarcophagus", "counter", "plant", "altar", "pew", "log", "stump", "stool", "sign", "cart", "chest", "rock", "bush", "hedge", "chair", "table", "crate", "barrel", "box", "skeleton", "corpse", "vent", "stone", "rockslide"};
            for (int i = 0; i < rangables.length; i++) {
            	if (rangables[i].contains(name1) || rangables[i].equalsIgnoreCase(name1) || rangables[i].endsWith(name1)) {
            		return true;
            	}
            }
        }
        return false;
    }
}
