package org.oly.game.cache.definitions;

import org.oly.game.cache.Cache;
import org.oly.game.cache.stream.InputStream;

public final class CachedNpcDefinition {

	public static final CachedNpcDefinition[] npcDefinitions = new CachedNpcDefinition[Cache.getCacheFileManagers()[2].getFilesSize(9)];

	public int turn90CWAnimation;
	public int varbitId;
	public int varpId;
	public int[] childrenIDs;
	public int width;
	public int turnValue;
	public int headIcon;
	public String[] actions;
	public boolean canRightClick;
	public int[] model;
	public int idleAnimation;
	public boolean render;
	public int turn180Animation;
	public boolean displayOnMinimap;
	public int[] originalModelColors;
	public int contrast;
	public int id;
	public int combatLevel;
	public String name;
	public int turn90CCAnimation;
	public int ambient;
	public int height;
	public int size;
	public int walkAnimation;
	public int[] headModels;
	public int[] modifiedModelColor;
	public int opcode15 = -1;
	public int opcode16 = -1;
	public boolean opcode109 = true;

	public static final CachedNpcDefinition getNPCDefinitions(int npcId) {
		if (npcId < 0 || npcId >= npcDefinitions.length) {
			return null;
		}
		CachedNpcDefinition def = npcDefinitions[npcId];
		if (def == null)
			npcDefinitions[npcId] = def = new CachedNpcDefinition(npcId);
		return def;
	}

	private CachedNpcDefinition(int id) {
		this.id = id;
		setDefaultsVariableValues();
		loadNpcDefinitions();
	}

	private final void loadNpcDefinitions() {
		byte[] data = Cache.getCacheFileManagers()[2].getFileData(9, id);
		if (data == null) {
			return;
		}
		readOpcodes(new InputStream(data));
	}

	private void setDefaultsVariableValues() {
		varpId = -1;
		canRightClick = true;
		idleAnimation = -1;
		actions = new String[5];
		render = false;
		turn90CWAnimation = -1;
		varbitId = -1;
		contrast = 0;
		ambient = 0;
		combatLevel = -1;
		headIcon = -1;
		height = 128;
		width = 128;
		turnValue = 32;
		walkAnimation = -1;
		displayOnMinimap = true;
		size = 1;
		turn180Animation = -1;
		name = "null";
		turn90CCAnimation = -1;
	}

	private final void readOpcodes(InputStream stream, int opcode) {
		if (opcode == 1) {
			int i = stream.readUnsignedByte();
			model = new int[i];
			for (int i_2_ = 0; (i_2_ ^ 0xffffffff) > (i ^ 0xffffffff); i_2_++)
				model[i_2_] = stream.readUnsignedShort();
		} else if (opcode != 2) {
			if (opcode == 12)
				size = stream.readUnsignedByte();
			else if ((opcode ^ 0xffffffff) == -14)
				idleAnimation = stream.readUnsignedShort();
			else if (opcode != 14) {
				if ((opcode ^ 0xffffffff) != -16) {
					if (opcode == 16)
						opcode16 = stream.readUnsignedShort();
					else if ((opcode ^ 0xffffffff) == -18) {
						walkAnimation = stream.readUnsignedShort();
						turn180Animation = stream.readUnsignedShort();
						turn90CWAnimation = stream.readUnsignedShort();
						turn90CCAnimation = stream.readUnsignedShort();
					} else if (opcode >= 30 && opcode < 35) {
						actions[opcode - 30] = stream.readString();
					} else if ((opcode ^ 0xffffffff) != -41) {
						if (opcode == 60) {
							int count = stream.readUnsignedByte();
							headModels = new int[count];
							for (int id = 0; count > id; id++)
							    headModels[id] = stream.readUnsignedShort();
						} else if ((opcode ^ 0xffffffff) != -94) {
							if ((opcode ^ 0xffffffff) != -96) {
								if (opcode != 97) {
									if ((opcode ^ 0xffffffff) != -99) {
										if ((opcode ^ 0xffffffff) == -100)
											render = true;
										else if ((opcode ^ 0xffffffff) == -101)
											ambient = stream.readByte();
										else if ((opcode ^ 0xffffffff) == -102)
											contrast  = stream.readByte() * 5;
										else if ((opcode ^ 0xffffffff) == -103)
											headIcon = stream.readUnsignedShort();
										else if (opcode != 103) {
											if ((opcode ^ 0xffffffff) == -107) {
												varbitId = stream.readUnsignedShort();
									    		if (varbitId == 65535)
									    			varbitId = -1;
									    		varpId = stream.readUnsignedShort();
									    		if (varpId == 65535)
									    			varpId = -1;
									    		int len = stream.readUnsignedByte();
									    		childrenIDs = new int[len + 1];
									    		for (int id = 0; len >= id; id++) {
									    			childrenIDs[id] = stream.readUnsignedShort();
									    			if (childrenIDs[id] == 65535)
									    				childrenIDs[id] = -1;
									    		}
											} else if ((opcode ^ 0xffffffff) == -108)
												canRightClick = false;
											else if (opcode == 109)
												opcode109 = false;
										} else
											turnValue = stream.readUnsignedShort();
									} else
										height = stream.readUnsignedShort();
								} else
									width = stream.readUnsignedShort();
							} else
								combatLevel = stream.readUnsignedShort();
						} else
							displayOnMinimap = false;
					} else {
						int count = stream.readUnsignedByte();
						modifiedModelColor = new int[count];
					    originalModelColors = new int[count];
					    for (int id = 0; id < count; id++) {
					    	originalModelColors[id] = stream.readUnsignedShort();
							modifiedModelColor[id] = stream.readUnsignedShort();
					    }
					}
				} else
					opcode15 = stream.readUnsignedShort();
			} else
				walkAnimation = stream.readUnsignedShort();
		} else
			name = stream.readString();
	}

	private final void readOpcodes(InputStream stream) {
		while (true) {
			int opcode = stream.readUnsignedByte();
			if (opcode == 0)
				break;
			readOpcodes(stream, opcode);
		}
	}
}