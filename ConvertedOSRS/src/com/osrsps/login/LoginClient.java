package com.osrsps.login;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import com.runecore.network.NetworkEncoder;
import com.runecore.network.io.Message;
import com.runecore.network.io.Message.PacketType;
import com.runecore.network.io.MessageBuilder;

public class LoginClient implements Runnable {
	
	private final WorldConfiguration configuration;
	private final String server;
	private Channel channel;
	private boolean shutdown = Boolean.FALSE;
	private boolean authenticated = Boolean.FALSE;
	private LoginServerResponseHandler handler;
	private Short requestId = 0;
	private LinkedBlockingQueue<Message> messageQueue = new LinkedBlockingQueue<Message>();
	
	public LoginClient(String server, WorldConfiguration configuration) {
		this.server = server;
		this.configuration = configuration;
	}
	
	private boolean connect() throws Exception {
		if(handler == null)
			throw new RuntimeException("Handler isn't attached! Set one before connecting");
		ExecutorService service = Executors.newCachedThreadPool();
		ClientBootstrap bootstrap = new ClientBootstrap(new NioClientSocketChannelFactory(service, service));
		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
			@Override
			public ChannelPipeline getPipeline() throws Exception {
				ChannelPipeline p = Channels.pipeline();
				p.addFirst("encoder", new NetworkEncoder());
				p.addFirst("decoder", new FrameDecoder() {
					@Override
					protected Object decode(ChannelHandlerContext chx, Channel chan, ChannelBuffer buf) throws Exception {
						int opcode = buf.readByte();
						if(opcode < 0) {
							buf.discardReadBytes();
							return null;
						}
						int length = buf.readByte();
						if(length > buf.readableBytes()) {
							buf.discardReadBytes();
							return null;
						}
						if (length <= buf.readableBytes() && length > 0) {
							byte[] payload = new byte[length];
							buf.readBytes(payload, 0, length);
							return new Message(opcode, PacketType.STANDARD, ChannelBuffers.wrappedBuffer(payload));
						}
						return null;
					}
				});
				p.addLast("handler", new LoginClientEventHandler(LoginClient.this));
				return p;
			}	
		});
		return (channel = bootstrap.connect(new InetSocketAddress(server, 1337)).await().getChannel()).isConnected();
	}
	
	public static void main(String[] args) throws Exception {
		WorldConfiguration configuration = new WorldConfiguration(1, 0xff, "Main World", "localhost");
		LoginClient client = new LoginClient("localhost", configuration);
		client.attach(new LoginServerResponseHandler() {
			@Override
			public void responseRecieved(LoginServerResponse response) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void setup() throws Exception {
		if(connect()) {
			authenticate();
			while(!ready())
				Thread.sleep(10);
			
		}
	}
	
	public void attach(LoginServerResponseHandler handler) {
		if(this.handler != null)
			throw new RuntimeException("Handler already attached!");
		this.handler = handler;
	}
	
	public void authenticate() {
		new Thread(this).start();
		ChannelBuffer buffer = ChannelBuffers.dynamicBuffer();
		buffer.writeByte(configuration.id());
		BufferTools.writeString(configuration.activity(), buffer);
		buffer.writeByte(configuration.mask());
		BufferTools.writeString(configuration.address(), buffer);
		channel.write(buffer);
	}
	
	public Short requestLogin(String username, String password) {
		System.out.println("Requesting bruhhh "+username);
		if(requestId == Short.MAX_VALUE)
			requestId = 0;
		Short req = this.requestId++;
		MessageBuilder builder = new MessageBuilder(1, PacketType.VAR_SHORT);
		builder.writeString2(username);
		builder.writeString2(password);
		builder.writeShort(req);
		channel.write(builder.toMessage());
		return req;
	}
	
	public void finaliseLogin(int fid, int requestId) {
		MessageBuilder builder = new MessageBuilder(2, PacketType.VAR_SHORT);
		builder.writeInt(fid);
		builder.writeShort(requestId);
		channel.write(builder.toMessage());
	}
	
	public void removePlayer(int fid) {
		MessageBuilder builder = new MessageBuilder(3, PacketType.VAR_SHORT);
		builder.writeInt(fid);
		channel.write(builder.toMessage());
	}
	
	public void queue(Message message) {
		this.messageQueue.offer(message);
	}

	@Override
	public void run() {
		while(!shutdown) {
			try {
				handle(messageQueue.take());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		channel.close();
		Logger.getGlobal().info("Login client shutdown.");
	}
	
	private void handle(Message message) {
		int opcode = message.getOpcode();
		if(!authenticated) {
			if(opcode != 1) {
				shutdown();
			} else {
				authenticated = true; 
				Logger.getGlobal().info("Authenticated with login server!");
			}
		} else {
			if(opcode == 2) {
				short rid = message.readShort();
				String displayName = message.readString2();
				byte usergroup = message.readByte();
				byte response = message.readByte();
				int forumId = message.readInt();
				LoginServerResponse resp = new LoginServerResponse(usergroup, response, rid, forumId, displayName);
				handler.responseRecieved(resp);
			}
		}	
	}
	
	public boolean ready() {
		return authenticated;
	}
	
	public void shutdown() {
		shutdown = true;
		queue(new Message(0, null, ChannelBuffers.buffer(1))); //null packet incase client is idle
	}
	
}