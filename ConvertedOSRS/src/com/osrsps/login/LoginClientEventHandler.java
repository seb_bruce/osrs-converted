package com.osrsps.login;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

import com.runecore.network.io.Message;

public class LoginClientEventHandler extends SimpleChannelHandler {
	
	private final LoginClient client;
	
	public LoginClientEventHandler(LoginClient client) {
		this.client = client;
	}
	
	@Override
	public void messageReceived(ChannelHandlerContext context, MessageEvent msg) {
		if(msg.getMessage() instanceof Message) {
			try {
				Message message = (Message)msg.getMessage();
				client.queue(message);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}	
	}
	
	public void channelDisconnected(ChannelHandlerContext context, ChannelStateEvent event) {
		//System.out.println("closed?");
		client.shutdown();
	}
	

}
