package com.osrsps.login;

public class WorldConfiguration {
	
	private final int id, mask;
	private final String activity, address;
	
	public WorldConfiguration(int id, int mask, String activity, String address) {
		this.id = id;
		this.mask = mask;
		this.activity = activity;
		this.address = address;
	}

	public int id() {
		return id;
	}

	public int mask() {
		return mask;
	}

	public String activity() {
		return activity;
	}

	public String address() {
		return address;
	}

}