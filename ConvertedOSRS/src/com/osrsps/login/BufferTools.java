package com.osrsps.login;

import org.jboss.netty.buffer.ChannelBuffer;

public class BufferTools {
	
	public static ChannelBuffer writeString(String s, ChannelBuffer buffer) {
		byte[] string = s.getBytes();
		buffer.writeByte(string.length);
		buffer.writeBytes(string);
		return buffer;
	}

}
