package com.osrsps.login;

public class LoginServerResponse {
	
	private final short requestId;
	private final byte usergroupId;
	private final String username;
	private final byte responseCode;
	private final int forumId;
	
	public LoginServerResponse(byte usergroupId, byte responseCode, short requestId, int forumId, String displayName) {
		this.usergroupId = usergroupId;
		this.responseCode = responseCode;
		this.requestId = requestId;
		this.forumId = forumId;
		this.username = displayName;
	}

	public short getRequestId() {
		return requestId;
	}

	public byte getUsergroupId() {
		return usergroupId;
	}

	public String getUsername() {
		return username;
	}

	public byte getResponseCode() {
		return responseCode;
	}

	public int getForumId() {
		return forumId;
	}

}