package com.vendetta.proxy;

import com.vendetta.proxy.network.NetworkAdapter;

public class Application {
	
	public static void main(String[] args) {
		NetworkAdapter.bind();
		System.out.println("Vendetta Proxy Server online!");
	}

}