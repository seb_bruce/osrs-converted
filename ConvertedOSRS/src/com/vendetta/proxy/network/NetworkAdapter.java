package com.vendetta.proxy.network;

import java.net.InetSocketAddress;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

public class NetworkAdapter {
	
	public static void bind() {
		ServerBootstrap bootstrap = new ServerBootstrap();
		bootstrap.setOption("child.tcpNoDelay", true);
		bootstrap.setPipelineFactory(new InboundPipeline());
		Executor e = Executors.newCachedThreadPool();
		bootstrap.setFactory(new NioServerSocketChannelFactory(e, e));
		bootstrap.bind(new InetSocketAddress(5555));
	}

}
