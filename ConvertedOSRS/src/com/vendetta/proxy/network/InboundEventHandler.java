package com.vendetta.proxy.network;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

import com.vendetta.proxy.Constants;
import com.vendetta.proxy.task.ConnectTask;
import com.vendetta.proxy.task.RelayToTask;

public class InboundEventHandler extends SimpleChannelHandler {
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
		e.getCause().printStackTrace();
	}
	
	@Override
	public void channelClosed(final ChannelHandlerContext ctx, ChannelStateEvent e) {
		if(ctx.getAttachment() == null)
			return;
		final ProxySession session = (ProxySession)ctx.getAttachment();
		session.asyncTask(new Runnable() {
			@Override
			public void run() {
				session.terminate();
				System.err.println("Connection from "+ctx.getChannel().getRemoteAddress()+" terminated");
			}
		});
	}
	
	@Override
	public final void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) {
		System.err.println("New Proxy connection from "+ctx.getChannel().getRemoteAddress()+" => "+Constants.FORWARD_TO_HOST+":"+Constants.FORWARD_TO_PORT);
		final ProxySession session = new ProxySession(ctx.getChannel());
		ctx.setAttachment(session);
		session.asyncTask(new ConnectTask(session));
	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent evt) throws Exception {
		final Object msg = evt.getMessage();
		final ProxySession session = (ProxySession) ctx.getAttachment();
		session.asyncTask(new RelayToTask(session, msg));
	}

}