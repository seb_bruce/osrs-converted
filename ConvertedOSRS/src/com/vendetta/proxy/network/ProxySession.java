package com.vendetta.proxy.network;

import java.net.InetSocketAddress;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

import com.vendetta.proxy.Constants;
import com.vendetta.proxy.client.OutboundPipeline;

public class ProxySession {
	
	private final Channel from;
	private Channel to;
	private Executor executor = Executors.newSingleThreadExecutor();
	
	public ProxySession(Channel from) {
		this.from = from;
	}
	
	public void connect() {
		ClientBootstrap bootstrap = new ClientBootstrap();
		Executor e = Executors.newCachedThreadPool();
		bootstrap.setFactory(new NioClientSocketChannelFactory(e, e));
		bootstrap.setPipelineFactory(new OutboundPipeline());
		ChannelFuture future = bootstrap.connect(new InetSocketAddress(Constants.FORWARD_TO_HOST, Constants.FORWARD_TO_PORT));
		if(!future.awaitUninterruptibly(20, TimeUnit.SECONDS)) {
			System.out.println("Connection timed out to remote host.");
			terminate();
			return;
		}
		Channel channel = future.getChannel();
		channel.setAttachment(this);
		this.to = channel;
	}
	
	public void relayTo(Object buffer) {
		if(!from.isConnected() || !to.isConnected()) {
			terminate();
			return;
		}
		to.write(buffer);
	}
	
	public void relayFrom(Object buffer) {
		if(!from.isConnected() || !to.isConnected()) {
			terminate();
			return;
		}
		from.write(buffer);
	}
	
	public void terminate() {
		to.disconnect();
		from.disconnect();
	}
	
	public void asyncTask(Runnable r) {
		executor.execute(r);
	}

}