package com.vendetta.proxy.client;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

import com.vendetta.proxy.network.ProxySession;
import com.vendetta.proxy.task.RelayFromTask;

public class OutboundEventHandler extends SimpleChannelHandler {
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
		e.getCause().printStackTrace();
	}
	
	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent evt) throws Exception {
		final Object msg = evt.getMessage();
		final ProxySession session = (ProxySession) ctx.getChannel().getAttachment();
		session.asyncTask(new RelayFromTask(session, msg));
	}

}