package com.vendetta.proxy.task;

import com.vendetta.proxy.network.ProxySession;

public class RelayFromTask implements Runnable {
	
	private final ProxySession SESSION;
	private final Object MESSAGE;
	
	public RelayFromTask(ProxySession session, Object message) {
		this.SESSION = session;
		this.MESSAGE = message;
	}

	@Override
	public void run() {
		SESSION.relayFrom(MESSAGE);
	}

}
