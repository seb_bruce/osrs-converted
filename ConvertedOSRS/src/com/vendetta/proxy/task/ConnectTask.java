package com.vendetta.proxy.task;

import com.vendetta.proxy.network.ProxySession;

public class ConnectTask implements Runnable {
	
	private final ProxySession SESSION;
	
	public ConnectTask(ProxySession session) {
		this.SESSION = session;
	}

	@Override
	public void run() {
		SESSION.connect();
	}

}
