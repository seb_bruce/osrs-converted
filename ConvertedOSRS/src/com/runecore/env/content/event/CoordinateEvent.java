package com.runecore.env.content.event;

import com.runecore.env.core.Tick;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;

public class CoordinateEvent extends Tick {
	
	private final Location location;
	private final Player player;
	private final Runnable task;

	public CoordinateEvent(Player player, Location location, Runnable task) {
		super(1);
		this.location = location;
		this.player = player;
		this.task = task;
	}

	@Override
	public void execute() {
		if(player.isDead() || player.isDead()) {
			stop();
			return;
		}
		if(player.getWalking().isMoving())
			return;
		if(!player.getLocation().equals(location))
			return;
		task.run();
		stop();
	}

	public Location getLocation() {
		return location;
	}

	public Player getPlayer() {
		return player;
	}

}