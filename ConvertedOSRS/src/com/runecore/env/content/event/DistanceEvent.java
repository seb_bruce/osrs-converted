package com.runecore.env.content.event;

import com.runecore.env.core.Tick;
import com.runecore.env.model.player.Player;

public class DistanceEvent extends Tick {
	
	private final Player player;
	private final Runnable task;
	
	public DistanceEvent(Player player, Runnable task) {
		super(1);
		this.player = player;
		this.task = task;
	}

	@Override
	public void execute() {
		if(player.isDead() || player.isDead()) {
			stop();
			return;
		}
		if(player.getWalking().isMoving())
			return;
		stop();
		task.run();
	}

}