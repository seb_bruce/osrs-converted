package com.runecore.env.content.event.impl;

import com.runecore.Application;
import com.runecore.env.Context;
import com.runecore.env.content.event.DistanceEvent;
import com.runecore.env.model.map.GameObject;
import com.runecore.env.model.map.ObjectAdapter;
import com.runecore.env.model.map.ObjectOption;
import com.runecore.env.model.player.Player;

public class GameObjectInteractionEvent extends DistanceEvent {

	public GameObjectInteractionEvent(final Player player, final GameObject object, final ObjectOption option) {
		super(player, new Runnable() {
			@Override
			public void run() {
				player.face(object.getLocation());
				for(ObjectAdapter adapter : Context.get().getObjectAdapters()) {
					if(adapter.accept(player, object, option)) {
					    adapter.handle(player, object, option);
					    return;
					}
				}
				if(Application.DEBUG) {
					System.err.println("Debug: Unhandled object option "+object.getIdentifier()+" @ "+object.getLocation()+" "+option);
				}
			}
		});
	}

}
