package com.runecore.env.content.event.impl;

import com.runecore.codec.event.SendMessageEvent;
import com.runecore.env.Context;
import com.runecore.env.content.event.CoordinateEvent;
import com.runecore.env.model.container.Container;
import com.runecore.env.model.item.GroundItem;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;
import com.runecore.env.world.World;

public class GroundItemEvent extends CoordinateEvent {

	public GroundItemEvent(final Player player, final Location location, final GroundItem item) {
		super(player, location, new Runnable() {
			@Override
			public void run() {
				if(!World.get().groundItems().contains(item)) {
					player.getWalking().reset();
					return;
				}
				Container inventory = player.get("inventory");
				if(inventory.add(item.getItem())) {
					World.get().remove(item);
				} else {
					Context.get().getActionSender().sendMessage(new SendMessageEvent(player, "You don't have enough space in your inventory."));
				}
			}
		});
	}

}
