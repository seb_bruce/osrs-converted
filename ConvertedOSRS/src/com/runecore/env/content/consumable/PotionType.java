package com.runecore.env.content.consumable;

public enum PotionType {
	
	NORMAL_POTION,
	SUPER_POTION,
	SARADOMIN_BREW,
	RESTORE,
	SUPER_RESTORE,
	PRAYER_POTION;

}
