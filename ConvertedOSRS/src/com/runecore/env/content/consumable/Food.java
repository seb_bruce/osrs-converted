package com.runecore.env.content.consumable;

public enum Food {

	COD(7, 339, -1),
	TROUT(7, 333, -1),
	SALMON(9, 329, -1),
	TUNA(10, 361, -1),
	LOBSTER(12, 379, -1),
	BASS(13, 365, -1),
	SWORDFISH(14, 373, -1),
	MONKFISH(16, 7946, -1),
	SEATURTLE(21, 397, -1),
	MANTARAY(22, 391, -1),
	SHARK(20, 385, -1),
	PIZZA(1, 2293, 2295),
	HALFMEATPIZZA(1, 2295, -1);
	
	private final int heal, id, replace;
	
	Food(int heal, int id, int replace) {
		this.heal = heal;
		this.id = id;
		this.replace = replace;
	}
	
	public static Food get(int id) {
		for(Food f : Food.values()) {
			if(f.getId() == id) {
				return f;
			}
		}
		return null;
	}
	
	public int getHeal() {
		return heal;
	}
	
	public int getId() {
		return id;
	}
	
	public int getReplace() {
		return replace;
	}

}
