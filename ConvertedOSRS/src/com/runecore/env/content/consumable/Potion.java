package com.runecore.env.content.consumable;

import com.runecore.env.model.player.Skills;

public enum Potion {
	
	MAGIC(new int[] { 3046, 3044, 3042, 3040 }, new int[] { Skills.MAGIC }, PotionType.NORMAL_POTION),
	RANGE(new int[] { 173, 171, 169, 2444 }, new int[] { Skills.RANGE }, PotionType.NORMAL_POTION),
	SUPER_STRENGTH(new int[] { 161, 159, 157, 2440 }, new int[] { Skills.STRENGTH }, PotionType.SUPER_POTION),
	SUPER_DEFENCE(new int[] { 167, 165, 163, 2442 }, new int[] { Skills.DEFENCE }, PotionType.SUPER_POTION),
	SUPER_ATTACK(new int[] { 149, 147, 145, 2436 }, new int[] { Skills.ATTACK }, PotionType.SUPER_POTION),
	RESTORE_POTION(new int[] { 131, 129, 127, 2430 }, new int[] {}, PotionType.RESTORE),
	PRAYER_POTION(new int[] { 143, 141, 139, 2435 }, new int[] { Skills.PRAYER }, PotionType.PRAYER_POTION),
	SUPER_RESTORE(new int[] { 3030, 3028, 3026, 3024 }, new int[] {}, PotionType.SUPER_RESTORE),
	SARADOMIN_BREW(new int[] { 6691, 6689, 6687, 6685 }, new int[] {}, PotionType.SARADOMIN_BREW);
	
	private final int[] ids, levels;
	private final PotionType type;
	
	Potion(int[] ids, int[] levels, PotionType type) {
		this.ids = ids;
		this.levels = levels;
		this.type = type;
	}
	
	public static Potion get(int id) {
		for(Potion p : Potion.values()) {
			for(int i : p.getIds()) {
				if(i == id)
					return p;
			}
		}
		return null;
	}

	public int[] getIds() {
		return ids;
	}

	public int[] getLevels() {
		return levels;
	}

	public PotionType getType() {
		return type;
	}
	
	public int getIndex(int id) {
		for(int i = 0; i < getIds().length; i++) {
			if(id == getIds()[i]) {
				return i;
			}
		}
		return -1;
	}

}
