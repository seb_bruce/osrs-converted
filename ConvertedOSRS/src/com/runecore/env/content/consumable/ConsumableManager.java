package com.runecore.env.content.consumable;

import com.runecore.codec.event.SendMessageEvent;
import com.runecore.env.Context;
import com.runecore.env.model.Cooldown;
import com.runecore.env.model.def.ItemDefinition;
import com.runecore.env.model.flag.Animation;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;
import com.runecore.env.model.player.Skills;

public class ConsumableManager {

	public static void consume(Player player, Food food, int slot) {
		if(!player.isReady(Cooldown.FOOD))
			return;
		if(!player.isAnimating())
			player.animate(Animation.create(829));
		player.lock(3);
		ItemDefinition def = ItemDefinition.forId(food.getId());
		Context.get().getActionSender().sendMessage(new SendMessageEvent(player, "You eat the "+def.getName()+"."));
		player.get("i").set(slot, food.getReplace() != -1 ? new Item(food.getReplace(), 1) : null);
		int health = player.getSkills().getLevel()[3];
		player.heal(food.getHeal());
		if(health < player.getSkills().getLevel()[3]) {
			Context.get().getActionSender().sendMessage(new SendMessageEvent(player, "It heals some health."));
		}
		player.setCooldown(Cooldown.FOOD, 3);
		player.addToCooldown(Cooldown.COMBAT, 2);
	}

	public static void consume(Player player, Potion potion, int slot) {
		if(!player.isReady(Cooldown.POTION) || !player.isReady(Cooldown.FOOD))
			return;
		Item item = player.get("i").get(slot);
		if(!player.isAnimating())
			player.animate(Animation.create(829));
		int index = potion.getIndex(item.getId());
		int transformTo = getTransform(potion, index);
		player.get("i").set(slot, new Item(transformTo, 1));
		String msg = "You drink some of your "+item.getDefinition().getName().toLowerCase().replaceAll("(4)", "").replaceAll("(3)", "").replaceAll("(2)", "").replaceAll("(1)", "")+".";
		Context.get().getActionSender().sendMessage(new SendMessageEvent(player, msg.replace("()", " potion")));
		player.setCooldown(Cooldown.FOOD, 3);
		player.setCooldown(Cooldown.POTION, 3);
		player.addToCooldown(Cooldown.COMBAT, 2);
		switch(potion.getType()) {
		case SUPER_POTION:
			for(int i = 0; i < potion.getLevels().length; i++) {
				int skill = potion.getLevels()[i];
				int modification = (int) Math.floor(5 + (player.getSkills().getRealLevel(skill) * 0.15));
				player.getSkills().increaseLevelToMaximumModification(skill, modification);
			}
			break;
		case NORMAL_POTION:
			int skill = potion.getLevels()[0];
			if (potion.name().toLowerCase().contains("super"))
				player.getSkills().increaseLevelToMaximum(skill, (int) (5 + (player.getSkills().getRealLevel(skill) * 0.15)));
			else
				player.getSkills().increaseLevelToMaximum(skill, (int) (1 + (player.getSkills().getRealLevel(skill) * 0.8)));
			break;
		case PRAYER_POTION:
			player.getSkills().increaseLevelToMaximum(Skills.PRAYER, player.getSkills().getRealLevel(Skills.PRAYER) / 3);
			break;
		case SARADOMIN_BREW:
			player.getSkills().increaseLevelToMaximumModification(Skills.HITPOINTS, (int) (player.getSkills().getRealLevel(Skills.HITPOINTS) * 0.15));
			player.getSkills().increaseLevelToMaximum(Skills.DEFENCE, (int) (player.getSkills().getRealLevel(Skills.DEFENCE) * 0.25));
			int[] toDecrease = new int[] { Skills.ATTACK, Skills.STRENGTH, Skills.MAGIC, Skills.RANGE };
			for (int i = 0; i < toDecrease.length; i++) {
				player.getSkills().decreaseLevelToMinimum(toDecrease[i], (int) (player.getSkills().getRealLevel(toDecrease[i]) * 0.10));
			}
			break;
		case RESTORE:
			int[] skills = new int[] { Skills.ATTACK, Skills.STRENGTH, Skills.DEFENCE, Skills.MAGIC, Skills.RANGE };
			for (int i = 0; i < skills.length; i++) {
				if (i == Skills.HITPOINTS)
					continue;
				player.getSkills().increaseLevelToMaximum(skills[i], 10 + (player.getSkills().getRealLevel(skills[i]) / 3));
			}
			break;
		case SUPER_RESTORE:
			for (int i = 0; i < 23; i++) {
				if (i == Skills.HITPOINTS)
					continue;
				player.getSkills().increaseLevelToMaximum(i, (int) ((player.getSkills().getRealLevel(i) * 0.25) + 9));
			}
			break;
		}
		int currentPotionDose = 0;
		for(int i = 0; i < potion.getIds().length; i++) {
			if(item.getId() == potion.getIds()[i]) {
				currentPotionDose = i + 1;
				break;
			}
		}
		Context.get().getActionSender().sendMessage(new SendMessageEvent(player, currentPotionDose > 1 ? ("You have " + (currentPotionDose - 1) + " dose" + (currentPotionDose > 2 ? "s" : "") + " of potion left.") : "You have finished your potion."));
		
	}

	private static int getTransform(Potion p, int index) {
		if(index == 0)
			return 229;
		return p.getIds()[index - 1];
	}

}
