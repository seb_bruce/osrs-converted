package com.runecore.env.content.shops;

import com.runecore.env.model.player.Player;

public interface ShopAdapter {
	
	public void buyItem(Player p, Shop shop, int slot, int amount);
	public void sellItem(Player p, Shop shop, int slot, int amount);
	public void valueItem(Player p, Shop shop, int slot);
	public int getValue(Shop shop, int slot);

}
