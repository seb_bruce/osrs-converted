package com.runecore.env.content.shops;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.runecore.env.content.shops.impl.DefaultShopAdapter;

public class ShopManager {
	
	private static final ShopManager INSTANCE = new ShopManager();
	private static final ShopAdapter DEFAULT_ADAPTER = new DefaultShopAdapter();
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private Map<Integer, Shop> shops = new HashMap<Integer, Shop>();
	
	public void init() {
		shops = ShopManager.load();
	}
	
	public ShopAdapter getAdapter(Shop shop) {
		return DEFAULT_ADAPTER;
	}
	
	public Shop getShop(int id) {
		return shops.get(id);
	}
	
	public static void save(Map<Integer, Shop> shops) {
		try {
			FileWriter fw = new FileWriter("./data/shops.json");
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(gson.toJson(shops));
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Map<Integer, Shop> load() {
		String json = null;
		try {
			File file = new File("./data/shops.json");
			if(!file.exists())
				return null;
			FileReader reader = new FileReader(file);
			char[] chars = new char[(int) file.length()];
			reader.read(chars);
			json = new String(chars);
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gson.fromJson(json, new TypeToken<Map<Integer, Shop>>(){}.getType());
	}
	
	public static ShopManager get() {
		return INSTANCE;
	}

}