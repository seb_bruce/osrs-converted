package com.runecore.env.content.shops;

import java.util.ArrayList;
import java.util.List;

import com.runecore.codec.ActionSender;
import com.runecore.codec.Events;
import com.runecore.codec.codec530.extras.ClientScript;
import com.runecore.codec.event.SendInterfaceConfigEvent;
import com.runecore.codec.event.SendInterfaceEvent;
import com.runecore.codec.event.SendItemContainerEvent;
import com.runecore.codec.event.SendStringEvent;
import com.runecore.env.Context;
import com.runecore.env.model.container.Container;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;

public class Shop {
	
	private List<Item> items = new ArrayList<Item>();
	private final String name;
	private final int id;
	
	public Shop(String name, int id) {
		this.name = name;
		this.id = id;
	}
	
	public void open(Player player) {
		player.addAttribute("shop", this);
		ActionSender a = Context.get().getActionSender();
		Container stock = Container.wrap(getItems());
		Container inventory = player.get("inv");
		Object[] invparams = new Object[]{"","","","Sell 100", "Sell 50", "Sell 10", "Sell 5", "Sell 1", "Value", -1, 0, 7, 4, 93, 621 << 16};
		Object[] shopparams = new Object[]{"","","","Buy 100", "Buy 50", "Buy 10", "Buy 5", "Buy 1", "Value", -1, 0, 4, 10, 7, (620 << 16) + 24};
		
		a.sendString(new SendStringEvent(player, name, 620, 22));
		Events.openInvInterface(player, 621);
		Events.openInterface(player, 620);
		
		a.sendInterfaceConfig(new SendInterfaceConfigEvent(player, 620, 34, false));
		
		a.sendItemContainer(new SendItemContainerEvent(player, inventory, -1, 64209, 93, true));
		a.sendItemContainer(new SendItemContainerEvent(player, stock, -1, 64271, 7, true));
		ClientScript.send(player, 150, invparams, "IviiiIsssssssss");
		ClientScript.send(player, 150, shopparams, "IviiiIsssssssss");
		ClientScript.sendAMask(player, 1278, (621 * 65536), 0, 27);
		ClientScript.sendAMask(player, 1278, (620 * 65536) + 24, 0, 40);
	}
	
	public void refresh(Player player) {
		ActionSender a = Context.get().getActionSender();
		Container stock = Container.wrap(getItems());
		Container inventory = player.get("inv");
		a.sendItemContainer(new SendItemContainerEvent(player, inventory, -1, 64209, 93, true));
		a.sendItemContainer(new SendItemContainerEvent(player, stock, -1, 64271, 7, true));
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

}