package com.runecore.env.content.shops.impl;

import com.runecore.codec.event.SendMessageEvent;
import com.runecore.env.Context;
import com.runecore.env.content.shops.Shop;
import com.runecore.env.content.shops.ShopAdapter;
import com.runecore.env.model.container.Container;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;

public class DefaultShopAdapter implements ShopAdapter {

	@Override
	public void buyItem(Player p, Shop shop, int slot, int amount) {
		Item item = shop.getItems().get(slot);
		if(item == null)
			return;
		long price = (getValue(shop, slot) * amount);
		if(price < 0 || price > Integer.MAX_VALUE)
			return;
		final int cost = (int) price;
		final Container inv = p.get("inventory");
		if(item.getAmount() <= 0) {
			Context.get().getActionSender().sendMessage(new SendMessageEvent(p, "The shop has run out of stock."));
			return;
		}
		if(inv.amountOf(995) < cost) {
			Context.get().getActionSender().sendMessage(new SendMessageEvent(p, "You don't have enough coins to buy this amount"));
			return;
		}
		inv.remove(new Item(995, cost));
		inv.add(new Item(item.getId(), amount));
		item.setAmount(item.getAmount() - amount);
		shop.refresh(p);
	}

	@Override
	public void sellItem(Player p, Shop shop, int slot, int amount) {
		Item item = p.get("inventory").get(slot);
		if(item == null)
			return;
		Context.get().getActionSender().sendMessage(new SendMessageEvent(p, "Sell: "+item.getDefinition().getName()+" x "+amount));
	
	}

	@Override
	public void valueItem(Player p, Shop shop, int slot) {
		Item item = shop.getItems().get(slot);
		if(item == null)
			return;
		Context.get().getActionSender().sendMessage(new SendMessageEvent(p, item.getDefinition().getName()+" is worth 420 swags"));
	}

	@Override
	public int getValue(Shop shop, int slot) {
		// TODO Auto-generated method stub
		return 1;
	}

}