package com.runecore.env.content;

import com.runecore.codec.Events;
import com.runecore.env.model.player.Player;

public class Interfaces {

	public static void closeInterfaces(Player p) {
		if(p.getFacade().getOpenInterface() != -1) {
			Events.closeInterface(p, 548, 11);
			Events.closeInterface(p, 548, 80);
		}
	}

}
