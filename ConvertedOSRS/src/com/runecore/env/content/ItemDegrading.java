package com.runecore.env.content;

import java.util.HashMap;
import java.util.Map;

import com.runecore.env.model.item.Item;

public class ItemDegrading {
	
	private static Map<Integer, DegradableItemSet> itemSetMap = new HashMap<Integer, DegradableItemSet>();
	
	static {
		DegradableItemSet[] sets = DegradableItemSet.values();
		for(DegradableItemSet set : sets) {
			for(int i : set.itemSet) {
				itemSetMap.put(i, set);
			}
		}
	}
	
	public enum DegradableItemSet {
		
		DHAROK_HELM(new int[] { 4716, 4880, 4881, 4882, 4883, 4884 });
		
		private final int[] itemSet;
		
		DegradableItemSet(int[] itemSet) {
			this.itemSet = itemSet;
		}
		
		public int getNextId(int id) {
			int index = -1;
			for(int i = 0; i < itemSet.length; i++) {
				if(itemSet[i] == id) {
					index = i;
					break;
				}
			}
			if(index != -1) {
				if(index + 1 <= itemSet.length)
					return itemSet[index + 1];
			}
			return -1;
		}
		
	}
	
	public static DegradableItemSet getSet(Item item) {
		return getSet(item.getId());
	}
		
	public static DegradableItemSet getSet(int item) {
		return itemSetMap.get(item);
	}

}