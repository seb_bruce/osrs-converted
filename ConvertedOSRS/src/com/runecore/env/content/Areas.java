package com.runecore.env.content;

import com.runecore.env.model.Entity;
import com.runecore.env.world.Location;

public class Areas {
	
	public static final Location[] EDGE_BANK = new Location[] {
		Location.locate(3091, 3488, 0), Location.locate(3097, 3500, 0)
	};
	
	public static final Location[][] SAFE_AREA = new Location[][] {
		EDGE_BANK
	};
	
	public static final Location[][] MULTI_AREA = new Location[][] {
		
	};
	
	public static boolean inArea(Entity a, Location[][] areas) {
		return inArea(a.getLocation(), areas);
	}

	public static boolean inArea(Entity a, Location[] bounds) {
		return inArea(a.getLocation(), bounds);
	}
	
	public static boolean inArea(Location l, Location[][] areas) {
		for(Location[] area : areas) {
			if(inArea(l, area))
				return true;
		}
		return false;
	}
	
	public static boolean inArea(Location l, Location[] bounds) {
		Location low = bounds[0];
		Location hi = bounds[1];
		return l.getX() >= low.getX() && l.getX() <= hi.getX() && l.getY() >= low.getY() && l.getY() <= hi.getY();
	}
	
}