package com.runecore.env;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import mgi.tools.jagcached.cache.Cache;

import com.osrsps.login.LoginClient;
import com.osrsps.login.WorldConfiguration;
import com.runecore.codec.ActionSender;
import com.runecore.codec.NPCUpdateCodec;
import com.runecore.codec.PacketCodec;
import com.runecore.codec.PlayerUpdateCodec;
import com.runecore.codec.ProtocolCodec;
import com.runecore.codec.codec530.packet.ItemInteractionPacket;
import com.runecore.env.content.shops.ShopManager;
import com.runecore.env.core.GameEngine;
import com.runecore.env.groovy.GroovyEngine;
import com.runecore.env.groovy.GroovyScript;
import com.runecore.env.login.LoginClientEventHandler;
import com.runecore.env.login.LoginProcessor;
import com.runecore.env.model.def.ItemDefinition;
import com.runecore.env.model.map.ObjectAdapter;
import com.runecore.env.widget.WidgetAdapter;
import com.runecore.env.widget.WidgetAdapterRepository;
import com.runecore.env.world.PlayerAdapter;
import com.runecore.env.world.World;
import com.runecore.network.NetworkContext;

/**
 * Context.java
 * @author Harry Andreas<harry@runecore.org>
 * Feb 8, 2013
 */
public class Context {
    
    /**
     * The instance of the server context
     */
    private static Context context;
    
    /**
     * The ProtocolCodec the context is set to use
     */
    private ProtocolCodec codec;
    
    /**
     * The PlayerUpdateCodec
     */
    private PlayerUpdateCodec playerUpdateCodec;
    
    /**
     * The NPCUpdateCodec
     */
    private NPCUpdateCodec npcUpdateCodec;
    
    /**
     * An array of PacketCodecs
     */
    private PacketCodec[] packetCodecs;
    
    /**
     * A map of WidgetAdapterRepositories
     */
    private final Map<Integer, WidgetAdapterRepository> widgetAdapters = new HashMap<Integer, WidgetAdapterRepository>();
    
    /**
     * A List of ObjectAdapters
     */
    private final List<ObjectAdapter> objectAdapters = new LinkedList<ObjectAdapter>();
    
    /**
     * The GameEngine for this Context
     */
    private final GameEngine gameEngine = new GameEngine();
    
    /**
     * The instance of the GroovyEngine
     */
    private GroovyEngine groovyEngine;
    
    /**
     * The instance of the LoginProcessor
     */
    private LoginProcessor loginProcessor;
    
    /**
     * 
     */
    private LoginClient loginClient;
    
    /**
     * The instance of the ActionSender
     */
    private ActionSender actionSender;
    
    /**
     * The instance of the PlayerAdapter
     */
    private PlayerAdapter playerAdapter;
    
    /**
     * XTEA keys for map decryption
     */
    private final Map<Integer, Integer[]> xteas = new HashMap<Integer, Integer[]>();
    
    /**
     * 
     */
    private Cache cache;
    
    /**
     * Construct the Context instance
     * @param protocol The ProtocolCodec the context is set to use
     */
    public Context(ProtocolCodec protocol) {
    	this.setCodec(protocol);
    }
    
    /**
     * Logger instance for this class
     */
    private static final Logger LOGGER = Logger.getLogger(Context.class.getName());
    
    /**
     * Configure the Context
     */
    public void configure() throws Exception {
    	LOGGER.info("Configuring context with codec "+getCodec().getClass().getName());
    	packetCodecs = new PacketCodec[255];
    	ItemDefinition.init();
		setLoginProcessor(new LoginProcessor());
    	new Thread(getLoginProcessor()).start();
    	new ItemInteractionPacket().init(this);
    	getCodec().init(this);
    	World.get().init();
    	ShopManager.get().init();
    	
    	WorldConfiguration configuration = new WorldConfiguration(1, 1, "Main World", "localhost");
    	setLoginClient(new LoginClient("192.168.0.11", configuration));
    	getLoginClient().attach(new LoginClientEventHandler(getLoginProcessor()));
    	LOGGER.info("Connecting to LoginServer...");
    	getLoginClient().setup();
    	
    	InetSocketAddress socket = new InetSocketAddress("0.0.0.0", 43594);
		NetworkContext network = new NetworkContext(socket);
		network.configure(context);
		network.bind();
		
		setCache(Cache.openCache("./data/cache/"));
		
    }
    
    public void refresh() {
    	getGroovyEngine().resetCache();
		String[] paths = context.getCodec().scriptPaths();
		for (String s : paths) {
			for (File f : new File(s).listFiles()) {
				if (f.isDirectory())
					continue;
				if(f.getName().contains("network"))
					continue;
				String scriptName = f.getName().replaceAll(".groovy", "");
				GroovyScript script = getGroovyEngine().initScript(scriptName);
				script.init(context);
			}
		}
		LOGGER.info("Reloaded scripts m8");
    }
    
    public void register(ObjectAdapter adapter) {
    	getObjectAdapters().add(adapter);
    }
    
    public void register(int index, PacketCodec codec) {
    	packetCodecs[index] = codec;
    }
    
    public void register(int index, WidgetAdapter adapter) {
    	WidgetAdapterRepository repo = getWidgetAdapters().get(index);
    	if(repo == null) {
    		repo = new WidgetAdapterRepository();
    		getWidgetAdapters().put(index, repo);
    	}
    	repo.register(adapter);
    }
    
    public static Context get() {
    	return context;
    }
    
    public static void set(Context ctx) {
    	context = ctx;
    }

    public ProtocolCodec getCodec() {
    	return codec;
    }

    public void setCodec(ProtocolCodec codec) {
		this.codec = codec;
    }
    
    public GroovyEngine getGroovyEngine() {
    	return groovyEngine;
    }

    public void setGroovyEngine(GroovyEngine groovyEngine) {
    	this.groovyEngine = groovyEngine;
    }

	public LoginProcessor getLoginProcessor() {
		return loginProcessor;
	}

	public void setLoginProcessor(LoginProcessor loginProcessor) {
		this.loginProcessor = loginProcessor;
	}

	public ActionSender getActionSender() {
		return actionSender;
	}

	public void setActionSender(ActionSender actionSender) {
		this.actionSender = actionSender;
	}

	public PlayerUpdateCodec getPlayerUpdateCodec() {
		return playerUpdateCodec;
	}

	public void setPlayerUpdateCodec(PlayerUpdateCodec playerUpdateCodec) {
		this.playerUpdateCodec = playerUpdateCodec;
	}

	public PacketCodec[] getPacketCodecs() {
		return packetCodecs;
	}

	public void setPacketCodecs(PacketCodec[] packetCodecs) {
		this.packetCodecs = packetCodecs;
	}

	public List<ObjectAdapter> getObjectAdapters() {
		return objectAdapters;
	}

	public GameEngine getGameEngine() {
		return gameEngine;
	}

	public Map<Integer, WidgetAdapterRepository> getWidgetAdapters() {
		return widgetAdapters;
	}

	public PlayerAdapter getPlayerAdapter() {
		return playerAdapter;
	}

	public void setPlayerAdapter(PlayerAdapter playerAdapter) {
		this.playerAdapter = playerAdapter;
	}

	public NPCUpdateCodec getNpcUpdateCodec() {
		return npcUpdateCodec;
	}

	public void setNpcUpdateCodec(NPCUpdateCodec npcUpdateCodec) {
		this.npcUpdateCodec = npcUpdateCodec;
	}

	public Cache getCache() {
		return cache;
	}

	public void setCache(Cache cache) {
		this.cache = cache;
	}

	public Map<Integer, Integer[]> getXteas() {
		return xteas;
	}

	public LoginClient getLoginClient() {
		return loginClient;
	}

	public void setLoginClient(LoginClient loginClient) {
		this.loginClient = loginClient;
	}


}