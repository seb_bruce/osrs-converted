package com.runecore.env.world;

import com.runecore.env.model.player.Player;

public interface PlayerAdapter {
	
	public boolean load(Player player);
	public boolean save(Player player);	

}
