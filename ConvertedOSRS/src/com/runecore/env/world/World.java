package com.runecore.env.world;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import com.runecore.codec.PacketCodec;
import com.runecore.codec.event.SendGroundItemEvent;
import com.runecore.env.Context;
import com.runecore.env.core.GameEngine;
import com.runecore.env.core.Tick;
import com.runecore.env.core.events.GarbageCollectionEvent;
import com.runecore.env.core.task.ConcurrentTask;
import com.runecore.env.core.task.GameTask;
import com.runecore.env.core.task.SequentialTask;
import com.runecore.env.model.Cooldown;
import com.runecore.env.model.EntityList;
import com.runecore.env.model.container.Inventory;
import com.runecore.env.model.item.GroundItem;
import com.runecore.env.model.npc.NPC;
import com.runecore.env.model.player.Player;
import com.runecore.network.io.Message;

/**
 * World.java
 * @author Harry Andreas<harry@runecore.org>
 *  Feb 11, 2013
 */
public class World implements GameTask {

	/**
	 * Logger instance for this class
	 */
	private static final Logger logger = Logger.getLogger(World.class.getName());

	/**
	 * World Instance
	 */
	private static final World INSTANCE = new World();

	/**
	 * EntityList of Players
	 */
	private final EntityList<Player> players;

	/**
	 * EntityList of NPCs
	 */
	private final EntityList<NPC> npcs;

	/**
	 * EntityList of GroundItems
	 */
	private final List<GroundItem> groundItems;

	/**
	 * 
	 */
	private long cycleTime = 0;

	/**
	 * Construct the world
	 */
	private World() {
		players = new EntityList<Player>(2048);
		npcs = new EntityList<NPC>(5000);
		groundItems = new ArrayList<GroundItem>();
		Context.get().getGameEngine().register(this, 600, TimeUnit.MILLISECONDS);
		Context.get().getGameEngine().register(new GarbageCollectionEvent(), 2, TimeUnit.MINUTES);
	}

	private List<GameTask> tasks = Collections.synchronizedList(new LinkedList<GameTask>());
	private LinkedList<Tick> ticksToAdd = new LinkedList<Tick>();
	private LinkedList<Tick> ticks = new LinkedList<Tick>();

	@Override
	public void execute(GameEngine engine) {
		List<GameTask> queuedTasks = new LinkedList<GameTask>();
		List<GameTask> preTasks = new LinkedList<GameTask>();
		List<GameTask> cacheTasks = new LinkedList<GameTask>();
		List<GameTask> updateTasks = new LinkedList<GameTask>();
		List<GameTask> postTasks = new LinkedList<GameTask>();
		if(!tasks.isEmpty()) {
			Iterator<GameTask> it$ = tasks.iterator();
			while(it$.hasNext()) {
				queuedTasks.add(it$.next());
				it$.remove();
			}
		}
		preTasks.add(new GameTask() {
			@Override
			public void execute(GameEngine engine) {
				handleNetwork();
				processTicks();
			}
		});
		for(final NPC n : getNpcs()) {
			preTasks.add(new GameTask() {
				@Override
				public void execute(GameEngine engine) {
					n.processTicks();
					n.tick();
					n.processDamageQueue();
				}
			});
			cacheTasks.add(new GameTask() {
				@Override
				public void execute(GameEngine engine) {
					n.getFlagManager().pulse();
				}
			});
			postTasks.add(new GameTask() {
				@Override
				public void execute(GameEngine engine) {
					n.getFlagManager().reset();
				}
			});
		}
		
		players.forEach(p -> playerTick(p, preTasks, cacheTasks, updateTasks, postTasks));
		
		final long start = System.currentTimeMillis();
		GameTask cycleTimeTask = new GameTask() {
			@Override
			public void execute(GameEngine engine) {
				World.get().setCycleTime(System.currentTimeMillis() - start);
			}
		};
		//Collections.shuffle(preTasks); //lol pid switching
		GameTask queued = new SequentialTask(queuedTasks.toArray(new GameTask[0]));
		GameTask pre = new SequentialTask(preTasks.toArray(new GameTask[0]));
		GameTask cache = new ConcurrentTask(cacheTasks.toArray(new GameTask[0]));
		GameTask update = new ConcurrentTask(updateTasks.toArray(new GameTask[0]));
		GameTask post = new SequentialTask(postTasks.toArray(new GameTask[0]));
		engine.register(new SequentialTask(queued, pre, cache, update, post, cycleTimeTask));
	}
	
	private void playerTick(Player p, List<GameTask> preTasks, List<GameTask> cacheTasks, List<GameTask> updateTasks, List<GameTask> postTasks) {
		preTasks.add(new GameTask() {
			@Override
			public void execute(GameEngine engine) {
				p.tick();
				p.processTicks();
				p.processDamageQueue();
				p.getWalking().processNextMovement();
			}
		});
		cacheTasks.add(new GameTask() {
			@Override
			public void execute(GameEngine engine) {
				p.getFlagManager().pulse();
			}
		});
		updateTasks.add(new GameTask() {
			@Override
			public void execute(GameEngine engine) {
				Context.get().getPlayerUpdateCodec().update(p);
				//Context.get().getNpcUpdateCodec().update(p);
			}
		});
		postTasks.add(new GameTask() {
			@Override
			public void execute(GameEngine engine) {
				p.getFlagManager().reset();
			}
		});
	}

	public void init() {
		/*
		List<NPCAutoSpawn> autospawns = NPCAutoSpawn.load();
		for(NPCAutoSpawn n : autospawns) {
			NPC npc = new NPC(Location.locate(n.getX(), n.getY(), n.getZ()), n.getId());
			npc.getFacade().setDirection(n.getDirection());
			register(npc);
		}
		logger.info("Autospawned "+autospawns.size()+" npcs from JSON!");
		*/
	}

	public void processTicks() {
		if (ticksToAdd.size() > 0) {
			ticks.addAll(ticksToAdd);
			ticksToAdd = new LinkedList<Tick>();
		}
		for (Iterator<Tick> it = ticks.iterator(); it.hasNext();) {
			if (!it.next().run()) {
				it.remove();
			}
		}
	}

	public void bulkQueue(GameTask... tasks) {
		for(GameTask task : tasks) {
			queue(task);
		}
	}

	public void bulkRegister(GameTask... tasks) {
		for(GameTask task : tasks) {
			register(task);
		}
	}

	public void bulkRegister(Tick... ticks) {
		Arrays.asList(ticks).forEach(tick -> register(tick));
	}

	public void queue(GameTask task) {
		tasks.add(task);
	}

	public void register(GameTask task) {
		Context.get().getGameEngine().register(task);
	}

	public void register(Tick tick) {
		ticksToAdd.add(tick);
	}

	/**
	 * Handles networking events, packet execution, disconnections etc
	 */
	private void handleNetwork() {
		Iterator<Player> players = getPlayers().iterator();
		while (players.hasNext()) {
			Player player = players.next();
			if (player == null) {
				players.remove();
				continue;
			}
			if (!player.getSession().getChannel().isConnected()) {
				if(!player.isReady(Cooldown.LOGOUT))
					continue;
				logger.info(player.getDefinition().getName() + " has left the world");
				//if(!Application.LOCAL_BOOT)
				//	Context.get().getLoginConnector().logout(player.getDefinition().getProtocolName(), false);
				player.getSession().getQueuedPackets().clear();
				remove(player);
				players.remove();
				continue;
			}
		}
		//the fuck is this doing here lmfao
		players = getPlayers().iterator();
		while (players.hasNext()) {
			Player player = players.next();
			if (!player.getSession().getQueuedPackets().isEmpty()) {
				Message packet = null;
				while ((packet = player.getSession().getQueuedPackets().poll()) != null) {
					int opcode = packet.getOpcode();
					PacketCodec codec = Context.get().getPacketCodecs()[opcode];
					if (codec != null) {
						try {
							codec.execute(player, packet);
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	private void remove(Player player) {
		if(Context.get().getPlayerAdapter() != null) {
			try {
				Context.get().getPlayerAdapter().save(player);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		Context.get().getLoginClient().removePlayer(player.getDefinition().getForumId());
	}

	public void remove(GroundItem item) {
		groundItems().remove(item);
		Context.get().getActionSender().removeGroundItem(new SendGroundItemEvent(item.getOwner(), item));
	}

	/**
	 * Registers a Player
	 * 
	 * @param player
	 *            The player to register
	 * @return If it was successful
	 */
	public boolean register(Player player) {
		return players.add(player);
	}

	/**
	 * Gets the player by the user name.
	 */
	public Player getPlayer(String name) {
		for (Player player : players) {
			if (player.getDefinition().getName().equalsIgnoreCase(name))
				return player;
		}
		return null;
	}

	/**
	 * Registers a Player
	 * 
	 * @param player
	 *            The player to register
	 * @return If it was successful
	 */
	public boolean register(NPC npc) {
		return npcs.add(npc);
	}

	/**
	 * Registers a GroundItem
	 * @param groundItem
	 * @return If it was successful
	 */
	public void register(GroundItem groundItem) {
		if (groundItem.getItem().getDefinition().isStackable()) {
			for (GroundItem g : groundItems()) {
				if (g.getOwner().equals(groundItem.getOwner()) && g.getLocation().equals(groundItem.getLocation()) && g.getItem().getId() == groundItem.getItem().getId()) {
					g.getItem().setAmount(g.getItem().getAmount() + groundItem.getItem().getAmount());
					Context.get().getActionSender().clearGroundItem(g.getOwner(), g);
					Context.get().getActionSender().sendGroundItem(new SendGroundItemEvent(g.getOwner(), g));
					return;
				}
			}
		}
		groundItems().add(groundItem);
		Context.get().getActionSender().clearGroundItem(groundItem.getOwner(), groundItem);
		Context.get().getActionSender().sendGroundItem(new SendGroundItemEvent(groundItem.getOwner(), groundItem));
	}

	public GroundItem getGroundItem(Player player, Location loc, int id) {
		for(GroundItem g : groundItems()) {
			if(g == null || g.getItem().getId() != id)
				continue;
			if(g.getLocation().equals(loc) && g.getOwner().equals(player)) {
				return g;
			}
		}
		return null;
	}

	/**
	 * Gets the EntityList of Players
	 * 
	 * @return
	 */
	public EntityList<Player> getPlayers() {
		return players;
	}

	/**
	 * Gets the singleton instance of the World
	 * 
	 * @return
	 */
	public static World get() {
		return INSTANCE;
	}

	public EntityList<NPC> getNpcs() {
		return npcs;
	}

	public List<GroundItem> groundItems() {
		return groundItems;
	}

	public long getCycleTime() {
		return cycleTime;
	}

	public void setCycleTime(long cycleTime) {
		this.cycleTime = cycleTime;
	}

}