package com.runecore.env.world.loader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.runecore.env.model.container.Container;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;
import com.runecore.env.world.PlayerAdapter;

public class GSONPlayerAdapter implements PlayerAdapter {
	
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	@Override
	public boolean load(Player player) {
		File charFile = new File("./data/chars/"+player.getDefinition().getProtocolName()+".json");
		if(!charFile.exists())
			return false;
		String json = null;
		try {
			FileReader reader = new FileReader(charFile);
			char[] chars = new char[(int) charFile.length()];
			reader.read(chars);
			json = new String(chars);
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		PlayerDetails details = gson.fromJson(json, PlayerDetails.class);
		player.getWalking().setRunningToggled(details.running == 1);
		player.getFacade().setCurrentEP(details.ep > 0 ? details.ep : 0);
		player.setLocation(Location.locate(details.x, details.y, details.z));
		player.getCombatDefinition().setSpecialAmount(details.specialAmount);
		List<Item> inventory = details.inventory;
		List<Item> equip = details.equipment;
		List<Item> bank = details.bank;
		List<Skill> skills = details.skills;
		for(Item i : inventory) {
			com.runecore.env.model.item.Item item = new com.runecore.env.model.item.Item(i.item, i.amount, i.degrade);
			player.get("inv").set(i.index, item);
		}
		for(Item i : equip) {
			com.runecore.env.model.item.Item item = new com.runecore.env.model.item.Item(i.item, i.amount, i.degrade);
			player.get("equipment").set(i.index, item);
		}
		if(bank != null) {
			for(Item i : bank) {
				com.runecore.env.model.item.Item item = new com.runecore.env.model.item.Item(i.item, i.amount, i.degrade);
				player.get("bank").set(i.index, item);
			}
		}
		for(Skill s : skills) {
			player.getSkills().getLevel()[s.index] = s.level;
			player.getSkills().getXp()[s.index] = s.xp;
		}
		player.getFacade().setMuted(details.muted);
		return true;
	}

	@Override
	public boolean save(Player player) {
		PlayerDetails pd = new PlayerDetails();
		pd.running = player.getWalking().isRunning() ? 1 : 0;
		pd.ep = player.getFacade().getCurrentEP();
		pd.x = player.getLocation().getX();
		pd.y = player.getLocation().getY();
		pd.z = player.getLocation().getZ();
		for(int i = 0; i < player.getSkills().getLevel().length; i++) {
			pd.skills.add(new Skill(i, player.getSkills().getLevel()[i], player.getSkills().getXp()[i]));
		}
		Container inventory = player.get("inv");
		Container equipment = player.get("equipment");
		Container bank = player.get("bank");
		for(int i = 0; i < inventory.capacity(); i++) {
			if(inventory.get(i) == null)
				continue;
			pd.inventory.add(new Item(i, inventory.get(i).getId(), inventory.get(i).getAmount(), inventory.get(i).getDegradeCount()));
		}
		for(int i = 0; i < equipment.capacity(); i++) {
			if(equipment.get(i) == null)
				continue;
			pd.equipment.add(new Item(i, equipment.get(i).getId(), equipment.get(i).getAmount(), equipment.get(i).getDegradeCount()));
		}
		for(int i = 0; i < bank.capacity(); i++) {
			if(bank.get(i) == null)
				continue;
			pd.bank.add(new Item(i, bank.get(i).getId(), bank.get(i).getAmount(), bank.get(i).getDegradeCount()));
		}
		pd.specialAmount = player.getCombatDefinition().getSpecialAmount();
		pd.muted = player.getFacade().isMuted();
		try {
			FileWriter fw = new FileWriter("./data/chars/" + player.getDefinition().getProtocolName() + ".json");
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(gson.toJson(pd));
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	private class PlayerDetails { 
		
		public int specialAmount, attackStyle, attackSkill;
		public List<Skill> skills = new ArrayList<Skill>();
		public List<Item> inventory = new ArrayList<Item>();
		public List<Item> equipment = new ArrayList<Item>();
		public List<Item> bank = new ArrayList<Item>();
		public int x, y, z, ep, running;
		public boolean muted = false;
		
	}
	
	private class Skill {
		
		public final int index, level;
		public final double xp;
		
		public Skill(int index, int level, double xp) {
			this.index = index;
			this.level = level;
			this.xp = xp;
		}
		
	}
	
	private class Item {
		
		public final int index, item, amount, degrade;
		
		public Item(int index, int item, int amount, int degrade) {
			this.index = index;
			this.item = item;
			this.amount = amount;
			this.degrade = degrade;
		}
		
	}

}