package com.runecore.env.core;

public abstract class Condition<T> {
	
	public abstract boolean validate(T node);

}
