package com.runecore.env.core.task;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import com.runecore.env.core.GameEngine;

/**
 * SequentialTask.java
 * @author Harry Andreas<harry@runecore.org> 
 * Feb 20, 2013
 */
public class SequentialTask implements GameTask {

	private Collection<GameTask> tasks;

	public SequentialTask(GameTask... tasks) {
		this.tasks = Collections.unmodifiableCollection(Arrays.asList(tasks));
	}

	@Override
	public void execute(GameEngine engine) {
		tasks.stream().forEach(task -> task.execute(engine));
	}

}