package com.runecore.env.core.task;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import com.runecore.env.core.GameEngine;

/**
 * ConcurrentTask.java
 * @author Harry Andreas<harry@runecore.org>
 *  Feb 20, 2013
 */
public class ConcurrentTask implements GameTask {

	private final Collection<GameTask> tasks;

	public ConcurrentTask(GameTask... tasks) {
		this.tasks = Collections.unmodifiableCollection(Arrays.asList(tasks));
	}

	@Override
	public void execute(GameEngine engine) {
		tasks.parallelStream().forEach(task -> task.execute(engine));
	}

}