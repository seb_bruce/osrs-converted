package com.runecore.env.core;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.runecore.env.Context;
import com.runecore.env.core.task.GameTask;

/**
 * GameEngine.java
 * @author Harry Andreas<harry@runecore.org> 
 * Feb 20, 2013
 */
public class GameEngine implements Runnable {

	private final BlockingQueue<GameTask> tasks = new LinkedBlockingQueue<GameTask>();
	private final ScheduledExecutorService logicService = Executors.newScheduledThreadPool(1);

	@Override
	public void run() {
		while (true) {
			try {
				register(tasks.take());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void register(final GameTask task, int delay, TimeUnit unit) {
		logicService.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				task.execute(Context.get().getGameEngine());
			}
		}, delay, delay, unit);
	}

	public void register(final GameTask task) {
		logicService.submit(new Runnable() {
			@Override
			public void run() {
				task.execute(Context.get().getGameEngine());
			}
		});
	}
	
	public void execute(List<Runnable> tasks) {
		execute(tasks.toArray(new Runnable[tasks.size()]));
	}
	
	public void execute(Runnable... tasks) {
		for(Runnable r : tasks) {
			try {
				r.run();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

}