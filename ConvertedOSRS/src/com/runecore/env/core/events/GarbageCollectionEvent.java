package com.runecore.env.core.events;

import java.util.logging.Logger;

import com.runecore.env.core.GameEngine;
import com.runecore.env.core.task.GameTask;
import com.runecore.env.world.World;
import com.runecore.util.MemoryUtils;

public class GarbageCollectionEvent implements GameTask {
	
	private static final Logger logger = Logger.getLogger(GarbageCollectionEvent.class.getName());

	@Override
	public void execute(GameEngine engine) {
		Runtime.getRuntime().runFinalization();
		Runtime.getRuntime().gc();
		long inUse = Runtime.getRuntime().totalMemory();
		logger.info("Memory Usage: "+MemoryUtils.readable(inUse, true)+" Cycle Time: "+World.get().getCycleTime()+"ms Players Online: "+World.get().getPlayers().size());
	}

}
