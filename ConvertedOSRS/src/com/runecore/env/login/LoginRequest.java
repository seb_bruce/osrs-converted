package com.runecore.env.login;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;

import com.osrsps.login.LoginServerResponse;

/**
 * LoginRequest.java
 * 
 * @author Harry Andreas<harry@runecore.org> Feb 9, 2013
 */
public class LoginRequest {

	private final String user, pass;
	private final int displayMode;
	private final Channel channel;
	private final ChannelHandlerContext chc;
	private boolean submitted = Boolean.FALSE;
	private LoginServerResponse response;
	public final long requestedAt;

	public LoginRequest(Channel channel, ChannelHandlerContext chc,
			String user, String pass, int displayMode) {
		this.channel = channel;
		this.chc = chc;
		this.user = user;
		this.pass = pass;
		this.displayMode = displayMode;
		this.requestedAt = System.currentTimeMillis();
	}

	public String getUser() {
		return user;
	}

	public String getPass() {
		return pass;
	}

	public int getDisplayMode() {
		return displayMode;
	}

	public Channel getChannel() {
		return channel;
	}

	public ChannelHandlerContext getChc() {
		return chc;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

	public LoginServerResponse getResponse() {
		return response;
	}

	public void setResponse(LoginServerResponse response) {
		this.response = response;
	}

}