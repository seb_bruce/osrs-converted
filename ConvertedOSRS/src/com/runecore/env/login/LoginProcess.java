package com.runecore.env.login;

import java.util.logging.Logger;

import com.runecore.codec.codec60.net.GamePacketDecoder;
import com.runecore.env.Context;
import com.runecore.env.core.GameEngine;
import com.runecore.env.core.task.GameTask;
import com.runecore.env.model.def.PlayerDefinition;
import com.runecore.env.model.flag.UpdateFlag;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.World;
import com.runecore.network.GameSession;
import com.runecore.network.io.MessageBuilder;

/**
 * LoginProcess.java
 * @author Harry Andreas<harry@runecore.org>
 *  Feb 10, 2013
 */
public class LoginProcess implements Runnable {

	/**
	 * Logger instance
	 */
	private static final Logger logger = Logger.getLogger(LoginProcess.class.getName());

	/**
	 * The LoginRequest instance
	 */
	private LoginRequest request;

	/**
	 * Construct the login process
	 * @param request
	 */
	public LoginProcess(LoginRequest request) {
		this.request = request;
	}

	@Override
	/**
	 * Execute the login process
	 */
	public void run() {
		if(request.getResponse() == null) {
			Short id = Context.get().getLoginClient().requestLogin(request.getUser(), request.getPass());
			Context.get().getLoginProcessor().await(id, request);
		} else {
			long elapsed = System.currentTimeMillis() - request.requestedAt;
			System.out.println("Response took "+elapsed+"ms");
			handleResponse();
		}
	}
	
	private void handleResponse() {
		 final byte responseCode = request.getResponse().getResponseCode();
		 final GameSession session = new GameSession(request.getChannel());
		 final PlayerDefinition definition = 
				 new PlayerDefinition(request.getResponse().getUsername(), request.getResponse().getForumId());
		 final Player player = new Player(session, definition);
		 GameTask task = new GameTask() {
			 @Override
			 public void execute(GameEngine engine) {
				 byte resp = responseCode;
				 if (resp == 2 && !World.get().register(player)) {
					 resp = 7;
				 }
				 if (resp == 2) {
					if(Context.get().getPlayerAdapter().load(player)) {
						 logger.info("Loaded profile using "+Context.get().getPlayerAdapter().getClass().getName());
					}
					request.getChc().getPipeline().replace("decoder", "decoder", new GamePacketDecoder(session));
					logger.info(definition.getName() + " has entered the world");
					Context c = Context.get();
					c.getLoginClient().finaliseLogin(request.getResponse().getForumId(), request.getResponse().getRequestId());
					c.getActionSender().sendLogin(player);
					player.death();
					player.getFlagManager().flag(UpdateFlag.APPERANCE);
				 } else {
					 player.getSession().write(new MessageBuilder().writeByte(resp).toMessage());
				 }
			 }
		 };
		 World.get().queue(task);
	}

}