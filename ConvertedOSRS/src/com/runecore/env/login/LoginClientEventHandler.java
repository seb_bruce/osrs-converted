package com.runecore.env.login;

import com.osrsps.login.LoginServerResponse;
import com.osrsps.login.LoginServerResponseHandler;

public class LoginClientEventHandler implements LoginServerResponseHandler {
	
	private final LoginProcessor processor;
	
	public LoginClientEventHandler(LoginProcessor processor) {
		this.processor = processor;
	}

	@Override
	public void responseRecieved(LoginServerResponse response) {
		processor.response(response);
	}

}