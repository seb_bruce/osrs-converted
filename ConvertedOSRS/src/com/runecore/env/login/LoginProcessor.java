package com.runecore.env.login;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import com.osrsps.login.LoginServerResponse;

/**
 * LoginProcessor.java
 * 
 * @author Harry Andreas<harry@runecore.org> Feb 9, 2013
 */
public class LoginProcessor implements Runnable {

	/**
	 * Variables for the LoginProcessor
	 */
	private BlockingQueue<LoginRequest> requests = new LinkedBlockingQueue<LoginRequest>();
	private ExecutorService executor = Executors.newSingleThreadExecutor();
	private Map<Short, LoginRequest> requestsAwaiting = new HashMap<Short, LoginRequest>();

	@Override
	/**
	 * Processes the login queue
	 */
	public void run() {
		while (true) {
			try {
				executor.submit(new LoginProcess(requests.take()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void await(Short requestId, LoginRequest request) {
		if(requestsAwaiting.containsKey(requestId)) {
			throw new RuntimeException("REQUEST ID IS ALREADY AWAITING RESPONSE!");
		}
		requestsAwaiting.put(requestId, request);
	}
	
	//handles the callback from login server etc
	//TODO: handle timeouts fam
	public void response(LoginServerResponse response) {
		Short key = response.getRequestId();
		if(!requestsAwaiting.containsKey(key)) {
			throw new RuntimeException("Request for key "+key+" not found!");
		}
		LoginRequest request = requestsAwaiting.get(key);
		request.setResponse(response);
		requestsAwaiting.remove(key);
		queue(request);
	}
	
	public void queue(LoginRequest request) {
		requests.offer(request);
	}

}