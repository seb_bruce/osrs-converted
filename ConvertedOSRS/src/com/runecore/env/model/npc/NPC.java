package com.runecore.env.model.npc;

import com.runecore.env.model.Entity;
import com.runecore.env.model.def.NPCDefinition;
import com.runecore.env.world.Location;

public class NPC extends Entity {
	
	private final NPCFacade facade = new NPCFacade();
	private final NPCDefinition definition;
	
	public NPC(Location location, int id) {
		setLocation(location);
		this.definition = NPCDefinition.forId(id);
		getFacade().setHitpoints(getDefinition().getHitpoints());
		getFacade().setSpawnLocation(location.clone());
	}
	
	@Override
	public NPCDefinition getDefinition() {
		return definition;
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
	}

	@Override
	public void dropLoot() {
		// TODO Auto-generated method stub
	}

	@Override
	public void restore() {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean isDead() {
		return getFacade().getHitpoints() <= 0;
	}

	@Override
	public void heal(int heal) {
		// TODO Auto-generated method stub
	}

	@Override
	public void hit(int damage) {
		// TODO Auto-generated method stub
	}

	public NPCFacade getFacade() {
		return facade;
	}

}