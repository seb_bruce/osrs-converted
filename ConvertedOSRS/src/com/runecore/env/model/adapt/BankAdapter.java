package com.runecore.env.model.adapt;

import com.runecore.env.model.container.Bank;
import com.runecore.env.model.container.Container;
import com.runecore.env.model.container.ContainerAdapter;
import com.runecore.env.model.player.Player;

public class BankAdapter implements ContainerAdapter {
	
	private final Player player;

	public BankAdapter(Player player) {
		this.player = player;
	}

	@Override
	public void itemChanged(Container container, int slot) {
		Bank.refresh(player);
	}

	@Override
	public void itemsChanged(Container container, int[] slots) {
		Bank.refresh(player);
	}

	@Override
	public void itemsChanged(Container container) {
		Bank.refresh(player);
	}

}
