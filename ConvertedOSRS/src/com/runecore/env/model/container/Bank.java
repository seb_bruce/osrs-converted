package com.runecore.env.model.container;

import com.runecore.codec.ActionSender;
import com.runecore.codec.Events;
import com.runecore.codec.codec530.extras.ClientScript;
import com.runecore.codec.event.SendInterfaceConfigEvent;
import com.runecore.codec.event.SendItemContainerEvent;
import com.runecore.codec.event.SendMessageEvent;
import com.runecore.codec.event.SendSettingEvent;
import com.runecore.codec.event.SendSettingEvent.SettingType;
import com.runecore.env.Context;
import com.runecore.env.model.adapt.BankAdapter;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;

public class Bank {

	public static void init(Player player) {
		player.get("bank").addListener(new BankAdapter(player));
	}

	public static void withdraw(Player player, int slot,int amount) {
		Item item = player.get("bank").get(slot);
		if (item != null) {
			if (amount > item.getAmount())
				amount = item.getAmount();
			if (player.get("i").hasRoomFor(new Item(item.getId(), amount))) {
				player.get("i").add(new Item(item.getId(), amount));
				if (item.getAmount() == amount) {
					player.get("bank").remove(item);
					player.get("bank").shift();
				} else
					item.setAmount(item.getAmount() - amount);
				player.get("i").fireItemsChanged();
				player.get("bank").fireItemsChanged();
			} else {
				Events.sendMsg(player, "You do not have enough space for this item.");
			}
		}
	}

	/**
	 * Deposits an item.
	 * 
	 * @param player
	 *            The player.
	 * @param slot
	 *            The slot in the player's inventory.
	 * @param id
	 *            The item id.
	 * @param amount
	 *            The amount of the item to deposit.
	 */
	public static void deposit(Player player, int slot, int id, int amount) {
		deposit(player, slot, id, amount, true);
	}

	/**
	 * Deposits an item.
	 * 
	 * @param player
	 *            The player.
	 * @param slot
	 *            The slot in the player's inventory.
	 * @param id
	 *            The item id.
	 * @param amount
	 *            The amount of the item to deposit.
	 */
	public static void deposit(Player player, int slot, int id, int amount,
			boolean fireItemsChanged) {
		boolean inventoryFiringEvents = player.get("i").isFiringEvents();
		player.get("i").setFiringEvents(false);
		try {
			Item item = player.get("i").get(slot);
			if (item == null) {
				return; // invalid packet, or client out of sync
			}
			if (item.getId() != id) {
				return; // invalid packet, or client out of sync
			}
			int transferAmount = player.get("i").getCount(id);
			if (transferAmount >= amount) {
				transferAmount = amount;
			} else if (transferAmount == 0) {
				return; // invalid packet, or client out of sync
			}
			boolean noted = item.getDefinition().isNoted();
			if (item.getDefinition().isStackable() || noted) {
				int bankedId = noted ? item.getDefinition().getId() - 1
						: item.getId();
				if (player.get("bank").freeSlots() < 1
						&& player.get("bank").getById(bankedId) == null) {
					Context.get().getActionSender().sendMessage(new SendMessageEvent(player, "You don't have enough space in your bank account."));
				}
				// we only need to remove from one stack
				int newInventoryAmount = item.getAmount() - transferAmount;
				Item newItem;
				if (newInventoryAmount <= 0) {
					newItem = null;
				} else {
					newItem = new Item(item.getId(), newInventoryAmount);
				}
				if (!player.get("bank").add(new Item(bankedId, transferAmount), -1)) {
					Context.get().getActionSender().sendMessage(new SendMessageEvent(player, "You don't have enough space in your bank account."));
				} else {
					player.get("i").set(slot, newItem);
					player.get("i").fireItemsChanged();
					player.get("bank").fireItemsChanged();
				}
			} else {
				if (player.get("bank").freeSlots() < transferAmount) {
					Context.get().getActionSender().sendMessage(new SendMessageEvent(player, "You don't have enough space in your bank account."));
				}
				if (!player.get("bank").add(new Item(item.getId(), transferAmount), -1)) {
					Context.get().getActionSender().sendMessage(new SendMessageEvent(player, "You don't have enough space in your bank account.")); 
				} else {
					// we need to remove multiple items
					for (int i = 0; i < transferAmount; i++) {
						/*
						 * if(i == 0) { player.get("i").set(slot, null); }
						 * else {
						 */
						player.get("i")
						.set(player.get("i").getSlotById(
								item.getId()), null);
						// }
					}
					if (fireItemsChanged)
						player.get("i").fireItemsChanged();
				}
			}
		} finally {
			player.get("i").setFiringEvents(inventoryFiringEvents);
		}
		//player.getActionSender().removeChatboxInterface();
	}


	public static void open(Player player) {
		player.getWalking().reset();
		ActionSender a = Context.get().getActionSender();
		player.get("bank").currentTab = 10;
		ClientScript.sendConfig2(player, 563, 4194304);
		a.sendSetting(new SendSettingEvent(player, 1248, -2013265920, SettingType.NORMAL));
		a.sendSetting(new SendSettingEvent(player, 1249, 5000, SettingType.NORMAL));
		ClientScript.sendAMask(player, 2760446, 762 << 16 | 73, 0, 506);
		ClientScript.sendAMask(player, 2360446, 763 << 16, 0, 27);
		a.sendBlankClientScript(player, 239, 1451);
		refresh(player);
		Events.openInterface(player, 762);
		Events.openInvInterface(player, 763);
		a.sendInterfaceConfig(new SendInterfaceConfigEvent(player, 762, 18, true));
		a.sendInterfaceConfig(new SendInterfaceConfigEvent(player, 762, 19, true));
		a.sendInterfaceConfig(new SendInterfaceConfigEvent(player, 762, 23, false));
		Container bank = player.get("bank");
		ClientScript.sendInterfaceScript(player, 192, bank.capacity() - bank.freeSlots());
	}

	public static void refresh(Player p) {
		ActionSender a = Context.get().getActionSender();
		Container bank = p.get("bank");
		ClientScript.sendInterfaceScript(p, 192, bank.capacity() - bank.freeSlots());
		int config = 0; 
		config += bank.getTabSize(2);
		config += bank.getTabSize(3) * 1024;
		config += bank.getTabSize(4) * 1048576;
		ClientScript.sendConfig2(p, 1246, config);
		//a.sendSetting(new SendSettingEvent(p, 1246, config, SettingType.NORMAL));
		config = 0;
		config += bank.getTabSize(5);
		config += bank.getTabSize(6) * 1024;
		config += bank.getTabSize(7) * 1048576;
		ClientScript.sendConfig2(p, 1247, config);
		int tab = (bank.currentTab == 10 ? 0 : bank.currentTab - 1);
		config = -2013265920;
		config += bank.getTabSize(8);
		config += bank.getTabSize(9) * 1024;
		config += tab * 134217728;
		ClientScript.sendConfig2(p, 1248, config);
		a.sendItemContainer(new SendItemContainerEvent(p, bank, -1, 64000, 95, false));
		p.get("i").fireItemsChanged();
	}

}
