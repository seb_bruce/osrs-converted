package com.runecore.env.model.container;

import com.runecore.codec.Events;
import com.runecore.codec.event.SendInterfaceConfigEvent;
import com.runecore.codec.event.SendItemContainerEvent;
import com.runecore.codec.event.SendStringEvent;
import com.runecore.env.Context;
import com.runecore.env.model.adapt.EquipmentAdapter;
import com.runecore.env.model.def.ItemDefinition;
import com.runecore.env.model.flag.UpdateFlag;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;

/**
 * Equipment.java
 * @author Harry Andreas<harry@runecore.org>
 * Feb 20, 2013
 */
public class Equipment {
	
	public static final int SLOT_HELM = 0;
	public static final int SLOT_CAPE = 1;
	public static final int SLOT_AMULET = 2;
	public static final int SLOT_WEAPON = 3;
	public static final int SLOT_CHEST = 4;
	public static final int SLOT_SHIELD = 5;
	public static final int SLOT_BOTTOMS = 7;
	public static final int SLOT_GLOVES = 9;
	public static final int SLOT_BOOTS = 10;
	public static final int SLOT_RING = 12;
	public static final int SLOT_ARROWS = 13;

	public static void init(Player player) {
		Container container = player.get("equip");
		container.addListener(new EquipmentAdapter(player));
		refresh(player);
	}
	
	public static void displayBonuses(Player player) {
		Object[] opts = new Object[]{"", "", "", "", "Wear<col=ff9040>", -1, 0, 7, 4, 93, 43909120};
/*		p.getWalkingQueue().reset();
		p.getFrames().clearMapFlag();
		p.getFrames().displayInterface(667);
		p.getBonuses().refresh();
		p.getFrames().displayInventoryInterface(670);
		p.getFrames().sendClientScript2(172, 149, opts, "IviiiIsssss");
		p.getFrames().sendAccessMask(1026, (670 * 65536), 0, 27);
		p.getFrames().sendAccessMask(1278, (667 * 65536) + 14, 0, 13);//shows rightclickin on equip
*/	
		player.getWalking().reset();
		player.getCombatDefinition().refresh();
		Events.openInterface(player, 667);
		Events.openInvInterface(player, 670);
		Context.get().getActionSender().sendClientScript2(player, 172, 149, opts, "IviiiIsssss");
	}
	
	public static void setWeapon(Player player) {
		Item wep = player.equipment().get(3);
		Player p = player;
		boolean hd = player.getSession().getDisplayMode() > 0;
		if(wep == null) {
			Context.get().getActionSender().sendTab(p, hd ? 93 : 83, 92);
			Context.get().getActionSender().sendString(new SendStringEvent(p, "Unarmed", 92, 0));
			//AttackInterfaceConfig.setButtonForAttackStyle(p, 92);
			return;
		}
		String weapon = wep.getDefinition().getName();
		int osrsconfig = getConfigForWeapon(weapon);
		Events.sendConfig(player, 843, osrsconfig);
	
		player.getCombat().determineCombatStyles();
		setSpecials(player);
	}
	
	private static void setSpecials(Player player) {
		Item wep = player.get("e").get(3);
		if(wep == null)
			return;
		player.getCombatDefinition().refreshSpecial();
	}

	public static void refresh(Player player) {
		player.getFlagManager().flag(UpdateFlag.APPERANCE);
		Container container = player.get("equip");
		Context c = Context.get();
		c.getActionSender().sendItemContainer(new SendItemContainerEvent(player, container, -1, 64208, 94, false));
		setWeapon(player);
		player.getCombatDefinition().refresh();
	}
	
	/**
	 * Checks if an item is 2 handed
	 * @author Graham Edgecombe
	 * @param def The ItemDefinition
	 * @return If it's two handed
	 */
	public static boolean isTwoHanded(ItemDefinition def) {
		String wepEquiped = def.getName();
		if(wepEquiped.endsWith("claws")) {
			return true;
		}
		if (wepEquiped.endsWith("2h sword"))
			return true;
		else if (wepEquiped.endsWith("longbow"))
			return true;
		else if (wepEquiped.equals("Seercull"))
			return true;
		else if (wepEquiped.endsWith("shortbow"))
			return true;
		else if (wepEquiped.endsWith("Longbow"))
			return true;
		else if (wepEquiped.endsWith("Shortbow"))
			return true;
		else if (wepEquiped.endsWith("bow full"))
			return true;
		else if (wepEquiped.endsWith("halberd"))
			return true;
		else if (wepEquiped.equals("Granite maul"))
			return true;
		else if (wepEquiped.equals("Karils crossbow"))
			return true;
		else if (wepEquiped.equals("Torags hammers"))
			return true;
		else if (wepEquiped.equals("Veracs flail"))
			return true;
		else if (wepEquiped.equals("Dharok's greataxe"))
			return true;
		else if (wepEquiped.equals("Guthans warspear"))
			return true;
		else if (wepEquiped.equals("Tzhaar-ket-om"))
			return true;
		else if (wepEquiped.endsWith("godsword"))
			return true;
		else if (wepEquiped.contains("Trident"))
			return true;
		else if (wepEquiped.equals("Saradomin sword"))
			return true;
		else
			return false;
	}
	
	private static int getConfigForWeapon(String weapon) {
		weapon=weapon.toLowerCase();
		if(weapon.contains("unarmed"))
			return 0;
		if (weapon.contains("battleaxe") || weapon.contains("greataxe")) {
			return 1;
		}
		if (weapon.contains("warhammer") || weapon.equals("granite maul") || weapon.contains("torag's hammer")) {
			return 2;
		}
		if (weapon.contains("toktz-xil-ul")) {
			return 2;
		} else if (weapon.endsWith("bow") && !weapon.endsWith("crossbow")) {
			return 3;
		}
		if (weapon.endsWith("claws")) {
			return 4;	
		} else if (weapon.endsWith("crossbow") && !weapon.startsWith("karil")) {
			return 5;
		} else if (weapon.endsWith("salamander")) {
			return 6;
		} else if (weapon.endsWith("chinchompa")) {
			return 7;
		} else if (weapon.endsWith("bazooka")) {
			return 8;
		} else if (weapon.contains("scimitar")||weapon.endsWith("longsword")) {//same, one is dagger?
			return 9;
		} else if (weapon.endsWith("godsword")||weapon.endsWith("2h sword")) {//same, one is sword? chop slash smash block
			return 10;
		} else if (weapon.endsWith("pickaxe")) {
			return 11;
		}
		if (weapon.contains("halberd")) {
			return 12;
		} 
		//TODO staff bash (dramen staff) no spellbook - what staff no spellbook - norm staffs
		if (weapon.contains("dramen staff") || weapon.equalsIgnoreCase("dragon cane")||weapon.equalsIgnoreCase("lunar staff")||weapon.equals("toktz-mej-tal")) {
			return 13;
		} 
		else if (weapon.endsWith("scythe")) {
			return 14;
		}
		if (weapon.contains("spear")||weapon.contains("zamorakian spear")||weapon.contains("guthans spear")||weapon.contains("hasta")) {
			return 15;
		}
		if (weapon.contains("barrelchest anchor")||weapon.equals("verac's flail")||weapon.contains("mace")) {
			return 16;
		}
		if (weapon.contains("dagger")||weapon.endsWith("sword")) {//stab lunge slash block
			return 17;
		}
		if (weapon.endsWith("staff")||weapon.endsWith("wand")||weapon.startsWith("start of ")) {//autocasting
			return 18;
		}
		if (weapon.contains("knife")||weapon.contains("dart")) {
			return 19;
		}
		if (weapon.contains("whip")||weapon.contains("tentacle")) {
			return 20;
		}
		if (weapon.endsWith("staff of the dead")) {//jab swipe fend (more of a spear)
			return 21;	
		}
		//TODO 22 chop slash smash block same as 10
		if (weapon.contains("tzhaar-ket-om")) {//obby rings - no rapid
			return 23;
		}
		//24 is spear identical to 15
		//25+ punch block unarmed
		return 0;
	}
	

}