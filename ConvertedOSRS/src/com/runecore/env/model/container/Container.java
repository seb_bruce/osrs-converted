package com.runecore.env.model.container;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.runecore.env.model.item.Item;

/**
 * A container holds a group of items.
 * 
 * @author Graham Edgecombe
 * 
 */
public class Container {

	public int currentTab = 10;
	public final int[] tabStartingSlot = new int[11];

	/**
	 * The type of container.
	 * 
	 * @author Graham Edgecombe
	 * 
	 */
	public enum Type {

		/**
		 * A standard container such as inventory.
		 */
		STANDARD,

		/**
		 * A container which always stacks, e.g. the bank, regardless of the
		 * item.
		 */
		ALWAYS_STACK,

	}

	/**
	 * The capacity of this container.
	 */
	private int capacity;

	/**
	 * The items in this container.
	 */
	private Item[] items;

	/**
	 * A list of listeners.
	 */
	private List<ContainerAdapter> listeners = new LinkedList<ContainerAdapter>();

	/**
	 * The container type.
	 */
	private Type type;

	/**
	 * Firing events flag.
	 */
	private boolean firingEvents = true;

	/**
	 * Creates the container with the specified capacity.
	 * 
	 * @param type
	 *            The type of this container.
	 * @param capacity
	 *            The capacity of this container.
	 */
	public Container(Type type, int capacity) {
		this.type = type;
		this.capacity = capacity;
		this.items = new Item[capacity];
	}
	
	public static Container wrap(List<Item> items) {
		Container c = new Container(Type.STANDARD, items.size());
		int index = 0;
		for(Item i : items) {
			c.set(index, i);
			index++;
		}
		return c;
	}

	/**
	 * Sets the firing events flag.
	 * 
	 * @param firingEvents
	 *            The flag.
	 */
	public void setFiringEvents(boolean firingEvents) {
		this.firingEvents = firingEvents;
	}

	/**
	 * Checks the firing events flag.
	 * 
	 * @return <code>true</code> if events are fired, <code>false</code> if not.
	 */
	public boolean isFiringEvents() {
		return firingEvents;
	}

	/**
	 * Gets the listeners of this container.
	 * 
	 * @return The listeners of this container.
	 */
	public Collection<ContainerAdapter> getListeners() {
		return Collections.unmodifiableCollection(listeners);
	}

	/**
	 * Adds a listener.
	 * 
	 * @param listener
	 *            The listener to add.
	 */
	public void addListener(ContainerAdapter listener) {
		listeners.add(listener);
		listener.itemsChanged(this);
	}

	/**
	 * Removes a listener.
	 * 
	 * @param listener
	 *            The listener to remove.
	 */
	public void removeListener(ContainerAdapter listener) {
		listeners.remove(listener);
	}

	/**
	 * Removes all listeners.
	 */
	public void removeAllListeners() {
		listeners.clear();
	}

	/**
	 * Shifts all items to the top left of the container leaving no gaps.
	 */
	public void shift() {
		Item[] old = items;
		items = new Item[capacity];
		int newIndex = 0;
		for (int i = 0; i < items.length; i++) {
			if (old[i] != null) {
				items[newIndex] = old[i];
				newIndex++;
			}
		}
		if (firingEvents) {
			fireItemsChanged();
		}
	}

	/**
	 * Gets the next free slot.
	 * 
	 * @return The slot, or <code>-1</code> if there are no available slots.
	 */
	public int freeSlot() {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Attempts to add an item into the next free slot.
	 * 
	 * @param item
	 *            The item.
	 * @return <code>true</code> if the item was added, <code>false</code> if
	 *         not.
	 */
	public boolean add(Item item) {
		if (item.getDefinition().isStackable()
				|| type.equals(Type.ALWAYS_STACK)) {
			for (int i = 0; i < items.length; i++) {
				if (items[i] != null && items[i].getId() == item.getId()) {
					int totalCount = item.getAmount() + items[i].getAmount();
					if (totalCount >= Integer.MAX_VALUE || totalCount < 1) {
						return false;
					}
					set(i, new Item(items[i].getId(), items[i].getAmount()
							+ item.getAmount(), item.getDegradeCount()));
					return true;
				}
			}
			int slot = freeSlot();
			if (slot == -1) {
				return false;
			} else {
				set(slot, item);
				return true;
			}
		} else {
			int slots = freeSlots();
			if (slots >= item.getAmount()) {
				boolean b = firingEvents;
				firingEvents = false;
				try {
					for (int i = 0; i < item.getAmount(); i++) {
						set(freeSlot(), new Item(item.getId(), item.getAmount(), item.getDegradeCount()));
					}
					if (b) {
						fireItemsChanged();
					}
					return true;
				} finally {
					firingEvents = b;
				}
			} else {
				return false;
			}
		}
	}
	
	public boolean add(Item item, int slot) {
		if (item == null)
			return false;
		int newSlot = (slot > -1) ? slot : freeSlot();
		if ((item.getDefinition().isStackable() || type
				.equals(Type.ALWAYS_STACK)) && !type.equals(Type.STANDARD)) {
			if (getCount(item.getId()) > 0) {
				newSlot = getSlotById(item.getId());
			}
		}
		if (newSlot == -1) {
			// the free slot is -1
			return false;
		}
		if (get(newSlot) != null) {
			newSlot = freeSlot();
		}
		if ((item.getDefinition().isStackable() || type
				.equals(Type.ALWAYS_STACK)) && !type.equals(Type.STANDARD)) {
			for (int i = 0; i < items.length; i++) {
				if (items[i] != null && items[i].getId() == item.getId()) {
					long totalCount = item.getAmount() + items[i].getAmount();
					if (totalCount >= Integer.MAX_VALUE || totalCount < 1) {
						return false;
					}
					set(i, new Item(items[i].getId(), items[i].getAmount()
							+ item.getAmount()));
					return true;
				}
			}
			if (newSlot == -1) {
				return false;
			} else {
				set(slot > -1 ? newSlot : freeSlot(), item);
				return true;
			}
		} else {
			int slots = freeSlots();
			if (slots >= item.getAmount()) {
				boolean b = firingEvents;
				firingEvents = false;
				try {
					for (int i = 0; i < item.getAmount(); i++) {
						set(slot > -1 ? newSlot : freeSlot(),
								new Item(item.getId()));
					}
					if (b) {
						fireItemsChanged();
					}
					return true;
				} finally {
					firingEvents = b;
				}
			} else {
				return false;
			}
		}
	}

	/**
	 * Gets the number of free slots.
	 * 
	 * @return The number of free slots.
	 */
	public int freeSlots() {
		return capacity - size();
	}

	/**
	 * Gets an item.
	 * 
	 * @param index
	 *            The position in the container.
	 * @return The item.
	 */
	public Item get(int index) {
		return items[index];
	}
	
	public Item[] getItems() {
		return items;
	}

	/**
	 * Gets an item by id.
	 * 
	 * @param id
	 *            The id.
	 * @return The item, or <code>null</code> if it could not be found.
	 */
	public Item getById(int id) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null) {
				continue;
			}
			if (items[i].getId() == id) {
				return items[i];
			}
		}
		return null;
	}

	/**
	 * Gets a slot by id.
	 * 
	 * @param id
	 *            The id.
	 * @return The slot, or <code>-1</code> if it could not be found.
	 */
	public int getSlotById(int id) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null) {
				continue;
			}
			if (items[i].getId() == id) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Sets an item.
	 * 
	 * @param index
	 *            The position in the container.
	 * @param item
	 *            The item.
	 */
	public void set(int index, Item item) {
		items[index] = item;
		if (firingEvents) {
			fireItemChanged(index);
		}
	}

	/**
	 * Gets the capacity of this container.
	 * 
	 * @return The capacity of this container.
	 */
	public int capacity() {
		return capacity;
	}

	/**
	 * Gets the size of this container.
	 * 
	 * @return The size of this container.
	 */
	public int size() {
		int size = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				size++;
			}
		}
		return size;
	}

	/**
	 * Clears this container.
	 */
	public void clear() {
		items = new Item[items.length];
		if (firingEvents) {
			fireItemsChanged();
		}
	}

	/**
	 * Returns an array representing this container.
	 * 
	 * @return The array.
	 */
	public Item[] toArray() {
		return items;
	}

	/**
	 * Checks if a slot is used.
	 * 
	 * @param slot
	 *            The slot.
	 * @return <code>true</code> if an item is present, <code>false</code>
	 *         otherwise.
	 */
	public boolean isSlotUsed(int slot) {
		return items[slot] != null;
	}

	/**
	 * Checks if a slot is free.
	 * 
	 * @param slot
	 *            The slot.
	 * @return <code>true</code> if an item is not present, <code>false</code>
	 *         otherwise.
	 */
	public boolean isSlotFree(int slot) {
		return items[slot] == null;
	}

	/**
	 * Removes an item.
	 * 
	 * @param item
	 *            The item to remove.
	 * @return The number of items removed.
	 */
	public int remove(Item item) {
		return remove(-1, item);
	}

	/**
	 * Removes an item.
	 * 
	 * @param preferredSlot
	 *            The preferred slot.
	 * @param item
	 *            The item to remove.
	 * @return The number of items removed.
	 */
	public int remove(int preferredSlot, Item item) {
		int removed = 0;
		if (item.getDefinition().isStackable()
				|| type.equals(Type.ALWAYS_STACK)) {
			int slot = getSlotById(item.getId());
			Item stack = get(slot);
			if (stack.getAmount() > item.getAmount()) {
				removed = item.getAmount();
				set(slot,
						new Item(stack.getId(), stack.getAmount()
								- item.getAmount()));
			} else {
				removed = stack.getAmount();
				set(slot, null);
			}
		} else {
			for (int i = 0; i < item.getAmount(); i++) {
				int slot = getSlotById(item.getId());
				if (i == 0 && preferredSlot != -1) {
					Item inSlot = get(preferredSlot);
					if (inSlot.getId() == item.getId()) {
						slot = preferredSlot;
					}
				}
				if (slot != -1) {
					removed++;
					set(slot, null);
				} else {
					break;
				}
			}
		}
		return removed;
	}

	/**
	 * Transfers an item from one container to another.
	 * 
	 * @param from
	 *            The container to transfer from.
	 * @param to
	 *            The container to transfer to.
	 * @param fromSlot
	 *            The slot in the original container.
	 * @param id
	 *            The item id.
	 * @return A flag indicating if the transfer was successful.
	 */
	public static boolean transfer(Container from, Container to, int fromSlot,
			int id) {
		Item fromItem = from.get(fromSlot);
		if (fromItem == null || fromItem.getId() != id) {
			return false;
		}
		if (to.add(fromItem)) {
			from.set(fromSlot, null);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Swaps two items.
	 * 
	 * @param fromSlot
	 *            From slot.
	 * @param toSlot
	 *            To slot.
	 */
	public void swap(int fromSlot, int toSlot) {
		Item temp = get(fromSlot);
		boolean b = firingEvents;
		firingEvents = false;
		try {
			set(fromSlot, get(toSlot));
			set(toSlot, temp);
			if (b) {
				fireItemsChanged(new int[] { fromSlot, toSlot });
			}
		} finally {
			firingEvents = b;
		}
	}

	/**
	 * Gets the total amount of an item, including the items in stacks.
	 * 
	 * @param id
	 *            The id.
	 * @return The amount.
	 */
	public int getCount(int id) {
		int total = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				if (items[i].getId() == id) {
					total += items[i].getAmount();
				}
			}
		}
		return total;
	}

	/**
	 * Inserts an item.
	 * 
	 * @param fromSlot
	 *            The old slot.
	 * @param toSlot
	 *            The new slot.
	 */
	public void insert(int fromSlot, int toSlot) {
		// we reset the item in the from slot
		Item from = items[fromSlot];
		if (from == null) {
			return;
		}
		items[fromSlot] = null;
		// find which direction to shift in
		if (fromSlot > toSlot) {
			int shiftFrom = toSlot;
			int shiftTo = fromSlot;
			for (int i = (toSlot + 1); i < fromSlot; i++) {
				if (items[i] == null) {
					shiftTo = i;
					break;
				}
			}
			Item[] slice = new Item[shiftTo - shiftFrom];
			System.arraycopy(items, shiftFrom, slice, 0, slice.length);
			System.arraycopy(slice, 0, items, shiftFrom + 1, slice.length);
		} else {
			int sliceStart = fromSlot + 1;
			int sliceEnd = toSlot;
			for (int i = (sliceEnd - 1); i >= sliceStart; i--) {
				if (items[i] == null) {
					sliceStart = i;
					break;
				}
			}
			Item[] slice = new Item[sliceEnd - sliceStart + 1];
			System.arraycopy(items, sliceStart, slice, 0, slice.length);
			System.arraycopy(slice, 0, items, sliceStart - 1, slice.length);
		}
		// now fill in the target slot
		items[toSlot] = from;
		if (firingEvents) {
			fireItemsChanged();
		}
	}

	/**
	 * Fires an item changed event.
	 * 
	 * @param slot
	 *            The slot that changed.
	 */
	public void fireItemChanged(int slot) {
		for (ContainerAdapter listener : listeners) {
			listener.itemChanged(this, slot);
		}
	}

	/**
	 * Fires an items changed event.
	 */
	public void fireItemsChanged() {
		for (ContainerAdapter listener : listeners) {
			listener.itemsChanged(this);
		}
	}

	/**
	 * Fires an items changed event.
	 * 
	 * @param slots
	 *            The slots that changed.
	 */
	public void fireItemsChanged(int[] slots) {
		for (ContainerAdapter listener : listeners) {
			listener.itemsChanged(this, slots);
		}
	}

	/**
	 * Checks if the container contains the specified item.
	 * 
	 * @param runeReqs
	 *            The item id.
	 * @return <code>true</code> if so, <code>false</code> if not.
	 */
	public boolean contains(Item[] items) {
		boolean hasItem = true;
		for (Item item : items) {
			if (amountOf(item.getId()) < 1)
				hasItem = false;
		}
		return hasItem;
	}

	/**
	 * Checks if there is room in the inventory for an item.
	 * 
	 * @param item
	 *            The item.
	 * @return <code>true</code> if so, <code>false</code> if not.
	 */
	public boolean hasRoomFor(Item item) {
		if (item.getDefinition().isStackable()
				|| type.equals(Type.ALWAYS_STACK)) {
			for (int i = 0; i < items.length; i++) {
				if (items[i] != null && items[i].getId() == item.getId()) {
					int totalCount = item.getAmount() + items[i].getAmount();
					if (totalCount >= Integer.MAX_VALUE || totalCount < 1) {
						return false;
					}
					return true;
				}
			}
			int slot = freeSlot();
			return slot != -1;
		} else {
			int slots = freeSlots();
			return slots >= item.getAmount();
		}
	}
	
	public int amountOf(int id) {
		int amount = 0;
		for(Item i : items) {
			if(i != null && i.getId() == id)
				amount += i.getAmount();
		}
		return amount;
	}
	
	public int getTabSize(int id) {
		return tabStartingSlot[id + 1] - tabStartingSlot[id];
	}
	
	public void increaseTabStartSlots(int startId) {
		for(int i = startId + 1; i < tabStartingSlot.length; i++) {
			tabStartingSlot[i]++;
		}
	}

	public void decreaseTabStartSlots(int startId) {
		if(startId == 10)
			return;
		for(int i = startId + 1; i < tabStartingSlot.length; i++) {
			tabStartingSlot[i]--;
		}
		if(getTabSize(startId) == 0) {
			currentTab = 10;
			collapseTab(startId);
		}
	}

	public int getTabByItemSlot(int itemSlot) {
		int tabId = 0;
		for(int i = 0; i < tabStartingSlot.length; i++) {
			if(itemSlot >= tabStartingSlot[i]) {
				tabId = i;
			}
		}
		return tabId;
	}
	
	public void collapseTab(int tabId) {
		int size = getTabSize(tabId);
		Item[] tempTabItems = new Item[size];
		for(int i = 0; i < size; i++) {
			tempTabItems[i] = get(tabStartingSlot[tabId] + i);
			set(tabStartingSlot[tabId] + i, null);
		}
		arrange();
		for(int i = tabId; i < tabStartingSlot.length - 1; i++) {
			tabStartingSlot[i] = tabStartingSlot[i + 1] - size;
		}
		tabStartingSlot[10] = tabStartingSlot[10] - size;
		for(int i = 0; i < size; i++) {
			int slot = this.freeSlot();
			set(slot, tempTabItems[i]);
		}
	}
	
	public void arrange() {
		Item[] oldData = getItems();
		items = new Item[this.capacity];
		int ptr = 0;
		for(int i = 0; i < capacity; i++) {
			if(oldData[i] == null)
				continue;
			if(oldData[i].getId() != -1) {
				items[ptr++] = oldData[i];
			}
		}
		for(int i = ptr; i < capacity; i++) {
			items[i] = null;
		}
	}

	public void remove(Item[] items) {
		for (Item item : items) 
			remove(item);
	}
	
}