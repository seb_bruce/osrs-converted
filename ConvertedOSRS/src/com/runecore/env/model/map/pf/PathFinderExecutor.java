package com.runecore.env.model.map.pf;

import com.runecore.env.model.Entity;
import com.runecore.env.world.Location;

/**
 * PathFinderExecutor.java
 * 
 * @author Harry Andreas<harry@runecore.org> Feb 20, 2013
 */
public class PathFinderExecutor {

	public static boolean walkTo(Entity e, Location destination) {
		DefaultPathFinder finder = new DefaultPathFinder();
		Location base = e.getLocation();
		int srcX = base.getLocalX();
		int srcY = base.getLocalY();
		int destX = destination.getLocalX(base);
		int destY = destination.getLocalY(base);
		PathState state = finder.findPath(e, e.getLocation(), srcX, srcY,
				destX, destY, base.getZ(), 0, e.getWalking().isRunning(),
				false, true);
		if (state == null)
			return false;
		if (!state.isRouteFound())
			return false;
		e.getWalking().reset();
		for (BasicPoint step : state.getPoints()) {
			e.getWalking().addStep(step.getX(), step.getY());
		}
		e.getWalking().finish();
		return true;
	}

}
