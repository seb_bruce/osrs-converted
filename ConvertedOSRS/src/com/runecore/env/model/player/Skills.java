package com.runecore.env.model.player;

import com.runecore.codec.event.RefreshLevelEvent;
import com.runecore.env.Context;
import com.runecore.util.LevelCalculation;

/**
 * Skills.java
 * 
 * @author Harry Andreas<harry@runecore.org> Feb 11, 2013
 */
//TODO add combatLevel saving, micro-optimization.
public class Skills {

	/**
	 * Variables for tlhe class;
	 */
	private final Player player;
	public static final int MAX_SKILL_COUNT = 23;
	private final int[] level = new int[MAX_SKILL_COUNT];
	private final double[] xp = new double[MAX_SKILL_COUNT];
	public static final String[] SKILL_NAME = { "Attack", "Defence",
		"Strength", "Hitpoints", "Range", "Prayer", "Magic", "Cooking",
		"Woodcutting", "Fletching", "Fishing", "Firemaking", "Crafting",
		"Smithing", "Mining", "Herblore", "Agility", "Thieving", "Slayer",
		"Farming", "Runecrafting", "Construction", "Hunter", "Summoning", };
	public static final int ATTACK = 0, DEFENCE = 1, STRENGTH = 2,
			HITPOINTS = 3, RANGE = 4, PRAYER = 5, MAGIC = 6, COOKING = 7,
			WOODCUTTING = 8, FLETCHING = 9, FISHING = 10, FIREMAKING = 11,
			CRAFTING = 12, SMITHING = 13, MINING = 14, HERBLORE = 15,
			AGILITY = 16, THIEVING = 17, SLAYER = 18, FARMING = 19,
			RUNECRAFTING = 20, CONSTRUCTION = 21, HUNTER = 22, SUMMONING = 23;

	/**
	 * Construct the instance
	 * 
	 * @param player
	 */
	public Skills(Player player) {
		for (int i = 0; i < MAX_SKILL_COUNT; i++) {
			getLevel()[i] = 99;
			getXp()[i] = 20000000;
		}
		/*getLevel()[1] = 20;
		getXp()[1] = 4548;
		getLevel()[0] = 118;
		getLevel()[2] = 118;*/
		//Not sure if that is needed? TODO
		this.player = player;
	}

	public void refresh() {
		for (int i = 0; i < MAX_SKILL_COUNT; i++) {
			refresh(i);
		}
	}

	public void refresh(int i) {
		Context c = Context.get();
		c.getActionSender().refreshLevel(
				new RefreshLevelEvent(getPlayer(), i, level[i], xp[i]));
	}

	/**
	 * Gets a real level
	 * @param index The skill index
	 * @return the real level
	 */
	public int getRealLevel(int index) {
		return LevelCalculation.getLevelForExperience(xp[index]);
	}

	public Player getPlayer() {
		return player;
	}

	public int[] getLevel() {
		return level;
	}

	public double[] getXp() {
		return xp;
	}

	public int getCombatLevel(boolean summ) {
		int attack = getRealLevel(ATTACK);
		int defence = getRealLevel(DEFENCE);
		int strength = getRealLevel(STRENGTH);
		int hitpoints = getRealLevel(HITPOINTS);
		int prayer = getRealLevel(PRAYER);
		int ranged = getRealLevel(RANGE);
		int magic = getRealLevel(MAGIC);
		double combatLevel = (defence + hitpoints + Math.floor(prayer / 2));
		double warrior = (attack + strength) * 0.325;
		double ranger = ranged * 0.4875;
		double mage = magic * 0.4875;
		double val = combatLevel + Math.max(warrior, Math.max(ranger, mage));
		return (int) val;
	}

	/**
	 * Checks if a level is below it's normal level for experience + a certain modification.
	 * This is used for consumables, e.g. 130/99 str, then drinking a super strength potion and it returning to 118/99.
	 *
	 * @param skill        The skill id.
	 * @param modification The modification amount.
	 * @return If the level is below it's normal level + a certain modification.
	 */
	public boolean isLevelBelowOriginalModification(int skill, int modification) {
		return level[skill] < (getRealLevel(skill) + modification);
	}

	/**
	 * Increases a level to its level for experience, depending on the modification amount.
	 *
	 * @param skill        The skill id.
	 * @param modification The modification amount.
	 */
	public void increaseLevelToMaximum(int skill, int modification) {
		if (isLevelBelowOriginal(skill)) {
			setLevel(skill, level[skill] + modification >= getRealLevel(skill) ? getRealLevel(skill) : level[skill] + modification);
		}
	}
	
	/**
	 * Adds experience to the given skill
	 * @param skill The skill to add experience to 
	 * @param experience The amount of experience to add
	 */
	public void addExperience(int skill, int experience) {
		xp[skill] += experience;
		level[skill] = LevelCalculation.getLevelForExperience(xp[skill]);
		refresh(skill);
	}

	/**
	 * Checks if a level is below it's normal level for experience.
	 * This is used for consumables, e.g. 130/99hp, then eating a manta ray and it returning to 99/99hp.
	 *
	 * @param skill The skill id.
	 * @return If the level is below it's normal level.
	 */
	public boolean isLevelBelowOriginal(int skill) {
		return level[skill] < getRealLevel(skill);
	}

	/**
	 * Increases a level to its level for experience + modification amount.
	 *
	 * @param skill        The skill id.
	 * @param modification The modification amount.
	 */
	public void increaseLevelToMaximumModification(int skill, int modification) {
		if (isLevelBelowOriginalModification(skill, modification)) {
			setLevel(skill, level[skill] + modification >= (getRealLevel(skill) + modification) ? (getRealLevel(skill) + modification) : level[skill] + modification);
		}
	}

	/**
	 * Decreases a level to its minimum.
	 *
	 * @param skill        The skill id.
	 * @param modification The modification amount.
	 */
	public void decreaseLevelToMinimum(int skill, int modification) {
		if (level[skill] > 1) {
			setLevel(skill, level[skill] - modification <= 1 ? 1 : level[skill] - modification);
		}
	}

	public void decreaseLevelToLevel(int skill, int modification, int minLevel) {
		if (level[skill] > minLevel) {
			setLevel(skill, level[skill] - modification <= 1 ? 1 : level[skill] - modification);
		}
	}

	/**
	 * Decreases a level to 0.
	 *
	 * @param skill        The skill id.
	 * @param modification The modification amount.
	 */
	public void decreaseLevelToZero(int skill, int modification) {
		if (level[skill] > 0) {
			setLevel(skill, level[skill] - modification <= 0 ? 0 : level[skill] - modification);
		}
	}

	public void setLevel(int index, int amount) {
		level[index] = amount;
		refresh(index);
	}

	public void hardSet(int index, int lvl) {
		if(index > 23 || lvl > 99 || lvl < 1 || index < 0)
			return;
		level[index] = lvl;
		xp[index] = LevelCalculation.getXPForLevel(lvl);
		refresh(index);
	}

}