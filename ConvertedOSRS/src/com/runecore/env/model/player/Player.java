package com.runecore.env.model.player;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.runecore.codec.Events;
import com.runecore.codec.event.SendInterfaceConfigEvent;
import com.runecore.codec.event.SendInterfaceEvent;
import com.runecore.codec.event.SendStringEvent;
import com.runecore.env.Context;
import com.runecore.env.core.Tick;
import com.runecore.env.model.Entity;
import com.runecore.env.model.container.Container;
import com.runecore.env.model.def.PlayerDefinition;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.npc.NPC;
import com.runecore.network.GameSession;
import com.runecore.util.Rights;

/**
 * Player.java
 * 
 * @author Harry Andreas<harry@runecore.org> Feb 10, 2013
 */
public class Player extends Entity {

	/**
	 * Variables for the Player No primative types in here!
	 */
	private PlayerDefinition definition;
	private final GameSession session;
	private Rights rights = Rights.NORMAL;
	private final PlayerFacade facade = new PlayerFacade();
	private final Skills skills;
	private final Looks looks;
	private Container inventory = new Container(Container.Type.STANDARD, 28);
	private Container equipment = new Container(Container.Type.STANDARD, 15);
	private Container bank = new Container(Container.Type.ALWAYS_STACK, 496);
	private final List<Player> localPlayers;
	private final List<NPC> localNpcs;
	private final boolean[] playerExists = new boolean[2048];
	
	/**
	 * The player's prayer drain tick
	 */
	private Tick prayerDrainTask;

	/**
	 * Construct the Player instance
	 * 
	 * @param session
	 *            The GameSession for the Player
	 * @param definition
	 *            The PlayerDefinition for this Player instance
	 */
	public Player(GameSession session, PlayerDefinition definition) {
		this.definition = definition;
		this.definition.setPlayer(this);
		this.session = session;
		this.session.setPlayer(this);
		this.skills = new Skills(this);
		this.looks = new Looks(this);
		this.localPlayers = new LinkedList<Player>();
		this.localNpcs = new LinkedList<NPC>();
	}
	
	public Container inventory() {
		return inventory;
	}
	
	public Container equipment() {
		return equipment;
	}

	public Container get(String container) {
		switch (container) {
		case "i":
		case "inv":
		case "inventory":
			return inventory;
		case "e":
		case "equip":
		case "equipment":
			return equipment;
		case "b":
		case "bank":
			return bank;
		}
		return null;
	}

	public int getRenderId() {
		Item item = get("e").get(3);
		if (item == null) {
			return 1426;
		}
		return item.getDefinition().getRenderAnimation();
	}

	@Override
	public boolean isDead() {
		return getSkills().getLevel()[3] <= 0;
	}

	@Override
	public void hit(int damage) {
		if(isDead())
			return;
		if(damage > getSkills().getLevel()[3]) 
			damage = getSkills().getLevel()[3];
		getSkills().getLevel()[3] -= damage;
		getSkills().refresh(3);
		if(isDead())
			death();
	}

	@Override
	public void tick() {
		//getPrayer().process();
		getActionQueue().process();
		processRestoration();
	}

	private void processRestoration() {
		long lastRestore;
		if (getAttribute("lastRestore", null) == null)
			lastRestore = 0;
		else
			lastRestore = (long) getAttribute("lastRestore", null);
		if (getAttribute("lastRestore", null) == null || lastRestore - System.currentTimeMillis() < 0) {
			if (getCombatDefinition().getSpecialAmount() < 100) {
				getCombatDefinition().setSpecialAmount(getCombatDefinition().getSpecialAmount() + 10);
				getCombatDefinition().refreshSpecial();
			}
			for (int i = 0; i < 23; i++) {
				if(i == 5 || i == 3)
					continue;
				int level = getSkills().getLevel()[i];
				if (level < getSkills().getRealLevel(i)) {
					getSkills().increaseLevelToMaximum(i, 1);
				} else if(level > getSkills().getRealLevel(i)) {
					getSkills().decreaseLevelToMinimum(i, 1);
				}
			}
			addAttribute("lastRestore", System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(30));
		}
	}

	/**
	 * Sends the earning potential details on the game screen.
	 * @param showInterface Whether to display the interface.
	 */
	public void displayEP(boolean showInterface) {
		boolean percentageGreen = getFacade().getCurrentEP() > 50;
		String percentageColor = percentageGreen ? "52D017" : "800000";
		Context.get().getActionSender().sendString(new SendStringEvent(this, "EP: <col=" + percentageColor + ">" + getFacade().getCurrentEP() + "%", 532, 1));
		if (showInterface) {
			Context.get().getActionSender().sendInterfaceConfig(new SendInterfaceConfigEvent(this, 532, 0, false));
			Context.get().getActionSender().sendInterface(new SendInterfaceEvent(this, 1, 548, 9, 532));
		}
		Context.get().getActionSender().sendString(new SendStringEvent(this, getFacade().getLowestLevel(this) + " - " + getFacade().getHighestLevel(this), 548, 6));
	}

	/**
	 * Displays the PvP skulls in the game screen.
	 * @param safe Whether the skull is to be the safe one or not.
	 */
	public void displayPvp(boolean safe) {
		Context.get().getActionSender().sendInterface(new SendInterfaceEvent(this, 1, 548, 7, 745));
		Context.get().getActionSender().sendInterfaceConfig(new SendInterfaceConfigEvent(this, 745, 0, true));
		if (safe) {
			Context.get().getActionSender().sendInterfaceConfig(new SendInterfaceConfigEvent(this, 745, 6, false));
			Context.get().getActionSender().sendInterfaceConfig(new SendInterfaceConfigEvent(this, 745, 3, true));
		} else {
			Context.get().getActionSender().sendInterfaceConfig(new SendInterfaceConfigEvent(this, 745, 6, true));
			Context.get().getActionSender().sendInterfaceConfig(new SendInterfaceConfigEvent(this, 745, 3, false));
		}
	}

	public void sendQuestTab() {
		for (int i = 17; i < 31; i++) {
			Context.get().getActionSender().sendString(new SendStringEvent(this, "", 274, i));
		}
	}

	@Override
	public PlayerDefinition getDefinition() {
		return definition;
	}

	public GameSession getSession() {
		return session;
	}

	public Skills getSkills() {
		return skills;
	}

	public Looks getLooks() {
		return looks;
	}

	public List<Player> getLocalPlayers() {
		return localPlayers;
	}

	public boolean[] getPlayerExists() {
		return playerExists;
	}

	@Override
	public void dropLoot() {
		/*
		Player killer = getCombatState().getKiller();
		if(killer == null)
			return;
		World.get().register(new GroundItem(killer, new Item(526, 1), getLocation().clone()));
		
		Events.sendMsg(killer, "You have defeated "+getDefinition().getName()+".");
		*/
		Events.sendMsg(this, "Oh dear, you have died!");
	}

	@Override
	public void restore() {
		for(int i = 0; i < getSkills().getLevel().length; i++) {
			getSkills().getLevel()[i] = getSkills().getRealLevel(i);
		}
		getSkills().refresh();
		getCombatDefinition().setSpecialAmount(100);
		getCombatDefinition().setSpecialActivated(false);
		getCombatDefinition().refreshSpecial();
	}

	@Override
	public void heal(int heal) {
		int max = getSkills().getRealLevel(3);
		getSkills().getLevel()[3] += heal;
		if(getSkills().getLevel()[3] > max) {
			getSkills().getLevel()[3] = max;
		}
		getSkills().refresh(3);
	}

	public List<NPC> getLocalNpcs() {
		return localNpcs;
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Entity))
			return false;
		Entity entity = (Entity)obj;
		return entity.getIndex() == getIndex();
	}

	public PlayerFacade getFacade() {
		return facade;
	}

	public Rights getRights() {
		return rights;
	}

	public void setRights(Rights rights) {
		this.rights = rights;
	}

	/**
	 * @return the prayerDrainTask
	 */
	public Tick getPrayerDrainTask() {
		return prayerDrainTask;
	}

	/**
	 * @param prayerDrainTask the prayerDrainTask to set
	 */
	public void setPrayerDrainTask(Tick prayerDrainTask) {
		this.prayerDrainTask = prayerDrainTask;
	}

}