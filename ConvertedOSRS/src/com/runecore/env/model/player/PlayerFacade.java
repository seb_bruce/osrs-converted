package com.runecore.env.model.player;

public class PlayerFacade {

	private int openInterface = -1;
	private int openOverlay = -1;
	private int currentEP = 0;
	private boolean muted = Boolean.FALSE;
	private boolean inventoryUpdate = Boolean.FALSE;
	
	public int getOpenOverlay() {
		return openOverlay;
	}
	
	public void setOpenOverlay(int openOverlay) {
		this.openOverlay = openOverlay;
	}
	
	public int getOpenInterface() {
		return openInterface;
	}
	
	public void setOpenInterface(int openInterface) {
		this.openInterface = openInterface;
	}
	
	/**
	 * Gets the highest level you can attack in PvP
	 * @return The integer value of the highest level attackable.
	 */
	public int getHighestLevel(Player player) {
		int wild = 12; //TODO wildy levels
		int cmb = player.getDefinition().getCombatLevel();
		int total = 0;
		int range = 12;
		total = cmb + range + (wild > 0 ? wild : 0);
		if (total > 138) {
			total = 138;
		}
		return total;
	}

	/**
	 * The lowest level you can attack in PvP
	 * @return The integer value of the lowest combat level
	 */
	public int getLowestLevel(Player player) {
		int wild = 12; //TODO wildy levels
		int cmb = player.getDefinition().getCombatLevel();
		int total = cmb - 12 - (wild > 0 ? wild : 0);
		if (total < 3) {
			total = 3;
		}
		return total;
	}

	/**
	 * @return the currentEP
	 */
	public int getCurrentEP() {
		return currentEP;
	}

	/**
	 * @param currentEP the currentEP to set
	 */
	public void setCurrentEP(int currentEP) {
		this.currentEP = currentEP;
	}

	public boolean isMuted() {
		return muted;
	}

	public void setMuted(boolean muted) {
		this.muted = muted;
	}

	public boolean isInventoryUpdate() {
		return inventoryUpdate;
	}

	public void setInventoryUpdate(boolean inventoryUpdate) {
		this.inventoryUpdate = inventoryUpdate;
	}

}