package com.runecore.env.model.combat;

import com.runecore.codec.Events;
import com.runecore.env.content.Areas;
import com.runecore.env.core.Tick;
import com.runecore.env.model.Cooldown;
import com.runecore.env.model.Entity;
import com.runecore.env.model.action.Action;
import com.runecore.env.model.combat.script.CombatScript;
import com.runecore.env.model.combat.script.MagicSpellScript;
import com.runecore.env.world.Location;

public class CombatEntityAction extends Action {

	private final Entity enemy;
	private MagicSpellScript magicSpell;

	public CombatEntityAction(Entity player, Entity enemy,
			MagicSpellScript magic) {
		this(player, enemy);
		this.setMagicSpell(magic);
		player.getWalking().reset();
	}

	public CombatEntityAction(Entity player, Entity enemy) {
		super(player, 0);
		this.enemy = enemy;
	}

	@Override
	public QueuePolicy getQueuePolicy() {
		return QueuePolicy.NEVER;
	}

	@Override
	public WalkablePolicy getWalkablePolicy() {
		return WalkablePolicy.NON_WALKABLE;
	}
	

	@Override
	public void execute() {
		// determine type on weapon changed event ok?
		if (getEntity().isDead() || getTarget().isDead()) {
			stop();
			return;
		}
		getEntity().faceEntity(getTarget());
		CombatScript script = CombatManager.get().getScript(getEntity());
		if (!script.canAttack(getEntity(), getTarget()))
			return;
		if(!getEntity().isNPC() && !getTarget().isNPC()) {
			/*
			if(Areas.safe(getEntity())) {
				getEntity().asPlayer().sendMessage("You cannot attack other players while in a safe zone!");
				stop();
				return;
			} else if(Areas.safe(getTarget())) {
				getEntity().asPlayer().sendMessage("You cannot attack other players in a safe zone!");
				stop();
				return;
			}
			*/
		}
		int targetCombatingWith = getTarget().getAttribute("cmbWith", -1);
		long targetCmbWithTime = getTarget().getAttribute("cmbWithTime", Long.parseLong("-1"));
		long elapsed = System.currentTimeMillis() - targetCmbWithTime;
		boolean multi = Areas.inArea(getEntity(), Areas.MULTI_AREA) && Areas.inArea(getTarget(), Areas.MULTI_AREA);
		if (elapsed < 6000 && targetCombatingWith != getEntity().getTurnToIndex() && !multi) {
			if(getEntity().isPlayer()) {
				Events.sendMsg(getEntity().player(), "Your target is already in combat.");
				stop();
				return;
			}
		} else if(!multi) {
			getEntity().addAttribute("cmbWith", getTarget().getTurnToIndex());
			getEntity().addAttribute("cmbWithTime", System.currentTimeMillis());
			getTarget().addAttribute("cmbWithTime", System.currentTimeMillis());
			getTarget().addAttribute("cmbWith", getEntity().getTurnToIndex());
		}
		
		if (!getEntity().isReady(Cooldown.COMBAT) /*&& script.getClass() != GraniteMaul.class*/)
			return;
		CombatHit[] damages = script.getDamage(getEntity(), getTarget());
		int cooldown = script.getCooldown(getEntity());
		System.out.println("cooldown= "+cooldown);
		getEntity().setCooldown(Cooldown.COMBAT, cooldown);
		getEntity().setCooldown(Cooldown.LOGOUT, 17, true);
		getTarget().setCooldown(Cooldown.LOGOUT, 17, true);
		getEntity().addToCooldown(Cooldown.ATTACK_ANIMATION, 2);
		script.execute(getEntity(), getTarget());
		
		for(CombatHit hit : damages)
			hit.execute();
		if (getMagicSpell() != null) {
			//getEntity().asPlayer().sendMessage("Stopping magic.");
			stop();
		}
	}
	
	@Override
	public void stop() {
		super.stop();
		if(getMagicSpell() != null) {
			getEntity().register("resetTurnTo", new Tick(2) {
				@Override
				public void execute() {
					getEntity().faceEntity(null);
					stop();
				} 
			});
		} else {
			getEntity().faceEntity(null);
		}
	}

	public Entity getTarget() {
		return enemy;
	}

	public MagicSpellScript getMagicSpell() {
		return magicSpell;
	}

	public void setMagicSpell(MagicSpellScript magicSpell) {
		this.magicSpell = magicSpell;
	}

}