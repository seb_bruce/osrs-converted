package com.runecore.env.model.combat;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.runecore.env.core.Tick;
import com.runecore.env.model.Cooldown;
import com.runecore.env.model.Entity;
import com.runecore.env.model.combat.AttackInterfaceButton.AttackButtonStyle;
import com.runecore.env.model.combat.Prayer.ActivePrayer;
import com.runecore.env.model.flag.Animation;
import com.runecore.env.model.flag.Damage;
import com.runecore.env.model.flag.Damage.DamageType;
import com.runecore.env.model.player.Player;
import com.runecore.env.model.player.Skills;

public class CombatHit implements Runnable {

	private final Damage hit;
	private final Entity dealer, enemy;
	private final int delay;

	public CombatHit(int damage, Entity dealer, Entity enemy, int delay) {
		this.delay = delay;
		this.dealer = dealer;
		this.enemy = enemy;
		DamageType type = damage > 0 ? DamageType.REGULAR_DAMAGE : DamageType.NO_DAMAGE;
		this.hit = new Damage(damage, type, dealer);
	}

	private void block() {
		if (enemy.isReady(Cooldown.ATTACK_ANIMATION))
			enemy.animate(Animation.create(enemy.getCombat().getDefenceEmote()));
	}

	public void execute() {
		final Runnable r = this;
		handlePreEffects();
		if (delay == -1) {
			block();
			r.run();
		} else {
			int blockDelay = delay - 1;
			if (blockDelay == 0) {
				block();
			} else
				enemy.register("combathitblock-" + hashCode(), new Tick(
						blockDelay) {
					@Override
					public void execute() {
						block();
						stop();
					}
				});
			enemy.register("combathit-" + hashCode(), new Tick(delay) {
				@Override
				public void execute() {
					r.run();
					stop();
				}
			});
		}
	}

	private void handlePreEffects() {
		double hpXp = (getHit().getHit()) * 1.33;
		double combatXp = (getHit().getHit()) * 4;
		AttackInterfaceButton button = getDealer().getCombat().getAttackStyleData();
		Map<Integer, Double> xpGains = new TreeMap<>();
		xpGains.put(3, hpXp);
		CombatEntityAction action = (CombatEntityAction) getDealer().getActionQueue().getCurrentAction();
		if (action.getMagicSpell() == null) {
			if (button.getStyle() == AttackButtonStyle.ACCURATE) {
				xpGains.put(Skills.ATTACK, combatXp);
			} else if (button.getStyle() == AttackButtonStyle.AGGRESSIVE) {
				xpGains.put(Skills.STRENGTH, combatXp);
			} else if (button.getStyle() == AttackButtonStyle.DEFENSIVE) {
				xpGains.put(Skills.DEFENCE, combatXp);
			} else if (button.getStyle() == AttackButtonStyle.CONTROLLED) {
				double shared = combatXp / 3;
				xpGains.put(Skills.ATTACK, shared);
				xpGains.put(Skills.DEFENCE, shared);
				xpGains.put(Skills.STRENGTH, shared);
			} else if(button.getStyle() == AttackButtonStyle.RANGE_ACCURATE) {
				xpGains.put(Skills.RANGE, combatXp);
			} else if(button.getStyle() == AttackButtonStyle.RAPID) {
				xpGains.put(Skills.RANGE, combatXp);
			} else if(button.getStyle() == AttackButtonStyle.LONGRANGE) {
				xpGains.put(Skills.DEFENCE, combatXp / 2);
				xpGains.put(Skills.RANGE, combatXp / 2);
			}
		} else {
			xpGains.put(Skills.MAGIC, action.getMagicSpell().getMinimumXP() + combatXp);
		}
		Player player = getDealer().player();
		xpGains.entrySet().stream().filter(e -> e.getValue() > 0).forEach(e -> player.getSkills().addExperience(e.getKey(), e.getValue().intValue()));
		xpGains.clear();
		if (getDealer().getPrayer().hasPrayerOn(ActivePrayer.SMITE) && getEnemy().isPlayer()) { //smite
			if (getHit().getHit() > 4) {
				Player enemy = getEnemy().player();
				int drain = getHit().getHit() / 4;
				if (enemy.getSkills().getLevel()[5] > 0) {
					//getEnemy().player().getSkills().drainPray((getHit().getHit()) / 4);
				}
			}
		}
	}

	public Damage getHit() {
		return hit;
	}

	public Entity getDealer() {
		return dealer;
	}

	public Entity getEnemy() {
		return enemy;
	}

	public int getDelay() {
		return delay;
	}

	@Override
	public void run() {
		if (hit.getHit() > 0) {
			if (enemy.getAttribute("veng_casted", Boolean.FALSE)) {
				int rebound = (int) Math.floor(hit.getHit() * 0.75);
				if (rebound > 0) {
					dealer.damage(new Damage(rebound, DamageType.REGULAR_DAMAGE, enemy));
					enemy.addAttribute("veng_casted", Boolean.FALSE);
					//enemy.asPlayer().getMask().sendForceChat("Taste vengeance!");
					//enemy.asPlayer().getFrames().sendPublicChatMessage(enemy.getIndex(), enemy.asPlayer().getRights().ordinal(), new ChatMessage(0, 0, "Taste vengeance!"));
					//enemy.forceTalk("Taste vengeance!");
				}
			}
		}
		enemy.damage(hit);
	}

}
