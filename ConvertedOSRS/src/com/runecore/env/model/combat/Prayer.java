package com.runecore.env.model.combat;

import java.util.HashMap;
import java.util.Map;

import com.runecore.codec.Events;
import com.runecore.env.core.Tick;
import com.runecore.env.model.Entity;
import com.runecore.env.model.player.Player;

public class Prayer {

	private Entity entity;
	
	public Prayer(Entity p) {
		this.entity = p;
	}
	
	private int enabledPrayers; //holds the prayers that are enabled as a bitmask
	private int overheadIcon;
	private boolean protectingItem;
	
	public enum ActivePrayer {
		THICK_SKIN(0, new int[] {3,9,25,26}, -1,0.5),
		BURST_OF_STRENGTH(1, new int[] {4,10,25,26, /*mage/range*/18,19,20,21,22,23}, -1,0.5),
		CLARITY_OF_THOUGHT(2, new int[] {5,11,25,26, /*mage/range*/18,19,20,21,22,23}, -1,0.5),
		
		ROCK_SKIN(3, new int[] {0,9,25,26}, -1,1),
		SUPERHUMAN_STR(4, new int[] {1,10,25,26, /*mage/range*/18,19,20,21,22,23}, -1,1),
		IMPROVED_REFLEXES(5, new int[] {2,11,25,26, /*mage/range*/18,19,20,21,22,23}, -1,1),
		
		RAPID_RESTORE(6, new int[] {}, -1,0.15),//no conflicts
		RAPID_HEAL(7, new int[] {}, -1,0.3),//no conflicts
		PROTECT_ITEM(8, new int[] {}, -1,0.3),//no conflicts
		
		STEEL_SKIN(9, new int[] {0,3,25,26}, -1,2),
		ULTIMATE_STR(10, new int[] {0,4,25,26, /*mage/range*/18,19,20,21,22,23}, -1,2),
		INCREDIBLE_REFLEXES(11, new int[] {2,5,25,26, /*mage/range*/18,19,20,21,22,23}, -1,2),
		
		PROTECT_FROM_MAGIC(12, new int[] {13,14,15,16,17}, 2,2),//only overhead icon conflicts
		PROTECT_FROM_MISSILES(13, new int[] {12,14,15,16,17}, 1,2),//only overhead icon conflicts
		PROTECT_FROM_MELEE(14, new int[] {12,13,15,16,17}, 0,2),//only overhead icon conflicts
		
		RETRIBUTION(15, new int[] {12,13,14,16,17}, 3,0.5),//only overhead icon conflicts
		REDEMPTION(16, new int[] {12,13,14,15,17}, 5,1),//only overhead icon conflicts
		SMITE(17, new int[] {12,13,14,15,16}, 4,2),//only overhead icon conflicts
		
		SHARP_EYE(18, new int[] {1,2,4,5,10,11,25,26, /*mage/range*/19,20,21,22,23}, -1,0.5),
		MYSTIC_WILL(19, new int[] {1,2,4,5,10,11,25,26, /*mage/range*/18,20,21,22,23}, -1,0.5),
		HAWK_EYE(20, new int[] {1,2,4,5,10,11,25,26, /*mage/range*/18,19,21,22,23}, -1,1),
		MYSTIC_LORE(21, new int[] {1,2,4,5,10,11,25,26, /*mage/range*/18,19,20,22,23}, -1,1),
		EAGLE_EYE(22, new int[] {1,2,4,5,10,11,25,26, /*mage/range*/18,19,20,21,23}, -1,2),
		MYSTIC_MIGHT(23, new int[] {1,2,4,5,10,11,25,26, /*mage/range*/18,19,20,21,22}, -1,2),
		
		//no prayer 24 it would appear
		CHIVALRY(25, new int[] {0,1,2,3,4,5,9,10,11,26,  /*mage/range*/18,19,20,21,22,23}, -1,2),
		PIETY(26, new int[] {0,1,2,3,4,5,9,10,11,25,  /*mage/range*/18,19,20,21,22,23}, -1,4)
		;
		//TODO: continue like this (increment the ids by 1 each time and make sure to keep the correct order of prayers);
		
		private int prayerId;
		private int[] conflicts;
		private int headicon = -1;
		private double drainrate = 0;
		
		ActivePrayer(int prayerId, int[] conflicts, int headicon, double drainrate) {
			this.prayerId = prayerId;
			this.conflicts=conflicts;
			this.headicon=headicon;
			this.drainrate=drainrate;
		}
		
		/**
		 * A map of object ids to foods.
		 */
		private static Map<Integer, ActivePrayer> foods = new HashMap<Integer, ActivePrayer>();

		/**
		 * Gets a food by an object id.
		 * 
		 * @param itemId
		 *            The object id.
		 * @return The food, or <code>null</code> if the object is not a food.
		 */
		public static ActivePrayer forId(int itemId) {
			return foods.get(itemId);
		}

		/**
		 * Populates the tree map.
		 */
		static {
			for (final ActivePrayer food : ActivePrayer.values()) {
				foods.put(food.prayerId, food);
			}
		}
	}
	
	//Call this when a player presses a prayer to turn it on
	private void turnPrayerOn(int prayerId) {
		enabledPrayers |= (int) Math.pow(2, prayerId); //sets bit to 1 in the bitmask
	}
	
	//Call this when a player presses a prayer to turn it off
	private void turnPrayerOff(int prayerId) {
		enabledPrayers &= ~((int) Math.pow(2, prayerId)); //unsets bit (to 0) in the bitmask
	}
	//TODO toggle bitmask
	
	//Call this when a player dies or runs out of prayer and all prayers need to close
	public void turnAllPrayersOff() {
		enabledPrayers = 0;
	}
	
	//Call this to check if the player has a prayer on so you can make some effect happen (like improve str by some % etc.)
	/**
	 * @param prayerId The type of the prayer.
	 * @return Whether the player has this prayer enabled.
	 */
	public boolean hasPrayerOn(ActivePrayer prayerType) {
		//my prayers: 0000 0111
		//for prayer 0, it is 0000 0001
		//AND OPERAND GIVES
		//0000 0001 i want to detect this bit in the mask
		
		int onValue = (int) Math.pow(2, prayerType.prayerId);//2^0 (prayer 0) = 1
		
		return (enabledPrayers & onValue) == onValue;//is 3 & 1 (binary result 1) == 1?
		//wont work for value 0.. even tho prayerId cna be 0
		//value 3 enabledPrayers wont work - that means pray 1 and 2 are on not prayer 3 is enabled
		
		//0000 0010 aka 2
		//0000 1111 aka 15
		//---- --X- aka match for 2 which is what we want
		
		//test 2^4 = 16 so is prayer 4 on?
		//our mask: 1111 1111
		//prayer 4: 0001 0000
		//--------------------
		//result:   0001 0000 TICK
	}
	
	//Call this when you need to send to the client which prayers are on
	public int getPrayerMask() {
		return enabledPrayers;
	}
	/**
	 * call this from the ActionButton packet recieved from the client.
	 * @param buttonId the Id of the button on the prayer interface
	 */
	public void activatePrayer(int buttonId){
		int prayerId = resovlePrayerForButton(buttonId);
		boolean putDraining=false;
		if(cantActivate(prayerId)){
			this.turnPrayerOff(prayerId);
			return;
		}
		if(prayerId != -1){
			ActivePrayer pray = ActivePrayer.forId(prayerId);
			boolean prayeron = this.hasPrayerOn(pray);
			if(!prayeron){
				if(this.getPrayerMask()==0){//we didnt have prayer on before
					putDraining=true;
				}
				turnPrayerOn(prayerId);//just turn it on initally if not on
				if(prayerId==8){
					entity.getPrayer().setProtectingItem(true);
				}
			} else {
				if(prayerId==8){
					entity.getPrayer().setProtectingItem(false);
				}
				turnPrayerOff(prayerId);
			}
			modifyInUsePrayers(prayerId);//here we turn some off as well
			if(entity.isPlayer())
				Events.sendConfig(entity.player(), 83, getPrayerMask());
			//client highlights - we dont
			//need to. only when our prayer drops 0 do we disable - WRONG WE NEED TO SEND
			//THE CONFIG BECAUSE OTHER PRAYRES MAY HAVE BEEN CONFLICTED AND TURNED OFF
			
			
			//TODO if all prayers disabled when activating, submit a draining event
			//when cancells when prayers are all off.
			if(putDraining){
				submitPrayerDrainingTickable();
			}
			int sound = getSound(!prayeron,prayerId);//need to switch this boolean around
			//if(sound != -1)
			//	entity.getFrames().sendSoundEffect(sound, 1, 0);
		}
	}
	
	private static final int DEACTIVATE_PRAYER = 2663;
	private int getSound(boolean prayeron, int prayid) {
		switch(prayid){
		case 0:
			return prayeron ? 2690 : DEACTIVATE_PRAYER;
		case 1:
			return prayeron ? 2688 : DEACTIVATE_PRAYER;
		case 2:
			return prayeron ? 2664 : DEACTIVATE_PRAYER;
		case 3:
			return prayeron ? 2684 : DEACTIVATE_PRAYER;
		case 4:
			return prayeron ? 2689 : DEACTIVATE_PRAYER;
		case 5:
			return prayeron ? 2662 : DEACTIVATE_PRAYER;
		case 6:
			return prayeron ? 2679 : DEACTIVATE_PRAYER;
		case 7:
			return prayeron ? 2678 : DEACTIVATE_PRAYER;
		case 8:
			return prayeron ? 1982 : DEACTIVATE_PRAYER;
		case 9:
			return prayeron ? 2687 : DEACTIVATE_PRAYER;
		case 10:
			return prayeron ? 2691 : DEACTIVATE_PRAYER;
		case 11:
			return prayeron ? 2667 : DEACTIVATE_PRAYER;
		case 12:
			return prayeron ? 2675 : DEACTIVATE_PRAYER;
		case 13:
			return prayeron ? 2677 : DEACTIVATE_PRAYER;
		case 14:
			return prayeron ? 2676 : DEACTIVATE_PRAYER;
		case 15:
			return prayeron ? 2682 : DEACTIVATE_PRAYER;
		case 16:
			return prayeron ? 2680 : DEACTIVATE_PRAYER;
		case 17:
			return prayeron ? 2686 : DEACTIVATE_PRAYER;
		case 18:
			return prayeron ? 2685 : DEACTIVATE_PRAYER;
		case 19:
			return prayeron ? 2670 : DEACTIVATE_PRAYER;
		case 20:
			return prayeron ? 2666 : DEACTIVATE_PRAYER;
		case 21:
			return prayeron ? 2668 : DEACTIVATE_PRAYER;
		case 22:
			return prayeron ? 2665 : DEACTIVATE_PRAYER;
		case 23:
			return prayeron ? 2669 : DEACTIVATE_PRAYER;
		case 25:
			return prayeron ? 3826 : DEACTIVATE_PRAYER;
		case 26:
			return prayeron ? 3825 : DEACTIVATE_PRAYER;
		}
		return -1;
	}
	
	private static final double[] prayerData = { 
		1.2, 1.2, 1.2, 1.2, 1.2,
		1.2, 0.6, 0.6,
		0.6, 3.6, 1.8,
		0.6, 0.6, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
		0.3, 0.3, 0.3, 1.2, 0.6, 0.18, 0.24, 0.15, 0.2, 0.18 };

	public static double[] PRAYER_DRAIN = {
	    12, // Thick Skin.
	    12, // Burst of Strength.
	    12, // Clarity of Thought.
	    12, // Sharp Eye.
	    12, // Mystic Will.
	    6, // Rock Skin.
	    6, // SuperHuman Strength.
	    6, // Improved Reflexes.
	    26, // Rapid restore
	    18, // Rapid Heal.
	    18, // Protect Items
	    6, // Hawk eye.
	    6, // Mystic Lore.
	    3, // Steel Skin.
	    3, // Ultimate Strength.
	    3, // Incredible Reflexes.
	    3, // Protect from Magic.
	    3, // Protect from Missiles.
	    3, // Protect from Melee.
	    3, // Eagle Eye.
	    3, // Mystic Might.
	    12, // Retribution.
	    6, // Redemption.
	    2, // Smite
	    0,//null
	    1.5, // Chivalry.
	    1.5, // Piety.
	};
	
	public void submitPrayerDrainingTickable() {
		entity.register("prayer-drain", new Tick(1) {
			@Override
			public void execute() {
				if (entity.isDead()) {
					this.stop();
					return;
				}
				if (entity.getPrayer().getPrayerMask() == 0) {
					this.stop();
					return;
				}
				double toRemove = 0.0;
				for(ActivePrayer pray : Prayer.ActivePrayer.values()){
					if(entity.getPrayer().hasPrayerOn(pray)){
						//toRemove += prayerData[pray.prayerId];//pray.drainrate/10;
						double drain = PRAYER_DRAIN[pray.prayerId];
						double bonus = 0.035 * entity.getCombatDefinition().getBonuses()[12];
						drain = drain * (1 + bonus);
						drain = 0.6 / drain;
						toRemove += drain;
					}
				}
				if (toRemove > 0) {
					//toRemove /= 2;
					//toRemove /= (1 + (0.035 * player.getBonuses().getBonus(12)));
					//toRemove *= (1 + 0.035 * player.getBonuses().getBonus(12));
				}
			    decreasePrayer((int)toRemove);
			    if(entity.isPlayer()) {
					Player player = entity.player();
					if(player.getSkills().getLevel()[5] < 1) {
						this.stop();
					}
			    }
			}
		});
	}
	
	private void decreasePrayer(int amount) {
		if(entity.isPlayer()) {
			Player player = entity.player();
			player.getSkills().getLevel()[5] -= amount;
			if(player.getSkills().getLevel()[5] < 0)
				player.getSkills().getLevel()[5] = 0;
			player.getSkills().refresh(5);
		}
	}
		
	private static final int[] PRAYER_LVL = {
		1, 4, 7, 10, 13, 16, 19, 22, 25, 28,
		31, 34, 37, 40, 43, 46, 49, 52, 8, 9, 26, 27, 44, 45, 0, 60, 70
	};
	private static final String[] PRAYER_NAME = {
		"Thick Skin", "Burst of Strength", "Clarity of Thought",  "Rock Skin", "Superhuman Strength",
		"Improved Reflexes", "Rapid Restore", "Rapid Heal", "Protect Item",  "Steel Skin", 
		"Ultimate Strength", "Incredible Reflexes", "Protect from Magic", "Protect from Ranged",
		"Protect from Melee", "Retribution", "Redemption", "Smite", 
		"Sharp Eye", "Mystic Will","Hawk Eye", "Mystic Lore","Eagle Eye", "Mystic Might",
		"", "Chivalry", "Piety"
	};

	/**
	 * check prayer level, if dead, if 0 prayer etc
	 */
	private boolean cantActivate(int prayer) {
		if (entity.isDead()) {
			return true;
		}
		if(entity.isPlayer()) {
			Player player = entity.player();
			if (prayer == 25) {//chiv
				if (player.getSkills().getRealLevel(1) < 65) {
					Events.sendMsg(player, "You need a Defence level of 65 to use Chivarly.");
					return true;
				}
			}
			if (prayer == 26) {//Piety
				if (player.getSkills().getRealLevel(1) < 70) {
					Events.sendMsg(player, "You need a Defence level of 70 to use Piety.");
					return true;
				}
			}	
			if (player.getSkills().getLevel()[5] <= 0) {
				return true;
			}
			if (player.getSkills().getRealLevel(5) < PRAYER_LVL[prayer]) {
				Events.sendMsg(player, String.format("You need a Prayer level of %n to use %s.", PRAYER_LVL[prayer], PRAYER_NAME[prayer]));
				return true;
			}
		}
		return false;
	}

	private boolean isHeadIconPrayer(int prayer) {
		return prayer >= 12 && prayer <= 17;
	}

	/**
	 * set the overhead, and disable other conflicting prayers
	 * prayers start at 0
	 */
	private void modifyInUsePrayers(int prayer) {
		boolean turningOn = this.hasPrayerOn(ActivePrayer.forId(prayer));
		int headIcon = ActivePrayer.forId(prayer).headicon;
		if(turningOn){
			for (int i : ActivePrayer.forId(prayer).conflicts) {
				this.turnPrayerOff(i);
				if (isHeadIconPrayer(i)) {
					setOverheadIcon(headIcon);
				}
			}
			if(this.isHeadIconPrayer(prayer)){
				setOverheadIcon(headIcon);
			}
		} else {
			if(this.isHeadIconPrayer(prayer)){
				setOverheadIcon(-1);
			}
		}
	}

	/**
	 * obtain the relative prayer ID in the enum when you click a button
	 * on the prayer interface
	 * button 4 is prayer ID 1
	 */
	private int resovlePrayerForButton(int buttonId) {
		if(buttonId >= 4 && buttonId <= 27){//was up to 21
			return (buttonId - 4);
		}
		if(buttonId==28 || buttonId==29)
			return (buttonId-3);
		return -1;
	}

	public int getOverheadIcon() {
		return overheadIcon;
	}

	public void setOverheadIcon(int overheadIcon) {
		this.overheadIcon = overheadIcon;
	}

	public boolean isProtectingItem() {
		return protectingItem;
	}

	public void setProtectingItem(boolean protectingItem) {
		this.protectingItem = protectingItem;
	}

}