package com.runecore.env.model.combat;

public class AttackInterfaceButton {

	private final int index;
	private final AttackButtonStyle style;
	private final AttackStyleBonus bonus;

	public AttackInterfaceButton(int button, AttackButtonStyle style,
			AttackStyleBonus bonus) {
		this.index = button;
		this.style = style;
		this.bonus = bonus;
	}

	public enum AttackButtonStyle {

		ACCURATE, AGGRESSIVE, DEFENSIVE, CONTROLLED, RAPID, LONGRANGE, RANGE_ACCURATE;

	}

	public enum AttackStyleBonus {
		SLASH, STAB, CRUSH, RANGED, MAGIC;
	}

	public int getIndex() {
		return index;
	}

	public AttackButtonStyle getStyle() {
		return style;
	}

	public AttackStyleBonus getBonus() {
		return bonus;
	}

}
