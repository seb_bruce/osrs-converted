package com.runecore.env.model.combat.script.spells;

import com.runecore.env.model.Entity;
import com.runecore.env.model.combat.CombatHit;
import com.runecore.env.model.combat.CombatType;
import com.runecore.env.model.combat.script.MagicSpellScript;
import com.runecore.env.model.flag.Animation;
import com.runecore.env.model.flag.Graphic;
import com.runecore.env.model.item.Item;
import com.runecore.util.Misc;

public class IceBarrageScript extends MagicSpellScript {

	@Override
	public Item[] getRequirements() {
		return new Item[] { 
			new Item(WATER_RUNE, 6), 
			new Item(BLOOD_RUNE, 2),
			new Item(DEATH_RUNE, 4) 
		};
	}

	@Override
	public int getSpellRequirement() {
		return 94;
	}

	@Override
	public CombatHit[] getDamage(Entity e, Entity target) {
		int max = getMaxHit(e, target);
		int hit = e.getCombat().canHit(target, this, CombatType.MAGIC, null) ? Misc
				.random(max) : 0;
		int delay = e.getCombat().getTickDelay(target, CombatType.MAGIC);
		if (hit == 0) {
			applyGraphic(e, target, delay, Graphic.create(85, 0, 100 << 16));
			return new CombatHit[] { new CombatHit(0, e, target, delay) };
		}
		//boolean orb = target.isFreezeImmune();
		//if (!orb) {
		//	target.setFreezeEffect(new FreezeEffect(e, 20000), 5000);
		//}
		boolean orb = false;
		applyGraphic(e, target, delay, Graphic.create(orb ? 1677 : 369, 0, orb ? 100 << 16 : 0));
		if(hit > 0)
			return new CombatHit[] { new CombatHit(hit, e, target, delay) };
		return new CombatHit[0];
	}

	@Override
	public void execute(Entity e, Entity target) {
		e.animate(Animation.create(1979));
	}

	@Override
	public int getSpellMax() {
		return 30;
	}

	@Override
	public int getSpellBook() {
		return 193;
	}

	@Override
	public int getMinimumXP() {
		return 52;
	}

}