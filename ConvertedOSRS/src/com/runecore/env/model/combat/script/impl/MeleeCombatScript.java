package com.runecore.env.model.combat.script.impl;

import com.runecore.env.model.Entity;
import com.runecore.env.model.combat.CombatHit;
import com.runecore.env.model.combat.CombatType;
import com.runecore.env.model.combat.script.CombatScript;
import com.runecore.env.model.container.Equipment;
import com.runecore.env.model.flag.Animation;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.map.pf.PathFinderExecutor;
import com.runecore.env.model.player.Player;
import com.runecore.util.Misc;

public class MeleeCombatScript implements CombatScript {

	@Override
	public int getCooldown(Entity e) {
		/*
		if (e.isPlayer()) {
			Player p = e.player();
			Item weapon = p.equipment().get(Equipment.SLOT_WEAPON);
			if (weapon != null)
				return weapon.getDefinition().getAttackSpeed();
		}
		*/
		return 4;
	}

	@Override
	public void execute(Entity e, Entity target) {
		if (e.isPlayer())
			e.animate(Animation.create(getAttackAnimation(e.player())));
	}

	@Override
	public boolean canAttack(Entity e, Entity target) {
		double distance = e.getLocation().distance(target.getLocation());
		boolean inRange = distance == 1;
		if(!inRange) {
			boolean bothMoving = e.getWalking().isMoving() && target.getWalking().isMoving();
			boolean bothRun = e.getWalking().getRunDir() != -1 && target.getWalking().getRunDir() != -1;
			if(bothMoving) {
				double distanceAllowed = bothRun ? 3.0 : 2.0;
				if(distance <= distanceAllowed)
					inRange = true;
			}
			PathFinderExecutor.walkTo(e, e.getLocation().closestTileOfEntity(target));
		}
		return inRange;
	}

	@Override
	public CombatHit[] getDamage(Entity e, Entity target) {
		// TODO Auto-generated method stub
		int max = e.getCombat().getMaxHit(CombatType.MELEE, target, 1.0);
		int random = Misc.random(max);
		int hit = e.getCombat().hit(target, CombatType.MELEE,
				e.getCombat().getAttackStyleData()) ? random : 0;
		return new CombatHit[] { new CombatHit(hit, e, target, 1) };
	}

	private static int getAttackAnimation(Player player) {
		int style = player.getCombat().getAttackStyleData().getIndex();
		Item item = player.equipment().get(Equipment.SLOT_WEAPON);
		if (item == null) {
			return style == 1 ? 423 : 422;
		}
		String itemName = item.getDefinition().getName().toLowerCase();
		if (itemName.contains("whip") || itemName.contains("tent")) {
			return 1658;
		} else if (itemName.contains("warham") || itemName.contains("mace")) {
			return 401;
		} else if (itemName.contains("scimitar") || itemName.contains("korasi")) {
			return 390;
		} else if (itemName.contains("longsword")) {
			return 451;
		} else if (itemName.contains("claws")) {
			return style == 1 ? 395 : 393;
		} else if (itemName.contains("dharok")) {
			return style == 2 ? 2066 : 2067;
		} else if (itemName.contains("verac")) {
			return 2062;
		} else if (itemName.contains("granite ma")) {
			return 1665;
		} else if (itemName.contains("guthan")) {
			return style == 2 ? 2081 : 2080;
		} else if (itemName.contains("torag")) {
			return 2068;
		} else if (itemName.contains("godsword")
				|| itemName.contains("2h sword")
				|| itemName.contains("saradomin swor")) {
			return style == 2 ? 7054 : style == 3 ? 7055 : 7045;
		} else if (itemName.equals("keris") || itemName.contains("dagger")
				|| itemName.endsWith("pickaxe")) {
			return style == 2 ? 395 : 396;
		} else if (itemName.contains("halberd")) {
			return 440;
		} else if (itemName.contains("tzhaar-ket-o")
				|| itemName.contains(" maul")) {
			return 13055;
		} else if (itemName.contains("battleaxe")
				|| itemName.contains("hatchet")) {
			return 395;
		} else if (itemName.contains("anchor")) {
			return 5865;
		} else if (itemName.contains("spear")) {
			return style == 1 ? 12009 : style == 2 ? 12005 : 12006;
		} else if (itemName.contains("chaotic staff")) {
			return 13047;
		}
		return 12029;
	}

}