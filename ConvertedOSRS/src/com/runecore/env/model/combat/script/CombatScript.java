package com.runecore.env.model.combat.script;

import com.runecore.env.model.Entity;
import com.runecore.env.model.combat.CombatHit;

public interface CombatScript {
    
    public int getCooldown(Entity e);
    public void execute(Entity e, Entity target);
    public boolean canAttack(Entity e, Entity target);
    public CombatHit[] getDamage(Entity e, Entity target);

}