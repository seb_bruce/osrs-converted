package com.runecore.env.model.combat.script;

import com.runecore.codec.Events;
import com.runecore.env.model.Cooldown;
import com.runecore.env.model.Entity;
import com.runecore.env.model.combat.CombatManager;
import com.runecore.env.model.combat.CombatType;
import com.runecore.env.model.player.Player;

public abstract class SpecialAttackScript implements CombatScript {

	private final int drain;
	private final int[] weapons;
	private final boolean enabled;
	private final CombatType type;

	public SpecialAttackScript() {
		SpecialAttackManifest manifest = getClass().getAnnotation(
				SpecialAttackManifest.class);
		if (manifest == null)
			throw new RuntimeException("Manifest missing for "+ getClass().getName());
		this.weapons = manifest.ids();
		this.drain = manifest.drain();
		this.enabled = manifest.enabled();
		this.type = manifest.type();
	}

	@Override
	public abstract int getCooldown(Entity e);

	@Override
	public void execute(Entity e, Entity target) {
		e.getCombatDefinition().setSpecialAmount(e.getCombatDefinition().getSpecialAmount() - this.drain);
		e.getCombatDefinition().setSpecialActivated(false);
		run(e, target);
	}

	public abstract void run(Entity e, Entity target);

	@Override
	public boolean canAttack(Entity e, Entity target) {
		if (e.getCombatDefinition().getSpecialAmount() < drain) {
			if (e.isPlayer()) {
				Player player = (Player) e;
				e.getCombatDefinition().setSpecialActivated(false);
				Events.sendMsg(player, "You don't have enough special energy left.");
			}
			e.setCooldown(Cooldown.COMBAT, 3);
			return false;
		}
		return CombatManager.get().getType(type).canAttack(e, target);
	}

	public boolean isEnabled() {
		return enabled;
	}

	public int getDrain() {
		return drain;
	}

	public int[] getWeapons() {
		return weapons;
	}

}