package com.runecore.env.model.combat.script.special;

import com.runecore.env.model.Entity;
import com.runecore.env.model.combat.CombatHit;
import com.runecore.env.model.combat.CombatType;
import com.runecore.env.model.combat.script.SpecialAttackManifest;
import com.runecore.env.model.combat.script.SpecialAttackScript;
import com.runecore.env.model.flag.Animation;
import com.runecore.env.model.flag.Graphic;
import com.runecore.util.Misc;

@SpecialAttackManifest(drain = 50, enabled = true, ids = { 11802 }, type = CombatType.MELEE)
public class ArmadylGodsword extends SpecialAttackScript {

	@Override
	public CombatHit[] getDamage(Entity e, Entity target) {
		int max = e.getCombat().getMaxHit(CombatType.MELEE, target, 1.25);
		int random = Misc.random(max);
		int hit = e.getCombat().hit(target, CombatType.MELEE,
				e.getCombat().getAttackStyleData()) ? random : 0;
		return new CombatHit[] { new CombatHit(hit, e, target, 1) };
	}

	@Override
	public int getCooldown(Entity e) {
		return 6;
	}

	@Override
	public void run(Entity e, Entity target) {
		e.graphics(Graphic.create(1211));
		e.animate(Animation.create(7061));
	}

}