package com.runecore.env.model.combat.script;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import com.runecore.env.model.combat.CombatType;

@Retention(RetentionPolicy.RUNTIME)
public @interface SpecialAttackManifest {
    
    int[] ids();
    int drain();
    CombatType type();
    boolean enabled();

}