package com.runecore.env.model.combat.script.spells;

import com.runecore.env.model.Entity;
import com.runecore.env.model.combat.CombatHit;
import com.runecore.env.model.combat.CombatType;
import com.runecore.env.model.combat.script.MagicSpellScript;
import com.runecore.env.model.flag.Animation;
import com.runecore.env.model.flag.Graphic;
import com.runecore.env.model.item.Item;
import com.runecore.util.Misc;

public class IceBlitzScript extends MagicSpellScript {

	@Override
	public int getSpellMax() {
		// TODO Auto-generated method stub
		return 26;
	}

	@Override
	public Item[] getRequirements() {
		return new Item[] { new Item(WATER_RUNE, 3), new Item(BLOOD_RUNE, 2),
				new Item(DEATH_RUNE, 2) };
	}

	@Override
	public int getSpellRequirement() {
		// TODO Auto-generated method stub
		return 82;
	}

	@Override
	public CombatHit[] getDamage(Entity e, Entity target) {
		int max = getMaxHit(e, target);
		int hit = e.getCombat().canHit(target, this, CombatType.MAGIC, null) ? Misc
				.random(max) : 0;
		int delay = e.getCombat().getTickDelay(target, CombatType.MAGIC);
		if (hit == 0) {
			applyGraphic(e, target, delay, Graphic.create(85, 0, 100 << 16));
			return new CombatHit[] { new CombatHit(0, e, target, delay) };
		}
		boolean orb = false;//target.isFreezeImmune();
		//if (!orb) {
		//	target.setFreezeEffect(new FreezeEffect(e, 15000), 5000);
		//}
		delay = delay + 2;
		applyGraphic(e, target, delay - 2, Graphic.create(367));
		if(hit > 0)
			return new CombatHit[] { new CombatHit(hit, e, target, delay - 1) };
		return new CombatHit[0];
	}

	@Override
	public void execute(Entity e, Entity target) {
		e.animate(Animation.create(1978));
		e.graphics(Graphic.create(366));
	}

	/* (non-Javadoc)
	 * @see org.legacy.model.combat.script.MagicSpellScript#getSpellBook()
	 */
	@Override
	public int getSpellBook() {
		// TODO Auto-generated method stub
		return 193;
	}

	/* (non-Javadoc)
	 * @see org.legacy.model.combat.script.MagicSpellScript#getMinimumXP()
	 */
	@Override
	public int getMinimumXP() {
		return 48;
	}

}
