package com.runecore.env.model.combat.script;

import com.runecore.codec.Events;
import com.runecore.env.core.Tick;
import com.runecore.env.model.Entity;
import com.runecore.env.model.combat.CombatHit;
import com.runecore.env.model.combat.Prayer.ActivePrayer;
import com.runecore.env.model.flag.Graphic;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.map.pf.PathFinderExecutor;
import com.runecore.env.model.player.Player;
import com.runecore.util.Misc;

public abstract class MagicSpellScript implements CombatScript {

	public static final int FIRE_RUNE = 554;
	public static final int WATER_RUNE = 555;
	public static final int AIR_RUNE = 556;
	public static final int EARTH_RUNE = 557;
	public static final int MIND_RUNE = 558;
	public static final int BODY_RUNE = 559;
	public static final int DEATH_RUNE = 560;
	public static final int NATURE_RUNE = 561;
	public static final int CHAOS_RUNE = 562;
	public static final int LAW_RUNE = 563;
	public static final int COSMIC_RUNE = 564;
	public static final int BLOOD_RUNE = 565;
	public static final int SOUL_RUNE = 566;
	public static final String MISSING_LEVEL_MESSAGE = "You need a magic level of lvl to cast this spell.";
	public static final String NO_RUNE_MESSAGE = "You don't have enough runes to cast this spell.";

	public abstract int getMinimumXP();
	public abstract int getSpellMax();
	public abstract int getSpellBook();
	public abstract Item[] getRequirements();

	public abstract int getSpellRequirement();

	public abstract CombatHit[] getDamage(Entity e, Entity target);

	public abstract void execute(Entity e, Entity target);

	public int getMaxHit(Entity e, Entity target) {
		int max = getSpellMax();
		if (target.getPrayer().hasPrayerOn(ActivePrayer.PROTECT_FROM_MAGIC))
			max = (int) (max * 0.6);
		return max;
	}

	@Override
	public int getCooldown(Entity e) {
		return 5;
	}

	@Override
	public boolean canAttack(Entity e, Entity target) {
		Item[] requirements = getRequirements();
		if (e.isPlayer()) {
			Player player = e.player();
			if (!hasRuneRequirements(requirements, player)) {
				Events.sendMsg(player, NO_RUNE_MESSAGE);
				e.getActionQueue().cancelQueuedActions();
				return false;
			}
		}
		double distance = e.getLocation().distance(target.getLocation());
		int distanceAllowed = 8;
		boolean inRange = distance <= distanceAllowed && distance != 0;
		if(!inRange) {
			boolean bothMoving = e.getWalking().isMoving() && target.getWalking().isMoving();
			boolean bothRun = e.getWalking().getRunDir() != -1 && target.getWalking().getRunDir() != -1;
			if(bothMoving) {
				double extraDistanceAllowed = bothRun ? 3.0 + distanceAllowed : 2.0 + distanceAllowed;
				if(distance <= extraDistanceAllowed)
					inRange = true;
			}
			PathFinderExecutor.walkTo(e, e.getLocation().closestTileOfEntity(target));
		}
		return inRange;
	}

	public static boolean hasRuneRequirements(Item[] reqs, Player player) {
		for (Item req : reqs) {
			if (player.get("inventory").getCount(req.getId()) < req.getAmount()) {
				return false;
			}
		}
		return true;
	}

	public static void removeRequirements(Item[] reqs, Player player) {

	}

	public static void sendWrongLevelMessage(Player p, int level) {
		Events.sendMsg(p,
				MagicSpellScript.MISSING_LEVEL_MESSAGE.replaceAll("lvl",
						Integer.toString(level)));
	}

	protected void applyGraphic(final Entity e, final Entity target, int delay,
			final Graphic g) {
		target.graphics(Graphic.create(-1));
		delay = delay - 1;
		if (delay < 1)
			delay = 1;
		target.register("magic-gfx-" + Misc.random(Short.MAX_VALUE),
				new Tick(delay) {
					@Override
					public void execute() {
						target.graphics(g);
						stop();
					}
				});
	}

}