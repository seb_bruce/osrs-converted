package com.runecore.env.model.combat.script.impl;

import com.runecore.codec.Events;
import com.runecore.env.model.Entity;
import com.runecore.env.model.combat.AttackInterfaceButton.AttackButtonStyle;
import com.runecore.env.model.combat.CombatHit;
import com.runecore.env.model.combat.CombatType;
import com.runecore.env.model.combat.script.CombatScript;
import com.runecore.env.model.container.Equipment;
import com.runecore.env.model.flag.Animation;
import com.runecore.env.model.flag.Graphic;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.map.pf.PathFinderExecutor;
import com.runecore.env.model.player.Player;
import com.runecore.util.Misc;

public class RangeCombatScript implements CombatScript {

	@Override
	public int getCooldown(Entity e) {
		if (e.isPlayer()) {
			Player p = e.player();
			Item weapon = p.equipment().get(Equipment.SLOT_WEAPON);
			if (weapon != null) {
				int speed = 5;//weapon.getDefinition().getAttackSpeed();
				if(e.getCombat().getAttackStyleData().getStyle() == AttackButtonStyle.RAPID) 
					speed--;
				return speed;
			}
		}
		return 4;
	}

	@Override
	public void execute(Entity e, Entity target) {
		e.getWalking().reset();
		if(e.isPlayer()) {
			Player p = e.player();
			Animation anim = getWeaponAnimation(p.equipment().get(Equipment.SLOT_WEAPON).getDefinition().getName().toLowerCase());
			if(anim != null) 
				p.animate(anim);
			Graphic pullback = getPullbackGraphic(p.equipment().get(Equipment.SLOT_ARROWS));
			if(pullback != null)
				p.graphics(pullback);
			sendProjectile(e, target, p.equipment().get(Equipment.SLOT_WEAPON), false);
		}
	}

	@Override
	public boolean canAttack(Entity e, Entity target) {
		if(e.isPlayer()) {
			Player p = e.player();
			int[] allowed = getAmmunitionAllowed(p.equipment().get(Equipment.SLOT_WEAPON));
			Item arrows = p.equipment().get(Equipment.SLOT_ARROWS);
			if(arrows == null) {
				Events.sendMsg(p, "You don't have any arrows in your quiver!");
				e.getActionQueue().stop();
				return false;
			}
			boolean canContinue = false;
			for(int i : allowed) {
				if(i == arrows.getId()) {
					canContinue = true;
					break;
				}
			}
			if(!canContinue) {
				Events.sendMsg(p, "You cannot use this ammo with this weapon!");
				e.getActionQueue().stop();
				return false;
			}
		}
		
		double distance = e.getLocation().distance(target.getLocation());
		int distanceAllowed = 8;
		if (e.isPlayer()) {
			Player p = e.player();
			Item weapon = p.equipment().get(Equipment.SLOT_WEAPON);
			if (weapon != null) {
				if(e.getCombat().getAttackStyleData().getStyle() == AttackButtonStyle.LONGRANGE) 
					distanceAllowed += 2;
			}
		}
		boolean inRange = distance <= distanceAllowed && distance != 0;
		if(!inRange) {
			boolean bothMoving = e.getWalking().isMoving() && target.getWalking().isMoving();
			boolean bothRun = e.getWalking().getRunDir() != -1 && target.getWalking().getRunDir() != -1;
			if(bothMoving) {
				double extraDistanceAllowed = bothRun ? 3.0 + distanceAllowed : 2.0 + distanceAllowed;
				if(distance <= extraDistanceAllowed)
					inRange = true;
			}
			PathFinderExecutor.walkTo(e, e.getLocation().closestTileOfEntity(target));
		}
		return inRange;
	}

	@Override
	public CombatHit[] getDamage(Entity e, Entity target) {
		double modifier = 1.0;
		if(e.isPlayer()) {
			Player pl = e.player();
			Item bolts = pl.equipment().get(Equipment.SLOT_ARROWS);
			if(bolts != null) {
				if(bolts.getId() == 9244) {
					int hit = Misc.random(6);
					if(hit == 3) {
						target.graphics(Graphic.create(756));
						modifier = 1.55;
					}
				}
			}
		}
		int max = e.getCombat().getMaxHit(CombatType.RANGED, target, modifier);
		int random = Misc.random(max);
		int hit = e.getCombat().canHit(target, this, CombatType.RANGED,
				e.getCombat().getAttackStyleData()) ? random : 0;
		int delay = e.getCombat().getTickDelay(target, CombatType.RANGED);
		return new CombatHit[] { new CombatHit(hit, e, target, delay) };
	}
	
	/**
	 * Gets the weapons attack animation
	 * @param type The ranged attack type
	 * @return The animation
	 */
	public Animation getWeaponAnimation(String name) {
		if(name.contains("knife"))
			return Animation.create(9055, 0);
		if(name.contains("karil's"))
			return Animation.create(2075);
		if(name.contains("longbow") || name.contains("shortbow") || name.contains("crystal") || name.contains("darkbow"))
			return Animation.create(426);
		if(name.contains("chinchompa"))
			return Animation.create(2779);
		if(name.contains("crossbow"))
			return Animation.create(4230);
		if(name.contains("jav")) 
				return Animation.create(9055);
		return null;
	}
	
	public Graphic getPullbackGraphic(Item item) {
		if(item == null) {
			return null;
		}
		switch(item.getId()) {
		case 20171:
			return Graphic.create(2962, 0);
		case 11212:
			return Graphic.create(1111, 0, 100 << 16);
		case 15243:
			return Graphic.create(2138, 0);
		case 882:
		case 883:
			return Graphic.create(19, 0, 100 << 16);
		case 884:
		case 885:
			return Graphic.create(18, 0, 100 << 16);
		case 886:
		case 887:
			return Graphic.create(20, 0, 100 << 16);
		case 888:
		case 889:
			return Graphic.create(21, 0, 100 << 16);
		case 890:
		case 891:
			return Graphic.create(22, 0, 100 << 16);
		case 892:
		case 893:
			return Graphic.create(24, 0, 100 << 16);
		case 4212:
			return Graphic.create(250, 0);
		case 13879:
			return Graphic.create(1836, 0);
		case 16452:
			return Graphic.create(2480, 0);
		}
		return null;
	}
	
	/**
	 * Gets the allowed ammunition allowed
	 * @param item
	 * @return
	 */
	public int[] getAmmunitionAllowed(Item item) {
		switch(item.getId()) {
		case 839: // norm
		case 841:
			return new int[] { 882, 884 };
		case 843: // oak
		case 845:
			return new int[] { 882, 884 , 886 };
		case 847: //willow
		case 849:
			return new int[] { 882, 884 , 886, 888 };
		case 851: //maple
		case 853:
			return new int[] { 882, 884 , 886, 888, 890 };
		case 855://yew
		case 857:
		case 859://mage
		case 861:
			return new int[] { 882, 884 , 886, 888, 890, 892 };
		case 14684: 
			return new int[] { 8882, 9139, 9140, 9141, 9142, 9143, };
		case 18357:
		case 9185:
			return new int[] { 9243, 9244, 9241, 9245 };
		case 11235:
			return new int[] { 882, 884 , 886, 888, 890, 892, 11212 };
		case 4734:
			return new int[] { 4740 };
		case 15241:
			return new int[] { 15243 };
		case 17295:
			return new int[] { 16452 };
		}
		return null;
	}
	
	public int getProjectile(Item item, Item arrows) {
		String wepName = item.getDefinition().getName().toLowerCase();
		if(wepName.contains("knife")) {
			int weaponId = item.getId();
			if (weaponId == 868) {
				return 218;
			} else if (weaponId == 867) {
				return 217;
			} else if (weaponId == 866) {
				return 216;
			} else if (weaponId == 865) {
				return 214;
			} else if (weaponId == 864) {
				return 212;
			} else if (weaponId == 863) {
				return 213;
			}
		}
		if(wepName.contains("cannon")) {
			return 2143;
		}
		if(wepName.contains("crystal")) {
			return 249;
		}
		if(wepName.contains("crossbow")) {
			return 27;
		}
		if(wepName.contains("jav")) {
			switch(item.getId()) {
			case 13879:
				return 1837;
			}
		}
		if(wepName.contains("bow")) {
			switch(arrows.getId()) {
			case 882:
			case 883:
				return 11;
			case 884:
			case 885:
				return 10;
			case 886:
			case 887:
				return 12;
			case 888:
			case 889:
				return 13;
			case 890:
			case 891:
				return 14;
			case 892:
			case 893:
				return 15;
			case 11212:
				return 1115;
			case 16452:
				return 2481;
			}
		}
		return -1;
	} 
	
	/**
	 * Sends the projectile
	 * @param source
	 * @param victim
	 * @param type
	 */
	private void sendProjectile(Entity source, Entity victim, Item wep, boolean spec) {
		String name = wep.getDefinition().getName().toLowerCase();
		int pro = 20;
		int start = 42;
		int[] speeds = new int[] {
			45, 23, 0	
		};
		if(source instanceof Player) {
			pro = getProjectile(source.player().equipment().get(Equipment.SLOT_WEAPON), source.player().equipment().get(Equipment.SLOT_ARROWS));
			if(name.contains("cannon")) {
				start = 20;
				speeds = new int[] { 
						25, 10, 0
				};
			}
			if(name.contains("chin")) {
				speeds = new int[] {
						15, 18, 0
				};
				start = 37;
			}
			if(name.contains("dark bow")) {
				speeds = new int[] {
						40, 23, 0
				};
				if(spec) {
					speeds[0] = 10;
				}
			}
			else if(name.contains("bow")|| name.contains("jav") || name.contains("knife")) {
				speeds = new int[] {
					40, 23, 0
				};
				if(name.contains("crossbow")) {
					speeds[1] = 10;
				}
				start = 35;
			}
		}
		//ProjectileManager.sendGlobalProjectile(source, victim, pro, start, 35,
		//		speeds[0], speeds[1], speeds[2]);
	}

}