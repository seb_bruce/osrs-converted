package com.runecore.env.model.combat;

import java.util.List;

import com.runecore.codec.Events;
import com.runecore.env.model.Entity;
import com.runecore.env.model.combat.AttackInterfaceButton.AttackButtonStyle;
import com.runecore.env.model.combat.AttackInterfaceButton.AttackStyleBonus;
import com.runecore.env.model.combat.Prayer.ActivePrayer;
import com.runecore.env.model.combat.script.CombatScript;
import com.runecore.env.model.container.Equipment;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;
import com.runecore.env.model.player.Skills;
import com.runecore.env.world.Location;
import com.runecore.util.Misc;

public class Combat {

	private final Entity entity;
	private int specialAttackEnergy = 100;
	private int attackStyleIndex = 0;
	private boolean specialAttackEnabled = Boolean.FALSE;
	private List<AttackInterfaceButton> currentStyles;

	public Combat(Entity entity) {
		this.entity = entity;
	}
	
	public static boolean canAttack(Location l, int myCombat, int combat) {
		int wildy = l.getWildernessLevel();
		double lowerBound = myCombat * 0.8;
		double higherBound = myCombat * 1.2;
		if(wildy > 0) {
			lowerBound -= wildy;
			higherBound += wildy;
		}
		if(lowerBound < 3)
			lowerBound = 3;
		if(higherBound > 138)
			higherBound = 138;
		return combat >= lowerBound && combat <= higherBound;
	}
	
	public void determineCombatStyles() {
		if(entity.isPlayer()) {
			Player player = entity.player();
			Item weapon = player.get("equipment").get(Equipment.SLOT_WEAPON);
			if(weapon == null || weapon.getDefinition().getCombatInterfaceKey() == null) {
				setCurrentStyles(CombatManager.get().getButtonSet("emptyting"));
			} else {
				setCurrentStyles(CombatManager.get().getButtonSet(weapon.getDefinition().getCombatInterfaceKey()));
			}
		}
	}

	public int getMaxHit(CombatType type, Entity target, double modifier) {
		int max = type == CombatType.MELEE ? getMeleeMaxHit(target) : getRangedMaxHit(target);

		if (type == CombatType.MELEE
				&& target.getPrayer().hasPrayerOn(ActivePrayer.PROTECT_FROM_MELEE)) {
			max = (int) (max * .6);
		} else if (type == CombatType.RANGED && target.getPrayer().hasPrayerOn(ActivePrayer.PROTECT_FROM_MISSILES)) {
			max = (int) (max * .6);
		}

		if (max < 0)
			max = 0;
		max *= modifier;
		Player d = (Player) target;
		return (int) (max);
	}

	public int getSpellRequirement() {
		if (entity.getActionQueue().getCurrentAction() instanceof CombatEntityAction) {
			CombatEntityAction action = (CombatEntityAction) entity
					.getActionQueue().getCurrentAction();
			if (action.getMagicSpell() != null)
				return action.getMagicSpell().getSpellRequirement();
		}
		return 1;
	}

	public int getTickDelay(Entity target, CombatType type) {
		int distance = entity.getLocation().getDistance(target.getLocation());
		return (distance > 2 ? 3 : 2) + (type == CombatType.MAGIC ? 2 : 0);
	}

	public int getWeaponRequirement() {
		return 1;
	}

	public int getBaseLevel(int index) {
		if (entity.isPlayer()) {
			return entity.player().getSkills().getRealLevel(index);
		}
		return 1;
	}

	public int getCurrentLevel(int index) {
		if (entity.isPlayer()) {
			return entity.player().getSkills().getLevel()[index];
		}
		return 1;
	}

	public double getDefensivePrayerBonus() {
		Prayer p = entity.getPrayer();
		if (p.hasPrayerOn(ActivePrayer.THICK_SKIN))
			return 1.05;
		if (p.hasPrayerOn(ActivePrayer.ROCK_SKIN))
			return 1.10;
		if (p.hasPrayerOn(ActivePrayer.STEEL_SKIN))
			return 1.15;
		if (p.hasPrayerOn(ActivePrayer.CHIVALRY))
			return 1.2;
		if (p.hasPrayerOn(ActivePrayer.PIETY))
			return 1.25;
		return 1.0;
	}

	public double getOffensivePrayerBonus(CombatType type) {
		Prayer p = entity.getPrayer();
		if (type == CombatType.MELEE) {
			if (p.hasPrayerOn(ActivePrayer.CLARITY_OF_THOUGHT))
				return 1.05;
			if (p.hasPrayerOn(ActivePrayer.IMPROVED_REFLEXES))
				return 1.10;
			if (p.hasPrayerOn(ActivePrayer.INCREDIBLE_REFLEXES))
				return 1.15;
			if (p.hasPrayerOn(ActivePrayer.CHIVALRY))
				return 1.15;
			if (p.hasPrayerOn(ActivePrayer.PIETY))
				return 1.2;
		} else if (type == CombatType.MAGIC) {
			if (p.hasPrayerOn(ActivePrayer.MYSTIC_WILL))
				return 1.05;
			if (p.hasPrayerOn(ActivePrayer.MYSTIC_LORE))
				return 1.10;
			if (p.hasPrayerOn(ActivePrayer.MYSTIC_MIGHT))
				return 1.15;
		} else if (type == CombatType.RANGED) {
			if (p.hasPrayerOn(ActivePrayer.SHARP_EYE))
				return 1.05;
			if (p.hasPrayerOn(ActivePrayer.HAWK_EYE))
				return 1.10;
			if (p.hasPrayerOn(ActivePrayer.EAGLE_EYE))
				return 1.15;
		}
		return 1.0;
	}

	public double getEquipmentTargetBoost(Entity target) {
		// slayer helm etc
		return 1.2;
	}

	public int getOffensiveStanceBonus(AttackInterfaceButton data) {
		if (data == null)
			return 0;
		if (data.getStyle() == AttackButtonStyle.ACCURATE)
			return 3;
		if (data.getStyle() == AttackButtonStyle.CONTROLLED)
			return 1;
		return 0;
	}

	public int getDefensiveStanceBonus(AttackInterfaceButton data) {
		if (data == null)
			return 0;
		if (data.getStyle() == AttackButtonStyle.LONGRANGE)
			return 3;
		if (data.getStyle() == AttackButtonStyle.CONTROLLED)
			return 1;
		if (data.getStyle() == AttackButtonStyle.DEFENSIVE)
			return 3;
		return 0;
	}

	public int getOffensiveBonus(AttackStyleBonus bonus) {
		if (bonus == AttackStyleBonus.STAB)
			return entity.getCombatDefinition().getAttackBonus(0);
		if (bonus == AttackStyleBonus.SLASH)
			return entity.getCombatDefinition().getAttackBonus(1);
		if (bonus == AttackStyleBonus.CRUSH)
			return entity.getCombatDefinition().getAttackBonus(2);
		if (bonus == AttackStyleBonus.MAGIC)
			return entity.getCombatDefinition().getAttackBonus(3);
		if (bonus == AttackStyleBonus.RANGED)
			return entity.getCombatDefinition().getAttackBonus(4);
		System.out.println("ay?");
		return 0;
	}

	public int getDefensiveBonus(AttackStyleBonus bonus) {
		if (bonus == AttackStyleBonus.STAB)
			return entity.getCombatDefinition().getDefenceBonus(0);
		if (bonus == AttackStyleBonus.SLASH)
			return entity.getCombatDefinition().getDefenceBonus(1);
		if (bonus == AttackStyleBonus.CRUSH)
			return entity.getCombatDefinition().getDefenceBonus(2);
		if (bonus == AttackStyleBonus.MAGIC)
			return entity.getCombatDefinition().getDefenceBonus(3);
		if (bonus == AttackStyleBonus.RANGED)
			return entity.getCombatDefinition().getDefenceBonus(4);
		return 0;
	}

	public double getSpecialBonus(CombatScript script) {
		/*if (script instanceof DragonDagger) {
			return 1.15;
		}
		
		 * Dragon Dagger - 1.15 (+15%) Dragon Scimitar - 1.15 (+15%) Dragon Mace
		 * - 0.85 (-15%) Dragon Battleaxe - 0.90 (-10%) Rune Claw - 1.15 (+15%)
		 * Vesta's Longsword - 1.20 (+20%) Brackish Blade - 2.0 (+100%) Magic
		 * Shortbow - 0.85 (-15%) Magic Shieldbow - 1.15 (+15%) Hand Cannon -
		 * 1.75 (+75%)
		 */

		return 1.0;
	}

	
	public boolean hit(Entity target, CombatType type, AttackInterfaceButton data) {
		
		int attackStat = type == CombatType.MELEE ? 0 : type == CombatType.RANGED ? Skills.RANGE : 6;
		int attackLevel = entity.isPlayer() ? entity.player().getSkills().getLevel()[attackStat] : 1;
		int defenceLevel = target.isPlayer() ? entity.player().getSkills().getLevel()[1] : 1;
		
		int attackBonus = getOffensiveBonus(data.getBonus()) / 2;
		int defenceBonus = target.getCombat().getDefensiveBonus(data.getBonus()) /10;
		
		attackBonus *= 1.2;
		
		int attackChance = attackLevel * attackBonus;
		int defenceChance = defenceLevel * defenceBonus;
		
		if(attackChance < 1)
			attackChance = 1;
		if(defenceChance < 1)
			defenceChance = 1;
		
		System.out.println("AttackChance= "+attackChance+" DefenceChance= "+defenceChance);
		return Misc.random(attackChance) > Misc.random(defenceChance);
		
	}
	
	/**
	 * lol this is so garbage
	 * @param target
	 * @param script
	 * @param off_combat_type
	 * @param data
	 * @return
	 */
	@SuppressWarnings("incomplete-switch")
	public boolean canHit(Entity target, CombatScript script,
			CombatType off_combat_type, AttackInterfaceButton data) {
		/*
		 * off denotes 'offensive', this prefix implies variable belongs to
		 * offensive entity.
		 * 
		 * def denotes 'defensive', this prefix implies variable belongs to
		 * defensive entity.
		 */

		AttackStyleBonus off_style = data != null ? data.getBonus()
				: AttackInterfaceButton.AttackStyleBonus.MAGIC; // stab, slash,
																// crush,
																// ranged, magic
		int off_weapon_requirement = getWeaponRequirement(); // weapon attack
																// level
																// requirement
		int off_spell_requirement = getSpellRequirement(); // spell magic level
															// requirement

		/*
		 * Name: off_base_*_level Type: Base value Description:
		 * 
		 * Base stat level pre any modifiers.
		 * 
		 * This value can be calculated using entity current xp.
		 * 
		 * Max value: 99
		 * 
		 * Examples:
		 * 
		 * 80/99 = 99 base level 116/95 = 95 base level
		 */

		int off_base_attack_level = getBaseLevel(Skills.ATTACK);
		int off_base_ranged_level = getBaseLevel(Skills.RANGE);
		int off_base_magic_level = getBaseLevel(Skills.MAGIC);

		/*
		 * Name: *_current_*_level Type: Base value Description:
		 * 
		 * Base stat level post visible stat modifiers.
		 * 
		 * 
		 * Examples:
		 * 
		 * 80/99 = 80 current level 116/95 = 116 current level
		 */

		double off_current_attack_level = this.getCurrentLevel(Skills.ATTACK);
		double off_current_ranged_level = this.getCurrentLevel(Skills.RANGE);
		double off_current_magic_level = this.getCurrentLevel(Skills.MAGIC);

		double def_current_defence_level = target.getCombat().getCurrentLevel(
				Skills.DEFENCE);
		double def_current_magic_level = target.getCombat().getCurrentLevel(
				Skills.MAGIC);

		/*
		 * Name: *_*_prayer_bonus Type: Multiplicative Bonus Description:
		 * 
		 * Multiplicative stat level modifier depending on chosen prayer(s),
		 * relevent to current calculation.
		 * 
		 * This bonus is granted for both physical and magic attacks.
		 * 
		 * Default value: 1.0
		 * 
		 * 
		 * Melee prayer offence:
		 * 
		 * Clarity of Thought - 1.05 (5%) Improved Reflexes - 1.10 (10%)
		 * Incredible Reflexes - 1.15 (15%) Chivalry - 1.15 (15%) Piety - 1.20
		 * (20%)
		 * 
		 * 
		 * Ranged prayer offence:
		 * 
		 * Sharp Eye - 1.05 (5%) Hawk Eye - 1.10 (10%) Eagle Eye - 1.15 (15%)
		 * Rigour - 1.20 (20%)
		 * 
		 * 
		 * Magic prayer offence:
		 * 
		 * Mystic Will - 1.05 (5%) Mystic Lore - 1.10 (10%) Mystic Might - 1.15
		 * (15%) Augury - 1.20 (20$)
		 * 
		 * 
		 * General prayer defence:
		 * 
		 * Thick Skin - 1.05 (5%) Rock Skin - 1.10 (10%) Steel Skin - 1.15 (15%)
		 * Chivalry - 1.20 (20%) Piety - 1.25 (25%)
		 */

		double off_attack_prayer_bonus = getOffensivePrayerBonus(CombatType.MELEE);
		double off_ranged_prayer_bonus = getOffensivePrayerBonus(CombatType.RANGED);
		double off_magic_prayer_bonus = getOffensivePrayerBonus(CombatType.MAGIC);

		double def_defence_prayer_bonus = target.getCombat()
				.getDefensivePrayerBonus();

		/*
		 * Name: off_additional_bonus Type: Multiplicative Bonus Description:
		 * 
		 * Multiplicative stat level modifier depending on worn equipment,
		 * relevent to current calculation.
		 * 
		 * This bonus is granted for both physical and magic attacks. This bonus
		 * is only granted to the offensive entity.
		 * 
		 * In the case that more than one of these conditions is met, modifiers
		 * are added together before being applied.
		 * 
		 * Default value: 1.0
		 * 
		 * 
		 * Versus general slayer task:
		 * 
		 * Melee:
		 * 
		 * Black Mask - 1.125 (12.5%) Slayer Helmet - 1.125 (12.5%) Full Slayer
		 * Helmet - 1.125 (12.5%)
		 * 
		 * Ranged:
		 * 
		 * Focus Sight - 1.125 (12.5%) Slayer Helmet - 1.125 (12.5%) Full Slayer
		 * Helmet - 1.125 (12.5%)
		 * 
		 * Magic:
		 * 
		 * Hexcrest - 1.125 (12.5%) Slayer Helmet - 1.125 (12.5%) Full Slayer
		 * Helmet - 1.125 (12.5%)
		 * 
		 * Versus dragon slayer task:
		 * 
		 * Melee:
		 * 
		 * Dragon Slayer Gloves - 1.10 (10%)
		 * 
		 * Versus undead:
		 * 
		 * Melee & Magic & Ranged:
		 * 
		 * Salve Amulet - 1.15 (15%) Salve Amulet (e) - 1.20 (20%)
		 * 
		 * 
		 * Examples:
		 * 
		 * Items: Salve Amulet, Black Mask Versus: Undead, Slayer Task Modifier:
		 * 1.275 (27.5%)
		 * 
		 * Items: Full Slayer Helmet, Dragon Slayer Gloves Versus: Dragon,
		 * Slayer Task Modifier: 1.225 (22.5%)
		 */

		double off_additional_bonus = getEquipmentTargetBoost(target);

		/*
		 * Name: *_stance_bonus Type: Additive Bonus Description:
		 * 
		 * Additive stat level modifier depending on chosen combat stance,
		 * relevent to current calculation.
		 * 
		 * This bonus is only granted for physical attacks (melee and ranged).
		 * 
		 * 
		 * Melee weapon offence:
		 * 
		 * Accurate - 3 level(s) Aggressive - 0 level(s) Controlled - 1 level(s)
		 * Defensive - 0 level(s)
		 * 
		 * 
		 * Ranged weapon offence:
		 * 
		 * Accurate - 3 level(s) Rapid - 0 level(s) Long Ranged - 0 level(s)
		 * 
		 * 
		 * Melee weapon defence:
		 * 
		 * Accurate - 0 level(s) Aggressive - 0 level(s) Controlled - 1 level(s)
		 * Defensive - 3 level(s)
		 * 
		 * 
		 * Ranged weapon defence:
		 * 
		 * Accurate - 0 level(s) Rapid - 0 level(s) Long Ranged - 3 level(s)
		 */

		int off_stance_bonus = getOffensiveStanceBonus(getAttackStyleData());

		int def_stance_bonus = target.getCombat().getDefensiveStanceBonus(
				target.getCombat().getAttackStyleData());

		/*
		 * Name: off_spell_bonus Type: Additive Bonus Description:
		 * 
		 * Additive stat level modifier depending on 'experience' using a spell.
		 * 
		 * "When you cast combat-related spells, there is a chance of failure.
		 * This is dependent on a few factors such as your magical attack
		 * bonuses, the magical Defence bonuses of your opponent and your Magic
		 * experience."
		 * 
		 * This bonus is calculated as .3 bonus level(s) per level above spell
		 * requirement.
		 * 
		 * This modifier uses BASE level, not CURRENT level. (ie. 112/99 = 99
		 * base level)
		 * 
		 * 
		 * Examples:
		 * 
		 * Current Magic Level: 99 Spell: Ice Blitz (Level 82) Effective Magic
		 * Level: 104
		 * 
		 * Current Magic Level: 99 Spell: Fire Blast (Level 59) Effective Magic
		 * Level: 111
		 */

		double off_spell_bonus = 0;

		switch (off_combat_type) {
		case MAGIC:
			if (off_base_magic_level > off_spell_requirement) {
				off_spell_bonus = (off_base_magic_level - off_spell_requirement) * .3;
			}
			break;
		}

		/*
		 * Name: off_weapon_bonus Type: Additive Bonus Description:
		 * 
		 * Additive stat level modifier depending on 'experience' using a
		 * weapon.
		 * 
		 * This is an experimental stat modifier based on the logic provided in
		 * the *_spell_bonus documentation.
		 * 
		 * This addition is justified by the observation that "dragon" weapons
		 * appear to be more accurate than their higher level counterparts at
		 * the exchange of worse equipment stats when used by high level
		 * characters.
		 * 
		 * This is an important balance point as part of the Runescape accuracy
		 * algorithm. This modifier provides a benefit to the attack stat level
		 * beyond weapon equip requirements, and an important trade off for
		 * those who choose to level their attack stat level further versus
		 * those who do not.
		 * 
		 * In addition low tier weaponry in currently utilised versions of the
		 * algorithm become completely obsolete and outclassed by high tier
		 * defence equipment giving the outcome of seemingly "overpowered"
		 * defence.
		 * 
		 * High level characters utilising high tier defence equipment against
		 * eachother will find lower tier weaponry a more viable choice, and
		 * will find it much more powerful against low tier armour.
		 * 
		 * These characters will also be granted a benefit versus characters
		 * with an inferior attack stat level.
		 * 
		 * This bonus is calculated as .3 bonus level(s) per level above weapon
		 * requirement.
		 * 
		 * This modifier uses BASE level, not CURRENT level. (ie. 112/99 = 99
		 * base level)
		 * 
		 * 
		 * Examples:
		 * 
		 * Current Attack Level: 99 Weapon: Dragon Dagger (Level 60) Effective
		 * Attack Level: 110
		 * 
		 * Current Attack Level: 99 Weapon Abyssal Whip (Level 70) Effective
		 * Attack Level: 107
		 * 
		 * Current Attack Level: 84 Weapon Abyssal Whip (Level 70) Effective
		 * Attack Level: 88
		 */

		double off_weapon_bonus = 0;

		switch (off_combat_type) {
		case MELEE:
			if (off_base_attack_level > off_weapon_requirement) {
				off_weapon_bonus = (off_base_attack_level - off_weapon_requirement) * .3;
			}
			break;
		case RANGED:
			if (off_base_ranged_level > off_weapon_requirement) {
				off_weapon_bonus = (off_base_ranged_level - off_weapon_requirement) * .3;
			}
			break;
		}

		/*
		 * Name: effective_attack Type: Base Value Description:
		 * 
		 * Base stat level post invisible stat modifiers.
		 * 
		 * This value is off_current_*_level with the following modifiers
		 * applied depending on combat type:
		 * 
		 * Melee combat:
		 * 
		 * off_attack_prayer_bonus off_additional_bonus off_stance_bonus
		 * off_weapon_bonus
		 * 
		 * 
		 * Ranged combat:
		 * 
		 * off_ranged_prayer_bonus off_additional_bonus off_stance_bonus
		 * off_weapon_bonus
		 * 
		 * 
		 * Magic combat:
		 * 
		 * off_magic_prayer_bonus off_additional_bonus off_spell_bonus
		 * 
		 * This value is rounded down.
		 */

		double effective_attack = 0;

		switch (off_combat_type) {
		case MELEE:
			effective_attack = Math
					.floor(((off_current_attack_level * off_attack_prayer_bonus) * off_additional_bonus)
							+ off_stance_bonus + off_weapon_bonus);
			break;
		case RANGED:
			effective_attack = Math
					.floor(((off_current_ranged_level * off_ranged_prayer_bonus) * off_additional_bonus)
							+ off_stance_bonus + off_weapon_bonus);
			break;
		case MAGIC:
			effective_attack = Math
					.floor(((off_current_magic_level * off_magic_prayer_bonus) * off_additional_bonus)
							+ off_spell_bonus);
			break;
		}

		/*
		 * Name: effective_magic Type: Base Value Description:
		 * 
		 * Additive stat level modifier depending on defending entity magic
		 * level.
		 * 
		 * Magic defence is calculated as 3 shares physical defence (or
		 * effective_defence) and 7 shares magic defence (or effective_magic).
		 * 
		 * In other words, this value makes up 70% of defence against magic
		 * spells. With the other 30% coming from effective_defence.
		 * 
		 * This value is def_current_magic_level multiplied by .7, or 70%.
		 * 
		 * This value is rounded down.
		 */

		double effective_magic = 0;

		switch (off_combat_type) {
		case MAGIC:
			effective_magic = Math.floor(def_current_magic_level * .7);
			break;
		}

		/*
		 * Name: effective_defence Type: Base Value Description:
		 * 
		 * Base stat level post invisible stat modifiers.
		 * 
		 * For Melee and Ranged combat this value is def_current_defence_level
		 * with the following modifiers applied:
		 * 
		 * def_defence_prayer_bonus def_defence_stance_bonus
		 * 
		 * 
		 * For magic combat there is a special case, this value is composed of
		 * two values combined:
		 * 
		 * effective_defence effective_magic
		 * 
		 * For magic combat def_current_defence_level has the following
		 * modifiers applied:
		 * 
		 * def_defence_prayer_bonus
		 * 
		 * These values are then multiplied by .3, and .7 respectively.
		 * Composing the final whole value using two values combined.
		 * 
		 * Magic defence is a special case as defence level and stat modifiers
		 * should only play a small part in the final value used to calculate
		 * magic defence.
		 * 
		 * In currently utilised versions of the algorithm it is common for
		 * magic attacks to miss(or "splash") a lot because melee defence levels
		 * and modifiers are used for the calculation of defence against magic
		 * spells but this is not their intended use. Rather, they are intended
		 * for defence against melee and ranged attacks.
		 * 
		 * It is worth noting the incredibly high effective_attack and
		 * effective_defence levels when using melee & ranged attacks in
		 * comparison to the pitiful effective_attack of magic attacks. These
		 * melee/ranged defence steroids were not intended as defence against
		 * magic, and the effective_attack of magic spells simply can not
		 * compare.
		 * 
		 * This new implementation of magic defence creates the desired effect
		 * of magic hitting often versus characters with great melee and ranged
		 * defences (melee equipment) in addition to current magic level playing
		 * a much greater role in defence against magic attacks as intended.
		 * 
		 * Characters are required to raise their magic level as defence against
		 * magic attacks.
		 * 
		 * This value is rounded down.
		 */

		double effective_defence = 0;

		switch (off_combat_type) {
		case MELEE:
			effective_defence = Math
					.floor((def_current_defence_level * def_defence_prayer_bonus)
							+ def_stance_bonus);
			break;
		case RANGED:
			effective_defence = Math
					.floor((def_current_defence_level * def_defence_prayer_bonus)
							+ def_stance_bonus);
			break;
		case MAGIC:
			effective_defence = Math
					.floor((def_current_defence_level * def_defence_prayer_bonus) * .3);
			effective_defence = effective_defence + effective_magic;
			break;
		}

		/*
		 * Name: off_equipment_*_attack Type: Base Value Description:
		 * 
		 * Base equipment stat value of offensive entity.
		 * 
		 * 
		 * Examples:
		 * 
		 * Stab Attack: 150 Slash Attack: 190 Crush Attack: 120 Ranged Attack: 5
		 * Magic Attack: 70 off_equipment_magic_attack = 70
		 * 
		 * Stab Attack: 150 Slash Attack: 190 Crush Attack: 120 Ranged Attack: 5
		 * Magic Attack: 70 off_equipment_stab_attack = 150
		 */

		int off_equipment_stab_attack = getOffensiveBonus(AttackStyleBonus.STAB);
		int off_equipment_slash_attack = getOffensiveBonus(AttackStyleBonus.SLASH);
		int off_equipment_crush_attack = getOffensiveBonus(AttackStyleBonus.CRUSH);
		int off_equipment_ranged_attack = getOffensiveBonus(AttackStyleBonus.RANGED);
		int off_equipment_magic_attack = getOffensiveBonus(AttackStyleBonus.MAGIC);

		/*
		 * Name: def_equipment_*_defence Type: Base Value Description:
		 * 
		 * Base equipment stat value of defensive entity.
		 * 
		 * 
		 * Examples:
		 * 
		 * Stab Defence: 80 Slash Defence: 320 Crush Defence: 250 Ranged
		 * Defence: 400 Magic Defence: 130 def_equipment_magic_defence = 130
		 * 
		 * Stab Defence: 80 Slash Defence: 320 Crush Defence: 250 Ranged
		 * Defence: 400 Magic Defence: 130 def_equipment_stab_defence = 80
		 */

		int def_equipment_stab_defence = target.getCombat().getDefensiveBonus(
				AttackStyleBonus.STAB);
		int def_equipment_slash_defence = target.getCombat().getDefensiveBonus(
				AttackStyleBonus.SLASH);
		int def_equipment_crush_defence = target.getCombat().getDefensiveBonus(
				AttackStyleBonus.CRUSH);
		int def_equipment_ranged_defence = target.getCombat()
				.getDefensiveBonus(AttackStyleBonus.RANGED);
		int def_equipment_magic_defence = target.getCombat().getDefensiveBonus(
				AttackStyleBonus.MAGIC);

		/*
		 * Name: *_equipment_bonus Type: Multiplicative Bonus Description:
		 * 
		 * Multiplicative stat level modifier depending on combat type and
		 * attack style, relevant to current calculation.
		 * 
		 * This bonus is granted for both physical and magic attacks.
		 * 
		 * Default value: 1.0
		 * 
		 * 
		 * Examples:
		 * 
		 * Combat Type: Magic Attack Style: n/a off_equipment_bonus =
		 * off_equipment_magic_attack def_equipment_bonus =
		 * def_equipment_magic_defence
		 * 
		 * Combat Type: Melee Attack Style: Slash off_equipment_bonus =
		 * off_equipment_slash_attack def_equipment_bonus =
		 * def_equipment_slash_defence
		 */

		int off_equipment_bonus = 0;

		int def_equipment_bonus = 0;

		switch (off_combat_type) {
		case MELEE:
			switch (off_style) {
			case STAB:
				off_equipment_bonus = off_equipment_stab_attack;
				def_equipment_bonus = def_equipment_stab_defence;
				break;
			case SLASH:
				off_equipment_bonus = off_equipment_slash_attack;
				def_equipment_bonus = def_equipment_slash_defence;
				break;
			case CRUSH:
				off_equipment_bonus = off_equipment_crush_attack;
				def_equipment_bonus = def_equipment_crush_defence;
				break;
			}
			break;
		case RANGED:
			off_equipment_bonus = off_equipment_ranged_attack;
			def_equipment_bonus = def_equipment_ranged_defence;
			break;
		case MAGIC:
			off_equipment_bonus = off_equipment_magic_attack;
			def_equipment_bonus = def_equipment_magic_defence;
			break;
		}

		/*
		 * Name: augmented_attack Type: Base Value Description:
		 * 
		 * Base stat level post equipment stat value modifiers.
		 * 
		 * This value is calculated as:
		 * 
		 * math.floor(((effective_attack + 8) * (off_equipment_bonus + 64)) /
		 * 10)
		 * 
		 * effective_attack and off_equipment_bonus have 8 and 64 added to their
		 * values respectively before multiplication. These values are also
		 * added to augmented_defence and act as means of eliminating negetive
		 * numbers.
		 * 
		 * Negetive numbers create a lot of issues and strange behaviour within
		 * the algorithm. We can eliminate these easily by giving both values
		 * identical amounts that exceed the max possible negetive value they
		 * can contain.
		 * 
		 * A negetive off_equipment_bonus value would be corrected to a positive
		 * value, for example, and 64 exceeds the max possible negetive
		 * equipment bonus achievable within the game.
		 * 
		 * Following the multiplication of the two values, we divide by 10 for
		 * no reason other than to make small, more easily digestable numbers.
		 * 
		 * This value is rounded down.
		 */

		double augmented_attack = 0;

		augmented_attack = Math
				.floor(((effective_attack + 8) * (off_equipment_bonus + 64)) / 10);

		/*
		 * Name augmented_defence Type: Base Value Description:
		 * 
		 * Base stat level post equipment stat value modifiers.
		 * 
		 * This value is calculated as:
		 * 
		 * math.floor(((effective_defence + 8) * (def_equipment_bonus + 64)) /
		 * 10)
		 * 
		 * The same safety operations and readability optimisations are made
		 * here as mentioned above.
		 */

		double augmented_defence = 0;

		augmented_defence = Math
				.floor(((effective_defence + 8) * (def_equipment_bonus + 64)) / 10);

		/*
		 * Name: hit_chance Type: Base Value Description:
		 * 
		 * Base probability to hit calculated as a fraction of the lesser value.
		 * 
		 * 
		 * If augmented_attack has a lesser value than augmented_defence this
		 * means the value will be below 50%. Therefore:
		 * 
		 * hit_chance = augmented_attack / (augmented_defence * 2)
		 * 
		 * This calculation will provide us with a value sub 0.5, providing the
		 * chance to hit vs a superior equiped opponent.
		 * 
		 * If augmented_attack has a greater value than augmented_defence this
		 * means the value will be above 50%. Therefore:
		 * 
		 * hit_chance = augmented_defence / (augmented_attack * 2)
		 * 
		 * This calculation will provide us with a value sur 0.5, providing the
		 * chance to hit vs a inferior equiped opponent.
		 * 
		 * 
		 * In addition to these calculations we subtract or add 1 to the value
		 * depending on whether augmented_attack is greater or lesser than
		 * augmented_defence. This small modification ensures that the lesser
		 * entity always has a value greater than 0 so we can ensure a minimum
		 * chance to block or hit of 1% at all times.
		 */

		double hit_chance = 0;

		if (augmented_attack < augmented_defence) {
			hit_chance = (augmented_attack - 1) / (augmented_defence * 2);
		} else {
			hit_chance = 1 - ((augmented_defence + 1) / (augmented_attack * 2));
		}

		/*
		 * Name: off_special_attack_bonus Type: Multiplicative Bonus
		 * Description:
		 * 
		 * Multiplicative hit chance modifier depending on offensive entity
		 * special attack.
		 * 
		 * This is an experimental chance modifier based on the assumption that
		 * certain special attacks modify offensive entity chance to hit post
		 * all other modifiers similar to how void equipment works.
		 * 
		 * Please note this is a delicate value and brash modifiers will break
		 * the balance of the the algorithm completely.
		 * 
		 * Default value: 1.0
		 * 
		 * 
		 * Below are recomendations for this modifiers value depending on
		 * special attack being used:
		 * 
		 * Dragon Dagger - 1.15 (+15%) Dragon Scimitar - 1.15 (+15%) Dragon Mace
		 * - 0.85 (-15%) Dragon Battleaxe - 0.90 (-10%) Rune Claw - 1.15 (+15%)
		 * Vesta's Longsword - 1.20 (+20%) Brackish Blade - 2.0 (+100%) Magic
		 * Shortbow - 0.85 (-15%) Magic Shieldbow - 1.15 (+15%) Hand Cannon -
		 * 1.75 (+75%)
		 * 
		 * Please note:
		 * 
		 * Anything not listed above DOES NOT have a specal attack accuracy
		 * modifier.
		 * 
		 * Korasi Sword is an exception to this and requires a special case
		 * which sets the off_hit_chance roll to 100.
		 * 
		 * I have not had the opportunity to test these values, however they are
		 * educated guesses based on observations made regarding simularities in
		 * max hit and accuracy calculations. These values unless stated
		 * otherwise, are NOT meant to be dramatic modifiers. Other systems
		 * present in this algorithm will do much of the heavy lifting with
		 * securing a hit when using special attacks.
		 */

		double off_special_attack_bonus = getSpecialBonus(script);

		/*
		 * Name: off_void_bonus Type: Multiplicative Bonus Description:
		 * 
		 * Multiplicative hit chance modifier depending on if a full set of void
		 * equipment is worn, and which type.
		 * 
		 * Default value: 1.0
		 * 
		 * 
		 * Void Set Modifier Values:
		 * 
		 * Melee - 1.10 (10%) Ranged - 1.10 (10%) Magic - 1.30 (30%)
		 */

		double off_void_bonus = 1.0;

		/*
		 * Name: def_protect_from_* Type: Multiplicative Bonus Description:
		 * 
		 * Multiplicative hit chance modifier depending on if the defensive
		 * entity is using a protect from * prayer that matches the offensive
		 * entities combat type.
		 * 
		 * Prior to the dice being rolled, if the defensive entity is using a
		 * protect from * prayer that corresponds with the offensive entities
		 * combat type hit_chance is multiplied by .6 reducing the offensive
		 * entities chance to hit, and increasing the defensive entities chance
		 * to block.
		 * 
		 * Protect from * prayers reduce damage and chance to hit by 60% in
		 * player vs player combat.
		 */

		boolean def_protect_from_melee = target.getPrayer().hasPrayerOn(ActivePrayer.PROTECT_FROM_MELEE);
		boolean def_protect_from_ranged = target.getPrayer().hasPrayerOn(ActivePrayer.PROTECT_FROM_MISSILES);
		boolean def_protect_from_magic = target.getPrayer().hasPrayerOn(ActivePrayer.PROTECT_FROM_MAGIC);
		
		
		/*
		 * Name: off_hit_chance Type: Base Value Description:
		 * 
		 * Base probability to hit post chance modifiers.
		 * 
		 * This value is used to roll a dice and determine if the offensive
		 * entity is granted a hit or not.
		 * 
		 * This is calculated as:
		 * 
		 * math.random(0, off_hit_chance) math.random(0, def_block_chance)
		 * 
		 * if(off_hit_chance > def_block_chance) { //hit } else { //block }
		 * 
		 * Both entities roll a dice between 0 and their calculated chance to
		 * hit or block, whoever has the greatest value is successful.
		 * 
		 * This provides a chance for the defensive entity to fail a block, as
		 * opposed to the defensive entity being ensured a block if the
		 * offensive entity does not successfully hit.
		 * 
		 * This is another problem present in the currently utilised version of
		 * the algorithm that gives the illusion of "overpowered defence".
		 * Giving the defensive entity an ensured block puts the offensive
		 * entity at a huge disadvantage.
		 */

		double off_hit_chance = 0;

		switch (off_combat_type) {
		case MELEE:
			if (def_protect_from_melee == true) {
				off_hit_chance = Math
						.floor((((hit_chance * off_special_attack_bonus) * off_void_bonus) * .6) * 100);
			} else {
				off_hit_chance = Math
						.floor(((hit_chance * off_special_attack_bonus) * off_void_bonus) * 100);
			}
			break;
		case RANGED:
			if (def_protect_from_ranged == true) {
				off_hit_chance = Math
						.floor((((hit_chance * off_special_attack_bonus) * off_void_bonus) * .6) * 100);
			} else {
				off_hit_chance = Math
						.floor(((hit_chance * off_special_attack_bonus) * off_void_bonus) * 100);
			}
			break;
		case MAGIC:
			if (def_protect_from_magic == true) {
				off_hit_chance = Math
						.floor(((hit_chance * off_void_bonus) * .6) * 100);
			} else {
				off_hit_chance = Math
						.floor((hit_chance * off_void_bonus) * 100);
			}
			break;
		}

		off_hit_chance = 0 + (int) (Math.random() * off_hit_chance);

		/*
		 * Name: def_block_chance Type: Base Value Description:
		 * 
		 * Base probability to block post chance modifiers.
		 * 
		 * This value is used to roll a dice and determine if the defensive
		 * entity is granted a block or not.
		 * 
		 * See above for details on calculation.
		 */

		double def_block_chance = 0;

		switch (off_combat_type) {
		case MELEE:
			if (def_protect_from_melee == true) {
				def_block_chance = Math
						.floor(101 - ((((hit_chance * off_special_attack_bonus) * off_void_bonus) * .6) * 100));
			} else {
				def_block_chance = Math
						.floor(101 - (((hit_chance * off_special_attack_bonus) * off_void_bonus) * 100));
			}
			break;
		case RANGED:
			if (def_protect_from_ranged == true) {
				def_block_chance = Math
						.floor(101 - ((((hit_chance * off_special_attack_bonus) * off_void_bonus) * .6) * 100));
			} else {
				def_block_chance = Math
						.floor(101 - (((hit_chance * off_special_attack_bonus) * off_void_bonus) * 100));
			}
			break;
		case MAGIC:
			if (def_protect_from_magic == true) {
				def_block_chance = Math
						.floor(101 - (((hit_chance * off_void_bonus) * .6) * 100));
			} else {
				def_block_chance = Math
						.floor(101 - ((hit_chance * off_void_bonus) * 100));
			}
			break;
		}

		def_block_chance = 0 + (int) (Math.random() * def_block_chance);
		return off_hit_chance > def_block_chance;
	}

	private int getMeleeMaxHit(Entity target) {
		double a = getStrengthLevel();
		double b = 1.0;
		Prayer prayer = entity.getPrayer();
		if (prayer.hasPrayerOn(ActivePrayer.BURST_OF_STRENGTH)) {
			b *= 1.05;
		} else if (prayer.hasPrayerOn(ActivePrayer.SUPERHUMAN_STR)) {
			b *= 1.1;
		} else if (prayer.hasPrayerOn(ActivePrayer.ULTIMATE_STR)) {
			b *= 1.15;
		} else if (prayer.hasPrayerOn(ActivePrayer.CHIVALRY)) {
			b *= 1.18;
		} else if (prayer.hasPrayerOn(ActivePrayer.PIETY)) {
			b *= 1.23;
		}
		if(getEntity().isPlayer() && wearingDharok(entity.player())) {
			float current = getEntity().player().getSkills().getLevel()[3];
			float max = getEntity().player().getSkills().getRealLevel(3) * 10;
			b *= ((1 - ((float)current / (float)max) * 0.95) + 1);
		}
		AttackInterfaceButton data = getAttackStyleData();
		double c = Math.round((a * b));
		if (data.getStyle() == AttackButtonStyle.CONTROLLED) {
			c += 10;
		} else if (data.getStyle() == AttackButtonStyle.AGGRESSIVE) {
			c += 30;
		}
		double d = entity.getCombatDefinition().getStrengthBonus();
		float max = (float) ((c + 8) * (d + 64) / 640);
		int maxRounded = Math.round(max);
		return maxRounded;
	}
	
	public boolean wearingDharok(Player p) {
		if(p.equipment().get(Equipment.SLOT_WEAPON) == null || !p.equipment().get(Equipment.SLOT_WEAPON).getDefinition().name.contains("Dharok"))
			return false;
		if(p.equipment().get(Equipment.SLOT_BOTTOMS) == null || !p.equipment().get(Equipment.SLOT_HELM).getDefinition().name.contains("Dharok"))
			return false;
		if(p.equipment().get(Equipment.SLOT_CHEST) == null || !p.equipment().get(Equipment.SLOT_CHEST).getDefinition().name.contains("Dharok"))
			return false;
		if(p.equipment().get(Equipment.SLOT_BOTTOMS) == null || !p.equipment().get(Equipment.SLOT_BOTTOMS).getDefinition().name.contains("Dharok"))
			return false;
		return true;
	}
	
	/**
	 * An array of Range Strengths
	 */
	private static final int[][] RANGE_STRENGTHS = {
		{9241, 85}, {9243, 105}, {9244, 117}, {9245, 120},
		{890, 31}, {892, 41}, {4740, 55}, {11212, 60},
		{15243, 150}, {4212, 70}, {15243, 150}, {8882, 49}, {9143, 100}, {20171, 160},
		{13879, 145}, {16452, 100}, {864, 3}, {863, 4}, {865, 7}, {869, 8}, 
		{866, 10}, {867, 14}, {868, 24}
	};

	/**
	 * Gets the ranged strength of the item
	 * @param i
	 * @return
	 */
	private static int getRangeStrength(Item i) {
		if(i == null) {
			return 0;
		}
		for(int[] item : RANGE_STRENGTHS) {
			if(item[0] == i.getId()) {
				return item[1];
			}
		}
		return 0;
	}
	
	private int getRangedMaxHit(Entity target) {
		double a = getStrengthLevel();
		double b = 1.0;
		Prayer prayer = entity.getPrayer();
		if (prayer.hasPrayerOn(ActivePrayer.SHARP_EYE)) {
			b *= 1.05;
		} else if (prayer.hasPrayerOn(ActivePrayer.HAWK_EYE)) {
			b *= 1.1;
		} else if (prayer.hasPrayerOn(ActivePrayer.EAGLE_EYE)) {
			b *= 1.15;
		}
		AttackInterfaceButton data = getAttackStyleData();
		double c = Math.round((a * b));
		if (data.getStyle() == AttackButtonStyle.LONGRANGE) {
			c += 10;
		} else if (data.getStyle() == AttackButtonStyle.RANGE_ACCURATE) {
			c += 30;
		}
		int rangeStr = 0;
		if(getEntity().isPlayer()) {
			Player pl = getEntity().player();
			rangeStr = getRangeStrength(pl.equipment().get(Equipment.SLOT_ARROWS));
		}
		double d = rangeStr;
		float max = (float) ((c + 8) * (d + 64) / 640);
		int maxRounded = Math.round(max);
		return maxRounded;
	}

	private int getStrengthLevel() {
		if (entity.isPlayer()) {
			return entity.player().getSkills().getLevel()[Skills.STRENGTH];
		}
		return 1;
	}

	public int getDefenceEmote() {
		if (this.entity instanceof Player) {
			Player target = (Player) entity;
			final short weaponId = (short) (target.equipment().get(3) == null ? -1
					: target.equipment().get(3).getId());
			final short shieldId = (short) (target.equipment().get(5) == null ? -1
					: target.equipment().get(5).getId());
			if (shieldId == -1 && weaponId == -1)
				return 424;
			String weaponName = weaponId == -1 ? "" : target.equipment()
					.get(3).getDefinition().getName();
			String shieldName = shieldId == -1 ? "" : target.equipment()
					.get(5).getDefinition().getName();
			if (weaponId == 4153) {
				return 1666;
			}
			if (shieldId != -1 && shieldName.contains("defender")
					|| shieldId == 20072)
				return 4177;
			if (weaponId == 15241) {
				return 12156;
			}
			if (weaponName.contains("Barrelchest"))
				return 5866;
			if (shieldId != -1 && shieldName.contains("shield")
					|| shieldId == 6524 || shieldName.contains("deflector"))
				return 1156;
			if (weaponId != -1 && weaponName.toLowerCase().contains("greataxe")) {
				return 12004;
			}
			if (weaponId != -1 && weaponName.toLowerCase().contains("spear")) {
				return 12008;
			}
			if (weaponId != -1 && weaponName.toLowerCase().contains("warham")) {
				return 403;
			}
			if (weaponId != -1
					&& (weaponName.contains("godsword")
							|| weaponName.contains("2h sword") || weaponName
								.contains("Saradomin sword")))
				return 7056;
			if (weaponId != -1
					&& (weaponName.contains("Keris") || weaponName
							.contains("dagger")))
				return 403;
			if (weaponName.contains("staff") || weaponName.contains("wand") || weaponName.contains("Staff")) {
				return 415;
			}
			if (weaponId != -1 && (weaponId == 18353)) {
				return 13054;
			}
			if (weaponName.contains("whip"))
				return 11974;
			if (weaponName.contains("scimitar")
					|| weaponName.contains("Brackish"))
				return 12030;
			if (weaponId == 15486) {
				return 12806;
			}
		}
		return 424;
	}

	public void refreshSpecialAttack() {
		if (entity instanceof Player) {
			Player p = (Player) entity;
			Events.sendConfig(p, 300, getSpecialAttackEnergy() * 10);
			Events.sendConfig(p, 301, isSpecialAttackEnabled() ? 1 : 0);
		}
	}
	
	public int getIndex(int button) {
		List<AttackInterfaceButton> buttons = getCurrentStyles();
		for(int i = 0; i < buttons.size(); i++) {
			if(buttons.get(i).getIndex() == button)
				return i;
		}
		return -1;
	}

	public void updateCombatStyle(int index) {
		int buttonIndex = getIndex(index);
		int maxIndex = this.currentStyles.size() - 1;
		if(buttonIndex != -1) {
			if (buttonIndex > maxIndex) {
				updateCombatStyle(0);
				return;
			}
		} else {
			updateCombatStyle(0);
			return;
		}
		setAttackStyleIndex(buttonIndex);
		Events.sendConfig(entity.player(), 43, (this.currentStyles.size() == 3 && buttonIndex == 2) ? buttonIndex + 1 : buttonIndex);
	}

	public AttackInterfaceButton getAttackStyleData() {
		if(getCurrentStyles() == null) {
			System.out.println("??");
		}
		return getCurrentStyles().get(getAttackStyleIndex());
	}

	public Entity getEntity() {
		return entity;
	}

	public int getSpecialAttackEnergy() {
		return specialAttackEnergy;
	}

	public void setSpecialAttackEnergy(int specialAttackEnergy) {
		this.specialAttackEnergy = specialAttackEnergy;
		if(this.specialAttackEnergy > 100)
			this.specialAttackEnergy = 100;
		refreshSpecialAttack();
	}
	
	public void setSpecialAttackEnergy2(int specialAttackEnergy) {
		this.specialAttackEnergy = specialAttackEnergy;
		if(this.specialAttackEnergy > 100)
			this.specialAttackEnergy = 100;
	}

	public boolean isSpecialAttackEnabled() {
		return specialAttackEnabled;
	}

	public void setSpecialAttackEnabled(boolean specialAttackEnabled) {
		this.specialAttackEnabled = specialAttackEnabled;
		refreshSpecialAttack();
	}

	public List<AttackInterfaceButton> getCurrentStyles() {
		return currentStyles;
	}

	public void setCurrentStyles(List<AttackInterfaceButton> currentStyles) {
		this.currentStyles = currentStyles;
		int maxIndex = this.currentStyles.size() - 1;
		if (getAttackStyleIndex() > maxIndex) {
			updateCombatStyle(0);
		}
	}

	public int getAttackStyleIndex() {
		return attackStyleIndex;
	}

	public void setAttackStyleIndex(int attackStyleIndex) {
		this.attackStyleIndex = attackStyleIndex;
	}

}
