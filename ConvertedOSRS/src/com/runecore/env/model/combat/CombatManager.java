package com.runecore.env.model.combat;

import java.io.File;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import com.google.common.collect.ImmutableSet;
import com.google.common.io.Files;
import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.runecore.env.model.Entity;
import com.runecore.env.model.combat.script.CombatScript;
import com.runecore.env.model.combat.script.MagicSpellScript;
import com.runecore.env.model.combat.script.SpecialAttackScript;
import com.runecore.env.model.combat.script.impl.MeleeCombatScript;
import com.runecore.env.model.combat.script.impl.RangeCombatScript;
import com.runecore.env.model.combat.script.spells.IceBarrageScript;
import com.runecore.env.model.combat.script.spells.IceBlitzScript;
import com.runecore.env.model.container.Equipment;
import com.runecore.env.model.def.ItemDefinition;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.npc.NPC;
import com.runecore.env.model.player.Player;

public class CombatManager {

	private static final CombatManager INSTANCE = new CombatManager();
	private final Map<Integer, SpecialAttackScript> specials = new TreeMap<Integer, SpecialAttackScript>();
	private final Map<String, List<AttackInterfaceButton>> attackButtons = new LinkedHashMap<String, List<AttackInterfaceButton>>();
	private final Map<Integer, MagicSpellScript> magicSpells = new TreeMap<Integer, MagicSpellScript>();
	private final CombatScript MELEE = new MeleeCombatScript();
	private final CombatScript RANGE = new RangeCombatScript();
	private static String[] RANGE_PHRASE = new String[] { "knife", "bow",
			"chinchompa", "dart", "crossbow" };

	public static CombatManager get() {
		return INSTANCE;
	}

	public void init() throws Exception {
		int size = ItemDefinition.size();
		for(int i = 1; i < size; i++) {
			ItemDefinition def = ItemDefinition.forId(i);
			if(def == null || def.getName() == null)
				continue;
			if (isRangedPhrase(def.getName().toLowerCase())) {
				def.setCombatType(CombatType.RANGED);
			} else {
				def.setCombatType(CombatType.MELEE);
			}
			
			if(!def.isNoted()) {
				String name = def.getName().toLowerCase();
				if(name.contains("whip") || name.contains("tent")) {
					def.setCombatInterfaceKey("whips");
				} else if(name.contains("dagger") || name.contains("rapier")) {
					def.setCombatInterfaceKey("daggers");
				} else if(name.contains("bow")) {
					def.setCombatInterfaceKey("bows");
				} else if(name.contains("dart")) {
					def.setCombatInterfaceKey("darts");
				} else if(name.contains("staff")) {
					def.setCombatInterfaceKey("staffs");
				} else if(name.contains("pick")) {
					def.setCombatInterfaceKey("pickaxes");
				} else if(name.contains("hatchet")) {
					def.setCombatInterfaceKey("axes");
				} else if(name.contains("halberd")) {
					def.setCombatInterfaceKey("halberds");
				} else if(name.contains("scim") || name.contains("Korasi")) {
					def.setCombatInterfaceKey("scims");
				} else if(name.contains("2h") || name.contains("godsword") || name.contains("saradomin sword") || name.contains("dharok")) {
					def.setCombatInterfaceKey("2h");
				} else if(name.contains("maul")) {
					def.setCombatInterfaceKey("mauls");
				} else if(name.contains("claws")) {
					def.setCombatInterfaceKey("claws");
				}
			} 
			
		}
		ClassPath path = ClassPath.from(getClass().getClassLoader());
		ImmutableSet<ClassInfo> set = path
				.getTopLevelClassesRecursive("com.runecore.env.model.combat.script.special");
		for (ClassInfo i : set) {
			Class<?> script = Class.forName(i.getName());
			SpecialAttackScript specScript = (SpecialAttackScript) script
					.newInstance();
			int[] binds = specScript.getWeapons();
			for (int bind : binds) {
				specials.put(bind, specScript);
			}
		}
		Type type = new TypeToken<Map<String, List<AttackInterfaceButton>>>() {
		}.getType();

		magicSpells.put(12648449, new IceBlitzScript());
		magicSpells.put(12648451, new IceBarrageScript());
		byte[] json = Files.toByteArray(new File("./data/world/buttons.json"));
		Map<String, List<AttackInterfaceButton>> fromFile = new Gson()
				.fromJson(new String(json), type);
		this.attackButtons.putAll(fromFile);
		Logger.getGlobal().info(
				"Loaded " + this.attackButtons.size() + " combat style configurations!");
		/*
		
		List<AttackInterfaceButton> scim_buts = new LinkedList<AttackInterfaceButton>();
		scim_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.ACCURATE, AttackStyleBonus.SLASH));
		scim_buts.add(new AttackInterfaceButton(1,
				AttackButtonStyle.AGGRESSIVE, AttackStyleBonus.SLASH));
		scim_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.CONTROLLED, AttackStyleBonus.STAB));
		scim_buts.add(new AttackInterfaceButton(3,
				AttackButtonStyle.DEFENSIVE, AttackStyleBonus.SLASH));
		attackButtons.put("scims", scim_buts);

		List<AttackInterfaceButton> whip_buts = new LinkedList<AttackInterfaceButton>();
		whip_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.ACCURATE, AttackStyleBonus.SLASH));
		whip_buts.add(new AttackInterfaceButton(1,
				AttackButtonStyle.CONTROLLED, AttackStyleBonus.SLASH));
		whip_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.DEFENSIVE, AttackStyleBonus.SLASH));
		attackButtons.put("whips", whip_buts);

		List<AttackInterfaceButton> dag_buts = new LinkedList<AttackInterfaceButton>();
		dag_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.ACCURATE, AttackStyleBonus.STAB));
		dag_buts.add(new AttackInterfaceButton(1,
				AttackButtonStyle.AGGRESSIVE, AttackStyleBonus.STAB));
		dag_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.CONTROLLED, AttackStyleBonus.SLASH));
		dag_buts.add(new AttackInterfaceButton(3,
				AttackButtonStyle.DEFENSIVE, AttackStyleBonus.STAB));
		attackButtons.put("daggers", dag_buts);

		List<AttackInterfaceButton> unarmed_buts = new LinkedList<AttackInterfaceButton>();
		unarmed_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.ACCURATE, AttackStyleBonus.CRUSH));
		unarmed_buts.add(new AttackInterfaceButton(1,
				AttackButtonStyle.AGGRESSIVE, AttackStyleBonus.CRUSH));
		unarmed_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.DEFENSIVE, AttackStyleBonus.CRUSH));
		attackButtons.put("emptyting", unarmed_buts);

		List<AttackInterfaceButton> bow_buts = new LinkedList<AttackInterfaceButton>();
		bow_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.ACCURATE, AttackStyleBonus.RANGED));
		bow_buts.add(new AttackInterfaceButton(1, AttackButtonStyle.RAPID,
				AttackStyleBonus.RANGED));
		bow_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.LONGRANGE, AttackStyleBonus.RANGED));
		attackButtons.put("bows", bow_buts);

		List<AttackInterfaceButton> dart_buts = new LinkedList<AttackInterfaceButton>();
		dart_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.ACCURATE, AttackStyleBonus.RANGED));
		dart_buts.add(new AttackInterfaceButton(1, AttackButtonStyle.RAPID,
				AttackStyleBonus.RANGED));
		dart_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.LONGRANGE, AttackStyleBonus.RANGED));
		attackButtons.put("darts", dart_buts);

		List<AttackInterfaceButton> staff_buts = new LinkedList<AttackInterfaceButton>();
		staff_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.ACCURATE, AttackStyleBonus.CRUSH));
		staff_buts.add(new AttackInterfaceButton(1,
				AttackButtonStyle.AGGRESSIVE, AttackStyleBonus.CRUSH));
		staff_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.DEFENSIVE, AttackStyleBonus.CRUSH));
		attackButtons.put("staffs", staff_buts);

		List<AttackInterfaceButton> pick_buts = new LinkedList<AttackInterfaceButton>();
		pick_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.ACCURATE, AttackStyleBonus.CRUSH));
		pick_buts.add(new AttackInterfaceButton(1,
				AttackButtonStyle.AGGRESSIVE, AttackStyleBonus.STAB));
		pick_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.CONTROLLED, AttackStyleBonus.CRUSH));
		pick_buts.add(new AttackInterfaceButton(3,
				AttackButtonStyle.DEFENSIVE, AttackStyleBonus.CRUSH));
		attackButtons.put("pickaxes", pick_buts);

		List<AttackInterfaceButton> axe_buts = new LinkedList<AttackInterfaceButton>();
		axe_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.ACCURATE, AttackStyleBonus.SLASH));
		axe_buts.add(new AttackInterfaceButton(1,
				AttackButtonStyle.AGGRESSIVE, AttackStyleBonus.CRUSH));
		axe_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.AGGRESSIVE, AttackStyleBonus.CRUSH));
		axe_buts.add(new AttackInterfaceButton(3,
				AttackButtonStyle.DEFENSIVE, AttackStyleBonus.SLASH));
		attackButtons.put("axe", axe_buts);

		List<AttackInterfaceButton> halberd_buts = new LinkedList<AttackInterfaceButton>();
		halberd_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.CONTROLLED, AttackStyleBonus.STAB));
		halberd_buts.add(new AttackInterfaceButton(1,
				AttackButtonStyle.AGGRESSIVE, AttackStyleBonus.SLASH));
		halberd_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.DEFENSIVE, AttackStyleBonus.STAB));
		attackButtons.put("halberds", halberd_buts);
		
		
		List<AttackInterfaceButton> twoh_buts = new LinkedList<>();
		
		twoh_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.ACCURATE, AttackStyleBonus.SLASH));
		twoh_buts.add(new AttackInterfaceButton(1,
				AttackButtonStyle.AGGRESSIVE, AttackStyleBonus.SLASH));
		twoh_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.AGGRESSIVE, AttackStyleBonus.CRUSH));
		twoh_buts.add(new AttackInterfaceButton(3,
				AttackButtonStyle.DEFENSIVE, AttackStyleBonus.SLASH));
		attackButtons.put("2h", twoh_buts);
		
		

		List<AttackInterfaceButton> maul_buts = new LinkedList<>();
		
		maul_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.ACCURATE, AttackStyleBonus.CRUSH));
		maul_buts.add(new AttackInterfaceButton(1,
				AttackButtonStyle.AGGRESSIVE, AttackStyleBonus.CRUSH));
		maul_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.DEFENSIVE, AttackStyleBonus.CRUSH));
		attackButtons.put("mauls", maul_buts);
		
		List<AttackInterfaceButton> claws_buts = new LinkedList<>();
		claws_buts.add(new AttackInterfaceButton(0,
				AttackButtonStyle.ACCURATE, AttackStyleBonus.SLASH));
		claws_buts.add(new AttackInterfaceButton(1,
				AttackButtonStyle.AGGRESSIVE, AttackStyleBonus.SLASH));
		claws_buts.add(new AttackInterfaceButton(2,
				AttackButtonStyle.CONTROLLED, AttackStyleBonus.STAB));
		claws_buts.add(new AttackInterfaceButton(3,
				AttackButtonStyle.DEFENSIVE, AttackStyleBonus.SLASH));
		attackButtons.put("claws", claws_buts);
		
		
		Gson g = new GsonBuilder().setPrettyPrinting().create();
		Files.write(g.toJson(attackButtons).getBytes(), new File(
				"./buttons.json"));
		*/
		Logger.getGlobal().info(
				"Loaded " + specials.size() + " special attack scripts!");
	}

	private boolean isRangedPhrase(String name) {
		for (String s : RANGE_PHRASE) {
			if (name.contains(s))
				return true;
		}
		return false;
	}

	public CombatScript getScript(Entity e) {
		if (e.getActionQueue().getCurrentAction() instanceof CombatEntityAction) {
			CombatEntityAction action = (CombatEntityAction) e.getActionQueue()
					.getCurrentAction();
			if (action.getMagicSpell() != null) {
				return action.getMagicSpell();
			}
		}
		if (e.getCombatDefinition().isSpecialActivated()
				&& !e.isNPC()) {
			Player player = (Player) e;
			Item weapon = player.equipment().get(Equipment.SLOT_WEAPON);
			if (weapon == null) {
				e.getCombatDefinition().setSpecialActivated(false);
				return getScript(e);
			}
			SpecialAttackScript script = specials.get(weapon.getId());
			if (script == null) {
				e.getCombatDefinition().setSpecialActivated(false);
				return getScript(e);
			}
			return script;
		} else {
			// TODO: CASTING SPELL
			if (Player.class.isAssignableFrom(e.getClass())) {
				Player player = (Player) e;
				Item weapon = player.equipment().get(Equipment.SLOT_WEAPON);
				if (weapon != null) {
					if (weapon.getDefinition().getCombatType() == CombatType.RANGED)
						return RANGE;
					return MELEE;
				}
			}
			return MELEE;
		}
	}

	public CombatScript getType(CombatType type) {
		if (type == CombatType.MELEE)
			return MELEE;
		else if (type == CombatType.RANGED)
			return RANGE;
		return MELEE;
	}

	public List<AttackInterfaceButton> getButtonSet(String id) {
		return attackButtons.get(id);
	}

	public Map<Integer, MagicSpellScript> getMagicSpells() {
		return magicSpells;
	}

}