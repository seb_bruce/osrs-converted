package com.runecore.env.model.combat;

public enum CombatType {
    
    MELEE,
    RANGED,
    MAGIC_SPELL,
    MAGIC,
    SPECIAL_ATTACK;

}
