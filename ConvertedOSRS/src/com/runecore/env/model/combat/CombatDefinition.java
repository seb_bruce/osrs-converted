package com.runecore.env.model.combat;

import com.runecore.codec.event.SendSettingEvent;
import com.runecore.codec.event.SendSettingEvent.SettingType;
import com.runecore.env.Context;
import com.runecore.env.model.Entity;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;

public class CombatDefinition {

	public CombatDefinition(Entity entity) {
		this.entity = entity;
	}

	public void refresh() {
		if(getEntity().isPlayer()) {
			Player player = getEntity().player();
			resetBonuses();
			for(Item i : player.get("e").getItems()) {
				if(i == null) {
					continue;
				}
				if(i.getDefinition() == null) 
					continue;
				int[] itemBonus = i.getDefinition().getBonus();
				for(int index = 0; index < itemBonus.length; index++) {
					bonuses[index] += itemBonus[index];
				}
			}
		}
	}

	public void refreshSpecial() {
		Context.get().getActionSender().sendSetting(new SendSettingEvent(getEntity().player(), 300, specialAmount * 10, SettingType.NORMAL));
		Context.get().getActionSender().sendSetting(new SendSettingEvent(getEntity().player(), 301, isSpecialActivated() ? 1 : 0, SettingType.NORMAL));
	}

	public void toggleSpecial() {
		setSpecialActivated(!isSpecialActivated());
		refreshSpecial();
	}

	public void toggleRetaliate() {
		//TODO auto retaliate
	}

	public void resetBonuses() {
		for(int i = 0; i < bonuses.length; i++) {
			bonuses[i] = 0;
		}
	}
	
	public int getAttackBonus(int index) {
		return bonuses[index];
	}
	
	public int getDefenceBonus(int index) {
		return bonuses[index + 4];
	}
	
	public int getStrengthBonus() {
		return bonuses[11];
	}

	public void drainSpecial(int amount) {
		if(amount > getSpecialAmount()) {
			amount = getSpecialAmount();
		}
		setSpecialAmount(getSpecialAmount() - amount);
		refreshSpecial();
	}

	public Entity getEntity() {
		return entity;
	}

	public int[] getBonuses() {
		return bonuses;
	}

	public int getSpecialAmount() {
		return specialAmount;
	}

	public void setSpecialAmount(int specialAmount) {
		this.specialAmount = specialAmount;
	}

	public boolean isSpecialActivated() {
		return specialActivated;
	}

	public void setSpecialActivated(boolean specialActivated) {
		this.specialActivated = specialActivated;
		this.refreshSpecial();
	}


	private int specialAmount;
	private boolean specialActivated;
	private final Entity entity;
	private final int[] bonuses = new int[15];
}