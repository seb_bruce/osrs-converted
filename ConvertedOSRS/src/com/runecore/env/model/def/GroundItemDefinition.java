package com.runecore.env.model.def;

public class GroundItemDefinition implements EntityDefinition {
	
	private final ItemDefinition definition;
	
	public GroundItemDefinition(ItemDefinition definition) {
		this.definition = definition;
	}

	@Override
	public String getName() {
		return definition.getName();
	}

	@Override
	public int getCombatLevel() {
		return -1;
	}

	@Override
	public int getSize() {
		return -1;
	}

	@Override
	public int getDeathAnimation() {
		return -1;
	}

}
