package com.runecore.env.model.def;

import java.util.HashMap;
import java.util.Map;

public class NPCDefinition implements EntityDefinition {
	
	private static Map<Integer, NPCDefinition> definitions = new HashMap<Integer, NPCDefinition>();
	
	private final String name;
	private final int id, combatLevel, size, deathAnimation, hitpoints;
	
	public static NPCDefinition forId(int id) {
		NPCDefinition def = definitions.get(id);
		if(def == null) {
			def = new NPCDefinition("Default", id, -1, 0, -1, 10);
			definitions.put(id, def);
		}
		return def;
	}
	
	public NPCDefinition(String name, int id, int combatLevel, int size, int deathAnimation, int hp) {
		this.name = name;
		this.id = id;
		this.combatLevel = combatLevel;
		this.size = size;
		this.deathAnimation = deathAnimation;
		this.hitpoints = hp;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getCombatLevel() {
		return combatLevel;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public int getDeathAnimation() {
		return deathAnimation;
	}
	
	public int getId() {
		return id;
	}

	public int getHitpoints() {
		return hitpoints;
	}

}