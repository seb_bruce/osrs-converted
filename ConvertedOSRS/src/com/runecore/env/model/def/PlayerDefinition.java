package com.runecore.env.model.def;

import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;
import com.runecore.util.NameUtils;

/**
 * PlayerDefinition.java
 * @author Harry Andreas<harry@runecore.org>
 *  Feb 10, 2013
 */
public class PlayerDefinition implements EntityDefinition {

	private final String name;
	private final String protocolName;
	private final int forumId;
	private Player player;
	
	public PlayerDefinition(String name, int forumId) {
		this.forumId = forumId;
		this.name = name;
		this.protocolName = NameUtils.formatNameForProtocol(name);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getCombatLevel() {
		return player.getSkills().getCombatLevel(false);
	}

	@Override
	public int getSize() {
		return 1;
	}

	public String getProtocolName() {
		return protocolName;
	}

	@Override
	public int getDeathAnimation() {
		return 836;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public int getForumId() {
		return forumId;
	}
	
	public int getStandAnimation() {
		Item weapon = getPlayer().get("equipment").get(3);
		if(weapon != null) {
			return weapon.getDefinition().getStandAnim();
		}
		return 808;
	}
	
	public int getRunAnimation() {
		Item weapon = getPlayer().get("equipment").get(3);
		if(weapon != null) {
			return weapon.getDefinition().getRunAnim();
		}
		return 824;
	}
	
	public int getWalkAnimation() {
		Item weapon = getPlayer().get("equipment").get(3);
		if(weapon != null) {
			return weapon.getDefinition().getWalkAnim();
		}
		return 819;
	}
	
	public int getTurnAnimation() {
		Item weapon = getPlayer().get("equipment").get(3);
		if(weapon != null) {
			return weapon.getDefinition().getTurnAnim();
		}
		return 819;
	}
	
	public int getTurn90Animation() {
		Item weapon = getPlayer().get("equipment").get(3);
		if(weapon != null) {
			return weapon.getDefinition().getTurn90Anim();
		}
		return 82;
	}
	
	public int getTurn180Animation() {
		Item weapon = getPlayer().get("equipment").get(3);
		if(weapon != null) {
			return weapon.getDefinition().getTurn180Anim();
		}
		return 820;
	}

}