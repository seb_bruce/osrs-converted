package com.runecore.env.model.def;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.runecore.env.model.combat.CombatType;
import com.runecore.env.model.container.Equipment;
import com.runecore.env.model.item.LevelRequirement;
import com.runecore.env.model.player.Looks;

/**
 * ItemDefinition.java
 * 
 * @author Harry Andreas<harry@runecore.org> Feb 18, 2013
 */
public final class ItemDefinition {

	private static final String[] DEGRADE_PHRASES = new String[] { 
		"dharok's", "ahrim's", "guthan's", "karil's", "verac's", "torag's", 
		"recoil"
	};
	public static int MAX_SIZE = 23000;
	private static Map<Integer, ItemDefinition> definitions = new HashMap<Integer, ItemDefinition>();
	private boolean fullHat;
	private boolean fullMask;
	private boolean fullBody;
	private boolean tradable;
	private boolean twoHanded;
	private boolean dropable;
	private boolean degrades;

	private int lowAlch;
	private int highAlch;
	private CombatType combatType;
	private String combatInterfaceKey;

	public static void init() throws Exception {
		
		ItemDefinition[] jsonDefs = new Gson().fromJson(new String(Files.toByteArray(new File("./data/world/items.json"))), ItemDefinition[].class);
		for(ItemDefinition def : jsonDefs) {
			definitions.put(def.id, def);
		}
		
		for (ItemDefinition def : ItemDefinition.definitions.values()) {
			if (def != null) {
				def.setEquipmentSlot(ItemDefinition.getItemType(def.getId()));
				def.setFullBody(Looks.isFullBody(def));
				def.setFullHat(Looks.isFullHat(def));
				def.setFullMask(Looks.isFullMask(def));
				def.setTwoHanded(Equipment.isTwoHanded(def));
				if (!def.isNoted()) {
					for (String s : DEGRADE_PHRASES) {
						if (def.getName().toLowerCase().contains(s)) {
							def.setDegrades(true);
							continue;
						}
					}
				}
			}
		}
	}
	
	

	public static ItemDefinition forId(int id) {
		if (id == -1)
			return null;
		ItemDefinition def = definitions.get(id);
		if (def == null) {
			def = new ItemDefinition();
			if(id == 12437) {
				def.setEquipmentSlot(Equipment.SLOT_CAPE);
			}
			definitions.put(id, def);
		}
		return def;
	}
	
	public static int size() {
		return definitions.size();
	}
	
	public static Iterator<ItemDefinition> all() {
		return definitions.values().iterator();
	}

	public static ItemDefinition forName(String name) {
		for (ItemDefinition definition : definitions.values()) {
			if (definition.name.equalsIgnoreCase(name)) {
				return definition;
			}
		}
		return null;
	}

	public static void clear() {
		definitions.clear();
	}
	
	private static final String[] CAPES = {"ava's", "cape","Cape","cloak", "3rd age cloak"};
	private static final String[] HATS = {"Bunny","sallet","cowl", "helm","hood","tiara","coif","Coif","hat","partyhat","Hat","full helm (t)","full helm (g)","hat (t)","hat (g)","cav","boater","helmet","mask","Helm of neitiznot"};
	private static final String[] BOOTS = {"boots","Boots"};
	private static final String[] GLOVES = {"gloves","gauntlets","Gloves","vambraces","vamb","bracers"};
	private static final String[] SHIELDS = {"Toktz-ket-xil","kiteshield","sq shield","Toktz-ket","books","book","kiteshield (t)","kiteshield (g)","kiteshield(h)","defender","shield"};
	private static final String[] AMULETS = {"scarf","stole", "amulet","necklace","Amulet of"};
	private static final String[] ARROWS = {"arrow","arrows","bolts (e)","bolt (e)","arrow(p)","arrow(+)","arrow(s)","bolt","bolts","Bolt rack","Opal bolts","Dragon bolts"};
	private static final String[] RINGS = {"ring", "Ring"};
	private static final String[] BODY = {"hauberk","platebody","chainbody","robetop","leathertop","platemail","top","brassard","Robe top","body","platebody (t)","platebody (g)","body(g)","body_(g)","chestplate","torso","shirt","Runecrafter robe",};
	private static final String[] LEGS = {"cuisse","knight robe", "platelegs","plateskirt","skirt","bottoms","chaps","platelegs (t)","platelegs (g)","bottom","skirt","skirt (g)","skirt (t)","chaps (g)","chaps (t)","tassets","legs"};
	private static final String[] WEAPONS = {"staff of light", "sceptre", "Tzhaar-Ket-Om","Excalibur","dark bow", "Pharaoh's","wand", "adze", "Karil's x-bow","warhammer","claws","scimitar","longsword","sword","longbow","shortbow","dagger","mace","halberd","spear",
	"whip","axe","flail","crossbow","Torags hammer's","dagger(p)", "dagger(p+)","dagger(p++)","dagger(+)","dagger(s)","spear(p)","spear(+)",
	"spear(s)","spear(kp)","maul","dart","dart(p)","javelin","javelin(p)","knife","knife(p)","Longbow","Shortbow",
	"Crossbow","Toktz-xil","Toktz-mej","Tzhaar-ket","staff","Staff","godsword","c'bow","Crystal bow","Dark bow", "anchor", "abyssal", "trident"};
	
	public static int getItemType(int wearId) {
		ItemDefinition definition = ItemDefinition.forId(wearId);
		if(definition == null)
			return -1;
		String weapon = ItemDefinition.forId(wearId).getName().toLowerCase();
		for(int i = 0; i < CAPES.length; i++) {
			if(weapon.contains(CAPES[i]))
				return Equipment.SLOT_CAPE;
		}
		for(int i = 0; i < HATS.length; i++) {
			if(weapon.contains(HATS[i]))
				return 0;
		}
		for(int i = 0; i < BOOTS.length; i++) {
			if(weapon.endsWith(BOOTS[i]) || weapon.startsWith(BOOTS[i]))
				return 10;
		}
		for(int i = 0; i < GLOVES.length; i++) {
			if(weapon.endsWith(GLOVES[i]) || weapon.startsWith(GLOVES[i]))
				return 9;
		}
		for(int i = 0; i < SHIELDS.length; i++) {
			if(weapon.contains(SHIELDS[i]))
				return 5;
		}
		for(int i = 0; i < AMULETS.length; i++) {
			if(weapon.endsWith(AMULETS[i]) || weapon.startsWith(AMULETS[i]))
				return 2;
		}
		for(int i = 0; i < ARROWS.length; i++) {
			if(weapon.endsWith(ARROWS[i]) || weapon.startsWith(ARROWS[i]))
				return 13;
		}
		for(int i = 0; i < RINGS.length; i++) {
			if(weapon.endsWith(RINGS[i]) || weapon.startsWith(RINGS[i]))
				return 12;
		}
		for(int i = 0; i < BODY.length; i++) {
			if(weapon.contains(BODY[i]) || wearId == 544 || wearId == 6107 || wearId == 1037)
				return 4;
		}
		for(int i = 0; i < LEGS.length; i++) {
			if(weapon.contains(LEGS[i]) || wearId == 542 || wearId == 6108 || wearId == 1033)
				return 7;
		}
		for(int i = 0; i < WEAPONS.length; i++) {
			if(weapon.endsWith(WEAPONS[i]) || weapon.startsWith(WEAPONS[i]))
				return 3;
		}
		return -1;
	}

	public String name;
	private String examine;
	private int id;
	public int equipId;
	private int[] bonuses = new int[14];
	private boolean stackable;
	private boolean noted;
	private double weight;
	private boolean members;
	private int attackSpeed, equipmentSlot;
	private int standAnim, runAnim, walkAnim, turnAnim, turn90Anim, turn180Anim;
	private Map<Integer, Integer> skillRequirements;
	private List<LevelRequirement> levelReqs = new ArrayList<LevelRequirement>();
	private boolean extraDefinitions;
	private int[] absorptionBonus = new int[3];

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public int getEquipId() {
		return equipId;
	}

	public int getRenderId() {
		return 0;
	}

	public int[] getBonus() {
		return bonuses;
	}

	public boolean isStackable() {
		return stackable || noted;
	}

	public boolean isNoted() {
		return noted;
	}

	public String getExamine() {
		return examine;
	}

	public double getWeight() {
		return weight;
	}

	public int getAttackSpeed() {
		return attackSpeed;
	}

	public int getEquipmentSlot() {
		return equipmentSlot;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setExamine(String examine) {
		this.examine = examine;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setEquipId(int equipId) {
		this.equipId = equipId;
	}

	public void setBonus(int[] bonus) {
		this.bonuses = bonus;
	}

	public void setBonusAtIndex(int index, int value) {
		this.bonuses[index] = value;
	}

	public void setStackable(boolean stackable) {
		this.stackable = stackable;
	}

	public void setNoted(boolean noted) {
		this.noted = noted;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public void setAttackSpeed(int attackSpeed) {
		this.attackSpeed = attackSpeed;
	}

	public void setEquipmentSlot(int equipmentSlot) {
		if(this.id == 12437) {
			this.equipmentSlot = Equipment.SLOT_CAPE;
			return;
		}
		this.equipmentSlot = equipmentSlot;
	}

	public void setMembers(boolean members) {
		this.members = members;
	}

	public boolean isMembers() {
		return members;
	}

	public void setExtraDefinitions(boolean extraDefinition) {
		this.extraDefinitions = extraDefinition;
	}

	public boolean isExtraDefinitions() {
		return extraDefinitions;
	}

	public void setAbsorptionBonus(int[] absorptionBonus) {
		this.absorptionBonus = absorptionBonus;
	}

	public int[] getAbsorptionBonus() {
		return absorptionBonus;
	}

	/**
	 * @return the fullHat
	 */
	public boolean isFullHat() {
		return fullHat;
	}

	/**
	 * @param fullHat
	 *            the fullHat to set
	 */
	public void setFullHat(boolean fullHat) {
		this.fullHat = fullHat;
	}

	/**
	 * @return the fullMask
	 */
	public boolean isFullMask() {
		return fullMask;
	}

	/**
	 * @param fullMask
	 *            the fullMask to set
	 */
	public void setFullMask(boolean fullMask) {
		this.fullMask = fullMask;
	}

	public boolean isFullBody() {
		return fullBody;
	}

	public void setFullBody(boolean fullBody) {
		this.fullBody = fullBody;
	}

	public boolean isTradable() {
		return tradable;
	}

	public void setTradable(boolean tradable) {
		this.tradable = tradable;
	}

	public boolean isTwoHanded() {
		return twoHanded;
	}

	public void setTwoHanded(boolean twoHanded) {
		this.twoHanded = twoHanded;
	}

	public boolean isDropable() {
		return dropable;
	}

	public void setDropable(boolean dropable) {
		this.dropable = dropable;
	}

	public int getLowAlch() {
		return lowAlch;
	}

	public void setLowAlch(int lowAlch) {
		this.lowAlch = lowAlch;
	}

	public int getHighAlch() {
		return highAlch;
	}

	public void setHighAlch(int highAlch) {
		this.highAlch = highAlch;
	}

	public int getStorePrice() {
		return 21;
	}
	
	public int getRenderAnimation() {
		if(getName().contains("scimitar")) {
			return 1426;
		}
		return 21;
	}
	
	public List<LevelRequirement> getLevelReqs() {
		return levelReqs;
	}


	public void setLevelReqs(List<LevelRequirement> levelReqs) {
		this.levelReqs = levelReqs;
	}

	public boolean isDegrades() {
		return degrades;
	}

	public void setDegrades(boolean degrades) {
		this.degrades = degrades;
	}

	public int getStandAnim() {
		return standAnim;
	}

	public int getRunAnim() {
		return runAnim;
	}

	public int getWalkAnim() {
		return walkAnim;
	}

	public int getTurnAnim() {
		return turnAnim;
	}

	public int getTurn90Anim() {
		return turn90Anim;
	}

	public int getTurn180Anim() {
		return turn180Anim;
	}



	public CombatType getCombatType() {
		return combatType;
	}



	public void setCombatType(CombatType combatType) {
		this.combatType = combatType;
	}



	public String getCombatInterfaceKey() {
		return combatInterfaceKey;
	}



	public void setCombatInterfaceKey(String combatInterfaceKey) {
		this.combatInterfaceKey = combatInterfaceKey;
	}

}