package com.runecore.env.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import com.runecore.env.core.Tick;
import com.runecore.env.model.action.ActionQueue;
import com.runecore.env.model.combat.Combat;
import com.runecore.env.model.combat.CombatDefinition;
import com.runecore.env.model.combat.Prayer;
import com.runecore.env.model.def.EntityDefinition;
import com.runecore.env.model.flag.Animation;
import com.runecore.env.model.flag.Damage;
import com.runecore.env.model.flag.FlagManager;
import com.runecore.env.model.flag.Graphic;
import com.runecore.env.model.flag.UpdateFlag;
import com.runecore.env.model.npc.NPC;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;
import com.runecore.env.world.World;

/**
 * Entity.java
 * @author Harry Andreas<harry@runecore.org> 
 * Feb 10, 2013
 */
public abstract class Entity {
	// 2662 3305
	
	private Location location = Location.locate(3093, 3493, 0);
	private final ActionQueue actionQueue = new ActionQueue();
	private final Combat combat = new Combat(this);
	private final Prayer prayer = new Prayer(this);
	private final CombatDefinition combatDefinition = new CombatDefinition(this);
	private final FlagManager flagManager = new FlagManager(this);
	private final Walking walking = new Walking(this);
	private Map<String, Tick> ticks = new HashMap<String, Tick>();
	private final Map<String, Object> attributes = new HashMap<String, Object>();
	private int index;
	private boolean locked = Boolean.FALSE;
	private final Queue<Damage> damageQueue = new LinkedList<Damage>();

	public abstract EntityDefinition getDefinition();
	public abstract void tick();
	public abstract void dropLoot();
	public abstract void restore();
	public abstract boolean isDead();
	public abstract void heal(int heal);
	public abstract void hit(int damage);
	
	public void lock(int ticks) {
		locked = Boolean.TRUE;
		World.get().register(new Tick(ticks) {
			@Override
			public void execute() {
				unlock();
				stop();
			}
		});
	}
	
	public void lock() {
		locked = Boolean.TRUE;
	}
	
	public void unlock() {
		locked = Boolean.FALSE;
	}
	
	public boolean isLocked() {
		return locked;
	}
	
	public void distanceEvent(Tick tick) {
		Tick distanceEvent = retrieve("distance_event");
		if(distanceEvent != null) {
			distanceEvent.stop();
		}
		if(tick != null)
			register("distance_event", tick, true);
	}

	public void addPoint(int x, int y) {
		int firstX = x - (getLocation().getRegionX() - 6) * 8;
		int firstY = y - (getLocation().getRegionY() - 6) * 8;
		getWalking().addStep(firstX, firstY);
	}

	public void addAttribute(String var, Object value) {
		attributes.put(var, value);
	}

	@SuppressWarnings("unchecked")
	public <T> T getAttribute(String string, T fail) {
		T object = (T) attributes.get(string);
		if (object != null) {
			return object;
		}
		return fail;
	}

	public void removeAttribute(String string) {
		attributes.remove(string);
	}

	public void register(String identifier, Tick tick, boolean replace) {
		if (ticks.containsKey(identifier) && !replace) {
			return;
		}
		ticks.put(identifier, tick);
	}

	public void register(String identifier, Tick tick) {
		register(identifier, tick, false);
	}

	public void remove(String identifier) {
		Tick tick = ticks.get(identifier);
		if (tick != null) {
			tick.stop();
			ticks.remove(identifier);
		}
	}

	public Tick retrieve(String string) {
		return ticks.get(string);
	}

	public boolean has(String string) {
		return ticks.containsKey(string);
	}
	
	public void setCooldown(Cooldown c, int ticks, boolean ignore) {
		if (!isReady(c) && !ignore)
			throw new RuntimeException("Cooldown " + c + " not ready!");
		register(c.name(), new Tick(ticks) {
			@Override
			public void execute() {
				stop();
			}
		}, true);
	}

	public void setCooldown(Cooldown c, int ticks) {
		this.setCooldown(c, ticks, false);
	}

	public void addToCooldown(Cooldown c, int ticks) {
		if (isReady(c)) {
			setCooldown(c, ticks);
		} else {
			Tick tick = retrieve(c.name());
			tick.setTime(tick.getTime() + ticks);
		}
	}

	public boolean isReady(Cooldown c) {
		return !has(c.name());
	}

	public void processTicks() {
		if (ticks.isEmpty()) {
			return;
		}
		Map<String, Tick> ticks = new HashMap<String, Tick>(this.ticks);
		Iterator<Map.Entry<String, Tick>> it = ticks.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Tick> entry = it.next();
			if (!entry.getValue().run()) {
				this.ticks.remove(entry.getKey());
			}
		}
	}
	

	public void animate(Animation a) {
		getFlagManager().flag(UpdateFlag.ANIMATION);
		getFlagManager().setAnimation(a);
	}

	public void graphics(Graphic g) {
		getFlagManager().flag(UpdateFlag.GRAPHICS);
		getFlagManager().setGraphic(g);
	}
	
	public void chat(String msg) {
		getFlagManager().flag(UpdateFlag.FORCE_CHAT);
		getFlagManager().setForceChat(msg);
	}
	
	public void teleport(Location location) {
		getFlagManager().setTeleportLocation(location);
	}
	
	public boolean isAnimating() {
		return getFlagManager().getAnimation() != null;
	}
	
	public void processDamageQueue() {
		if(isLocked())
			return;
		while(true) {
			if(getDamageQueue().isEmpty())
				return;
			if(getFlagManager().getFirstDamage() != null || getFlagManager().getSecondDamage() != null) {
				return;
			}
			if(getFlagManager().getFirstDamage() == null) {
				damage(getDamageQueue().poll());
			}
			if(getFlagManager().getSecondDamage() == null) {
				damage(getDamageQueue().poll());
			}
		}
	}
	
	public boolean damage(Damage damage) {
		if(damage == null)
			return false;
		if(isLocked()) {
			getDamageQueue().add(damage);
			return false;
		}
		if(getFlagManager().getFirstDamage() == null) {
			getFlagManager().setFirstDamage(damage);
			getFlagManager().flag(UpdateFlag.DAMAGE_ONE);
			hit(damage.getHit());
			return true;
		}
		if(getFlagManager().getSecondDamage() == null) {
			getFlagManager().setSecondDamage(damage);
			getFlagManager().flag(UpdateFlag.DAMAGE_TWO);
			hit(damage.getHit());
			return true;
		}
		getDamageQueue().add(damage);
		return false;
	}
	
	public void face(Location location) {
		getFlagManager().flag(UpdateFlag.LOCATION_FOCUS);
		getFlagManager().setFaceLocation(location);
	}
	
	public void faceEntity(Entity entity) {
		if(entity == null && getFlagManager().getFocusOn() == null)
			return;
		getFlagManager().flag(UpdateFlag.ENTITY_FOCUS);
		getFlagManager().setFocusOn(entity);
	}
	
	public void death() {
		if(!isDead())
			return;
		int delay = 2 + (isAnimating() ? 1 : 0);
		lock();
		Tick animationTick = new Tick(delay) {
			@Override
			public void execute() {
				animate(Animation.create(getDefinition().getDeathAnimation()));
				stop();
			}
		};
		Tick respawnTick = new Tick(delay + 5) {
			@Override
			public void execute() {
				getDamageQueue().clear();
				animate(Animation.create(-1));
				restore();
				dropLoot();
				if(isNPC())
					npc().getFacade().setHidden(true);
				else
					teleport(Location.locate(3093, 3493, 0));
				//getWalking().resetFreeze();
				stop();
			}
		};
		Tick unlockTick = new Tick(delay + 7) {
			@Override
			public void execute() {
				unlock();
				stop();
			}
		};
		if(isNPC()) {
			Tick npcRespawnTick = new Tick(delay + 50) {
				@Override
				public void execute() {
					teleport(npc().getFacade().getSpawnLocation());
					npc().getFacade().setHidden(false);
					stop();
				}
			};
			World.get().register(npcRespawnTick);
		}
		World.get().bulkRegister(animationTick, respawnTick, unlockTick);
	}

	public Queue<Damage> getDamageQueue() {
		return damageQueue;
	}
	
	public int getTurnToIndex() {
		if (isPlayer()) {
			return getIndex() + 32768;
		} else {
			return getIndex();
		}
	}
	
	
	
	public boolean isNPC() {
		return (this instanceof NPC);
	}
	
	public NPC npc() {
		return (NPC)this;
	}

	public boolean isPlayer() {
		return (this instanceof Player);
	}

	public Player player() {
		return (Player) this;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location loc) {
		this.location = loc;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public FlagManager getFlagManager() {
		return flagManager;
	}

	public Walking getWalking() {
		return walking;
	}

	public CombatDefinition getCombatDefinition() {
		return combatDefinition;
	}

	public ActionQueue getActionQueue() {
		return actionQueue;
	}

	public Combat getCombat() {
		return combat;
	}
	public Prayer getPrayer() {
		return prayer;
	}

}
