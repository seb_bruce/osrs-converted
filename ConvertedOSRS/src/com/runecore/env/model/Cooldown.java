package com.runecore.env.model;

public enum Cooldown {
    
    COMBAT,
    FOOD,
    DYING,
    LOGOUT,
    POTION,
    VENGEANCE,
    ATTACK_ANIMATION,
    YELL,
    DICE;

}
