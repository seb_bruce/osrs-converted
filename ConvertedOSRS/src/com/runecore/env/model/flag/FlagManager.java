package com.runecore.env.model.flag;

import java.util.BitSet;

import com.runecore.env.model.Entity;
import com.runecore.env.world.Location;
import com.runecore.network.io.MessageBuilder;
/**
 * FlagManager.java
 * 
 * @author Harry Andreas<harry@runecore.org> Feb 12, 2013
 */
public class FlagManager {

	/**
	 * Construct the FlagManager
	 * 
	 * @param p
	 */
	public FlagManager(Entity p) {
		this.entity = p;
	}

	/**
	 * A BitSet of Flagged UpdateFlags
	 */
	private final Entity entity;
	private final BitSet bitSet = new BitSet();
	private Animation animation;
	private Graphic graphic;
	private Damage firstDamage;
	private Damage secondDamage;
	private boolean mapRegionChanged = true;
	private boolean didTele = false;
	private Location teleportLocation;
	private Location lastKnownRegion;
	private ChatMessage chatMessage;
	private Entity focusOn;
	private Location faceLocation; 
	private MessageBuilder cachedApperance;
	private String forceChat;
	
	
	public void pulse() {
		if(flagged(UpdateFlag.APPERANCE)) {
			setCachedApperance(entity.player().getLooks().generate(entity.player()));
		}
	}

	/**
	 * Is the UpdateFlag flagged
	 * 
	 * @param flag
	 *            The UpdateFlag to check
	 * @return
	 */
	public boolean flagged(UpdateFlag flag) {
		return bitSet.get(flag.ordinal());
	}

	/**
	 * Is an update needed?
	 * 
	 * @return If an update is needed
	 */
	public boolean updateNeeded() {
		return !bitSet.isEmpty() || teleportUpdate()
				|| entity.getWalking().getWalkDir() != -1
				|| entity.getWalking().getRunDir() != -1;
	}

	/**
	 * Flag an update flag
	 * 
	 * @param flag
	 *            The UpdateFlag to flag
	 */
	public void flag(UpdateFlag flag) {
		bitSet.set(flag.ordinal(), Boolean.TRUE);
	}

	/**
	 * Sets the cache
	 */
	public void reset() {
		setTeleportLocation(null);
		bitSet.clear();
		setAnimation(null);
		setGraphic(null);
		setMapRegionChanged(false);
		setDidTele(false);
		setChatMessage(null);
		setFirstDamage(null);
		setSecondDamage(null);
		setFaceLocation(null);
		setForceChat(null);
	}
	
	public void chatMessage(ChatMessage msg) {
		flag(UpdateFlag.CHAT_MESSAGE);
		setChatMessage(msg);
	}

	public Location teleportLocation() {
		return getTeleportLocation();
	}

	public boolean teleportUpdate() {
		return getTeleportLocation() != null;
	}

	public boolean isMapRegionChanged() {
		return mapRegionChanged;
	}

	public void setMapRegionChanged(boolean mapRegionChanged) {
		this.mapRegionChanged = mapRegionChanged;
	}

	public Location lastKnownRegion() {
		return lastKnownRegion;
	}

	public void setLastKnownRegion(Location lastKnownRegion) {
		this.lastKnownRegion = lastKnownRegion;
	}

	public Entity entity() {
		return entity;
	}

	public Animation getAnimation() {
		return animation;
	}

	public void setAnimation(Animation animation) {
		this.animation = animation;
	}

	public Graphic getGraphic() {
		return graphic;
	}

	public void setGraphic(Graphic graphic) {
		this.graphic = graphic;
	}

	public Location getTeleportLocation() {
		return teleportLocation;
	}

	public void setTeleportLocation(Location teleportLocation) {
		this.teleportLocation = teleportLocation;
	}

	public boolean isDidTele() {
		return didTele;
	}

	public void setDidTele(boolean didTele) {
		this.didTele = didTele;
	}

	public ChatMessage getChatMessage() {
		return chatMessage;
	}

	public void setChatMessage(ChatMessage chatMessage) {
		this.chatMessage = chatMessage;
	}

	public Damage getFirstDamage() {
		return firstDamage;
	}

	public void setFirstDamage(Damage firstDamage) {
		this.firstDamage = firstDamage;
	}

	public Damage getSecondDamage() {
		return secondDamage;
	}

	public void setSecondDamage(Damage secondDamage) {
		this.secondDamage = secondDamage;
	}

	public Entity getFocusOn() {
		return focusOn;
	}

	public void setFocusOn(Entity focusOn) {
		this.focusOn = focusOn;
	}

	public Location getFaceLocation() {
		return faceLocation;
	}

	public void setFaceLocation(Location faceLocation) {
		this.faceLocation = faceLocation;
	}

	public MessageBuilder getCachedApperance() {
		return cachedApperance;
	}

	public void setCachedApperance(MessageBuilder cachedApperance) {
		this.cachedApperance = cachedApperance;
	}

	public String getForceChat() {
		return forceChat;
	}

	public void setForceChat(String forceChat) {
		this.forceChat = forceChat;
	}

}
