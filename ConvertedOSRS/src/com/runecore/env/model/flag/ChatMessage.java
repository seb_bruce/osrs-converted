package com.runecore.env.model.flag;

public class ChatMessage {
	
	/**
	 * Variables for the mask block
	 */
	private final int effect, colour;
	private final String chatMsg;
	private final byte[] packed;
	
	/**
	 * Construct the chat msg
	 * @param effect
	 * @param msg
	 */
	public ChatMessage(int effect, String msg, byte[] packed) {
		this.effect = effect;
		this.chatMsg = msg;
		this.packed = packed;
		this.colour = -1;
	}
	
	public ChatMessage(int effect, int colour, String msg, byte[] packed) {
		this.effect = effect;
		this.chatMsg = msg;
		this.packed = packed;
		this.colour = colour;
	}
	

	/**
	 * @return the effect
	 */
	public int getEffect() {
		return effect;
	}

	/**
	 * @return the chatMsg
	 */
	public String getChatMsg() {
		return chatMsg;
	}

	public byte[] getPacked() {
		return packed;
	}

	public int getColour() {
		return colour;
	}

}