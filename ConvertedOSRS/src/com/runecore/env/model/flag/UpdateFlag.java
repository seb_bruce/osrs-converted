package com.runecore.env.model.flag;

/**
 * UpdateFlag.java
 * @author Harry Andreas<harry@runecore.org>
 * Feb 12, 2013
 */
public enum UpdateFlag {
    
    APPERANCE,
    ANIMATION,
    GRAPHICS,
    FORCE_CHAT,
    CHAT_MESSAGE,
    DAMAGE_ONE,
    DAMAGE_TWO,
    ENTITY_FOCUS,
    LOCATION_FOCUS;

}
