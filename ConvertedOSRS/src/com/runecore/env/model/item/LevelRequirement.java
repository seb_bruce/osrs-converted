package com.runecore.env.model.item;

import com.runecore.env.model.player.Player;

public class LevelRequirement {
	
	private final int levelIndex, level;
	
	public LevelRequirement(int index, int level) {
		this.levelIndex = index;
		this.level = level;
	}
	
	public boolean meetsRequirement(Player player) {
		if(player.getSkills().getRealLevel(getLevelIndex()) < level)
			return false;
		return true;
	}

	public int getLevelIndex() {
		return levelIndex;
	}

	public int getLevel() {
		return level;
	}

}
