package com.runecore.env.model.item;

import com.runecore.env.model.Entity;
import com.runecore.env.model.def.GroundItemDefinition;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;

public class GroundItem {
	
	private final Player owner;
	private final Item item;
	private final GroundItemDefinition definition;
	private Location location;
	
	public GroundItem(Player owner, Item item, Location location) {
		this.owner = owner;
		this.item = item;
		setLocation(location);
		definition = new GroundItemDefinition(getItem().getDefinition());
	}

	
	public GroundItemDefinition getDefinition() {
		return definition;
	}


	public Player getOwner() {
		return owner;
	}

	public Item getItem() {
		return item;
	}

	public Location getLocation() {
		return location;
	}


	public void setLocation(Location location) {
		this.location = location;
	}

}