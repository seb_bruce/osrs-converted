package com.runecore.env.model;

import java.util.Deque;
import java.util.LinkedList;

import com.runecore.codec.Events;
import com.runecore.codec.event.SendPlayerOptionEvent;
import com.runecore.codec.event.SendSettingEvent;
import com.runecore.codec.event.SendSettingEvent.SettingType;
import com.runecore.env.Context;
import com.runecore.env.content.Areas;
import com.runecore.env.model.map.Directions;
import com.runecore.env.model.map.Directions.NormalDirection;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;
import com.runecore.util.Misc;

/**
 * Walking.java
 * 
 * @author Harry Andreas<harry@runecore.org> Feb 12, 2013
 */
public class Walking {

	/**
	 * Represents a single point in the queue.
	 * @author Graham Edgecombe
	 *
	 */
	private static class Point {
		
		/**
		 * The x-coordinate.
		 */
		private final int x;
		
		/**
		 * The y-coordinate.
		 */
		private final int y;
		
		/**
		 * The direction to walk to this point.
		 */
		private final int dir;
		
		/**
		 * Creates a point.
		 * @param x X coord.
		 * @param y Y coord.
		 * @param dir Direction to walk to this point.
		 */
		public Point(int x, int y, int dir) {
			this.x = x;
			this.y = y;
			this.dir = dir;
		}
		
	}
	
	/**
	 * The maximum size of the queue. If there are more points than this size,
	 * they are discarded.
	 */
	public static final int MAXIMUM_SIZE = 50;
	
	/**
	 * 
	 */
	private int energy = 100;
	
	/**
	 * The entity.
	 */
	private Entity entity;
	
	/**
	 * The queue of waypoints.
	 */
	public Deque<Point> waypoints = new LinkedList<Point>();
	
	/**
	 * Run toggle (button in client).
	 */
	private boolean runToggled = false;
	
	/**
	 * Run for this queue (CTRL-CLICK) toggle.
	 */
	private boolean runQueue = false;
	
	/**
	 * Creates the <code>WalkingQueue</code> for the specified
	 * <code>Entity</code>.
	 * @param entity The entity whose walking queue this is. 
	 */
	public Walking(Entity entity) {
		this.entity = entity;
	}
	
	/**
	 * Sets the run toggled flag.
	 * @param runToggled The run toggled flag.
	 */
	public void setRunningToggled(boolean runToggled) {
		this.runToggled = runToggled;
		//if(entity.isPlayer()) {
		//	Events.sendConfig(entity.player(), 173, runToggled ? 1 : 0);
		//}
	}
	
	/**
	 * Sets the run queue flag.
	 * @param runQueue The run queue flag.
	 */
	public void setRunningQueue(boolean runQueue) {
		this.runQueue = runQueue;
	}
	
	/**
	 * Gets the run toggled flag.
	 * @return The run toggled flag.
	 */
	public boolean isRunningToggled() {
		return runToggled;
	}
	
	/**
	 * Gets the running queue flag.
	 * @return The running queue flag.
	 */
	public boolean isRunningQueue() {
		return runQueue;
	}
	
	/**
	 * Checks if any running flag is set.
	 * @return <code>true</code. if so, <code>false</code> if not.
	 */
	public boolean isRunning() {
		return runToggled || runQueue;
	}
	
	private int walkDir, runDir;
	
	/**
	 * Resets the walking queue so it contains no more steps.
	 */
	public void reset() {
		//entity.
		runQueue = false;
		waypoints.clear();
		waypoints.add(new Point(entity.getLocation().getX(), entity.getLocation().getY(), -1));
	}
	
	/**
	 * Checks if the queue is empty.
	 * @return <code>true</code> if so, <code>false</code> if not.
	 */
	public boolean isEmpty() {
		return waypoints.isEmpty();
	}
	
	/**
	 * Removes the first waypoint which is only used for calculating
	 * directions. This means walking begins at the correct time.
	 */
	public void finish() {
		waypoints.removeFirst();
	}
	
	/**
	 * Adds a single step to the walking queue, filling in the points to the
	 * previous point in the queue if necessary.
	 * @param x The local x coordinate.
	 * @param y The local y coordinate.
	 */
	public void addStep(int x, int y) {
		/*
		 * The RuneScape client will not send all the points in the queue.
		 * It just sends places where the direction changes.
		 * 
		 * For instance, walking from a route like this:
		 * 
		 * <code>
		 * *****
		 *     *
		 *     *
		 *     *****
		 * </code>
		 * 
		 * Only the places marked with X will be sent:
		 * 
		 * <code>
		 * X***X
		 *     *
		 *     *
		 *     X***X
		 * </code>
		 * 
		 * This code will 'fill in' these points and then add them to the
		 * queue.
		 */
		
		/*
		 * We need to know the previous point to fill in the path.
		 */
		if(waypoints.size() == 0) {
			/*
			 * There is no last point, reset the queue to add the player's
			 * current location.
			 */
			reset();
		}
		
		/*
		 * We retrieve the previous point here.
		 */
		Point last = waypoints.peekLast();
		
		/*
		 * We now work out the difference between the points.
		 */
		int diffX = x - last.x;
		int diffY = y - last.y;
		
		/*
		 * And calculate the number of steps there is between the points.
		 */
		int max = Math.max(Math.abs(diffX), Math.abs(diffY));
		for(int i = 0; i < max; i++) {
			/*
			 * Keep lowering the differences until they reach 0 - when our
			 * route will be complete.
			 */
			if(diffX < 0) {
				diffX++;
			} else if(diffX > 0) {
				diffX--;
			}
			if(diffY < 0) {
				diffY++;
			} else if(diffY > 0) {
				diffY--;
			}
			
			/*
			 * Add this next step to the queue.
			 */
			addStepInternal(x - diffX, y - diffY);
		}
	}

	/**
	 * Adds a single step to the queue internally without counting gaps.
	 * This method is unsafe if used incorrectly so it is private to protect
	 * the queue.
	 * @param x The x coordinate of the step.
	 * @param y The y coordinate of the step.
	 */
	private void addStepInternal(int x, int y) {
		/*
		 * Check if we are going to violate capacity restrictions.
		 */
		if(waypoints.size() >= MAXIMUM_SIZE) {
			/*
			 * If we are we'll just skip the point. The player won't get a
			 * complete route by large routes are not probable and are more
			 * likely sent by bots to crash servers.
			 */
			return;
		}
		
		/*
		 * We retrieve the previous point (this is to calculate the direction
		 * to move in).
		 */
		Point last = waypoints.peekLast();
		
		/*
		 * Now we work out the difference between these steps.
		 */
		int diffX = x - last.x;
		int diffY = y - last.y;
		
		/*
		 * And calculate the direction between them.
		 */
		int dir = Directions.getMoveDirection(diffX, diffY);
		
		/*
		 * Check if we actually move anywhere.
		 */
		if(dir > -1) {
			/*
			 * We now have the information to add a point to the queue! We create
			 * the actual point object and add it.
			 */
			waypoints.add(new Point(x, y, dir));
			
		}
	}
	
	/**
	 * Processes the next player's movement.
	 */
	public void processNextMovement() {
		/*
		 * Store the teleporting flag.
		 */
		boolean teleporting = entity.getFlagManager().getTeleportLocation() != null;
		/*
		 * The points which we are walking to.
		 */
		Point walkPoint = null, runPoint = null;
		
		/*
		 * Checks if the player is teleporting i.e. not walking.
		 */
		if(teleporting) {
			/*
			 * Reset the walking queue as it will no longer apply after the
			 * teleport.
			 */
			reset();
			/*
			 * Set the 'teleporting' flag which indicates the player is
			 * teleporting.
			 */
			entity.getFlagManager().setDidTele(true);
			/*
			 * Sets the player's new location to be their target.
			 */
			entity.setLocation(entity.getFlagManager().getTeleportLocation());
			
			//entity.updateCoverage(entity.getTeleportTarget());
			/*
			 * Resets the teleport target.
			 */
			entity.getFlagManager().setTeleportLocation(null);
		} else {
			/*
			 * If the player isn't teleporting, they are walking (or standing
			 * still). We get the next direction of movement here.
			 */
			walkPoint = getNextPoint();
			
			/*
			 * Technically we should check for running here.
			 */
			if(runToggled || runQueue) {
				//if (!entity.getAttributes().isSet("stopActions")) {
					runPoint = getNextPoint();
				//}
			}
			/*
			 * Run energy
			 */
			if (runPoint != null) {
				if (entity instanceof Player) {
					Player player = (Player) entity;
					if (getEnergy() > 0) {
						double change = /*player.getAttributes().isSet("staminapot") ? 0.3D :*/ 1;
						//if (!player.getAttributes().isSet("infrun"))
							//setEnergy(getEnergy() - change);//-1 every tick. reduce by 70% means 1.7 ticks
					} else {//so just .3 per tick
						//Events.sendConfig(player, 173, 0);
						setRunningQueue(false);
						setRunningToggled(false);
					}
				}
			} else {//walking/no movement restores energy
				if (entity instanceof Player) {
					Player p = (Player) entity;
					//double extra = p.getAttributes().isSet("staminapot") ? 0.175D : 0;
					//if (p.getVariables().getRunningEnergy() < 100) {
					//	p.getVariables().setRunningEnergy(p.getVariables().getRunningEnergy() + 0.25 + extra, true);
					//}
				}
			}
			/*
			 * Now set the sprites.
			 */
			int walkDir = walkPoint == null ? -1 : walkPoint.dir;
			int runDir = runPoint == null ? -1 : runPoint.dir;
			this.setWalkDir(walkDir);
			this.setRunDir(runDir);
		}
		/*
		 * Check for a map region change, and if the map region has changed, set
		 * the appropriate flag so the new map region packet is sent.
		 */
		int bottomLeftX = 0, bottomLeftY = 0;
		bottomLeftX = (entity.getFlagManager().lastKnownRegion().getRegionX() - 6) * 8;
		bottomLeftY = (entity.getFlagManager().lastKnownRegion().getRegionY() - 6) * 8;
		
		int diffX = entity.getLocation().getX() - bottomLeftX;
		int diffY = entity.getLocation().getY() - bottomLeftY;
		boolean changed = false;
		if (diffX < 16) {
			changed = true;
		} else if (diffX >= 88) {
			changed = true;
		}
		if (diffY < 16) {
			changed = true;
		} else if (diffY >= 88) {
			changed = true;
		}
		if (changed) {
			/*
			 * Set the map region changing flag so the new map region packet is
			 * sent upon the next update.
			 */
			entity.getFlagManager().setMapRegionChanged(true);
		}
		
	}
	
	public void toggleRun() {
		boolean set = !isRunningToggled();
		setRunningToggled(set);
	}
	
	/**
	 * Gets the next point of movement.
	 * @return The next point.
	 */
	public Point getNextPoint() {
		/*
		 * Take the next point from the queue.
		 */
		Point p = waypoints.poll();
			
		/*
		 * Checks if there are no more points.
		 */
		if(p == null || p.dir == -1) {
			/*
			 * Return <code>null</code> indicating no movement happened.
			 */
			return null;
		} else {
			/*
			 * Set the player's new location.
			 */
			int diffX = Directions.MOVEMENT_DIRECTION_DELTA_X[p.dir];
			int diffY = Directions.MOVEMENT_DIRECTION_DELTA_Y[p.dir];
			entity.setLocation(entity.getLocation().transform(diffX, diffY, 0));
			//entity.updateCoverage(entity.getLocation().transform(diffX, diffY, 0));
			return p;
		}
	}

	public boolean isMoving() {
		if (getWalkDir() != -1 || getRunDir() != -1) {
			return true;
		}
		return false;
	}

	public void forceWalk(int i, int j) {
		this.addStep(entity.getLocation().getX() + i, entity.getLocation().getY() + j);
		finish();
	}
	public void forceWalk(NormalDirection dir) {
		int i=0, j=0;
		switch(dir.intValue()){
		case 1://s
			j=-1;break;
		case 3://w
			i=-1;break;
		case 4://e
			i=1;break;
		case 6://n
			j=1;break;
		}
		this.addStep(entity.getLocation().getX() + i, entity.getLocation().getY() + j);
		finish();
	}

	public int getWalkDir() {
		return walkDir;
	}

	public void setWalkDir(int walkDir) {
		this.walkDir = walkDir;
	}

	public int getRunDir() {
		return runDir;
	}

	public void setRunDir(int runDir) {
		this.runDir = runDir;
	}

	public int getEnergy() {
		return energy;
	}

	public void setEnergy(int energy) {
		this.energy = energy;
	}

}
