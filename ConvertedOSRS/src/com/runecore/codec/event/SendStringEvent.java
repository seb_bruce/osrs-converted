package com.runecore.codec.event;

import com.runecore.env.model.player.Player;

public class SendStringEvent {
	
	private Player player;
	private final String message;
	private final int window, subWindow;
	
	public SendStringEvent(Player player, String msg, int window, int sub) {
		this.setPlayer(player);
		this.message = msg;
		this.window = window;
		this.subWindow = sub;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public String getMessage() {
		return message;
	}

	public int getWindow() {
		return window;
	}

	public int getSubWindow() {
		return subWindow;
	}

}
