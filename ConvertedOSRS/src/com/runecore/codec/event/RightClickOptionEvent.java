package com.runecore.codec.event;

import com.runecore.env.model.player.Player;

public class RightClickOptionEvent {
	
	private final Player player;
	private final int setting, inter, off, len;

	public RightClickOptionEvent(Player player, int setting, int inter, int off, int len) {
		this.player = player;
		this.setting = setting;
		this.inter = inter;
		this.off = off;
		this.len = len;
	}

	public Player getPlayer() {
		return player;
	}

	public int getSetting() {
		return setting;
	}

	public int getInter() {
		return inter;
	}

	public int getOff() {
		return off;
	}

	public int getLen() {
		return len;
	}

}
