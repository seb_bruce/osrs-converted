package com.runecore.codec.event;

import com.runecore.env.model.player.Player;

public class SendInterfaceConfigEvent {
	
	private final Player player;
	private final int window, component;
	private final boolean value;
	
	public SendInterfaceConfigEvent(Player player, int window, int component, boolean value) {
		this.player = player;
		this.window = window;
		this.component = component;
		this.value = value;
	}

	public Player getPlayer() {
		return player;
	}

	public int getWindow() {
		return window;
	}

	public int getComponent() {
		return component;
	}

	public boolean isValue() {
		return value;
	}

}