package com.runecore.codec.event;

import com.runecore.env.model.item.GroundItem;
import com.runecore.env.model.player.Player;

public class SendGroundItemEvent {
	
	private final Player player;
	private final GroundItem groundItem;
	
	public SendGroundItemEvent(Player p, GroundItem groundItem) {
		this.player = p;
		this.groundItem = groundItem;
	}

	public Player getPlayer() {
		return player;
	}

	public GroundItem getGroundItem() {
		return groundItem;
	}

}