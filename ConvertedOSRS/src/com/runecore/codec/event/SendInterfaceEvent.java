package com.runecore.codec.event;

import com.runecore.env.model.player.Player;

/**
 * SendInterfaceEvent.java
 * 
 * @author Harry Andreas<harry@runecore.org> Feb 10, 2013
 */
public class SendInterfaceEvent {

	private final Player player;
	private final int position, windowId, interfaceId, childId;
	private final boolean walkable;
	
	public SendInterfaceEvent(Player player, int interfaceId) {
		this.player = player;
		this.interfaceId = interfaceId;
		this.windowId = 0;
		this.childId = 0;
		this.position = 0;
		this.walkable = false;
	}

	public SendInterfaceEvent(Player player, int position, int windowId, int interfaceId, int childId) {
		this.player = player;
		this.position = position;
		this.windowId = windowId;
		this.interfaceId = interfaceId;
		this.childId = childId;
		this.walkable = false;
	}

	public SendInterfaceEvent(Player player, int position, int windowId,
			int interfaceId, boolean walk) {
		this.player = player;
		this.position = position;
		this.windowId = windowId;
		this.interfaceId = interfaceId;
		this.childId = -1;
		this.walkable = walk;
	}

	public Player getPlayer() {
		return player;
	}

	public int getPosition() {
		return position;
	}

	public int getWindowId() {
		return windowId;
	}

	public int getInterfaceId() {
		return interfaceId;
	}

	public int getChildId() {
		return childId;
	}

	public boolean isWalkable() {
		return walkable;
	}

}