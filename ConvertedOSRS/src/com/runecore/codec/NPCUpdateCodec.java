package com.runecore.codec;

import com.runecore.env.model.player.Player;

public interface NPCUpdateCodec {
	
	public void update(Player player);

}
