package com.runecore.codec;

import com.runecore.env.Context;
import com.runecore.env.content.Areas;
import com.runecore.env.model.container.Container;
import com.runecore.env.model.def.ItemDefinition;
import com.runecore.env.model.flag.UpdateFlag;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.World;
import com.runecore.util.Rights;

public class Commands {

	public static void handle(Player player, String[] split) {
		if(!Areas.inArea(player, Areas.SAFE_AREA)) {
			Events.sendMsg(player, "You cannot use commands in a non safe area!");
			return;
		}
		String cmd = split[0].toLowerCase();
		Container equipment = player.get("equipment");
		if (cmd.equalsIgnoreCase("players")) {
			int count = World.get().getPlayers().size();
			Events.sendMsg(player, "There " + (count == 1 ? "is" : "are") + " currently " + count + " player" + (count == 1 ? "" : "s") + " available.");
			return;
		}
		if(cmd.equalsIgnoreCase("item")) {
			int id = Integer.parseInt(split[1]);
			int am = split.length > 2 ? Integer.parseInt(split[2]) : 1;
			ItemDefinition definition = ItemDefinition.forId(id);
			Container inv = player.get("inventory");
			if(definition.isStackable()) {
				if(inv.hasRoomFor(new Item(id, am))) {
					inv.add(new Item(id, am));
				} else {
					Events.sendMsg(player, "You don't have enough inventory space to spawn that many!");
				}
			} else {
				if(am > 28)
					am = 28;
				for(int i = 0; i < am; i++) {
					if(inv.hasRoomFor(new Item(id, 1))) {
						inv.add(new Item(id, 1));
					} else {
						Events.sendMsg(player, "You don't have enough inventory space to spawn that many!");
						return;
					}
				}
			}
		}
		if(cmd.equalsIgnoreCase("pure")) {
			if(equipment.freeSlots() != equipment.capacity()) {
				Events.sendMsg(player, "Please take off your equipment before attempting to use this command!");
				return;
			}
			setStats(player, new int[][] { {0, 60}, { 1, 1 }, {2, 99}, {3, 99}, {4, 99}, {5, 52}, {6, 99} });
			player.getFlagManager().flag(UpdateFlag.APPERANCE);
		}
		if(cmd.equalsIgnoreCase("maxed")) {
			if(equipment.freeSlots() != equipment.capacity()) {
				Events.sendMsg(player, "Please take off your equipment before attempting to use this command!");
				return;
			}	
			setStats(player, new int[][] { {0, 99}, { 1, 99 }, {2, 99}, {3, 99}, {4, 99}, {5, 99}, {6, 99} });
			player.getFlagManager().flag(UpdateFlag.APPERANCE);
		}
		if(cmd.equalsIgnoreCase("bpure")) {
			if(equipment.freeSlots() != equipment.capacity()) {
				Events.sendMsg(player, "Please take off your equipment before attempting to use this command!");
				return;
			}
			setStats(player, new int[][] { {0, 70}, { 1, 70 }, {2, 99}, {3, 99}, {4, 99}, {5, 70}, {6, 99} });
			player.getFlagManager().flag(UpdateFlag.APPERANCE);
		}
		if(cmd.equalsIgnoreCase("zerker")) {
			if(equipment.freeSlots() != equipment.capacity()) {
				Events.sendMsg(player, "Please take off your equipment before attempting to use this command!");
				return;
			}
			setStats(player, new int[][] { {0, 70}, { 1, 45 }, {2, 99}, {3, 99}, {4, 99}, {5, 52}, {6, 99} });
			player.getFlagManager().flag(UpdateFlag.APPERANCE);
		}
		if(player.getRights() != Rights.NORMAL) {
			if(cmd.equalsIgnoreCase("saveall")) {
				long start = System.currentTimeMillis();
				for(Player p : World.get().getPlayers()) {
					Context.get().getPlayerAdapter().save(p);
				}
				long elapsed = System.currentTimeMillis() - start;
				Events.sendMsg(player, "Saved "+World.get().getPlayers().size()+" in "+elapsed+"ms.");
			}
		}
	}
	
	private static void setStats(Player p, int[][] stats) {
		for(int[] i : stats) {
			p.getSkills().hardSet(i[0], i[1]);
		}
	}

}
