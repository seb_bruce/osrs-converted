package com.runecore.codec;

import com.runecore.codec.event.SendInterfaceEvent;
import com.runecore.codec.event.SendMessageEvent;
import com.runecore.codec.event.SendSettingEvent;
import com.runecore.codec.event.SendSettingEvent.SettingType;
import com.runecore.env.Context;
import com.runecore.env.model.player.Player;

public class Events {

	public static void sendMsg(Player player, String msg) {
		Context.get().getActionSender().sendMessage(new SendMessageEvent(player, msg));
	}
	
	public static void closeInterface(Player player, int window, int pos) {
		player.getFacade().setOpenInterface(-1);
		Context.get().getActionSender().closeInterface(player, window, pos);
	}
	
	public static void openOverlay(Player player, int overlay) {
		player.getFacade().setOpenOverlay(overlay);
		Context.get().getActionSender().sendInterface(new SendInterfaceEvent(player, 1, 548, 5, overlay));
	}
	
	public static void openInvInterface(Player player, int inter) {
		Context.get().getActionSender().sendInterface(new SendInterfaceEvent(player, 0, 548, 80, inter));
	}
	
	public static void openInterface(Player player, int inter) {
		player.getFacade().setOpenInterface(inter);
		Context.get().getActionSender().sendInterface(new SendInterfaceEvent(player, 0, 548, 11, inter));
	}
	
	public static void sendConfig(Player player, int configId, int value) {
		Context.get().getActionSender().sendSetting(new SendSettingEvent(player, configId, value, SettingType.NORMAL));
	}

}