package com.runecore.codec.codec60.net;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import com.runecore.codec.generic.FileRequest;

public class JS5Decoder extends FrameDecoder {

	@Override
	protected Object decode(ChannelHandlerContext ctx, Channel chan, ChannelBuffer buff) throws Exception {
		if(buff.readableBytes() >= 4) {
			final int type = buff.readByte() & 0xff;
			final int cache = buff.readByte() & 0xff;
			final int id = buff.readShort() & 0xffff;
			if (type == 0 || type == 1) {
				return new FileRequest(chan, type, cache, id);
			}
		}
		return null;
	}

}
