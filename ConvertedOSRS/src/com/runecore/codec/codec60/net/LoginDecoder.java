package com.runecore.codec.codec60.net;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import com.runecore.env.login.LoginRequest;
import com.runecore.network.io.MessageBuilder;
import com.runecore.util.BufferUtils;

public class LoginDecoder extends FrameDecoder {

	@Override
	protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer) throws Exception {
		int loginType = buffer.readByte() & 0xFF;
		if(loginType != 16 && loginType != 18) { //16=normal, 18=reconnection?
			throw new RuntimeException("Incorrect login type "+loginType);
		}
		int loginSize = buffer.readShort() & 0xFF;
		int version = buffer.readInt();
		boolean lowMemoryVersion = (buffer.readByte() & 0xFF) == 1;
		boolean displayMode = (buffer.readByte() & 0xFF) == 1;
		
		//System.out.println("version "+version+" "+lowMemoryVersion+" "+displayMode);
		

		/* Isaac key */
		int[] isaacSeed = new int[4];
		for (int i=0; i<4; i++) {
			isaacSeed[i] = buffer.readInt();
		}

		//System.out.println("Seeds " + Arrays.toString(isaacSeed));

		buffer.readLong(); // Dunno TODO from the type block

		String password = BufferUtils.readRS2String(buffer);
		String user = BufferUtils.readRS2String(buffer).trim();

		//System.out.println("userpass " + user + ", " + password);

		buffer.readByte(); // Dunno TODO
		long uid = buffer.readLong();
		byte[] random_dat = new byte[24];
		buffer.readBytes(random_dat);
		
		buffer.skipBytes(buffer.readableBytes());
		
		//2 is ok response
		//3 is invalid passsword?
		//4 banned
		//5 already logged in
		//6 updated
		//7 is full
		//8 login server offline
		//9 too many connections from uid
		//10 bad session id
		//11 account locked
		//12 need members
		//13
		return new LoginRequest(channel, ctx, user, password, -1);
	}

}
