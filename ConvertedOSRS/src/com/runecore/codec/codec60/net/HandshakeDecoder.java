package com.runecore.codec.codec60.net;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import com.runecore.network.io.MessageBuilder;

public class HandshakeDecoder extends FrameDecoder {

	@Override
	protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer) throws Exception {
		if(ctx.getPipeline().get(HandshakeDecoder.class) != null)
		    ctx.getPipeline().remove(this);
		int service = buffer.readByte();
		if(service == 15) {
			if(60 != buffer.readInt()) {
				ctx.getChannel().write(new MessageBuilder().writeByte(6).toMessage()).addListener(ChannelFutureListener.CLOSE);
			} else {
				ctx.getChannel().write(new MessageBuilder().writeByte(0).toMessage());
				ctx.getPipeline().addBefore("encoder", "decoder", new com.runecore.codec.codec60.net.JS5Decoder());
			}
		} else if(service == 14) {
			ctx.getChannel().write(new MessageBuilder().writeByte(0).toMessage());
			ctx.getPipeline().addBefore("encoder", "decoder", new com.runecore.codec.codec60.net.LoginDecoder());
		}
		return null;
	}

}
