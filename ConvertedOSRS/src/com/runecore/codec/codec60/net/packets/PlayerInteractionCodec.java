package com.runecore.codec.codec60.net.packets;

import com.runecore.codec.PacketCodec;
import com.runecore.env.model.Entity;
import com.runecore.env.model.combat.CombatEntityAction;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.World;
import com.runecore.network.io.Message;

public class PlayerInteractionCodec implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		int opcode = message.getOpcode();
		int pid = -1;
		boolean run = false;
		if(opcode == 135) { //attack
			pid = message.readLEShort();
			run = message.readByte() == 1;
		} else if(opcode == 217) { //follow
			run = message.readByte() == 1;
			pid = message.readShort();
		} else if(opcode == 141) { //trade
			pid = message.readLEShortA();
			run = message.readByte() == 1;
		}
		player.getActionQueue().cancelQueuedActions();
		if(opcode == 135) {
			Entity target = World.get().getPlayers().get(pid);
			player.getActionQueue().addAction(new CombatEntityAction(player, target));
		}
		
		System.out.println("Player Interaction: "+opcode+" Target -> "+pid+" Run -> "+run);
	}

}
