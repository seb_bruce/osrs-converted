package com.runecore.codec.codec60.net.packets;

import java.util.Arrays;

import com.runecore.codec.PacketCodec;
import com.runecore.env.Context;
import com.runecore.env.model.player.Player;
import com.runecore.env.widget.WidgetAdapterRepository;
import com.runecore.network.io.Message;

public class InterfaceActionPacketCodec implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		int hash = message.readInt();
		int parent = hash >> 16;
		int child = hash & 0xFFFF;
		
		if(message.getOpcode() != 76) {
			message.readShort(); //no clue
			message.readShort(); //no clue
		}
		System.out.println(Arrays.toString(new int[] { parent, child, hash, message.getOpcode() }));
		WidgetAdapterRepository adapters = Context.get().getWidgetAdapters().get(parent);
		if(adapters != null) {
			adapters.handle(player, new int[] { parent, child, hash, message.getOpcode() });
		}
		
	}

}
