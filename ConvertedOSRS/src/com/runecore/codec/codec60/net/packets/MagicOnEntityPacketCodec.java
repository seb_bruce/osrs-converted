package com.runecore.codec.codec60.net.packets;

import com.runecore.codec.PacketCodec;
import com.runecore.env.model.combat.CombatEntityAction;
import com.runecore.env.model.combat.CombatManager;
import com.runecore.env.model.combat.script.MagicSpellScript;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.World;
import com.runecore.network.io.Message;

public class MagicOnEntityPacketCodec implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		int opcode = message.getOpcode();
		
		if(opcode == 243) { //on player
			
			int interfaceHash = message.readInt();
			int childButton = interfaceHash & 0xFFFF;
			int spellBook = interfaceHash >> 16;
			boolean run = message.readByte() == 1 ;//setting
			int pid = message.readLEShort();//pid
			message.readShort();//dunno
			
			Player target = World.get().getPlayers().get(pid).player();
			if(target != null) {
				
				MagicSpellScript script = CombatManager.get().getMagicSpells().get(interfaceHash);
				if(script == null)
					System.out.println(String.format("Book %d, Spell %d, Hash %d", spellBook, childButton, interfaceHash));
				else
					player.getActionQueue().addAction(new CombatEntityAction(player, target, script));
				
			}
			
			
		}
		
	}

}
