package com.runecore.codec.codec60.net.packets;

import com.runecore.codec.PacketCodec;
import com.runecore.env.model.map.pf.PathFinderExecutor;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;
import com.runecore.network.io.Message;

public class WalkingPacketCodec implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		if(player.isDead())
			return;
		int size = message.getLength();
		if(message.getOpcode() == 52){
			size -= 14;
		}
		final int steps = (size - 5) / 2;
		final boolean runSteps = message.readByteC() == 1;
		int firstY = message.readLEShortA();
		int firstX = message.readLEShortA();
		if(steps>0){
			final int[][] path = new int[steps][2];
			for (int i = 0; i < steps; i++) {
				path[i][0] = message.readByte();
				path[i][1] = message.readByteS();
			}
			if (steps > 0) {
				firstX += path[steps - 1][0];
				firstY += path[steps - 1][1];
			}
		} else if (steps<0) {
			System.out.println("ERROR: walk steps negative size "+steps+" for "+player.getDefinition().getName());
		}
		player.getWalking().setRunningQueue(runSteps);
		if (firstX < 0 || firstY < 0)
			return;
		player.getActionQueue().clearNonWalkableActions();
		PathFinderExecutor.walkTo(player, Location.locate(firstX, firstY, player.getLocation().getZ()));
	}

}
