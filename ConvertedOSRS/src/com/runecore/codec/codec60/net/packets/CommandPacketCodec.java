package com.runecore.codec.codec60.net.packets;

import com.runecore.codec.PacketCodec;
import com.runecore.env.model.container.Container;
import com.runecore.env.model.container.Equipment;
import com.runecore.env.model.flag.UpdateFlag;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;
import com.runecore.network.io.Message;

public class CommandPacketCodec implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		String command = message.readRS2String();
		System.out.println("Command= "+command);
		if(command.equalsIgnoreCase("ags")) {
			player.get("inventory").add(new Item(11802, 1));
			player.get("equipment").set(3, new Item(11802, 1));
			player.getFlagManager().flag(UpdateFlag.APPERANCE);
		} else if(command.equalsIgnoreCase("whip")) {
			player.get("equipment").set(3, new Item(12006, 1));
			player.getFlagManager().flag(UpdateFlag.APPERANCE);
		} else if(command.equalsIgnoreCase("swag")) {
			Container equip = player.get("equipment");
			equip.set(Equipment.SLOT_HELM, new Item(10828));
			equip.set(Equipment.SLOT_CAPE, new Item(6570));
			equip.set(Equipment.SLOT_AMULET, new Item(6585));
			equip.set(Equipment.SLOT_CHEST, new Item(11832));
			equip.set(Equipment.SLOT_BOTTOMS, new Item(11834));
			equip.set(Equipment.SLOT_WEAPON, new Item(12006, 1));
			equip.set(Equipment.SLOT_SHIELD, new Item(11283));
			equip.set(Equipment.SLOT_BOOTS, new Item(11840));
			equip.set(Equipment.SLOT_GLOVES, new Item(7462));
		} else if(command.startsWith("item")) {
			String[] split = command.split(" ");
			int item = Integer.parseInt(split[1]);
			int amount = 1;
			if(split.length > 2)
				amount = Integer.parseInt(split[2]);
			player.get("inventory").add(new Item(item, amount));
		} else if(command.startsWith("empty")) {
			player.inventory().clear();
		}
	}

}
