package com.runecore.codec.codec60.net.packets;

import com.runecore.codec.Events;
import com.runecore.codec.PacketCodec;
import com.runecore.env.model.def.ItemDefinition;
import com.runecore.env.model.player.Player;
import com.runecore.network.io.Message;

public class ItemExaminePacketCodec implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		int itemId = message.readLEShortA();
		ItemDefinition definition = ItemDefinition.forId(itemId);
		if(definition != null) {
			Events.sendMsg(player, definition.getExamine());
		}
	}

}
