package com.runecore.codec.codec60.net.packets;

import com.runecore.codec.PacketCodec;
import com.runecore.env.model.container.Container;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;
import com.runecore.network.io.Message;

public class ItemMovePacketCodec implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		message.readByteA();
		int newSlot = message.readShortA();
		int oldSlot = message.readLEShort();
		int face = message.readLEInt() >> 16;
		if(face == 149) { //inventory
			Container inv = player.get("inventory");
			Item newSlotItem = inv.get(newSlot);
			Item oldSlotItem = inv.get(oldSlot);
			inv.setFiringEvents(false);
			inv.set(oldSlot, newSlotItem);
			inv.setFiringEvents(true);
			inv.set(newSlot, oldSlotItem);
		}
	}

}
