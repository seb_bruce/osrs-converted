package com.runecore.codec.codec60.net.packets;

import java.util.List;

import com.runecore.codec.PacketCodec;
import com.runecore.codec.event.SendMessageEvent;
import com.runecore.env.Context;
import com.runecore.env.model.container.Container;
import com.runecore.env.model.container.Equipment;
import com.runecore.env.model.flag.UpdateFlag;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.item.LevelRequirement;
import com.runecore.env.model.player.Looks;
import com.runecore.env.model.player.Player;
import com.runecore.env.model.player.Skills;
import com.runecore.network.io.Message;

public class EquipItemCodec implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		int hash = message.readInt();
		int face = hash >> 16;
		int slot = message.readLEShortA() & 0xFF;
		int itemId = message.readLEShort() & 0xFFFF;
		if(face == 149) {
			if(slot < 0 || slot > 28) {
				return;
			}
			Item item = player.get("inventory").get(slot);
			if(item == null || item.getId() != itemId) {
				return;
			}
			player.getActionQueue().cancelQueuedActions();
			handleEquip(player, item, slot);
		}
	}
	
	private void handleEquip(Player player, Item wieldItem, int slot) {
		int wieldSlot = wieldItem.getDefinition().getEquipmentSlot();
		if(wieldSlot == -1) {
			return;
		}
		List<LevelRequirement> reqs = wieldItem.getDefinition().getLevelReqs();
		for(LevelRequirement lr : reqs) {
			if(!lr.meetsRequirement(player)) {
				Context.get().getActionSender().sendMessage(new SendMessageEvent(player, "You need a"+(lr.getLevelIndex() == 0 ? "n": "")+" "+Skills.SKILL_NAME[lr.getLevelIndex()]+" level of "+lr.getLevel()+" to wear this item."));
				return;
			}
		}
		boolean twoHanded = wieldItem.getDefinition().isTwoHanded();
		Container inventory = player.get("inv");
		Container equipment = player.get("equip");
		if(twoHanded && inventory.freeSlots() < 1 && equipment.get(5) != null) {
			Context.get().getActionSender().sendMessage(new SendMessageEvent(player, "Not enough free space in your inventory."));
			return;
		}
		if(twoHanded && equipment.get(Looks.SLOT_SHIELD) != null) {
			inventory.add(equipment.get(Looks.SLOT_SHIELD));
			equipment.set(Looks.SLOT_SHIELD, null);
		}
		inventory.set(slot, null);
		if(wieldSlot == 3) {
			player.getCombatDefinition().setSpecialActivated(false);
			player.getCombatDefinition().refreshSpecial();
			if(twoHanded && equipment.get(5) != null) {
				player.getCombatDefinition().setSpecialActivated(false);
				player.getCombatDefinition().refreshSpecial();
				if(!inventory.add(equipment.get(5))) {
					inventory.add(equipment.get(5));
					return;
				}
				equipment.set(5, null);
			}
		} else if(wieldSlot == 5) {
			if(equipment.get(3) != null && Equipment.isTwoHanded(equipment.get(3).getDefinition())) {
				if(!inventory.add(equipment.get(3))) {
					player.getCombatDefinition().setSpecialActivated(false);
					player.getCombatDefinition().refreshSpecial();
					inventory.add(equipment.get(3));
					return;
				}
				equipment.set(3, null);
			}
		}
		if(equipment.get(wieldSlot) != null && (wieldItem.getId() != equipment.get(wieldSlot).getDefinition().getId() || !wieldItem.getDefinition().isStackable())) {
			inventory.set(slot, equipment.get(wieldSlot));
			equipment.set(wieldSlot, null);
		}
		int oldAmt = 0;
		if(equipment.get(wieldSlot) != null) {
			oldAmt = equipment.get(wieldSlot).getAmount();
		}
		Item item2 = new Item(wieldItem.getId(), oldAmt+wieldItem.getAmount(), wieldItem.getDegradeCount());
		equipment.set(wieldSlot, item2);
		player.getCombatDefinition().refresh();
		player.getFlagManager().flag(UpdateFlag.APPERANCE);
	}

}
