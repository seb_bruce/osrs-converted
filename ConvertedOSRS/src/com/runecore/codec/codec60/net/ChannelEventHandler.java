package com.runecore.codec.codec60.net;

import java.util.concurrent.ExecutorService;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

import com.runecore.codec.generic.FileRequest;
import com.runecore.env.Context;
import com.runecore.env.login.LoginRequest;
import com.runecore.network.GameSession;
import com.runecore.network.io.Message;

public class ChannelEventHandler extends SimpleChannelHandler {
	
	private final ExecutorService service;

	public ChannelEventHandler(ExecutorService service) {
		this.service = service;
	}
	@Override
	public final void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
		if(e.getMessage() instanceof FileRequest) {
			/**
			 * TODO: Context this so we can deal with the urgency of requests,
			 * fine for dev though just not production
			 */
			service.submit((FileRequest)e.getMessage());
		} else if(e.getMessage() instanceof LoginRequest) {
			Context.get().getLoginProcessor().queue((LoginRequest) e.getMessage());
		} else if(e.getMessage() instanceof Message) {
			GameSession session = (GameSession) ctx.getAttachment();
			session.queue((Message) e.getMessage());
		}
	}
	
}
