package com.runecore.codec.codec60.net;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import com.runecore.network.GameSession;
import com.runecore.network.io.Message;
import com.runecore.network.io.Message.PacketType;

public class GamePacketDecoder extends FrameDecoder {

	private static final int[] PACKET_LENGTHS = new int[256];

	static {
		for(int i=0; i<PACKET_LENGTHS.length; i++){
			PACKET_LENGTHS[i]=-3;//unknown
		}
		
		
		//now set known sizes
		PACKET_LENGTHS[164]=-1;//commands
		PACKET_LENGTHS[132]=0;//close interface (empty)
		
		PACKET_LENGTHS[52]=-1;//movement minimap/screen walk
		PACKET_LENGTHS[246]=13;//appearance setup
		
		PACKET_LENGTHS[44]=4;//enter amount integer
		PACKET_LENGTHS[58]=-1;//enter text
		PACKET_LENGTHS[65]=-1;//text string
		
		PACKET_LENGTHS[135]=3;//player op
		PACKET_LENGTHS[141]=3;//player op
		PACKET_LENGTHS[62]=3;//player op
		PACKET_LENGTHS[82]=3;//player op
		
		PACKET_LENGTHS[229]=-1;//string
		PACKET_LENGTHS[157]=-1;//string
		PACKET_LENGTHS[22]=-1;//string
		PACKET_LENGTHS[143]=3;//private/public settings
		PACKET_LENGTHS[96]=-1;//unknown string +2 other bytes
		PACKET_LENGTHS[178]=-1;//public chat
		PACKET_LENGTHS[12]=-2;//private message
		PACKET_LENGTHS[250]=6;//button3
		PACKET_LENGTHS[222]=-1;//add ignore
		
		PACKET_LENGTHS[99]=8;//facebutton
		PACKET_LENGTHS[215]=8;//facebutton
		PACKET_LENGTHS[69]=8;//facebutton
		PACKET_LENGTHS[130]=8;//facebutton
		PACKET_LENGTHS[145]=8;//facebutton
		PACKET_LENGTHS[23]=8;//facebutton
		PACKET_LENGTHS[172]=8;//facebutton
		PACKET_LENGTHS[38]=8;//facebutton
		PACKET_LENGTHS[57]=8;//facebutton
		PACKET_LENGTHS[93]=8;//facebutton
		
		PACKET_LENGTHS[144]=4;//map region related
		PACKET_LENGTHS[133]=0;//unknown MAP REGION UPDATED recieved once WE send region
		
		PACKET_LENGTHS[114]=-1;//unknown
		
		PACKET_LENGTHS[32]=0;//unknown, unspecified size, copies a buffer

		PACKET_LENGTHS[61]=9;//magic on npc
		PACKET_LENGTHS[141]=3;//player op
		PACKET_LENGTHS[7]=11;//item on npc
		PACKET_LENGTHS[166]=8;//item op2
		
		PACKET_LENGTHS[94]=8;//equip item
		PACKET_LENGTHS[179]=3;//unknown
		PACKET_LENGTHS[4]=3;//npc op atk
		PACKET_LENGTHS[135]=3;//player op atk
		PACKET_LENGTHS[73]=13;//magic on object
		PACKET_LENGTHS[217]=3;//player op follo
		PACKET_LENGTHS[76]=4;//button1
		
		PACKET_LENGTHS[181]=7;//unknown
		PACKET_LENGTHS[121]=15;//unknown
		PACKET_LENGTHS[156]=7;//pick up item

		PACKET_LENGTHS[240]=3;//npc op
		PACKET_LENGTHS[214]=8;//item op
		PACKET_LENGTHS[75]=15;//item on object
		
		PACKET_LENGTHS[82]=3;//unknown
		PACKET_LENGTHS[102]=13;//unknown
		
		PACKET_LENGTHS[198]=7;//unknown
		PACKET_LENGTHS[37]=7;//unknown
		
		PACKET_LENGTHS[115]=3;//unknown
		PACKET_LENGTHS[108]=2;//obj examine
		PACKET_LENGTHS[138]=2;//item examine
		
		PACKET_LENGTHS[36]=3;//player op
		PACKET_LENGTHS[224]=3;//npc op
		PACKET_LENGTHS[95]=16;//unknown
		
		PACKET_LENGTHS[53]=3;//npc op
		
		PACKET_LENGTHS[189]=11;//magic on item
		PACKET_LENGTHS[250]=6;//button2
		PACKET_LENGTHS[173]=7;//unknown
		
		PACKET_LENGTHS[86]=3;//npc op
		PACKET_LENGTHS[45]=8;//facebutton
		PACKET_LENGTHS[210]=2;//npc examine
		
		PACKET_LENGTHS[21]=7;//object op
		PACKET_LENGTHS[197]=16;//item on item
		PACKET_LENGTHS[62]=3;//unknown
		
		PACKET_LENGTHS[243]=9;//magic on player
		PACKET_LENGTHS[192]=8;//facebutton
		PACKET_LENGTHS[200]=7;//unknown
		
		PACKET_LENGTHS[106]=14;//object something
		PACKET_LENGTHS[134]=8;//facebutton
		PACKET_LENGTHS[154]=8;//drop item
		
		PACKET_LENGTHS[47]=7;//unknown
		PACKET_LENGTHS[206]=7;//unknown
		PACKET_LENGTHS[76]=4;//button
		
		PACKET_LENGTHS[19]=7;//object op1
		PACKET_LENGTHS[150]=8;//item select
		PACKET_LENGTHS[241]=8;//facebutton
		PACKET_LENGTHS[255]=8;//facebutton
		//76 again

		PACKET_LENGTHS[107]=-1;//enter text
		
		PACKET_LENGTHS[63]=16;//swap items on interface

		//164 again
		//107 again
		
		PACKET_LENGTHS[59]=-1;//mouse click position
		PACKET_LENGTHS[163]=4;//some sort of position var-byte
		PACKET_LENGTHS[42]=-2;//key pressed var-short
		PACKET_LENGTHS[100]=4;//camera orentation
		PACKET_LENGTHS[201]=1;//unknown, flag
		//201 again
		PACKET_LENGTHS[204]=9;//swap items
		PACKET_LENGTHS[236]=-1;//walk movement var-byte
		
		PACKET_LENGTHS[55]=0;//empty afk
		//32 again
		PACKET_LENGTHS[169]=-1;//var-byte class check
		//246 again
		//132 again
		PACKET_LENGTHS[39]=-1;//remove friend string var-byte
	}

	/**
	 * GameSession for the GamePacketDecoder
	 */
	private final GameSession session;

	/**
	 * Construct the GamePacketDecoder
	 * 
	 * @param session
	 *            The GameSession for this GamePacketDecoder
	 */
	public GamePacketDecoder(GameSession session) {
		this.session = session;
		session.getChannel().getPipeline().getContext("handler")
				.setAttachment(session);
	}

	/**
	 * Decode the Game Packets
	 */
	@Override
	protected Object decode(ChannelHandlerContext chc, Channel chan,
			ChannelBuffer buffer) throws Exception {
		if (chc.getAttachment() == null) {
			chc.setAttachment(session);
		}
		if (buffer.readableBytes() > 1000) {
			chan.close();
			return null;
		}
		if (buffer.readable()) {
			int opcode = buffer.readUnsignedByte();
			int length = PACKET_LENGTHS[opcode];
			if (opcode < 0 || opcode > 255) {
				buffer.discardReadBytes();
				return null;
			}
			if (length == -1) {
				if (buffer.readable()) {
					length = buffer.readUnsignedByte();
				}
			} else if(length == -2) {
				if (buffer.readable()) {
					length = buffer.readShort();
				}
			} else if(length == -3) {
				if(buffer.readable()) {
					length = buffer.readableBytes();
				}
			}
			if (length <= buffer.readableBytes() && length > 0) {
				byte[] payload = new byte[length];
				buffer.readBytes(payload, 0, length);
				return new Message(opcode, PacketType.STANDARD,
						ChannelBuffers.wrappedBuffer(payload));
			}
		}
		return null;
	}

}
