package com.runecore.codec.codec60.net.component;

import com.runecore.env.model.player.Player;
import com.runecore.env.widget.WidgetAdapter;

public class PrayerBookAdapter implements WidgetAdapter {

	@Override
	public boolean handle(Player player, int[] data) {
		if(data[0] == 271) {
			player.getPrayer().activatePrayer(data[1]);
			return true;
		}
		return false;
	}

}
