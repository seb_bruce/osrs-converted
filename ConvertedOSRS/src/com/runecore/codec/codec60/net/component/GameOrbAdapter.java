package com.runecore.codec.codec60.net.component;

import com.runecore.env.model.player.Player;
import com.runecore.env.widget.WidgetAdapter;

public class GameOrbAdapter implements WidgetAdapter {

	@Override
	public boolean handle(Player player, int[] data) {
		if(data[0] == 548 && data[1] == 94) {
			player.getActionQueue().cancelQueuedActions();
			player.getWalking().toggleRun();
			return true;
		}
		return false;
	}

}
