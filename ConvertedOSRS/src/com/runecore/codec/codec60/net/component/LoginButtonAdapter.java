package com.runecore.codec.codec60.net.component;

import com.runecore.codec.Events;
import com.runecore.env.Context;
import com.runecore.env.model.Cooldown;
import com.runecore.env.model.player.Player;
import com.runecore.env.widget.WidgetAdapter;

public class LoginButtonAdapter implements WidgetAdapter {

	@Override
	public boolean handle(Player player, int[] data) {
		if(data[0] == 182 && data[1] == 6) {
			if(player.isReady(Cooldown.LOGOUT)) {
				player.getActionQueue().cancelQueuedActions();
				Context.get().getActionSender().sendLogout(player);
			} else
				Events.sendMsg(player, "You need to wait 10 seconds from the end of combat to logout!");
			return true;
		}
		return false;
	}

}
