package com.runecore.codec.codec60.net.component;

import java.util.Arrays;

import com.runecore.env.model.player.Player;
import com.runecore.env.widget.WidgetAdapter;

public class AttackStyleTabAdapter implements WidgetAdapter {
	
	private final int[] indexes = { 3, 7, 11, 15 };
	private final int[] indexes_three = { 3, 7, 15 };

	@Override
	public boolean handle(Player player, int[] data) {
		if(data[0] == 593) {
			int comp = data[1];
			if(comp == 3 || comp == 7 || comp == 11 || comp == 15) {
				int index = Arrays.binarySearch(player.getCombat().getCurrentStyles().size() == 4 ? indexes : indexes_three, comp);
				player.getCombat().updateCombatStyle(index);
				return true;
			} else if(comp == 30) {
				player.getCombatDefinition().toggleSpecial();
				return true;
			} else if(comp == 27) {
				player.getCombatDefinition().toggleRetaliate();
				return true;
			}
		}
		return false;
	}

}
