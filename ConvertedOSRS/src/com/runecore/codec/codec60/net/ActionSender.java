package com.runecore.codec.codec60.net;

import java.util.LinkedList;

import com.runecore.codec.event.RefreshLevelEvent;
import com.runecore.codec.event.RightClickOptionEvent;
import com.runecore.codec.event.SendAccessMaskEvent;
import com.runecore.codec.event.SendGroundItemEvent;
import com.runecore.codec.event.SendInterfaceConfigEvent;
import com.runecore.codec.event.SendInterfaceEvent;
import com.runecore.codec.event.SendItemContainerEvent;
import com.runecore.codec.event.SendMessageEvent;
import com.runecore.codec.event.SendPlayerOptionEvent;
import com.runecore.codec.event.SendSettingEvent;
import com.runecore.codec.event.SendStringEvent;
import com.runecore.codec.event.SendWindowPaneEvent;
import com.runecore.env.Context;
import com.runecore.env.model.container.Equipment;
import com.runecore.env.model.container.Inventory;
import com.runecore.env.model.item.GroundItem;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;
import com.runecore.network.io.Message.PacketType;
import com.runecore.network.io.MessageBuilder;
import com.runecore.util.Rights;

public class ActionSender implements com.runecore.codec.ActionSender {

	@Override
	public void sendLogin(Player player) {
		player.setRights(Rights.ADMINISTRATOR);
		sendLoginResponse(player);
		sendMapRegion(player, true);
		refreshGameInterfaces(player);
		player.getSkills().refresh();
		Equipment.init(player);
		Inventory.init(player);
		//updateRunEnergy(player);
		sendMessage(new SendMessageEvent(player, "Welcome to RuneScape."));
		sendPlayerOption(new SendPlayerOptionEvent(player, "Attack", 1, true));
		sendPlayerOption(new SendPlayerOptionEvent(player, "Follow", 3, false));
		sendPlayerOption(new SendPlayerOptionEvent(player, "Trade", 4, false));
	}

	@Override
	public void sendLogout(Player player) {
		player.getSession().write(new MessageBuilder(37).toMessage());
	}

	@Override
	public void sendSetting(SendSettingEvent event) {
		int value = event.getValue();
		if(value <= 127 && value >= -127) {
			MessageBuilder buf = new MessageBuilder(60);
			buf.writeShortA(event.getSetting());
			buf.writeByteC((byte) value);
			event.getPlayer().getSession().write(buf.toMessage());
		} else {
			MessageBuilder buf = new MessageBuilder(76);
			buf.writeLEInt(value);
			buf.writeLEShortA(event.getSetting());
			event.getPlayer().getSession().write(buf.toMessage());
		}
	}

	@Override
	public void sendMessage(SendMessageEvent event) {
		MessageBuilder builder = new MessageBuilder(154, com.runecore.network.io.Message.PacketType.VAR_BYTE);
		builder.writeSmart(event.getType());
		builder.writeByte(0);
		builder.writeString(event.getMessage());
		event.getPlayer().getSession().write(builder.toMessage());
	}

	@Override
	public void sendAccessMask(SendAccessMaskEvent event) {
		MessageBuilder buf = new MessageBuilder(55);	
		buf.writeInt2(event.getSet());
		buf.writeInt1(event.getInter() << 16 | event.getChild());
		buf.writeShort(event.getLength());
		buf.writeLEShortA(event.getOffset());
		event.getPlayer().getSession().write(buf.toMessage());
	}

	@Override
	public void refreshLevel(RefreshLevelEvent event) {
		MessageBuilder builder = new MessageBuilder(121);
		builder.writeByte(event.getIndex()).writeByte(event.getLevel()).writeLEInt((int) event.getXp());
		event.getPlayer().getSession().write(builder.toMessage());
	}

	@Override
	public void sendInterface(SendInterfaceEvent event) {
		MessageBuilder builder = new MessageBuilder(105);
		builder.writeBoolean(event.isWalkable());
		builder.writeLEShortA(event.getInterfaceId());
		builder.writeLEInt((event.getWindowId() << 16) | event.getPosition());
		event.getPlayer().getSession().write(builder.toMessage());
	}

	@Override
	public void sendWindowPane(SendWindowPaneEvent event) {
		MessageBuilder builder = new MessageBuilder(226);
		builder.writeLEShortA(event.getPane());
		event.getPlayer().getSession().write(builder.toMessage());
	}

	@Override
	public void sendLoginResponse(Player player) {
		MessageBuilder builder = new MessageBuilder();
		builder.writeByte(2);
		builder.writeBoolean(false);
		builder.writeInt(0);
		builder.writeByte(player.getRights().ordinal()); //move to entity.facade
		builder.writeBoolean(false);
		builder.writeShort(player.getIndex()); // Index
		builder.writeBoolean(true); /* Member */
		player.getSession().write(builder.toMessage());
	}

	@Override
	public void sendMapRegion(Player player, boolean login) {
		
		int x = player.getLocation().getX();
		int z = player.getLocation().getY();

		int base_x = x / 8;
		int base_z = z / 8;

		int botleft_x = (base_x - 6) * 8;
		int botleft_z = (base_z - 6) * 8;

		int baseX = base_x;
		int baseY = base_z;
		int localX = x - botleft_x;
		int localZ = z - botleft_z;

		player.getFlagManager().setLastKnownRegion(Location.locate(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ()));
		MessageBuilder builder = new MessageBuilder(238, com.runecore.network.io.Message.PacketType.VAR_SHORT);
		builder.writeShortA(localX);//350
		builder.writeByte((byte)player.getLocation().getZ());
		builder.writeShortA(baseY);//53
		/* Calculate map keys needed */
		LinkedList<Integer[]> keys = new LinkedList<>();
		
		int rx=player.getLocation().getLocalX(), 
				ry=player.getLocation().getLocalX(),
				cx=player.getLocation().getRegionX(),
				cy=player.getLocation().getRegionY(), 
				rid=0;//region and chunk x/y
		boolean var17 = false;//force send
		if ((48 == cx / 8 || cx / 8 == 49) && cy / 8 == 48) {
			var17 = true;
		}
		if (48 == cx / 8 && cy / 8 == 148) {
			var17 = true;
		}
		for (rx = (cx - 6) / 8; rx <= (6 + cx) / 8; ++rx) {
			for (ry = (cy - 6) / 8; ry <= (6 + cy) / 8; ++ry) {
				rid = (rx << 8) + ry;
				if (!var17 || 49 != ry && ry != 149 && ry != 147 && rx != 50 && (rx != 49 || 47 != ry)) {
					//LandscapeLoader.loadLandscapeCache(rid);
					Integer[] dd = Context.get().getXteas().get(rid);
					keys.add(dd);
					//System.out.println("adding keys.");
				}
			}
		}
		for (Integer[] keyset : keys) {
			for (int key : keyset) {
				builder.writeInt(key);
			}
		}

		builder.writeShortA(baseX);//50
		builder.writeLEShortA(localZ);//400
		player.getSession().write(builder.toMessage());
	}

	@Override
	public void refreshGameInterfaces(Player player) {
		this.sendWindowPane(new SendWindowPaneEvent(player, 548, -1));
		send(player, 320, PANE_FIXED, 130, true);
		send(player, 149, PANE_FIXED, 132, true);
		send(player, 387, PANE_FIXED, 133, true);
		send(player, 271, PANE_FIXED, 134, true);
		send(player, 193, PANE_FIXED, 135, true);
		send(player, 429, PANE_FIXED, 137, true);
		send(player, 432, PANE_FIXED, 138, true);
		send(player, 182, PANE_FIXED, 139, true);
		send(player, 261, PANE_FIXED, 140, true);
		send(player, 464, PANE_FIXED, 141, true);
		send(player, 589, PANE_FIXED, 136, true);
		send(player, 593, PANE_FIXED, 129, true);

		send(player, 137, PANE_FIXED, 120, true);
		send(player, 239, PANE_FIXED, 142, true);
		send(player, 274, PANE_FIXED, 131, true);
	}
	
	private static final int PANE_FIXED = 548;
	
	public void send(Player player, int id, int target, int targetChild, boolean walkable) {
		this.sendInterface(new SendInterfaceEvent(player, targetChild, target, id, walkable));
	}

	@Override
	public void refreshAccessMasks(Player player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendPlayerOption(SendPlayerOptionEvent event) {
		MessageBuilder builder = new MessageBuilder(231, PacketType.VAR_BYTE);
		builder.writeBoolean(event.isPriority()).writeByteA(event.getSlot()).writeString(event.getOption());
		event.getPlayer().getSession().write(builder.toMessage());
	}

	@Override
	public void sendItemContainer(SendItemContainerEvent event) {
		int size = event.getContainer().capacity();
		MessageBuilder builder = new MessageBuilder(235, PacketType.VAR_SHORT);
		builder.writeInt(event.getInter() <<  16 | event.getChild());
		builder.writeShort(event.getType());
		builder.writeShort(size);
		for(int i = 0; i < size; i++) {
			Item item = event.getContainer().get(i);
			if(item == null) {
				builder.writeByte(0);
				builder.writeLEShortA(0);
			} else {
				int amount = item.getAmount();
				if(amount > 254) {
					builder.writeByte(255);
					builder.writeInt(amount);
				} else {
					builder.writeByte(amount);
				}
				builder.writeLEShortA(item.getId() + 1);
			}
		}
		event.getPlayer().getSession().write(builder.toMessage());
	}

	@Override
	public void sendString(SendStringEvent event) {
		
		
	}

	@Override
	public void updateRunEnergy(Player player) {
		player.getSession().write(new MessageBuilder(206).writeByte(player.getWalking().getEnergy()).toMessage());
	}

	@Override
	public void sendTab(Player player, int tab, int inter) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendInterfaceConfig(SendInterfaceConfigEvent event) {
		MessageBuilder builder = new MessageBuilder(2);
		builder.writeInt(event.getWindow() << 16 | event.getComponent());
		builder.writeByteA(event.isValue() ? 0 : 1);
		event.getPlayer().getSession().write(builder.toMessage());
		
	}

	@Override
	public void sendRightClickOptions(RightClickOptionEvent event) {
		
		
	}

	@Override
	public void sendBlankClientScript(Player p, int script, int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendLocation(Player p, Location l) {
		int regionX = p.getFlagManager().lastKnownRegion().getRegionX();
		int regionY = p.getFlagManager().lastKnownRegion().getRegionY();
		MessageBuilder buf = new MessageBuilder(202);
		buf.writeByteC( (l.getX() - ((regionX - 6) * 8)));
		buf.writeByte( (l.getY() - ((regionY - 6) * 8)));
		p.getSession().write(buf.toMessage());
	}

	@Override
	public void clearGroundItem(Player player, GroundItem groundItem) {
		sendLocation(player, groundItem.getLocation());
		MessageBuilder buf = new MessageBuilder(155);
		buf.writeByteC(0);
		buf.writeShort(groundItem.getItem().getId());
		player.getSession().write(buf.toMessage());
	}

	@Override
	public void sendGroundItem(SendGroundItemEvent event) {
		sendLocation(event.getPlayer(), event.getGroundItem().getLocation());
		MessageBuilder builder = new MessageBuilder(67);
		builder.writeByte(0); //?
		builder.writeLEShortA(event.getGroundItem().getItem().getId());
		builder.writeLEShort(event.getGroundItem().getItem().getAmount());
		event.getPlayer().getSession().write(builder.toMessage());
	}

	@Override
	public void removeGroundItem(SendGroundItemEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeInterface(Player player, int window, int pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendClientScript2(Player player, int id2, int id,
			Object[] params, String types) {
		// TODO Auto-generated method stub
		
	}

}
