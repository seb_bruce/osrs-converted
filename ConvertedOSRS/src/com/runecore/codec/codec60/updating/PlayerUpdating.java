package com.runecore.codec.codec60.updating;

import java.util.Iterator;

import com.runecore.codec.PlayerUpdateCodec;
import com.runecore.env.Context;
import com.runecore.env.model.flag.ChatMessage;
import com.runecore.env.model.flag.UpdateFlag;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.World;
import com.runecore.network.io.MessageBuilder;
import com.runecore.network.io.Message.PacketType;

public class PlayerUpdating implements PlayerUpdateCodec {

	@Override
	public void update(Player player) {
		if(player.getFlagManager().isMapRegionChanged()) {
			Context.get().getActionSender().sendMapRegion(player, false);
		}
		MessageBuilder packet = new MessageBuilder(246, PacketType.VAR_SHORT);
		MessageBuilder block = new MessageBuilder();
		packet.startBitAccess();
		writeMyMovement(player, packet);
		writeBlock(player, block, false);
		packet.writeBits(8, player.getLocalPlayers().size());
		
		Iterator<Player> it$ = player.getLocalPlayers().iterator();
		while(it$.hasNext()) {
			Player p = it$.next();
			if(p != null && player.getLocation().withinDistance(p.getLocation(), 16) && World.get().getPlayers().contains(p) && !p.getFlagManager().isDidTele()) {
				writeLocalPlayerMovement(p, packet);
				writeBlock(p, block, false);
			} else {
				player.getPlayerExists()[p.getIndex()] = false;
				packet.writeBits(1, 1);
				packet.writeBits(2, 3);
				it$.remove();
			}
		}
		
		//disable multiplayer for the time being
		
		int added = 0;
		for(Player p : World.get().getPlayers()) {
			if(added >= 15 || player.getLocalPlayers().size() >= 250) {
				break;
			}
			if(p == null) {
				continue;
			}
			if(p.getIndex() == player.getIndex() || player.getPlayerExists()[p.getIndex()] || !player.getLocation().withinDistance(p.getLocation(), 16)) {
				continue;
			}
			added++;
			player.getLocalPlayers().add(p);
			player.getPlayerExists()[p.getIndex()] = true;
			writeNewLocalPlayer(p, player, packet);
			writeBlock(p, block, true);
		}
		
		if(block.position() > 0) {
			packet.writeBits(11, 2047);
		}
		packet.finishBitAccess();
		if(block.position() > 0) {
			packet.writeBytes(block.getBuffer());
		}
		player.getSession().write(packet.toMessage());
	}
	
	private void writeNewLocalPlayer(Player other, Player local, MessageBuilder builder) {
		builder.writeBits(11, other.getIndex());
		
		int yPos = other.getLocation().getY() - local.getLocation().getY();
		int xPos = other.getLocation().getX() - local.getLocation().getX();
		
		if(xPos < 0) {
			xPos += 32;
		}
		if(yPos < 0) {
			yPos += 32;
		}
		

		builder.writeBits(3, 6); //direction apparently
		builder.writeBits(1, 1);
		builder.writeBits(5, xPos);
		builder.writeBits(1, 1);
		builder.writeBits(5, yPos);
	}
	
	private void writeMyMovement(Player player, MessageBuilder packet) {
		if(player.getFlagManager().isDidTele() || player.getFlagManager().isMapRegionChanged()) {
			packet.writeBits(1, 1);
			packet.writeBits(2, 3);
			packet.writeBits(1, player.getFlagManager().isDidTele() ? 1 : 0);
			packet.writeBits(7, player.getLocation().getLocalX(player.getFlagManager().lastKnownRegion()));
			packet.writeBits(2, player.getLocation().getZ());
			packet.writeBits(7, player.getLocation().getLocalY(player.getFlagManager().lastKnownRegion()));
			packet.writeBits(1, player.getFlagManager().updateNeeded() ? 1 : 0);
		} else {
			if(player.getWalking().getWalkDir() == -1) {
				packet.writeBits(1,  player.getFlagManager().updateNeeded() ? 1 : 0);
				if(player.getFlagManager().updateNeeded()) {
					packet.writeBits(2, 0);
				}
			} else {
				if(player.getWalking().getRunDir() != -1) {
					packet.writeBits(1, 1);
					packet.writeBits(2, 2);
					packet.writeBits(3, player.getWalking().getWalkDir());
					packet.writeBits(3, player.getWalking().getRunDir());
					packet.writeBits(1, player.getFlagManager().updateNeeded() ? 1 : 0);
				} else {
					packet.writeBits(1, 1);
					packet.writeBits(2, 1);
					packet.writeBits(3, player.getWalking().getWalkDir());
					packet.writeBits(1, player.getFlagManager().updateNeeded() ? 1 : 0);
				}
			}
		}
	}
	
	private void writeLocalPlayerMovement(Player player, MessageBuilder builder) {
		if(player.getWalking().getWalkDir() == -1) {
			if(player.getFlagManager().updateNeeded()) {
				builder.writeBits(1, 1);
				builder.writeBits(2, 0);
			} else {
				builder.writeBits(1, 0);
			}
		} else if(player.getWalking().getRunDir() == -1) {
			builder.writeBits(1, 1);
			builder.writeBits(2, 1);
			builder.writeBits(3, player.getWalking().getWalkDir());
			builder.writeBits(1, player.getFlagManager().updateNeeded() ? 1 : 0);
		} else {
			builder.writeBits(1, 1);
			builder.writeBits(2, 2);
			builder.writeBits(3, player.getWalking().getWalkDir());
			builder.writeBits(3, player.getWalking().getRunDir());
			builder.writeBits(1, player.getFlagManager().updateNeeded() ? 1 : 0);
		}
	}
	
	//pregenerate these blocks for each player each cycle
	private void writeBlock(Player player, MessageBuilder builder, boolean force) {
		if(!player.getFlagManager().updateNeeded() && !force) {
			return;
		}
		int mask = 0;

		if(player.getFlagManager().flagged(UpdateFlag.LOCATION_FOCUS)) {
			mask |= 0x1;
		}
		if(player.getFlagManager().flagged(UpdateFlag.FORCE_CHAT)) {
			mask |= 0x2;
		}
		if(player.getFlagManager().flagged(UpdateFlag.DAMAGE_ONE)) {
			mask |= 0x100;
		}
		if(player.getFlagManager().flagged(UpdateFlag.ENTITY_FOCUS)) {
			mask |= 0x10;
		}
		if(force || player.getFlagManager().flagged(UpdateFlag.APPERANCE)) {
			mask |= 0x80;
		}
		if(player.getFlagManager().flagged(UpdateFlag.GRAPHICS)) {
			mask |= 0x400;
		}

		if(player.getFlagManager().flagged(UpdateFlag.ANIMATION)) {
			mask |= 0x20;
		} 
		
		//TODO: forced movement
		

		if(player.getFlagManager().flagged(UpdateFlag.DAMAGE_TWO)) {
			mask |= 0x8;
		}
		if(player.getFlagManager().flagged(UpdateFlag.CHAT_MESSAGE)) {
			mask |= 0x40;
		}
		
		if(mask >= 0x100) {
			mask |= 0x4;
			builder.writeByte((mask & 0xFF));
			builder.writeByte((mask >> 8));
		} else {
			builder.writeByte(mask);
		}
		

		if(player.getFlagManager().flagged(UpdateFlag.LOCATION_FOCUS)) {
			int x = player.getFlagManager().getFaceLocation().getX();
			int y = player.getFlagManager().getFaceLocation().getY();
			builder.writeLEShort(x = 2 * x + 1);
			builder.writeShort(y = 2 * y + 1);
		}
		

		if(player.getFlagManager().flagged(UpdateFlag.FORCE_CHAT)) {
			builder.writeString(player.getFlagManager().getForceChat());
		}
		if(player.getFlagManager().flagged(UpdateFlag.DAMAGE_ONE)) {
			builder.writeLEShortA(player.getFlagManager().getFirstDamage().getHit());
			builder.writeByte(player.getFlagManager().getFirstDamage().getType().ordinal());
			builder.writeByteS(player.getSkills().getLevel()[3]);
			builder.writeByteA(player.getSkills().getRealLevel(3));
		}

		if(player.getFlagManager().flagged(UpdateFlag.ENTITY_FOCUS)) {
			builder.writeLEShort(player.getFlagManager().getFocusOn() != null ? player.getFlagManager().getFocusOn().getTurnToIndex() : -1);
		}

		if(force || player.getFlagManager().flagged(UpdateFlag.APPERANCE)) {
			MessageBuilder app = player.getLooks().generate(player);
			
			byte[] l = app.toMessage().getBuffer().array();
			byte[] looksBlock = new byte[l.length + 1];
			System.arraycopy(l, 0, looksBlock, 1, l.length);
			looksBlock[0] = (byte) l.length;

			for (int i = 1; i<l.length; i++) {
				looksBlock[i] += 128;
			}
			builder.writeBytes(looksBlock);
		}
		if(player.getFlagManager().flagged(UpdateFlag.GRAPHICS)) {
			builder.writeShort(player.getFlagManager().getGraphic().getId());
			builder.writeInt(player.getFlagManager().getGraphic().getDelay() | player.getFlagManager().getGraphic().getHeight() << 16);
		}
		if(player.getFlagManager().flagged(UpdateFlag.ANIMATION)) {
			builder.writeShortA(player.getFlagManager().getAnimation().getId());
			builder.writeByteC(player.getFlagManager().getAnimation().getDelay());
		}
		//TODO: forcemovement mask
		if(player.getFlagManager().flagged(UpdateFlag.DAMAGE_TWO)) {
			builder.writeLEShortA(player.getFlagManager().getSecondDamage().getHit());
			builder.writeByte(player.getFlagManager().getSecondDamage().getType().ordinal());
			builder.writeByte(player.getSkills().getLevel()[3]);
			builder.writeByte(player.getSkills().getRealLevel(3));
		}
		if(player.getFlagManager().flagged(UpdateFlag.CHAT_MESSAGE)) {
			writeChatMask(player, builder);
		}
	}
	
	private void writeChatMask(Player player, MessageBuilder builder) {
		ChatMessage message = player.getFlagManager().getChatMessage();
		builder.writeLEShort((message.getColour() & 0xFF) << 8 | message.getEffect() & 0xFF);
		builder.writeByteS((byte) player.getRights().ordinal());
		builder.writeByteC(0);//OSRS only flag
		int chars = message.getChatMsg().length();
		builder.writeByte(chars);
		builder.writeBytes(message.getPacked(), 0, chars);
	}

}