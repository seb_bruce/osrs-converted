package com.runecore.codec.codec60;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import org.jboss.netty.channel.ChannelPipeline;
import org.oly.game.cache.Cache;
import org.oly.game.cache.FileManager;

import com.runecore.cache.LandscapeParser;
import com.runecore.codec.ProtocolCodec;
import com.runecore.codec.codec60.net.ActionSender;
import com.runecore.codec.codec60.net.ChannelEventHandler;
import com.runecore.codec.codec60.net.HandshakeDecoder;
import com.runecore.codec.codec60.net.component.AttackStyleTabAdapter;
import com.runecore.codec.codec60.net.component.GameOrbAdapter;
import com.runecore.codec.codec60.net.component.LoginButtonAdapter;
import com.runecore.codec.codec60.net.component.PrayerBookAdapter;
import com.runecore.codec.codec60.net.packets.ChatPacketCodec;
import com.runecore.codec.codec60.net.packets.CommandPacketCodec;
import com.runecore.codec.codec60.net.packets.EquipItemCodec;
import com.runecore.codec.codec60.net.packets.InterfaceActionPacketCodec;
import com.runecore.codec.codec60.net.packets.ItemExaminePacketCodec;
import com.runecore.codec.codec60.net.packets.ItemMovePacketCodec;
import com.runecore.codec.codec60.net.packets.MagicOnEntityPacketCodec;
import com.runecore.codec.codec60.net.packets.PlayerInteractionCodec;
import com.runecore.codec.codec60.net.packets.WalkingPacketCodec;
import com.runecore.codec.codec60.updating.PlayerUpdating;
import com.runecore.env.Context;
import com.runecore.env.model.combat.CombatManager;
import com.runecore.env.world.loader.GSONPlayerAdapter;
import com.runecore.network.NetworkEncoder;

public class ProtocolOSRS60 implements ProtocolCodec {

	@Override
	public void init(Context context) {
		try {
			Cache.init();
			FileManager.load(Cache.DIRECTORY);
			load(context);
			LandscapeParser.init();
			CombatManager.get().init();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//switch this to mysql later :D
		context.setPlayerUpdateCodec(new PlayerUpdating());
		context.setActionSender(new ActionSender());
		context.setPlayerAdapter(new GSONPlayerAdapter());
		
		//setup ComponentAdapters
		context.register(593, new AttackStyleTabAdapter());
		context.register(182, new LoginButtonAdapter());
		context.register(548, new GameOrbAdapter());
		context.register(271, new PrayerBookAdapter());
		//assign packets bruh
		
		context.register(164, new CommandPacketCodec());
		context.register(138, new ItemExaminePacketCodec());
		context.register(94, new EquipItemCodec());
		context.register(204, new ItemMovePacketCodec());
		context.register(178, new ChatPacketCodec());
		context.register(243, new MagicOnEntityPacketCodec());
		
		WalkingPacketCodec walkingCodec = new WalkingPacketCodec();
		Arrays.asList(236, 52).forEach(bind -> context.register(bind, walkingCodec));
		
		PlayerInteractionCodec playerInteraction = new PlayerInteractionCodec();
		Arrays.asList(135, 217, 141).forEach(bind -> context.register(bind, playerInteraction));
		
		InterfaceActionPacketCodec interfaceInteraction = new InterfaceActionPacketCodec();
		Arrays.asList(99, 215, 69, 130, 145, 23, 172, 38, 57, 93, 76).forEach(bind -> context.register(bind, interfaceInteraction));

	}

	@Override
	public void setup(ChannelPipeline pipeline) {
		pipeline.addLast("decoder", new HandshakeDecoder());
		pipeline.addLast("encoder", new NetworkEncoder());
		pipeline.addLast("handler", new ChannelEventHandler(Executors.newFixedThreadPool(4)));
	}
	
	private static void load(Context context) throws IOException {
		ByteBuffer buffer = ByteBuffer.wrap(Files.readAllBytes(new File("./data/world/keys.bin").toPath()));
		while (buffer.remaining() > 0) {
			int map = buffer.getShort() & 0xFFFF;
			Integer[] k = new Integer[4];
			for (int i = 0; i < 4; i++)
				k[i] = buffer.getInt();
			context.getXteas().put(map, k);
		}
		Logger.getGlobal().info("Loaded map decryption key sets "+context.getXteas().size());
	}

	@Override
	public String[] scriptPaths() {
		return null;
	}

}