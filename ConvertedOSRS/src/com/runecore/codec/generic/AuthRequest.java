package com.runecore.codec.generic;

public class AuthRequest {
	
	/**
	 * The revision for authentication
	 */
	private final int revision;

	/**
	 * Construct the packet
	 * 
	 * @param revision
	 */
	public AuthRequest(int revision) {
		this.revision = revision;
	}

	/**
	 * @return the revision
	 */
	public int getRevision() {
		return revision;
	}


}
