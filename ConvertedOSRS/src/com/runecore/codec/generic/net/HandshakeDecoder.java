package com.runecore.codec.generic.net;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import com.runecore.codec.codec530.net.JS5Decoder;
import com.runecore.codec.codec530.nodelist.NodeListDecoder;
import com.runecore.codec.generic.AuthRequest;
import com.runecore.codec.generic.LoginKeyRequest;

public class HandshakeDecoder extends FrameDecoder {

	@Override
	protected Object decode(ChannelHandlerContext ctx, Channel chan, ChannelBuffer buffer) throws Exception {
		if(ctx.getPipeline().get(HandshakeDecoder.class) != null)
		    ctx.getPipeline().remove(this);
		int service = buffer.readByte();
		if(service == 15) {
			ctx.getPipeline().addBefore("encoder", "decoder", new JS5Decoder());
			return new AuthRequest(buffer.readInt());
		} else if(service == 14) {
			buffer.readByte();
			//ctx.getPipeline().addBefore("encoder", "decoder", new AuthenticationDecoder());
			return new LoginKeyRequest();
		} else {
			NodeListDecoder.prepareResponse(chan, buffer.readInt()!= 0, true);
			ctx.getPipeline().addBefore("encoder", "decoder", new NodeListDecoder());
		}
		return null;
	}

}
