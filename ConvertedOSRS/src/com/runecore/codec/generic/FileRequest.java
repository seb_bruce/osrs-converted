package com.runecore.codec.generic;

import java.nio.ByteBuffer;

import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.oly.game.cache.UpdateServer;

import com.runecore.cache.fs.ArchiveFile2;
import com.runecore.env.Context;
import com.runecore.network.io.Message;
import com.runecore.network.io.MessageBuilder;

public class FileRequest implements Runnable {
	
	public static Object object = new Object();

	/**
	 * Attributes for the request
	 */
	private final int file, priority, container;

	/**
	 * The channel to write to
	 */
	private Channel channel;

	/**
	 * Construct the request
	 * 
	 * @param channel
	 *            The channel requesting
	 * @param priority
	 *            The priority
	 * @param container
	 *            The container
	 * @param file
	 *            The file in the container
	 */
	public FileRequest(Channel channel, int priority, int container, int file) {
		this.file = file;
		this.container = container;
		this.priority = priority;
		this.channel = channel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			if(!channel.isConnected())
				return;
			Message m = UpdateServer.getPacketRequest(container, file);
			if(m != null)
				channel.write(m);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
