package com.runecore.codec.codec530.components;

import com.runecore.codec.Events;
import com.runecore.env.Context;
import com.runecore.env.model.player.Player;
import com.runecore.env.widget.WidgetAdapter;

public class LogoutAdapter implements WidgetAdapter {

	@Override
	public boolean handle(Player player, int[] data) {
		if(data[1] == 6) {
			
			Context.get().getActionSender().sendLogout(player);
			return true;
		}
		return false;
	}

}
