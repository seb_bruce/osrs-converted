package com.runecore.codec.codec530.components;

import com.runecore.env.model.player.Player;
import com.runecore.env.widget.WidgetAdapter;

public class RunOrbAdapter implements WidgetAdapter {

	@Override
	public boolean handle(Player player, int[] data) {
		if(data[0] == 750 && data[1] == 1) {
			//player.getWalking().toggleRun();
			return true;
		}
		return false;
	}

}
