package com.runecore.codec.codec530.extras;

import com.runecore.env.model.player.Player;
import com.runecore.network.io.Message.PacketType;
import com.runecore.network.io.MessageBuilder;

public class ClientScript {
	
	private static int count = 0;
	
	public static void send(Player p, int id, Object[] params, String types) {
		if (params.length != types.length()) {
			throw new IllegalArgumentException("params size should be the same as types length");
		}
		MessageBuilder packet = new MessageBuilder(115, PacketType.VAR_SHORT)
		.writeShort(count++)
		.writeString(types);
		int idx = 0;
		for (int i = types.length() - 1;i >= 0;i--) {
			if (types.charAt(i) == 's') {
				packet.writeString((String) params[idx]);
			} else {
				packet.writeInt((Integer) params[idx]);
			}
			idx++;
		}
		packet.writeInt(id);
		p.getSession().write(packet.toMessage());
	}
	
	public static void sendAMask(Player player, int set, int inter, int min, int len) {
		MessageBuilder spb = new MessageBuilder(165)
		.writeLEShort(count++)
		.writeLEShort(len)
		.writeInt(inter)
		.writeShortA(min)
		.writeInt1(set);
		player.getSession().write(spb.toMessage());
	}	
	
	public static void sendInterfaceScript(Player player, int id, int value) {
		MessageBuilder packet = new MessageBuilder(65);
		packet.writeLEShort(count++);
		packet.writeByteC(value);
		packet.writeLEShortA(id);
		player.getSession().write(packet.toMessage());
	}
	
	public static void sendConfig2(Player p, int id, int value) {
		p.getSession().write(new MessageBuilder(226).writeInt(value).writeShortA(id).toMessage());
	}


}