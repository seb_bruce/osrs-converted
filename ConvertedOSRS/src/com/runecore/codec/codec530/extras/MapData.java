package com.runecore.codec.codec530.extras;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapData {
	
	public static final Map<Integer, int[]> mapData = new HashMap<Integer, int[]>();
	public static final List<Integer> keys = new ArrayList<Integer>();
	
	public static void init() throws Exception {
		File file = new File("data/530keys.dat");
		if (!file.exists()) {
			System.err.println("530 region keys dont exist mate");
			System.exit(1);
			return;
		}
		RandomAccessFile raf = new RandomAccessFile(file, "rw");
		ByteBuffer buffer = raf.getChannel().map(MapMode.READ_ONLY, 0, raf.length());
		while (buffer.remaining() > 0) {
			int id = buffer.getShort() & 0xFFFF;
			int[] key = new int[4];
			for (int i2 = 0; i2 < 4; i2++) {
				key[i2] = buffer.getInt();
			}
			keys.add(id);
			mapData.put(id, key);
		}
		raf.close();
	}
	
	public static int[] getKeys(int x, int y) {
		int[] k = getMapData((x << 8) | y);
		if (k == null)
			k = new int[] { 0, 0, 0, 0};
		return k;
	}
	
	public static int[] getMapData(int region) {
		return mapData.get(region);
	}

}
