package com.runecore.codec.codec530.nodelist;

public class WorldInformation {

	private final String activity;
	private final int country;
	private final int flag;
	private final String ip;
	private final int worldId;

	public WorldInformation(int worldId, int flag, String activity, String ip,
			int country) {
		this.worldId = worldId;
		this.flag = flag;
		this.activity = activity;
		this.ip = ip;
		this.country = country;
	}

	public String getActivity() {
		return activity;
	}

	public int getCountry() {
		return country;
	}

	public int getFlag() {
		return flag;
	}

	public String getIp() {
		return ip;
	}

	public int getWorldId() {
		return worldId;
	}

}