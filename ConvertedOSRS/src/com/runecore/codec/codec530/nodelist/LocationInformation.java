package com.runecore.codec.codec530.nodelist;

public class LocationInformation {
	
	private final String name;
	private final int flag;
	
	public LocationInformation(int flag, String name) {
		this.name = name;
		this.flag = flag;
	}

	public int getFlag() {
		return flag;
	}

	public String getName() {
		return name;
	}

}