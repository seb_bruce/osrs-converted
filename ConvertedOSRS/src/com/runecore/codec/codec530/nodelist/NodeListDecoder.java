package com.runecore.codec.codec530.nodelist;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import com.runecore.network.io.MessageBuilder;

public class NodeListDecoder extends FrameDecoder {
	
	private static final List<WorldInformation> worldList = new ArrayList<WorldInformation>();
	private static final List<LocationInformation> locList = new ArrayList<LocationInformation>();
    public static final int COUNTRY_AUSTRALIA = 16;
    public static final int COUNTRY_BELGIUM = 22;
    public static final int COUNTRY_BRAZIL = 31;
    public static final int COUNTRY_CANADA = 38;
    public static final int COUNTRY_DENMARK = 58;
    public static final int COUNTRY_FINLAND = 69;
    public static final int COUNTRY_IRELAND = 101;
    public static final int COUNTRY_MEXICO = 152;
    public static final int COUNTRY_NETHERLANDS = 161;
    public static final int COUNTRY_NORWAY = 162;
    public static final int COUNTRY_SWEDEN = 191;
    public static final int COUNTRY_UK = 77;
    public static final int COUNTRY_USA = 225;
    public static final int FLAG_HIGHLIGHT = 16;
    public static final int FLAG_LOOTSHARE = 8;
    public static final int FLAG_MEMBERS = 1;
    public static final int FLAG_NON_MEMBERS = 0;
    public static final int FLAG_PVP = 4;
    
	static {
		locList.add(new LocationInformation(COUNTRY_NETHERLANDS, "NL")); // 0
		//ID, FLAGS, NAME, HOST, LOCATION ID SEE ABOVE
		worldList.add(new WorldInformation(1, FLAG_NON_MEMBERS, "RuneCore", "127.0.0.1", 0));
	}

	@Override
	protected Object decode(ChannelHandlerContext arg0, Channel arg1, ChannelBuffer arg2) throws Exception {
		prepareResponse(arg1, arg2.readInt() != 0, true);
		return null;
	}
	
	public static void prepareResponse(Channel channel, boolean worldStatus, boolean worldConfiguration) {
		ChannelBuffer buffer = ChannelBuffers.dynamicBuffer();
		//packet header
		buffer.writeByte(1); //writing player count
		buffer.writeByte(1); //writing configuration + region information
		putSmart(buffer, locList.size());
		for (LocationInformation li : locList) {
			putSmart(buffer, li.getFlag());
			putJagString(buffer, li.getName());
		}
		putSmart(buffer, 1);
		putSmart(buffer, worldList.size());
		putSmart(buffer, worldList.size());
		for (WorldInformation wi : worldList) {
			putSmart(buffer, wi.getWorldId() - 1);
			buffer.writeByte(wi.getCountry());
			buffer.writeInt(wi.getFlag());
			putJagString(buffer, wi.getActivity());
			putJagString(buffer, wi.getIp());
		}
		int val = 0x94DA4A87;
		buffer.writeInt(val);
		buffer.writeInt(worldList.size());
		for(WorldInformation wi : worldList) {
			putSmart(buffer, wi.getWorldId() -1);
			buffer.writeShort(500);
		}
		
		MessageBuilder builder = new MessageBuilder();
		builder.writeByte(0);
		builder.writeShort(buffer.writerIndex());
		builder.writeBytes(buffer);
		channel.write(builder.toMessage());
		
	}

	/**
	 * Writes a string
	 * 
	 * @param buffer
	 *            The ChannelBuffer
	 * @param string
	 *            The string being wrote.
	 */
	public static void putJagString(ChannelBuffer buffer, String string) {
		buffer.writeByte((byte) 0);
		buffer.writeBytes(string.getBytes(Charset.forName("ISO-8859-1")));
		buffer.writeByte((byte) 0);
	}

	/**
	 * Writes a smart
	 * 
	 * @param buffer
	 *            The ChannelBuffer
	 * @param value
	 *            The value being wrote
	 */
	public static void putSmart(ChannelBuffer buffer, int value) {
		if (value < 128)
			buffer.writeByte(value);
		else
			buffer.writeShort(32768 + value);
	}


}