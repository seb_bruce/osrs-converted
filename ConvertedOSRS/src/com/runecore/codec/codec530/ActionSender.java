package com.runecore.codec.codec530;

import java.util.ArrayList;
import java.util.List;

import com.runecore.codec.codec530.extras.MapData;
import com.runecore.codec.event.RefreshLevelEvent;
import com.runecore.codec.event.RightClickOptionEvent;
import com.runecore.codec.event.SendAccessMaskEvent;
import com.runecore.codec.event.SendGroundItemEvent;
import com.runecore.codec.event.SendInterfaceConfigEvent;
import com.runecore.codec.event.SendInterfaceEvent;
import com.runecore.codec.event.SendItemContainerEvent;
import com.runecore.codec.event.SendMessageEvent;
import com.runecore.codec.event.SendPlayerOptionEvent;
import com.runecore.codec.event.SendSettingEvent;
import com.runecore.codec.event.SendSettingEvent.SettingType;
import com.runecore.codec.event.SendStringEvent;
import com.runecore.codec.event.SendWindowPaneEvent;
import com.runecore.env.content.Areas;
import com.runecore.env.model.container.Bank;
import com.runecore.env.model.container.Equipment;
import com.runecore.env.model.container.Inventory;
import com.runecore.env.model.item.GroundItem;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;
import com.runecore.network.io.Message.PacketType;
import com.runecore.network.io.MessageBuilder;

public class ActionSender implements com.runecore.codec.ActionSender {
	
	private int frameIndex = 0;

	@Override
	public void sendLogin(Player player) {
		sendLoginResponse(player);
		sendMapRegion(player, false);
		refreshGameInterfaces(player);
		player.getSkills().refresh();
		Inventory.init(player);
		Equipment.init(player);
		Bank.init(player);
		updateRunEnergy(player);
		sendMessage(new SendMessageEvent(player, "Welcome to Vendetta."));
		sendMessage(new SendMessageEvent(player, "Commands: ::pure ::bpure ::maxed ::zerker ::item ::setlevel"));
		sendPlayerOption(new SendPlayerOptionEvent(player, "Attack", 1, true));
		sendPlayerOption(new SendPlayerOptionEvent(player, "Follow", 2, false));
		sendPlayerOption(new SendPlayerOptionEvent(player, "Trade with", 3, false));
		player.displayEP(true);
		player.displayPvp(Areas.inArea(player, Areas.SAFE_AREA));
		player.sendQuestTab();
	}

	@Override
	public void sendLogout(Player player) {
		player.getSession().write(new MessageBuilder(86).toMessage());
	}
	
	@Override
	public void sendClientScript2(Player player, int id2, int id, Object[] params, String types) {
		MessageBuilder packet = new MessageBuilder(115, PacketType.VAR_SHORT);
		packet.writeShort(frameIndex++);
		packet.writeString(types);
		int idx = 0;
		for (int i = types.length() - 1;i >= 0;i--) {
			if (types.charAt(i) == 's') {
				packet.writeString((String) params[idx]);
			} else {
				packet.writeInt((Integer) params[idx]);
			}
			idx++;
		}
		packet.writeInt(id);
		player.getSession().write(packet.toMessage());
	}
	
	@Override
	public void sendPlayerOption(SendPlayerOptionEvent event) {
		MessageBuilder builder = new MessageBuilder(44, PacketType.VAR_BYTE);
		builder.writeLEShortA(65535);
		builder.writeByte(event.isPriority() ? 1 : 0);
		builder.writeByte(event.getSlot());
		builder.writeString(event.getOption());
		event.getPlayer().getSession().write(builder.toMessage());
	}

	@Override
	public void sendMessage(SendMessageEvent event) {
		MessageBuilder builder = new MessageBuilder(70, PacketType.VAR_BYTE);
		builder.writeString(event.getMessage());
		event.getPlayer().getSession().write(builder.toMessage());
	}

	@Override
	public void sendLoginResponse(Player player) {
		MessageBuilder builder = new MessageBuilder();
		builder.writeByte(2); // resp
		builder.writeByte(2); // rights;
		builder.writeByte((byte) 0);
		builder.writeByte((byte) 0);//Flagged, will generate mouse packets
		builder.writeByte((byte) 0);
		builder.writeByte((byte) 0);
		builder.writeByte((byte) 0);
		builder.writeByte((byte) 0); // Generates packets
		builder.writeShort(player.getIndex());//PlayerID
		builder.writeByte((byte) 1); // membership flag #1?..this one enables all GE boxes
		builder.writeByte((byte) 1); // membership flag #2?
		player.getSession().write(builder.toMessage());
	}

	@Override
	public void sendInterface(SendInterfaceEvent event) {
		MessageBuilder builder = new MessageBuilder(155);
		builder.writeByte(event.getPosition());
		builder.writeInt2(event.getWindowId() << 16 | event.getInterfaceId());
		frameIndex++;
		builder.writeShortA(frameIndex);
		builder.writeShort(event.getChildId());
		event.getPlayer().getSession().write(builder.toMessage());
	}

	@Override
	public void sendWindowPane(SendWindowPaneEvent event) {
		MessageBuilder builder = new MessageBuilder(145);
		frameIndex++;
		builder.writeLEShortA(event.getPane());
		builder.writeByteA(0);
		builder.writeLEShortA(frameIndex);
		event.getPlayer().getSession().write(builder.toMessage());
	}

	@Override
	public void refreshGameInterfaces(Player player) {
		sendWindowPane(new SendWindowPaneEvent(player, 548, -1));
		sendInterface(new SendInterfaceEvent(player, 1, 752, 8, 137));
		int[][] tabs = { {14, 751}, {75, 752}, {70, 748}, {71, 749},
		{72, 750}, {83, 92}, {84, 320}, {85, 274},
		{86, 149}, {87, 387}, {88, 271}, {89, 193}, 
		{91, 550}, {92, 551}, {93, 589}, {94, 261}, 
		{95, 464}, {96, 187}, {97, 182}, {10, 754}};
		for(int[] tab : tabs) {
			sendInterface(new SendInterfaceEvent(player, 1, tab[1] == 137 ? 752 : 548, tab[0], tab[1]));
		}
	}

	@Override
	public void refreshAccessMasks(Player player) {

	}

	@Override
	public void sendMapRegion(Player player, boolean login) {
		player.getFlagManager().setLastKnownRegion(Location.locate(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ()));
		List<Integer> regions = new ArrayList<Integer>();
		Location loc = player.getLocation();
		MessageBuilder builder = new MessageBuilder(162, PacketType.VAR_SHORT);
		builder.writeShortA(loc.getLocalX());
		for (int xCalc = (loc.getRegionX() - 6) / 8; xCalc <= ((loc.getRegionX() + 6) / 8); xCalc++) {
			for (int yCalc = (loc.getRegionY() - 6) / 8; yCalc <= ((loc.getRegionY() + 6) / 8); yCalc++) {
				int region = yCalc + (xCalc << 8);
				int[] mapData = MapData.getMapData(region);
				if (mapData == null) {
					mapData = new int[4];
					for (int i = 0; i < 4; i++) {
						mapData[i] = 0;
					}
				}
				regions.add(region);
				builder.writeInt2(mapData[0]);
				builder.writeInt2(mapData[1]);
				builder.writeInt2(mapData[2]);
				builder.writeInt2(mapData[3]);
			}
		}
		builder.writeByteS(loc.getZ());
		builder.writeShort(loc.getRegionX());
		builder.writeShortA(loc.getRegionY());
		builder.writeShortA(loc.getLocalY());
		player.getSession().write(builder.toMessage());
		for (int i : regions) {
			int[] mapData = MapData.getMapData(i);
			if (mapData == null) {
				mapData = new int[4];
				for (int ii = 0; ii < 4; ii++) {
					mapData[ii] = 0;
				}
			}
			//LandscapeParser.parseLandscape(i, mapData);
		}
	}

	@Override
	public void refreshLevel(RefreshLevelEvent event) {
		MessageBuilder builder = new MessageBuilder(38);
		builder.writeByteA(event.getLevel());
		builder.writeInt1((int)event.getXp());
		builder.writeByte(event.getIndex());
		event.getPlayer().getSession().write(builder.toMessage());
	}

	@Override
	public void sendSetting(SendSettingEvent event) {
		if(event.getType() == SettingType.NORMAL) {
			if(event.getValue() < 128) {
				MessageBuilder builder = new MessageBuilder(60);
				builder.writeShortA(event.getSetting());
				builder.writeByteC(event.getValue());
				event.getPlayer().getSession().write(builder.toMessage());
			} else {
				MessageBuilder builder = new MessageBuilder(226);
				builder.writeInt(event.getValue());
				builder.writeShortA(event.getSetting());
				event.getPlayer().getSession().write(builder.toMessage());
			}
		} else if(event.getType() == SettingType.B) {
			if (event.getValue() < Byte.MIN_VALUE || event.getValue() > Byte.MAX_VALUE) {
				
			} else {
				
			}
		}
	}

	@Override
	public void sendAccessMask(SendAccessMaskEvent event) {
		
	}

	@Override
	public void sendItemContainer(SendItemContainerEvent event) {
		MessageBuilder builder = new MessageBuilder(105, PacketType.VAR_SHORT);
		builder.writeShort(event.getInter());
		builder.writeShort(event.getChild());
		builder.writeShort(event.getType());
		builder.writeShort(event.getContainer().capacity());
		for(Item i : event.getContainer().getItems()) {
			int id = -1;
			int am = 0;
			if(i != null) {
				id = i.getId();
				am = i.getAmount();
			}
			if(am > 254) {
				builder.writeByteS(255);
				builder.writeInt(am);
			} else {
				builder.writeByteS(am);
			}
			builder.writeShort(id + 1);
		}
		event.getPlayer().getSession().write(builder.toMessage());
	}
	
	@Override
	public void sendString(SendStringEvent event) {
		MessageBuilder builder = new MessageBuilder(171, PacketType.VAR_SHORT);
		builder.writeInt2(event.getWindow() << 16 | event.getSubWindow());
		builder.writeString(event.getMessage());
		frameIndex++;
		builder.writeShortA(frameIndex);
		event.getPlayer().getSession().write(builder.toMessage());
	}
	
	@Override
	public void updateRunEnergy(Player player) {
		player.getSession().write(new MessageBuilder(234).writeByte(100).toMessage());
	}
	
	@Override
	public void sendTab(Player player, int tab, int inter) {
		sendInterface(new SendInterfaceEvent(player, 1, inter == 137 ? 752 : 548, tab, inter));
	}
	
	@Override
	public void sendInterfaceConfig(SendInterfaceConfigEvent event) {
		MessageBuilder builder = new MessageBuilder(21);
		builder.writeByteC(event.isValue() ? 0 : 1);
		frameIndex++;
		builder.writeShort(frameIndex);
		builder.writeLEInt(event.getWindow() << 16 | event.getComponent());
		event.getPlayer().getSession().write(builder.toMessage());
	}
	
	@Override
	public void sendRightClickOptions(RightClickOptionEvent event) {
		frameIndex++;
		MessageBuilder builder = new MessageBuilder(165);
		builder.writeLEShort(frameIndex);
		builder.writeLEShort(event.getLen());
		builder.writeInt(event.getInter());
		builder.writeShortA(event.getOff());
		builder.writeInt1(event.getSetting());
		event.getPlayer().getSession().write(builder.toMessage());
	}
	
	@Override
	public void sendBlankClientScript(Player p, int script, int id) {
		frameIndex++;
		MessageBuilder builder = new MessageBuilder(115, PacketType.VAR_SHORT);
		builder.writeShort(frameIndex);
		builder.writeString("");
		builder.writeInt(id);
		p.getSession().write(builder.toMessage());
	}
	
	public void sendLocation(Player p, Location l) {
		MessageBuilder builder = new MessageBuilder(26);
		int regionX = p.getFlagManager().lastKnownRegion().getRegionX();
		int regionY = p.getFlagManager().lastKnownRegion().getRegionY();
		builder.writeByteC((l.getX()-((regionX-6)*8)));
		builder.writeByte((l.getY()-((regionY-6)*8)));
		p.getSession().write(builder.toMessage());
	}
	
    public void clearGroundItem(Player player, GroundItem groundItem) {
    	if (groundItem != null) {
    		sendLocation(player, groundItem.getLocation());
    		MessageBuilder builder = new MessageBuilder(240);
			builder.writeByte(0);
			builder.writeShort(groundItem.getItem().getId());
			player.getSession().write(builder.toMessage());
		}
    }
	
	public void sendGroundItem(SendGroundItemEvent event) {
		sendLocation(event.getPlayer(), event.getGroundItem().getLocation());
		MessageBuilder builder = new MessageBuilder(33);
		builder.writeLEShort(event.getGroundItem().getItem().getId());
		builder.writeByte(0);
		builder.writeShortA(event.getGroundItem().getItem().getAmount());
		event.getPlayer().getSession().write(builder.toMessage());
	}
	
	public void removeGroundItem(SendGroundItemEvent event) {
		sendLocation(event.getPlayer(), event.getGroundItem().getLocation());
		MessageBuilder builder = new MessageBuilder(240);
		builder.writeByte(0);
		builder.writeShort(event.getGroundItem().getItem().getId());
		event.getPlayer().getSession().write(builder.toMessage());
	}
	
	public void closeInterface(Player player, int window, int pos) {
		MessageBuilder builder = new MessageBuilder(149);
		frameIndex++;
		builder.writeShort(frameIndex);
		builder.writeShort(window);
		builder.writeShort(pos);
		player.getSession().write(builder.toMessage());
	}

}