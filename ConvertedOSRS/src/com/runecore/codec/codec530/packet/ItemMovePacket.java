package com.runecore.codec.codec530.packet;

import com.runecore.codec.PacketCodec;
import com.runecore.env.Context;
import com.runecore.env.groovy.GroovyScript;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.player.Player;
import com.runecore.network.io.Message;

public class ItemMovePacket implements GroovyScript, PacketCodec {

	@Override
	public void execute(Player p, Message m) {
		int fromId;
		int toId;
		int id;
		switch(m.getOpcode()) {
			case 231:
				fromId = m.readShort();
				m.readLEShort();
				id = m.readLEShort();
				toId = m.readShortA();
				m.readByteS();
				switch(id) {
					case 149:
						if(fromId < 0 || fromId >= 28 || toId < 0 || toId >= 28) {
							break;
						}
						Item from = p.get("i").get(fromId);
						Item to = p.get("i").get(toId);
						p.get("i").set(fromId, to);
						p.get("i").set(toId, from);
				break;
				case 179:
					id = m.readInt() >> 16;
					m.readInt();
					fromId = m.readShort() & 0xFFFF;
					toId = m.readLEShort() & 0xFFFF;
					if(id == 763) {
						if(fromId < 0 || fromId >= 28 || toId < 0 || toId >= 28) {
							break;
						}
						from = p.get("i").get(fromId);
						to = p.get("i").get(toId);
						p.get("i").set(fromId, to);
						p.get("i").set(toId, from);
					}
				break;
			}
		}
		
	}

	@Override
	public void init(Context context) {
		context.register(179, this);
		context.register(231, this);
	}

}
