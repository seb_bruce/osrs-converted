package com.runecore.codec.codec530.packet;

import java.util.List;

import com.runecore.codec.PacketCodec;
import com.runecore.codec.event.SendGroundItemEvent;
import com.runecore.codec.event.SendMessageEvent;
import com.runecore.env.Context;
import com.runecore.env.content.consumable.ConsumableManager;
import com.runecore.env.content.consumable.Food;
import com.runecore.env.content.consumable.Potion;
import com.runecore.env.content.shops.Shop;
import com.runecore.env.content.shops.ShopManager;
import com.runecore.env.groovy.GroovyScript;
import com.runecore.env.model.container.Bank;
import com.runecore.env.model.container.Container;
import com.runecore.env.model.container.Equipment;
import com.runecore.env.model.flag.UpdateFlag;
import com.runecore.env.model.item.GroundItem;
import com.runecore.env.model.item.Item;
import com.runecore.env.model.item.LevelRequirement;
import com.runecore.env.model.player.Looks;
import com.runecore.env.model.player.Player;
import com.runecore.env.model.player.Skills;
import com.runecore.env.world.World;
import com.runecore.network.io.Message;

public class ItemInteractionPacket implements GroovyScript, PacketCodec {
	
	@Override
	public void init(Context context) {
		context.register(55, this);
		context.register(81, this);
		context.register(156, this);
		context.register(135, this);
		context.register(196, this);
		context.register(124, this);
		context.register(199, this);
		context.register(234, this);
		context.register(168, this);
	}
	
	@Override
	public void execute(Player player, Message message) {
		if(player.isDead())
			return;
		player.distanceEvent(null);
		if(message.getOpcode() == 55) {
			handleEquipping(player, message);
		} else if(message.getOpcode() == 81) {
			handleRemoveItem(message, player);
		} else if(message.getOpcode() == 156) {
			handleActionClick(message, player);
		} else if(message.getOpcode() == 135) {
			handleDropItem(message, player);
		} else if(message.getOpcode() == 196) {
			handleClickTwo(player, message);
		} else if(message.getOpcode() == 124) {
			handleClickThree(player, message);
		} else if(message.getOpcode() == 199) {
			handleClickFour(player, message);
		} else if(message.getOpcode() == 234) {
			handleClickFive(player, message);
		} else if(message.getOpcode() == 168) {
			handleClickSix(player, message);
		}
	}
	
	private void handleClickTwo(Player player, Message m) {
		int interfaceId = m.readShort();
		int child = m.readShort();
		int slot = m.readShort();
		System.out.println("inter "+interfaceId+" child "+child+" slot "+slot);
		if(interfaceId == 763) {
			Bank.deposit(player, slot, player.get("i").get(slot).getId(), 5);
		}
		if(interfaceId == 762) {
			
		}
		if(interfaceId == 620) {
			Shop openShop = player.getAttribute("shop", null);
			if(openShop == null)
				return;
			ShopManager.get().getAdapter(openShop).buyItem(player, openShop, slot, 1);
		} else if(interfaceId == 621) {
			Shop openShop = player.getAttribute("shop", null);
			if(openShop == null)
				return;
			ShopManager.get().getAdapter(openShop).sellItem(player, openShop, slot, 1);
		}
	}
	
	private void handleClickThree(Player player, Message m) {
		int interfaceId = m.readShort();
		@SuppressWarnings("unused")
		int child = m.readShort();
		int slot = m.readShort();
		if(interfaceId == 763) {
			Bank.deposit(player, slot, player.get("i").get(slot).getId(), 10);
		}
		if(interfaceId == 762) {
			
		}
		if(interfaceId == 620) {
			Shop openShop = player.getAttribute("shop", null);
			if(openShop == null)
				return;
			ShopManager.get().getAdapter(openShop).buyItem(player, openShop, slot, 5);
		} else if(interfaceId == 621) {
			Shop openShop = player.getAttribute("shop", null);
			if(openShop == null)
				return;
			ShopManager.get().getAdapter(openShop).sellItem(player, openShop, slot, 5);
		}
	}
	
	private void handleClickFour(Player player, Message packet) {
		int interfaceId = packet.readShort();
		@SuppressWarnings("unused")
		int child = packet.readShort();
		int slot = packet.readShort();
		if(interfaceId == 763) {
			Bank.deposit(player, slot, player.get("i").get(slot).getId(), 100);
		}
		if(interfaceId == 762) {
			
		}
		if(interfaceId == 620) {
			Shop openShop = player.getAttribute("shop", null);
			if(openShop == null)
				return;
			ShopManager.get().getAdapter(openShop).buyItem(player, openShop, slot, 10);
		} else if(interfaceId == 621) {
			Shop openShop = player.getAttribute("shop", null);
			if(openShop == null)
				return;
			ShopManager.get().getAdapter(openShop).sellItem(player, openShop, slot, 10);
		}
	}
	
	private void handleClickFive(Player player, Message packet) {
		int interfaceId = packet.readShort();
		@SuppressWarnings("unused")
		int child = packet.readShort();
		int slot = packet.readShort();
		if(interfaceId == 620) {
			Shop openShop = player.getAttribute("shop", null);
			if(openShop == null)
				return;
			ShopManager.get().getAdapter(openShop).buyItem(player, openShop, slot, 50);
		} else if(interfaceId == 621) {
			Shop openShop = player.getAttribute("shop", null);
			if(openShop == null)
				return;
			ShopManager.get().getAdapter(openShop).sellItem(player, openShop, slot, 50);
		}
	}
	
	private void handleClickSix(Player player, Message packet) {
		int interfaceId = packet.readShort();
		@SuppressWarnings("unused")
		int child = packet.readShort();
		int slot = packet.readShort();
		if(interfaceId == 763) {
			Bank.deposit(player, slot, player.get("i").get(slot).getId(), Integer.MAX_VALUE);
		}
		if(interfaceId == 762) {
			
		}
		if(interfaceId == 620) {
			Shop openShop = player.getAttribute("shop", null);
			if(openShop == null)
				return;
			ShopManager.get().getAdapter(openShop).buyItem(player, openShop, slot, 100);
		} else if(interfaceId == 621) {
			Shop openShop = player.getAttribute("shop", null);
			if(openShop == null)
				return;
			ShopManager.get().getAdapter(openShop).sellItem(player, openShop, slot, 100);
		}
	}
	
	
	private void handleDropItem(Message m, Player s) {
		Player p = s;
		m.readShortA(); //item id
		int slot = m.readShortA();
		m.readLEShort(); //interface
		if(slot < 0 || slot >= 28 || p.get("i").get(slot) == null) {
			return;
		}
		Item slotItem = p.get("inventory").get(slot);
		GroundItem groundItem = new GroundItem(p, slotItem, p.getLocation().clone());
		World.get().register(groundItem);
		p.get("inventory").set(slot, null);
	}
	
	@SuppressWarnings("unused")
	private void handleActionClick(Message m, Player p) {
		int slot = m.readLEShortA();
		int id = m.readShortA();
		int childId = m.readLEShort();
		int interfaceId = m.readLEShort();
		if (slot > 28 || slot < 0 || p.isDead()) {
			return;
		}
		if(interfaceId == 149) { //inventory
			 Item item = p.get("i").get(slot);
			 if(item == null)
				 return;
			 Food food = Food.get(item.getId());
			 if(food != null) {
				 ConsumableManager.consume(p, food, slot);
				 return;
			 }
			 Potion potion = Potion.get(item.getId());
			 if(potion != null) {
				 ConsumableManager.consume(p, potion, slot);
				 return;
			 }
		}
	}
	
	private void handleEquipping(Player player, Message message) {
		@SuppressWarnings("unused")
		int id = message.readLEShort();
		int slot = message.readShortA();
		@SuppressWarnings("unused")
		int inter = message.readInt();
		if(slot < 0 || slot >= 28) {
			return;
		}
		Item wieldItem = player.get("inv").get(slot);
		if(wieldItem == null) {
			return;
		}
		int wieldSlot = wieldItem.getDefinition().getEquipmentSlot();
		if(wieldSlot == -1) {
			return;
		}
		List<LevelRequirement> reqs = wieldItem.getDefinition().getLevelReqs();
		for(LevelRequirement lr : reqs) {
			if(!lr.meetsRequirement(player)) {
				Context.get().getActionSender().sendMessage(new SendMessageEvent(player, "You need a"+(lr.getLevelIndex() == 0 ? "n": "")+" "+Skills.SKILL_NAME[lr.getLevelIndex()]+" level of "+lr.getLevel()+" to wear this item."));
				return;
			}
		}
		boolean twoHanded = wieldItem.getDefinition().isTwoHanded();
		Container inventory = player.get("inv");
		Container equipment = player.get("equip");
		if(twoHanded && inventory.freeSlots() < 1 && equipment.get(5) != null) {
			Context.get().getActionSender().sendMessage(new SendMessageEvent(player, "Not enough free space in your inventory."));
			return;
		}
		if(twoHanded && equipment.get(Looks.SLOT_SHIELD) != null) {
			inventory.add(equipment.get(Looks.SLOT_SHIELD));
			equipment.set(Looks.SLOT_SHIELD, null);
		}
		inventory.set(slot, null);
		if(wieldSlot == 3) {
			player.getCombatDefinition().setSpecialActivated(false);
			player.getCombatDefinition().refreshSpecial();
			if(twoHanded && equipment.get(5) != null) {
				player.getCombatDefinition().setSpecialActivated(false);
				player.getCombatDefinition().refreshSpecial();
				if(!inventory.add(equipment.get(5))) {
					inventory.add(equipment.get(5));
					return;
				}
				equipment.set(5, null);
			}
		} else if(wieldSlot == 5) {
			if(Equipment.isTwoHanded(equipment.get(3).getDefinition()) && equipment.get(3) != null) {
				if(!inventory.add(equipment.get(3))) {
					player.getCombatDefinition().setSpecialActivated(false);
					player.getCombatDefinition().refreshSpecial();
					inventory.add(equipment.get(3));
					return;
				}
				equipment.set(3, null);
			}
		}
		if(equipment.get(wieldSlot) != null && (wieldItem.getId() != equipment.get(wieldSlot).getDefinition().getId() || !wieldItem.getDefinition().isStackable())) {
			inventory.set(slot, equipment.get(wieldSlot));
			equipment.set(wieldSlot, null);
		}
		int oldAmt = 0;
		if(equipment.get(wieldSlot) != null) {
			oldAmt = equipment.get(wieldSlot).getAmount();
		}
		Item item2 = new Item(wieldItem.getId(), oldAmt+wieldItem.getAmount(), wieldItem.getDegradeCount());
		equipment.set(wieldSlot, item2);
		player.getCombatDefinition().refresh();
		player.getFlagManager().flag(UpdateFlag.APPERANCE);
	}
	
	private void handleRemoveItem(Message m, Player p) {
		int slot = m.readShortA();
		int itemId = m.readShort();
		m.readShort();
		int widget = m.readShort();
		Item slotItem = p.get("e").get(slot);
		if(slotItem == null || slotItem.getId() != itemId) {
			return;
		}
		if(widget == 387) {
			if(!p.get("i").add(slotItem)) {
				Context.get().getActionSender().sendMessage(new SendMessageEvent(p, "You don't have enough space in your inventory."));
				return;
			}
			p.get("e").set(slot, null);
			p.getFlagManager().flag(UpdateFlag.APPERANCE);
		}
	}
	
}
