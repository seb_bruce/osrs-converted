package com.runecore.codec.codec530.packet;

import com.runecore.codec.PacketCodec;
import com.runecore.env.Context;
import com.runecore.env.model.player.Player;
import com.runecore.env.widget.WidgetAdapterRepository;
import com.runecore.network.io.Message;

public class WidgetInteractionPacket implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		if(player.isLocked() || player.isDead())
			return;
		player.distanceEvent(null);
		int inter = message.readShort() & 0xFFFF;
		int child = message.readShort() & 0xFFFF;
		int buttonId2 = 0;
		if(message.getLength() >= 6) {
			buttonId2 = message.readShort() & 0xFFFF;
		}
		if(buttonId2 == 65535) {
			buttonId2 = 0;
		}
		int[] data = {
			inter, child, buttonId2, message.getOpcode()
		};
		WidgetAdapterRepository adapters = Context.get().getWidgetAdapters().get(inter);
		if(adapters != null) {
			adapters.handle(player, data);
		}
	}

}