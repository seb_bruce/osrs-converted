package com.runecore.codec.codec530.packet;

import com.runecore.codec.Commands;
import com.runecore.codec.PacketCodec;
import com.runecore.codec.event.SendInterfaceEvent;
import com.runecore.codec.event.SendMessageEvent;
import com.runecore.env.Context;
import com.runecore.env.content.shops.ShopManager;
import com.runecore.env.model.container.Bank;
import com.runecore.env.model.flag.Animation;
import com.runecore.env.model.flag.Graphic;
import com.runecore.env.model.npc.NPC;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;
import com.runecore.env.world.World;
import com.runecore.network.io.Message;

public class CommandPacket implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		if(player.isLocked() || player.isDead())
			return;
		String command = message.readRS2String().toLowerCase();
		String[] split = command.split(" ");
		Commands.handle(player, split);
		if(split[0].equalsIgnoreCase("npc")) {
			World.get().register(new NPC(player.getLocation().clone(), Integer.parseInt(split[1])));
			Context.get().getActionSender().sendMessage(new SendMessageEvent(player, "Spawned npc"));
		}
		if(split[0] == "shop") {
			ShopManager.get().getShop(1).open(player);
		}
		if(split[0] == "pos") {
			Context.get().getActionSender().sendMessage(new SendMessageEvent(player, player.getLocation().toString()));
		}
		if(split[0] == "home") {
			player.teleport(Location.locate(3093, 3493, 0));
		}
		if(split[0] == "ags") {
			player.animate(Animation.create(7074));
			player.graphics(Graphic.create(1222));
		}
		if(split[0] == "anim") {
			player.animate(Animation.create(Integer.parseInt(split[1])));
		}
		if(split[0].equalsIgnoreCase("bank")) {
			Bank.open(player);
		}
		if(split[0] == "refresh") {
			Context.get().refresh();
		}
		if(split[0] == "setlevel") {
			int index = Integer.parseInt(split[1]);
			int lvl = Integer.parseInt(split[2]);
			player.getSkills().hardSet(index, lvl);
		}
		if(split[0].equalsIgnoreCase("spec")) {
			player.getCombatDefinition().setSpecialAmount(10000);
			player.getCombatDefinition().refreshSpecial();
		}
		if(split[0].equalsIgnoreCase("empty")) {
			player.get("inv").clear();
		}
		if(split[0] == "ancients") {
			Context.get().getActionSender().sendInterface(new SendInterfaceEvent(player, 1, 548, 89, 193));
		}
		if(split[0].equalsIgnoreCase("lunars")) {
			Context.get().getActionSender().sendInterface(new SendInterfaceEvent(player, 1, 548, 89, 430));
		}
	}

}
