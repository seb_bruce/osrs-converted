package com.runecore.codec.codec530.packet;

import com.runecore.codec.PacketCodec;
import com.runecore.env.content.Interfaces;
import com.runecore.env.model.npc.NPC;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.World;
import com.runecore.network.io.Message;

public class NPCOptionPacket implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		if(player.isLocked() || player.isDead())
			return;
		Interfaces.closeInterfaces(player);
		player.distanceEvent(null);
		if(message.getOpcode() == 3) {
			handleAttack(player, message);
		} else if(message.getOpcode() == 78) {
			handleSecondClick(player, message);
		} else if(message.getOpcode() == 148) {
			handleThirdClick(player, message);
		} else if(message.getOpcode() == 239) {
			handleMagicAttack(player, message);
		}
	}
	
	private void handleAttack(Player player, Message message) {
		int index = message.readLEShortA();
		NPC n = (NPC) World.get().getNpcs().get(index);
		
	}
	
	private void handleSecondClick(Player player, Message message) {
		int index = message.readLEShortA();
		NPC n = (NPC) World.get().getNpcs().get(index);
	}
	
	private void handleThirdClick(Player player, Message message) {
		int index = message.readShortA();
		NPC n = (NPC) World.get().getNpcs().get(index);
	}
	
	private void handleMagicAttack(Player player, Message m) {
		int childId = m.readLEShort();
		int interfaceId = m.readLEShort();
		int junk = m.readShortA();
		int index = m.readLEShortA();
		NPC n = (NPC) World.get().getNpcs().get(index);
	}

}
