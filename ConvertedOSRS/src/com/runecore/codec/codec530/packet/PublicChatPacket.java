package com.runecore.codec.codec530.packet;

import com.runecore.codec.PacketCodec;
import com.runecore.env.model.flag.ChatMessage;
import com.runecore.env.model.player.Player;
import com.runecore.network.io.Message;
import com.runecore.util.ChatUtils;

public class PublicChatPacket implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		if(player.getFacade().isMuted()) {
			return;
		}
		int colour = message.readByte();
		int effects  = message.readByte();
		int size = message.getLength() - 2;
		int fx = colour << 8 + effects;
		byte[] chatData = new byte[size];
		chatData = ChatUtils.remaining(message);
		String unpacked = ChatUtils.textUnpack(chatData, size);
		byte[] packed = new byte[size];
		ChatUtils.textPack(packed, unpacked);
		player.getFlagManager().chatMessage(new ChatMessage(fx, unpacked, packed));
	}

}
