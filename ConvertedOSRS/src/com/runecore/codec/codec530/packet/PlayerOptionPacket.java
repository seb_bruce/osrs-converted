package com.runecore.codec.codec530.packet;

import com.runecore.codec.PacketCodec;
import com.runecore.env.content.Interfaces;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.World;
import com.runecore.network.io.Message;

public class PlayerOptionPacket implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		if(player.isLocked() || player.isDead())
			return;
		Interfaces.closeInterfaces(player);
		player.distanceEvent(null);
		if(message.getOpcode() == 68) {
			int index = message.readLEShortA();
			if(index < 0 || index >= 2000 || player.isDead()) {
				return;
			}
			Player victim = (Player) World.get().getPlayers().get(index);
		} else if(message.getOpcode() == 195) {
			message.readShortA();
			int id = message.readLEShort();
			int interfaceId = message.readLEShort();
			int index = message.readLEShortA();
			if(index < 0 || index >= 2000 || player.isDead()) {
				return;
			}
			Player victim = (Player) World.get().getPlayers().get(index);
			//System.out.println("Interface: "+interfaceId+" Id: "+id);
			
		}
	}

}
