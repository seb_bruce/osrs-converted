package com.runecore.codec.codec530.packet;

import com.runecore.codec.PacketCodec;
import com.runecore.env.content.Interfaces;
import com.runecore.env.content.event.impl.GroundItemEvent;
import com.runecore.env.model.item.GroundItem;
import com.runecore.env.model.map.pf.PathFinderExecutor;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;
import com.runecore.env.world.World;
import com.runecore.network.io.Message;

public class GroundItemPickupPacket implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		if(player.isLocked() || player.isDead())
			return;
		Interfaces.closeInterfaces(player);
		player.distanceEvent(null);
		int x = message.readLEShort();
		int id  = message.readShort();
		int y = message.readLEShortA();
		Location loc = Location.locate(x, y, player.getLocation().getZ());
		GroundItem groundItem = World.get().getGroundItem(player, loc, id);
		if(groundItem == null) {
			return;
		}
		PathFinderExecutor.walkTo(player, loc);
		player.distanceEvent(new GroundItemEvent(player, loc, groundItem));
	}

}
