package com.runecore.codec.codec530.packet;

import com.runecore.codec.Events;
import com.runecore.codec.PacketCodec;
import com.runecore.env.content.Interfaces;
import com.runecore.env.model.player.Player;
import com.runecore.network.io.Message;

public class WalkingPacket implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		if(player.isLocked() || player.isDead())
			return;
		Interfaces.closeInterfaces(player);
		player.distanceEvent(null);
		/*
		if (player.getWalking().isFrozen()) {
			player.getWalking().reset();
			Events.sendMsg(player, "A magical force stops you from moving!");
			return;
		}
		*/
 		int size = message.getLength();
		if(message.getOpcode() == 39) {
			size -= 14;
		}
		/*
		player.getWalking().reset();
		player.getCombatState().cancel();
		int steps = (size - 5) / 2;
		int[] pathX = new int[steps];
		int[] pathY = new int[steps];
		// writeWordBigEndianA = writeLEShortA
		boolean run = message.readByteA() == 1;
		int firstX = message.readShort() - (player.getLocation().getRegionX() - 6) * 8;
		int firstY = message.readShortA() - (player.getLocation().getRegionY() - 6) * 8;
		player.getWalking().setIsRunning(run);
		for(int i = 0; i < steps; i ++) {
			pathX[i] = message.readByteA();
			pathY[i] = message.readByteS();
		}
		player.getWalking().addToWalkingQueue(firstX, firstY);
		for(int i = 0; i < steps; i++) {
			pathX[i] += firstX;
			pathY[i] += firstY;
			player.getWalking().addToWalkingQueue(pathX[i], pathY[i]);
		}
		*/
	}

}
