package com.runecore.codec.codec530.packet;

import com.runecore.codec.PacketCodec;
import com.runecore.env.content.Interfaces;
import com.runecore.env.content.event.impl.GameObjectInteractionEvent;
import com.runecore.env.model.map.GameObject;
import com.runecore.env.model.map.ObjectOption;
import com.runecore.env.model.map.pf.PathFinderExecutor;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;
import com.runecore.network.io.Message;

public class ObjectInteractionPacket implements PacketCodec {

	@Override
	public void execute(Player player, Message message) {
		if(player.isLocked() || player.isDead())
			return;
		Interfaces.closeInterfaces(player);
		player.distanceEvent(null);
		if(message.getOpcode() == 254) {
			handleFirstOption(player, message);
		}
	}
	
	private void handleFirstOption(Player player, Message message) {
		int objectX = message.readLEShort();
		int objectId = message.readShortA() & 0xffff;
		int objectY = message.readShort();
		int z = player.getLocation().getZ();
		if (z > 3) {
			z = 0;
		}
		Location loc = Location.locate(objectX, objectY, z);
		GameObject object = new GameObject(objectId, loc);
		PathFinderExecutor.walkTo(player, loc);
		player.distanceEvent(new GameObjectInteractionEvent(player, object, ObjectOption.OPTION_1));
	}

}
