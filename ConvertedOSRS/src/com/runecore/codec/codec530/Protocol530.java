package com.runecore.codec.codec530;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jboss.netty.channel.ChannelPipeline;

import com.runecore.cache.fs.Cache;
import com.runecore.codec.ProtocolCodec;
import com.runecore.codec.codec530.components.AttackStyleTabAdapter;
import com.runecore.codec.codec530.components.LogoutAdapter;
import com.runecore.codec.codec530.components.PrayerAdapter;
import com.runecore.codec.codec530.components.RunOrbAdapter;
import com.runecore.codec.codec530.components.ShopComponentAdapter;
import com.runecore.codec.codec530.extras.MapData;
import com.runecore.codec.codec530.net.EventHandler;
import com.runecore.codec.codec530.packet.CommandPacket;
import com.runecore.codec.codec530.packet.GroundItemPickupPacket;
import com.runecore.codec.codec530.packet.ItemMovePacket;
import com.runecore.codec.codec530.packet.NPCOptionPacket;
import com.runecore.codec.codec530.packet.ObjectInteractionPacket;
import com.runecore.codec.codec530.packet.PlayerOptionPacket;
import com.runecore.codec.codec530.packet.PublicChatPacket;
import com.runecore.codec.codec530.packet.WalkingPacket;
import com.runecore.codec.codec530.packet.WidgetInteractionPacket;
import com.runecore.codec.codec530.update.NPCUpdate;
import com.runecore.codec.codec530.update.PlayerUpdating;
import com.runecore.codec.generic.net.HandshakeDecoder;
import com.runecore.env.Context;
import com.runecore.env.model.player.Player;
import com.runecore.env.widget.WidgetAdapter;
import com.runecore.env.world.loader.GSONPlayerAdapter;
import com.runecore.network.NetworkEncoder;

public class Protocol530 implements ProtocolCodec {
	
	private ExecutorService executor;

	@Override
	public void init(Context context) {
		try {
			//Cache.get().init();
			//MapData.init();
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.executor = Executors.newFixedThreadPool(10);
		context.setActionSender(new ActionSender());
		//initPackets(context);
		//assignComponents(context);
		//ActivePrayer.init();
		//CombatState.init();
		new ItemMovePacket().init(context);
		new PlayerUpdating().init(context);
		new NPCUpdate().init(context);
		context.setPlayerAdapter(new GSONPlayerAdapter());
		
	}
	
	private void initPackets(Context context) {
		context.register(44, new CommandPacket());
		context.register(215, new WalkingPacket());
		context.register(39, new WalkingPacket());
		context.register(77, new WalkingPacket());
		context.register(184, new WidgetInteractionPacket());
		context.register(155, new WidgetInteractionPacket());
		context.register(10, new WidgetInteractionPacket());
		context.register(132, new WidgetInteractionPacket());
		context.register(237, new PublicChatPacket());
		context.register(68, new PlayerOptionPacket());
		context.register(195, new PlayerOptionPacket());
		context.register(254, new ObjectInteractionPacket());
		context.register(66, new GroundItemPickupPacket());
		context.register(3, new NPCOptionPacket());
		context.register(78, new NPCOptionPacket());
		context.register(148, new NPCOptionPacket());
		context.register(239, new NPCOptionPacket());
	}
	
	private void assignComponents(Context context) {
		
		AttackStyleTabAdapter attack = new AttackStyleTabAdapter();
		context.register(92, attack);
		context.register(89, attack);
		context.register(88, attack);
		context.register(82, attack);
		context.register(78, attack);
		context.register(77, attack);
		context.register(76, attack);
		context.register(182, new LogoutAdapter());
		context.register(271, new PrayerAdapter());
		context.register(750, new RunOrbAdapter());
		context.register(620, new ShopComponentAdapter());
		context.register(621, new ShopComponentAdapter());
	}

	@Override
	public void setup(ChannelPipeline pipeline) {
		pipeline.addLast("decoder", new HandshakeDecoder());
		pipeline.addLast("encoder", new NetworkEncoder());
		pipeline.addLast("handler", new EventHandler(executor));
	}

	@Override
	public String[] scriptPaths() {
		return new String[] { 
				"./data/scripts/", "./data/scripts/530/", 
				"./data/scripts/530/packets/", "./data/scripts/530/widget/",
				"./data/scripts/530/object/"
		};
	}

}