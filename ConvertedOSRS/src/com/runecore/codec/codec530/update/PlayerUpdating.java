package com.runecore.codec.codec530.update;


import java.util.Iterator;

import com.runecore.codec.PlayerUpdateCodec;
import com.runecore.env.Context;
import com.runecore.env.groovy.GroovyScript;
import com.runecore.env.model.flag.ChatMessage;
import com.runecore.env.model.flag.UpdateFlag;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.World;
import com.runecore.network.io.Message.PacketType;
import com.runecore.network.io.MessageBuilder;

public class PlayerUpdating implements GroovyScript, PlayerUpdateCodec {
	
	@Override
	public void init(Context context) {
		context.setPlayerUpdateCodec(this);
	}

	public void update(Player player) {
		if(player.getFlagManager().isMapRegionChanged()) {
			Context.get().getActionSender().sendMapRegion(player, false);
		}
		MessageBuilder packet = new MessageBuilder(246, PacketType.VAR_SHORT);
		MessageBuilder block = new MessageBuilder();
		packet.startBitAccess();
		
		writeMyMovement(player, packet);
		writeBlock(player, block, false);
		
		packet.writeBits(8, player.getLocalPlayers().size());
		Iterator<Player> it$ = player.getLocalPlayers().iterator();
		while(it$.hasNext()) {
			Player p = it$.next();
			if(p != null && player.getLocation().withinDistance(p.getLocation(), 16) && World.get().getPlayers().contains(p) && !p.getFlagManager().isDidTele()) {
				writeLocalPlayerMovement(p, packet);
				writeBlock(p, block, false);
			} else {
				player.getPlayerExists()[p.getIndex()] = false;
				packet.writeBits(1, 1);
				packet.writeBits(2, 3);
				it$.remove();
			}
		}
		int added = 0;
		for(Player p : World.get().getPlayers()) {
			if(added >= 15 || player.getLocalPlayers().size() >= 250) {
				break;
			}
			if(p == null) {
				continue;
			}
			if(p.getIndex() == player.getIndex() || player.getPlayerExists()[p.getIndex()] || !player.getLocation().withinDistance(p.getLocation(), 16)) {
				continue;
			}
			added++;
			player.getLocalPlayers().add(p);
			player.getPlayerExists()[p.getIndex()] = true;
			writeNewLocalPlayer(p, player, packet);
			writeBlock(p, block, true);
		}
		if(block.position() > 0) {
			packet.writeBits(11, 2047);
		}
		packet.finishBitAccess();
		if(block.position() > 0) {
			packet.writeBytes(block.getBuffer());
		}
		player.getSession().write(packet.toMessage());
	}
	
	private void writeNewLocalPlayer(Player p, Player player, MessageBuilder builder) {
		builder.writeBits(11, p.getIndex());
		int yPos = p.getLocation().getY() - player.getLocation().getY();
		int xPos = p.getLocation().getX() - player.getLocation().getX();
		if(xPos < 0) {
			xPos += 32;
		}
		if(yPos < 0) {
			yPos += 32;
		}
		builder.writeBits(1, 1);
		builder.writeBits(5, xPos);
		builder.writeBits(3, 1);
		builder.writeBits(1, 1);
		builder.writeBits(5, yPos);
	}
	
	private void writeMyMovement(Player player, MessageBuilder packet) {
		if(player.getFlagManager().isDidTele() || player.getFlagManager().isMapRegionChanged()) {
			packet.writeBits(1, 1);
			packet.writeBits(2, 3);
			packet.writeBits(7, player.getLocation().getLocalY(player.getFlagManager().lastKnownRegion()));
			packet.writeBits(1, 1);
			packet.writeBits(2, player.getLocation().getZ());
			packet.writeBits(1, player.getFlagManager().updateNeeded() ? 1 : 0);
			packet.writeBits(7, player.getLocation().getLocalX(player.getFlagManager().lastKnownRegion()));
		} else {
			if(player.getWalking().getWalkDir() == -1) {
				packet.writeBits(1,  player.getFlagManager().updateNeeded() ? 1 : 0);
				if(player.getFlagManager().updateNeeded()) {
					packet.writeBits(2, 0);
				}
			} else {
				if(player.getWalking().getRunDir() != -1) {
					packet.writeBits(1, 1);
					packet.writeBits(2, 2);
					packet.writeBits(1, 1);
					packet.writeBits(3, player.getWalking().getWalkDir());
					packet.writeBits(3, player.getWalking().getRunDir());
					packet.writeBits(1, player.getFlagManager().updateNeeded() ? 1 : 0);
				} else {
					packet.writeBits(1, 1);
					packet.writeBits(2, 1);
					packet.writeBits(3, player.getWalking().getWalkDir());
					packet.writeBits(1, player.getFlagManager().updateNeeded() ? 1 : 0);
				}
			}
		}
	}
	
	private void writeLocalPlayerMovement(Player player, MessageBuilder builder) {
		if(player.getWalking().getWalkDir() == -1) {
			if(player.getFlagManager().updateNeeded()) {
				builder.writeBits(1, 1);
				builder.writeBits(2, 0);
			} else {
				builder.writeBits(1, 0);
			}
		} else if(player.getWalking().getRunDir() == -1) {
			builder.writeBits(1, 1);
			builder.writeBits(2, 1);
			builder.writeBits(3, player.getWalking().getWalkDir());
			builder.writeBits(1, player.getFlagManager().updateNeeded() ? 1 : 0);
		} else {
			builder.writeBits(1, 1);
			builder.writeBits(2, 2);
			builder.writeBits(1, 1);
			builder.writeBits(3, player.getWalking().getWalkDir());
			builder.writeBits(3, player.getWalking().getRunDir());
			builder.writeBits(1, player.getFlagManager().updateNeeded() ? 1 : 0);
		}
	}
	
	private void writeBlock(Player player, MessageBuilder builder, boolean force) {
		if(!player.getFlagManager().updateNeeded() && !force) {
			return;
		}
		int mask = 0;
		if(player.getFlagManager().flagged(UpdateFlag.CHAT_MESSAGE)) {
			mask |= 0x80;
		}
		if(player.getFlagManager().flagged(UpdateFlag.DAMAGE_ONE)) {
			mask |= 0x1;
		}
		if(player.getFlagManager().flagged(UpdateFlag.ANIMATION)) {
			mask |= 0x8;
		}
		if(force || player.getFlagManager().flagged(UpdateFlag.APPERANCE)) {
			mask |= 0x4;
		}
		if(player.getFlagManager().flagged(UpdateFlag.ENTITY_FOCUS)) {
			mask |= 0x2;
		}
		if(player.getFlagManager().flagged(UpdateFlag.FORCE_CHAT)) {
			mask |= 0x20;
		}
		if(player.getFlagManager().flagged(UpdateFlag.DAMAGE_TWO)) {
			mask |= 0x200;
		}
		if(player.getFlagManager().flagged(UpdateFlag.GRAPHICS)) {
			mask |= 0x100;
		}
		if(player.getFlagManager().flagged(UpdateFlag.LOCATION_FOCUS)) {
			mask |= 0x40;
		}
		if(mask >= 0x100) {
			mask |= 0x10;
			builder.writeByte((mask & 0xFF));
			builder.writeByte((mask >> 8));
		} else {
			builder.writeByte(mask);
		}
		if(player.getFlagManager().flagged(UpdateFlag.CHAT_MESSAGE)) {
			writeChatMask(player, builder);
		}
		if(player.getFlagManager().flagged(UpdateFlag.DAMAGE_ONE)) {
			int ratio =  player.getSkills().getLevel()[3] * 255 / player.getSkills().getRealLevel(3);
			if (player.getSkills().getLevel()[3] > player.getSkills().getRealLevel(3)) {
				ratio = player.getSkills().getRealLevel(3) * 255 / player.getSkills().getRealLevel(3);
			}
			builder.writeByte(player.getFlagManager().getFirstDamage().getHit());
			builder.writeByteA(player.getFlagManager().getFirstDamage().getType().ordinal());
			builder.writeByteS(ratio);
		}
		if(player.getFlagManager().flagged(UpdateFlag.ANIMATION)) {
			builder.writeShort(player.getFlagManager().getAnimation().getId());
			builder.writeByte(player.getFlagManager().getAnimation().getDelay());
		}
		if(force || player.getFlagManager().flagged(UpdateFlag.APPERANCE)) {
			MessageBuilder app = player.getLooks().generate(player);
			builder.writeByteA(app.position() & 0xFF);
			builder.writeBytes(app.getBuffer());
		}
		if(player.getFlagManager().flagged(UpdateFlag.ENTITY_FOCUS)) {
			builder.writeShortA(player.getFlagManager().getFocusOn() != null ? player.getFlagManager().getFocusOn().getTurnToIndex() : -1);
		}
		if(player.getFlagManager().flagged(UpdateFlag.FORCE_CHAT)) {
			builder.writeString(player.getFlagManager().getForceChat());
		}
		if(player.getFlagManager().flagged(UpdateFlag.DAMAGE_TWO)) {
			builder.writeByte(player.getFlagManager().getSecondDamage().getHit());
			builder.writeByteS(player.getFlagManager().getSecondDamage().getType().ordinal());
		}
		if(player.getFlagManager().flagged(UpdateFlag.GRAPHICS)) {
			builder.writeLEShort(player.getFlagManager().getGraphic().getId());
			builder.writeInt2(player.getFlagManager().getGraphic().getHeight() << 16 + player.getFlagManager().getGraphic().getDelay());
		}
		if(player.getFlagManager().flagged(UpdateFlag.LOCATION_FOCUS)) {
			int x = player.getFlagManager().getFaceLocation().getX();
			int y = player.getFlagManager().getFaceLocation().getY();
			builder.writeShort(x = 2 * x + 1);
			builder.writeLEShortA(y = 2 * y + 1);
		}
	}
	
	private void writeChatMask(Player player, MessageBuilder builder) {
		ChatMessage msg = player.getFlagManager().getChatMessage();
		builder.writeLEShort(msg.getEffect());
		builder.writeByte(player.getRights().ordinal());
		byte[] chatString = msg.getPacked();
		builder.writeByte(chatString.length);
		builder.writeBytesReverse(chatString, chatString.length, 0);
	}
	
}
