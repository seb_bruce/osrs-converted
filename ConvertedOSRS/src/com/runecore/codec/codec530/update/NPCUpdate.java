package com.runecore.codec.codec530.update;

import java.util.Iterator;

import com.runecore.codec.NPCUpdateCodec;
import com.runecore.env.Context;
import com.runecore.env.groovy.GroovyScript;
import com.runecore.env.model.flag.UpdateFlag;
import com.runecore.env.model.npc.NPC;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.World;
import com.runecore.network.io.Message.PacketType;
import com.runecore.network.io.MessageBuilder;

public class NPCUpdate implements NPCUpdateCodec, GroovyScript {

	@Override
	public void init(Context context) {
		context.setNpcUpdateCodec(this);
	}

	@Override
	public void update(Player player) {
		MessageBuilder packet = new MessageBuilder(32, PacketType.VAR_SHORT);
		MessageBuilder block = new MessageBuilder();
		packet.startBitAccess();
		packet.writeBits(8, player.getLocalNpcs().size());
		Iterator<NPC> it$ = player.getLocalNpcs().iterator();
		while(it$.hasNext()) {
			NPC npc = it$.next();
			if(npc != null && player.getLocation().withinDistance(npc.getLocation(), 16) && World.get().getNpcs().contains(npc) && !npc.getFlagManager().isDidTele() && !npc.getFacade().isHidden()) {
				applyMovement(npc, packet);
				if(npc.getFlagManager().updateNeeded()) {
					applyBlock(npc, block);
				}
			} else {
				packet.writeBits(1, 1);
				packet.writeBits(2, 3);
				it$.remove();
			}
		}
		int added = 0;
		for(NPC n : World.get().getNpcs()) {
			if(player.getLocalNpcs().size() >= 250 || added >= 5)
				break;
			if(n == null || n.getFacade().isHidden() || player.getLocalNpcs().contains(n) || !player.getLocation().withinDistance(n.getLocation(), 16)) {
				continue;
			}
			added++;
			addLocalNPC(player, n, packet);
			if(n.getFlagManager().updateNeeded()) {
				applyBlock(n, block);
			}
		}
		if(block.position() >= 3) {
			packet.writeBits(15, 32767);
		}
		packet.finishBitAccess();
		packet.writeBytes(block);
		player.getSession().write(packet.toMessage());
	}
	
	private static final byte[] XLATE_DIRECTION_TO_CLIENT = new byte[] { 1, 2, 4, 7, 6, 5, 3, 0 };
	
	private void applyMovement(NPC n, MessageBuilder packet) {
		if(n.getWalking().getWalkDir() == -1) {
			if(n.getFlagManager().updateNeeded()) {
				packet.writeBits(1, 1);
				packet.writeBits(2, 0);
			} else {
				packet.writeBits(1, 0);
			}
		} else if(n.getWalking().getRunDir() == -1) {
			packet.writeBits(1, 1);
			packet.writeBits(2, 1);
			packet.writeBits(3, XLATE_DIRECTION_TO_CLIENT[n.getWalking().getWalkDir()]); //do this in a sec
			packet.writeBits(1, n.getFlagManager().updateNeeded() ? 1 : 0);
		} else {
			packet.writeBits(1, 1);
			packet.writeBits(2, 2);
			packet.writeBits(3, n.getWalking().getWalkDir());
			packet.writeBits(3, n.getWalking().getRunDir());
			packet.writeBits(1, n.getFlagManager().updateNeeded() ? 1 : 0);
		}
	}
	
	private void addLocalNPC(Player p, NPC n, MessageBuilder packet) {
		p.getLocalNpcs().add(n);
		int y = n.getLocation().getY() - p.getLocation().getY();
		if(y < 0) {
			y += 32;
		}
		int x = n.getLocation().getX() - p.getLocation().getX();
		if(x < 0) {
			x += 32;
		}
		packet.writeBits(15, n.getIndex());
		packet.writeBits(1, 1);
		packet.writeBits(3, n.getFacade().getDirection()); //face direction IMPLEMENT THIS
		packet.writeBits(1, n.getFlagManager().updateNeeded() ? 1 : 0);
		packet.writeBits(5, y);
		packet.writeBits(14, n.getDefinition().getId());
		packet.writeBits(5, x);
	}
	
	//TODO GENERATE CACHE BEFORE UPDATING YO
	
	private void applyBlock(NPC npc, MessageBuilder block) {
		int mask = 0x0;
		if(npc.getFlagManager().flagged(UpdateFlag.DAMAGE_ONE)) {
			mask |= 0x40;
		}
		if(npc.getFlagManager().flagged(UpdateFlag.DAMAGE_TWO)) {
			mask |= 0x2;
		}
		if(npc.getFlagManager().flagged(UpdateFlag.ANIMATION)) {
			mask |= 0x10;
		}
		if(npc.getFlagManager().flagged(UpdateFlag.ENTITY_FOCUS)) {
			mask |= 0x4;
		}
		if(npc.getFlagManager().flagged(UpdateFlag.GRAPHICS)) {
			mask |= 0x80;
		}
		if(npc.getFlagManager().flagged(UpdateFlag.FORCE_CHAT)) {
			mask |= 0x20;
		}
		if(npc.getFlagManager().flagged(UpdateFlag.LOCATION_FOCUS)) {
			mask |= 0x200;
		}
		
		if (mask >= 0x100) {
			mask |= 0x8;
			block.writeByte((mask & 0xFF));
			block.writeByte((mask >> 8));
		} else {
			block.writeByte((mask & 0xFF));
		}
		
		if(npc.getFlagManager().flagged(UpdateFlag.DAMAGE_ONE)) {
			int ratio = npc.getFacade().getHitpoints() * 255 / npc.getDefinition().getHitpoints();
			block.writeByte(npc.getFlagManager().getFirstDamage().getHit());
			block.writeByteC(npc.getFlagManager().getFirstDamage().getType().ordinal());
			block.writeByteS(ratio);
		}
		if(npc.getFlagManager().flagged(UpdateFlag.DAMAGE_TWO)) {
			block.writeByteC(npc.getFlagManager().getSecondDamage().getHit());
			block.writeByteS(npc.getFlagManager().getSecondDamage().getType().ordinal());
		}
		if(npc.getFlagManager().flagged(UpdateFlag.ANIMATION)) {
			block.writeShort(npc.getFlagManager().getAnimation().getId());
			block.writeByte(npc.getFlagManager().getAnimation().getDelay());
		}
		if(npc.getFlagManager().flagged(UpdateFlag.ENTITY_FOCUS)) {
			block.writeShortA(npc.getFlagManager().getFocusOn().getTurnToIndex());
		}
		if(npc.getFlagManager().flagged(UpdateFlag.GRAPHICS)) {
			block.writeShortA(npc.getFlagManager().getGraphic().getId());
			block.writeLEInt(npc.getFlagManager().getGraphic().getHeight() << 16 + npc.getFlagManager().getGraphic().getDelay());
		}
		if(npc.getFlagManager().flagged(UpdateFlag.FORCE_CHAT)) {
			block.writeString(npc.getFlagManager().getForceChat());
		}
		if(npc.getFlagManager().flagged(UpdateFlag.LOCATION_FOCUS)) {
			int x = npc.getFlagManager().getFaceLocation().getX();
			int y = npc.getFlagManager().getFaceLocation().getY();
			block.writeShortA(x = 2 * x + 1);
			block.writeShort(y = 2 * y + 1);
		}
	}

}