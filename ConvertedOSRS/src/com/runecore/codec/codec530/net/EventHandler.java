package com.runecore.codec.codec530.net;

import java.security.SecureRandom;
import java.util.concurrent.ExecutorService;

import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

import com.runecore.codec.generic.AuthRequest;
import com.runecore.codec.generic.FileRequest;
import com.runecore.codec.generic.LoginKeyRequest;
import com.runecore.env.Context;
import com.runecore.env.login.LoginRequest;
import com.runecore.network.GameSession;
import com.runecore.network.io.Message;
import com.runecore.network.io.MessageBuilder;

public class EventHandler extends SimpleChannelHandler {

	private final ExecutorService service;

	public EventHandler(ExecutorService service) {
		this.service = service;
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
		
	}

	@Override
	public final void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
		if(e.getMessage() instanceof AuthRequest) {
			AuthRequest request = (AuthRequest)e.getMessage();
			System.out.println("Request: "+request.getRevision());
			if(request.getRevision() != 60) {
				ctx.getChannel().write(new MessageBuilder().writeByte(6).toMessage()).addListener(ChannelFutureListener.CLOSE);
			} else {
				ctx.getChannel().write(new MessageBuilder().writeByte(0).toMessage());
			}
		} else if (e.getMessage() instanceof LoginKeyRequest) {
			MessageBuilder builder = new MessageBuilder();
			builder.writeByte(0);
			builder.writeLong(new SecureRandom().nextLong());
			ctx.getChannel().write(builder.toMessage());
		} else if (e.getMessage() instanceof LoginRequest) {
			Context.get().getLoginProcessor().queue((LoginRequest) e.getMessage());
		} else if (e.getMessage() instanceof Message) {
			GameSession session = (GameSession) ctx.getAttachment();
			session.queue((Message) e.getMessage());
		} else if(e.getMessage() instanceof FileRequest) {
			service.submit((FileRequest)e.getMessage());
		}
	}
}