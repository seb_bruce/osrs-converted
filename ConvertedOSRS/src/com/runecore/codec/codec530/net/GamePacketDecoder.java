package com.runecore.codec.codec530.net;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import com.runecore.network.GameSession;
import com.runecore.network.io.Message;
import com.runecore.network.io.Message.PacketType;

public class GamePacketDecoder extends FrameDecoder {

	private static final int[] PACKET_LENGTHS = new int[256];

	static {
		for (int i = 0; i < 256; i++) {
			PACKET_LENGTHS[i] = -3;
		}
		PACKET_LENGTHS[0] = 0;
        PACKET_LENGTHS[1] = -3;
        PACKET_LENGTHS[2] = -3;
        PACKET_LENGTHS[3] = 2; // Attack NPC
        PACKET_LENGTHS[4] = -3;
        PACKET_LENGTHS[5] = -3;
        PACKET_LENGTHS[6] = -3;
        PACKET_LENGTHS[7] = -3;
        PACKET_LENGTHS[8] = -3;
        PACKET_LENGTHS[9] = -3;
        PACKET_LENGTHS[10] = 4; // Actionbuttons #2
        PACKET_LENGTHS[11] = -3;
        PACKET_LENGTHS[12] = -3;
        PACKET_LENGTHS[13] = -3;
        PACKET_LENGTHS[14] = -3;
        PACKET_LENGTHS[15] = -3;
        PACKET_LENGTHS[16] = -3;
        PACKET_LENGTHS[17] = -3;
        PACKET_LENGTHS[18] = -3;
        PACKET_LENGTHS[19] = -3;
        PACKET_LENGTHS[20] = -3;
        PACKET_LENGTHS[21] = 4; // Camera
        PACKET_LENGTHS[22] = 1; // Focus
        PACKET_LENGTHS[23] = 4; // Enter amount
        PACKET_LENGTHS[24] = -3;
        PACKET_LENGTHS[25] = -3;
        PACKET_LENGTHS[26] = -3;
        PACKET_LENGTHS[27] = 16; // Item on item
        PACKET_LENGTHS[28] = -3;
        PACKET_LENGTHS[29] = -3;
        PACKET_LENGTHS[30] = 2; // Fourth click NPC (trade slayermaster).
        PACKET_LENGTHS[31] = -3;
        PACKET_LENGTHS[32] = -3;
        PACKET_LENGTHS[33] = -3;
        PACKET_LENGTHS[34] = 8; // Add ignore
        PACKET_LENGTHS[35] = -3;
        PACKET_LENGTHS[36] = -3;
        PACKET_LENGTHS[37] = -3;
        PACKET_LENGTHS[38] = -3;
        PACKET_LENGTHS[39] = -1; // Walk
        PACKET_LENGTHS[40] = -3;
        PACKET_LENGTHS[41] = -3;
        PACKET_LENGTHS[42] = -3;
        PACKET_LENGTHS[43] = -3;
        PACKET_LENGTHS[44] = -1; // Command
        PACKET_LENGTHS[45] = -3;
        PACKET_LENGTHS[46] = -3;
        PACKET_LENGTHS[47] = -3;
        PACKET_LENGTHS[48] = -3;
        PACKET_LENGTHS[49] = -3;
        PACKET_LENGTHS[50] = -3;
        PACKET_LENGTHS[51] = -3;
        PACKET_LENGTHS[52] = -3;
        PACKET_LENGTHS[53] = 6; // Interface option #9
        PACKET_LENGTHS[54] = -3;
        PACKET_LENGTHS[55] = 8; // Equip item
        PACKET_LENGTHS[56] = -3;
        PACKET_LENGTHS[57] = 8; // Delete friend
        PACKET_LENGTHS[58] = -3;
        PACKET_LENGTHS[59] = -3;
        PACKET_LENGTHS[60] = -3;
        PACKET_LENGTHS[61] = -3;
        PACKET_LENGTHS[62] = -3;
        PACKET_LENGTHS[63] = -3;
        PACKET_LENGTHS[64] = 6; // Interface option #8
        PACKET_LENGTHS[65] = -3;
        PACKET_LENGTHS[66] = 6;  // Pick up item
        PACKET_LENGTHS[67] = -3;
        PACKET_LENGTHS[68] = 2; // Attack player
        PACKET_LENGTHS[69] = -3;
        PACKET_LENGTHS[70] = -3;
        PACKET_LENGTHS[71] = 2; // Trade player
        PACKET_LENGTHS[72] = -3;
        PACKET_LENGTHS[73] = -3;
        PACKET_LENGTHS[74] = -3;
        PACKET_LENGTHS[75] = 6; // Mouse click
        PACKET_LENGTHS[76] = -3;
        PACKET_LENGTHS[77] = -1; // Walk
        PACKET_LENGTHS[78] = 2; // Second click NPC
        PACKET_LENGTHS[79] = 12; // Swapping inventory places in shop, bank and duel
        PACKET_LENGTHS[80] = -3;
        PACKET_LENGTHS[81] = 8; // Unequip item
        PACKET_LENGTHS[82] = -3;
        PACKET_LENGTHS[83] = -3;
        PACKET_LENGTHS[84] = 6; // Object third click
        PACKET_LENGTHS[85] = -3;
        PACKET_LENGTHS[86] = -3;
        PACKET_LENGTHS[87] = -3;
        PACKET_LENGTHS[88] = -3;
        PACKET_LENGTHS[89] = -3;
        PACKET_LENGTHS[90] = -3;
        PACKET_LENGTHS[91] = -3;
        PACKET_LENGTHS[92] = 2; // Inventory item examine.
        PACKET_LENGTHS[93] = 0; // Ping
        PACKET_LENGTHS[94] = -3;
        PACKET_LENGTHS[95] = -3;
        PACKET_LENGTHS[96] = -3;
        PACKET_LENGTHS[97] = -3;
        PACKET_LENGTHS[98] = 4; // Toggle sound setting
        PACKET_LENGTHS[99] = -3;
        PACKET_LENGTHS[100] = -3;
        PACKET_LENGTHS[101] = -3;
        PACKET_LENGTHS[102] = -3;
        PACKET_LENGTHS[103] = -3;
        PACKET_LENGTHS[104] = 8; // Join clan chat
        PACKET_LENGTHS[105] = -3;
        PACKET_LENGTHS[106] = 2; // Follow player
        PACKET_LENGTHS[107] = -3;
        PACKET_LENGTHS[108] = -3;
        PACKET_LENGTHS[109] = -3;
        PACKET_LENGTHS[110] = 0; // Region loading, size varies
        PACKET_LENGTHS[111] = 2; // Grand Exchange item search
        PACKET_LENGTHS[112] = -3;
        PACKET_LENGTHS[113] = -3;
        PACKET_LENGTHS[114] = -3;
        PACKET_LENGTHS[115] = 10; // Use item on npc
        PACKET_LENGTHS[116] = -3;
        PACKET_LENGTHS[117] = -3;
        PACKET_LENGTHS[118] = -3;
        PACKET_LENGTHS[119] = -3;
        PACKET_LENGTHS[120] = 8; // Add friend
        PACKET_LENGTHS[121] = -3;
        PACKET_LENGTHS[122] = -3;
        PACKET_LENGTHS[123] = -3;
        PACKET_LENGTHS[124] = 6; //Interface option #3
        PACKET_LENGTHS[125] = -3;
        PACKET_LENGTHS[126] = -3;
        PACKET_LENGTHS[127] = -3;
        PACKET_LENGTHS[128] = -3;
        PACKET_LENGTHS[129] = -3;
        PACKET_LENGTHS[130] = -3;
        PACKET_LENGTHS[131] = -3;
        PACKET_LENGTHS[132] = 6; // Actionbuttons #3
        PACKET_LENGTHS[133] = -3;
        PACKET_LENGTHS[134] = 14; // Item on object
        PACKET_LENGTHS[135] = 8; // Drop item
        PACKET_LENGTHS[136] = -3;
        PACKET_LENGTHS[137] = 7; // Unknown, nothing major
        PACKET_LENGTHS[138] = -3;
        PACKET_LENGTHS[139] = -3;
        PACKET_LENGTHS[140] = -3;
        PACKET_LENGTHS[141] = -3;
        PACKET_LENGTHS[142] = -3;
        PACKET_LENGTHS[143] = -3;
        PACKET_LENGTHS[144] = -3;
        PACKET_LENGTHS[145] = -3;
        PACKET_LENGTHS[146] = -3;
        PACKET_LENGTHS[147] = -3;
        PACKET_LENGTHS[148] = 2; // Third click NPC
        PACKET_LENGTHS[149] = -3;
        PACKET_LENGTHS[150] = -3;
        PACKET_LENGTHS[151] = -3;
        PACKET_LENGTHS[152] = -3;
        PACKET_LENGTHS[153] = 8; // Inventory click item #2 (check RC pouch)
        PACKET_LENGTHS[154] = -3;
        PACKET_LENGTHS[155] = 6; // Actionbutton
        PACKET_LENGTHS[156] = 8; // Inventory click item (food etc)
        PACKET_LENGTHS[157] = 10; // Privacy options
        PACKET_LENGTHS[158] = -3;
        PACKET_LENGTHS[159] = -3;
        PACKET_LENGTHS[160] = -3;
        PACKET_LENGTHS[161] = 8; // Item right click option #1 (rub/empty)
        PACKET_LENGTHS[162] = 8; // Clan chat kick
        PACKET_LENGTHS[163] = -3;
        PACKET_LENGTHS[164] = -3;
        PACKET_LENGTHS[165] = -3;
        PACKET_LENGTHS[166] = 6; // Interface option #7
        PACKET_LENGTHS[167] = -3;
        PACKET_LENGTHS[168] = 6; // Interface option #6
        PACKET_LENGTHS[169] = -3;
        PACKET_LENGTHS[170] = -3;
        PACKET_LENGTHS[171] = -3;
        PACKET_LENGTHS[172] = -3;
        PACKET_LENGTHS[173] = -3;
        PACKET_LENGTHS[174] = -3;
        PACKET_LENGTHS[175] = -3;
        PACKET_LENGTHS[176] = -3;
        PACKET_LENGTHS[177] = 2; // Junk, no real purpose
        PACKET_LENGTHS[178] = -3;
        PACKET_LENGTHS[179] = -3;
        PACKET_LENGTHS[180] = 2; // Accept trade (chatbox)
        PACKET_LENGTHS[181] = -3;
        PACKET_LENGTHS[182] = -3;
        PACKET_LENGTHS[183] = -3;
        PACKET_LENGTHS[184] = 7; // Close interface
        PACKET_LENGTHS[185] = -3;
        PACKET_LENGTHS[186] = -3;
        PACKET_LENGTHS[187] = -3;
        PACKET_LENGTHS[188] = 9; // Clan ranks
        PACKET_LENGTHS[189] = -3;
        PACKET_LENGTHS[190] = -3;
        PACKET_LENGTHS[191] = -3;
        PACKET_LENGTHS[192] = -3;
        PACKET_LENGTHS[193] = -3;
        PACKET_LENGTHS[194] = 6; // Object second click
        PACKET_LENGTHS[195] = 8; // Magic on player
        PACKET_LENGTHS[196] = 6; // Interface option #2
        PACKET_LENGTHS[197] = -3;
        PACKET_LENGTHS[198] = -3;
        PACKET_LENGTHS[199] = 6; //Interface option #4
        PACKET_LENGTHS[200] = -3;
        PACKET_LENGTHS[201] = -1; // Send PM
        PACKET_LENGTHS[202] = -3;
        PACKET_LENGTHS[203] = -3;
        PACKET_LENGTHS[204] = -3;
        PACKET_LENGTHS[205] = -3;
        PACKET_LENGTHS[206] = 8; // Operate item
        PACKET_LENGTHS[207] = -3;
        PACKET_LENGTHS[208] = -3;
        PACKET_LENGTHS[209] = -3;
        PACKET_LENGTHS[210] = -3;
        PACKET_LENGTHS[211] = -3;
        PACKET_LENGTHS[212] = -3;
        PACKET_LENGTHS[213] = 8; // Delete ignore
        PACKET_LENGTHS[214] = -3;
        PACKET_LENGTHS[215] = -1; // Walk
        PACKET_LENGTHS[216] = -3;
        PACKET_LENGTHS[217] = -3;
        PACKET_LENGTHS[218] = 2; // Fifth click NPC
        PACKET_LENGTHS[219] = -3;
        PACKET_LENGTHS[220] = -3;
        PACKET_LENGTHS[221] = -3;
        PACKET_LENGTHS[222] = -3;
        PACKET_LENGTHS[223] = -3;
        PACKET_LENGTHS[224] = -3;
        PACKET_LENGTHS[225] = -3;
        PACKET_LENGTHS[226] = -3;
        PACKET_LENGTHS[227] = -3;
        PACKET_LENGTHS[228] = -3;
        PACKET_LENGTHS[229] = -3;
        PACKET_LENGTHS[230] = -3;
        PACKET_LENGTHS[231] = 9; // Swap item slot
        PACKET_LENGTHS[232] = -3;
        PACKET_LENGTHS[233] = -3;
        PACKET_LENGTHS[234] = 6; //Interface option #5
        PACKET_LENGTHS[235] = -3;
        PACKET_LENGTHS[236] = -3;
        PACKET_LENGTHS[237] = -1; // Public chat
        PACKET_LENGTHS[238] = -3;
        PACKET_LENGTHS[239] = 8; // Magic on NPC
        PACKET_LENGTHS[240] = -3;
        PACKET_LENGTHS[241] = -3;
        PACKET_LENGTHS[242] = -3;
        PACKET_LENGTHS[243] = 6; // Screen type (fullscreen, small HD etc)
        PACKET_LENGTHS[244] = 8; // Enter text
        PACKET_LENGTHS[245] = 0; // Idle logout
        PACKET_LENGTHS[246] = -3;
        PACKET_LENGTHS[247] = 6; // Object 4th option
        PACKET_LENGTHS[248] = -3;
        PACKET_LENGTHS[249] = -3;
        PACKET_LENGTHS[250] = -3;
        PACKET_LENGTHS[251] = -3;
        PACKET_LENGTHS[252] = -3;
        PACKET_LENGTHS[253] = -3;
        PACKET_LENGTHS[254] = 6; // First click object
        PACKET_LENGTHS[255] = -3;
	}

	/**
	 * GameSession for the GamePacketDecoder
	 */
	private final GameSession session;

	/**
	 * Construct the GamePacketDecoder
	 * 
	 * @param session
	 *            The GameSession for this GamePacketDecoder
	 */
	public GamePacketDecoder(GameSession session) {
		this.session = session;
		session.getChannel().getPipeline().getContext("handler")
				.setAttachment(session);
	}

	/**
	 * Decode the Game Packets
	 */
	@Override
	protected Object decode(ChannelHandlerContext chc, Channel chan,
			ChannelBuffer buffer) throws Exception {
		if (chc.getAttachment() == null) {
			chc.setAttachment(session);
		}
		if (buffer.readableBytes() > 1000) {
			chan.close();
			return null;
		}
		if (buffer.readable()) {
			int opcode = buffer.readUnsignedByte();
			int length = PACKET_LENGTHS[opcode];
			if (opcode < 0 || opcode > 255) {
				buffer.discardReadBytes();
				return null;
			}
			if (length == -1) {
				if (buffer.readable()) {
					length = buffer.readUnsignedByte();
				}
			}
			if (length <= buffer.readableBytes() && length > 0) {
				byte[] payload = new byte[length];
				buffer.readBytes(payload, 0, length);
				return new Message(opcode, PacketType.STANDARD,
						ChannelBuffers.wrappedBuffer(payload));
			}
		}
		return null;
	}

}