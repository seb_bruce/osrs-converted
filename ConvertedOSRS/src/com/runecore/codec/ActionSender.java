package com.runecore.codec;

import com.runecore.codec.event.RefreshLevelEvent;
import com.runecore.codec.event.RightClickOptionEvent;
import com.runecore.codec.event.SendAccessMaskEvent;
import com.runecore.codec.event.SendGroundItemEvent;
import com.runecore.codec.event.SendInterfaceConfigEvent;
import com.runecore.codec.event.SendInterfaceEvent;
import com.runecore.codec.event.SendItemContainerEvent;
import com.runecore.codec.event.SendMessageEvent;
import com.runecore.codec.event.SendPlayerOptionEvent;
import com.runecore.codec.event.SendSettingEvent;
import com.runecore.codec.event.SendStringEvent;
import com.runecore.codec.event.SendWindowPaneEvent;
import com.runecore.env.model.item.GroundItem;
import com.runecore.env.model.player.Player;
import com.runecore.env.world.Location;

/**
 * ActionSender.java
 * @author Harry Andreas<harry@runecore.org>
 * Feb 10, 2013
 */
public interface ActionSender {
    
	public void sendLogin(Player player);
    public void sendLogout(Player player);
    public void sendSetting(SendSettingEvent event);
    public void sendMessage(SendMessageEvent event);
    public void sendAccessMask(SendAccessMaskEvent event);
    public void refreshLevel(RefreshLevelEvent event);
    public void sendInterface(SendInterfaceEvent event);
    public void sendWindowPane(SendWindowPaneEvent event);
    public void sendLoginResponse(Player player);
    public void sendMapRegion(Player player, boolean login);
    public void refreshGameInterfaces(Player player);
    public void refreshAccessMasks(Player player);
    public void sendPlayerOption(SendPlayerOptionEvent event);
    public void sendItemContainer(SendItemContainerEvent event);
    public void sendString(SendStringEvent event);
    public void updateRunEnergy(Player player);
    public void sendTab(Player player, int tab, int inter);
    public void sendInterfaceConfig(SendInterfaceConfigEvent event);
    public void sendRightClickOptions(RightClickOptionEvent event);
    public void sendBlankClientScript(Player p, int script, int id);
    public void sendLocation(Player p, Location l);
    public void clearGroundItem(Player player, GroundItem groundItem);
    public void sendGroundItem(SendGroundItemEvent event);
    public void removeGroundItem(SendGroundItemEvent event);
    public void closeInterface(Player player, int window, int pos);
    public void sendClientScript2(Player player, int id2, int id, Object[] params, String types);

}
