package com.runecore.cache;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.oly.game.cache.Cache;
import org.oly.game.cache.definitions.CachedObjectDefinition;
import org.oly.game.cache.stream.InputStream;

import com.runecore.env.Context;
import com.runecore.env.model.map.GameObject;
import com.runecore.env.model.map.RegionClipping;
import com.runecore.env.world.Location;


/**
 * @author 'Mystic Flow <Steven@rune-server.org>
 */
public class LandscapeParser {

	private static Map<Integer, Boolean> activeMaps = new HashMap<Integer, Boolean>();
	
	public static void setMapLoaded(int regionId){
		activeMaps.put(regionId, Boolean.TRUE);
	}
	
	public static int hash(String str) {
		int hash = 0;
		for (int i = 0; i < str.length(); i++) {
			hash = str.charAt(i) + ((hash << 5) - hash);
		}
		return hash;
	}
	
	public static void init() {
		for(int i : Context.get().getXteas().keySet()) {
			parseLandscape(i);
		}
	}
	
	private static boolean parse(int area) {
		return true;
	}

	public static void parseLandscape(final int regionId) {
		if(activeMaps.get(regionId) != null || regionId < 1) {
			//System.out.println("??");
			return;
		}
		long a=System.currentTimeMillis();
		activeMaps.put(regionId, Boolean.TRUE);//we've attempted to load this region from cache
		Integer[] xteaKeys = Context.get().getXteas().get(new Integer(regionId));
		
		int regionX = (regionId >> 8) * 64;
		int regionY = (regionId & 0xff) * 64;
		int aX = ((regionX >> 3) / 8), bY = ((regionY >> 3) / 8);
		
		//very clever. see 317 client and cache for an insight into this.
		//in idx5, we use the XTEA keys to decrypt the landscape which is in byte[] array form.
		//there are two stores. lX_Y and mX_Y - standing for landscape and object map
		byte[] landContainerData = Cache.getCacheFileManagers()[5].getFileData(Cache.getCacheFileManagers()[5].getContainerId("l"+ aX +"_"+ bY), 0, xteaKeys);
				
		int mapArchiveId = Cache.getCacheFileManagers()[5].getContainerId("m" + aX + "_" + bY);
		if(mapArchiveId==-1) {
			//System.out.println("ok");
			return;
		}
		//System.out.println("AY");
		
		byte[] mapContainerData = Cache.getCacheFileManagers()[5].getFileData(Cache.getCacheFileManagers()[5].getContainerId("m"+ aX +"_"+bY), 0);
		
		byte[][][] mapSettings = new byte[4][64][64];//clipping laws
		
		if(mapArchiveId != -1) {
			InputStream mapStream = new InputStream(mapContainerData);
			//For all 4 heights, tiles 0-64 of both x and y, read a byte over and over. minimum bytes read: 16384. depending on value, do something: 
			for (int plane = 0; plane < 4; plane++) {
				for (int x = 0; x < 64; x++) {
					for (int y = 0; y < 64; y++) {
						while (true) {
							 int v = mapStream.readUnsignedByte();
                             if (v == 0) {
                                 break;
                             } else if (v == 1) {
                            	 mapStream.skip(1);
                                 break;
                             } else if (v <= 49) {
                            	 mapStream.skip(1);
                             } else if (v <= 81) {
                            	 mapSettings[plane][x][y] = (byte) (v - 49);
                             }
						}
					}
				}
			}
		}
		for (int plane = 0; plane < 4; plane++) {
			for (int x = 0; x < 64; x++) {
				for (int y = 0; y < 64; y++) {
					 if ((mapSettings[plane][x][y] & 1) == 1) {
                         int height = plane;
                         if ((mapSettings[1][x][y] & 2) == 2) {
                             height--;
                         }
                         int absX = x+regionX;
                         int absY = y+regionY;
                         if (height >= 0 && height <= 3) {
                        	 RegionClipping.addClipping(absX, absY, plane, 0x200000);
                         }
                     }
				}
			}
		}
		if (landContainerData != null) {
			InputStream landStream = new InputStream(landContainerData);
			int objectId = -1;
			int incr;
			while ((incr = landStream.readUnsignedSmart()) != 0) {
				objectId += incr;
				int location = 0;
				int incr2;
				while ((incr2 = landStream.readUnsignedSmart()) != 0) {
					location += incr2 - 1;
					int localX = (location >> 6 & 0x3f);
					int localY = (location & 0x3f);
					int plane = location >> 12;
					int objectData = landStream.readUnsignedByte();
					int type = objectData >> 2;
					int rotation = objectData & 0x3;
					if (localX < 0 || localX >= 64 || localY < 0 || localY >= 64)
						continue;
					if (objectId <= 0){
						//end of stream/error
						continue;
					}
					if(objectId > CachedObjectDefinition.objectDefinitions.length){
						//GUI.println(Logg.ERR, "invalid object "+objectId+" in object map");
						continue;
					}
					if (mapSettings != null && (mapSettings[1][localX][localY] & 2) == 2)
						plane--;
					if (plane >= 0 && plane <= 3) {
						
						Location loc = Location.locate(localX + regionX, localY + regionY, plane);
						//create a new object, add it, set it the fuck up, profit
						//Location loc = Location.locate(localX + x, localY + y, height);
						RegionClipping.addClipping(objectId, rotation, loc, type);

						//World.getWorld().getRegionManager().getRegionByLocation(loc).getGameObjects().add(object);
						//RegionClipping.addClipping(-1, rotation, loc, type);
						//RegionClipping.addClipping(loc.getX(), loc.getY(), loc.getZ(), rotation);
					}
				}
			}
			//for (GameObject deleted : World.getWorld().getObjectManager().getDeletedObjects()) {
			//	if (deleted != null) {
			//		RegionClipping.removeClipping(deleted);
			//	}
			//}
		}
	}
	
	public static byte[] unzip(String file) throws Exception {
		if(!new File(file).exists())
			return null;
		GZIPInputStream gis = new GZIPInputStream(new FileInputStream(file));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
	    int len;
	    while ((len = gis.read(buffer)) > 0) {
	    	baos.write(buffer, 0, len);
	    }
		gis.close();
		baos.close();
		return baos.toByteArray();
	}
	
    public static int readSmart2(ByteBuffer buffer) {
        int i = 0;
        int i_33_ = readSmart(buffer);
        while (i_33_ == 32767) {
            i_33_ = readSmart(buffer);
            i += 32767;
        }
        i += i_33_;
        return i;
    }
    
    public static int readSmart(ByteBuffer buffer) {
        int i = buffer.get() & 0xff;
        if (i >= 128)
            return buffer.getShort() - 32768;
        return buffer.get();
    }
}