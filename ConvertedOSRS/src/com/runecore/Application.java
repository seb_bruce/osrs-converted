package com.runecore;

import java.util.logging.Logger;

import com.runecore.codec.codec60.ProtocolOSRS60;
import com.runecore.env.Context;

/**
 * Application.java
 * @author Harry Andreas<harry@runecore.org> 
 * Feb 8, 2013
 */
public class Application {
	
	private static final Logger LOGGER = Logger.getLogger(Application.class.getName());
	public static final boolean DEBUG = Boolean.TRUE;
	public static boolean LOCAL_BOOT = Boolean.FALSE;
	public static String NAME = "OS-RSPS";

	/**
	 * Invoked on execution of the Application
	 * 
	 * @param args
	 *            The arguments for the application
	 */
	public static void main(String[] args) throws Exception {
		LOCAL_BOOT = System.getProperty("os.name").contains("Linux") ? false : true;
		LOGGER.info("Starting "+NAME+" ("+(LOCAL_BOOT ? "local" : "hosted")+")...");
		Context.set(new Context(new ProtocolOSRS60()));
		Context.get().configure();
		LOGGER.info("Context configuration completed, server is now online");
		Thread.currentThread().setName("GameEngine");
		Context.get().getGameEngine().run();
	}

}