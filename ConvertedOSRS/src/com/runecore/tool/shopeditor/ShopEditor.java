package com.runecore.tool.shopeditor;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.UIManager;

import com.runecore.env.content.shops.Shop;
import com.runecore.env.content.shops.ShopManager;
import com.runecore.env.model.def.ItemDefinition;
import com.runecore.env.model.item.Item;

public class ShopEditor extends JFrame {

	private static Map<Integer, Shop> shops = null;
	
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTable table;
	private static JList<String> list;
	
	private static int getNextIndex() {
		for(int i = 0; i < 2000; i++) {
			if(shops.get(i) == null)
				return i;
		}
		return -1;
	}
	
	public void reindex() {
		
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					shops = ShopManager.load();
					if(shops == null)
						shops = new HashMap<Integer, Shop>();
					System.err.println("Loaded "+shops.size()+" from json!");
					ItemDefinition.init();
					ShopEditor frame = new ShopEditor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public class ShopListModel implements ListModel<String> {

		@Override
		public void addListDataListener(ListDataListener l) {
			// TODO Auto-generated method stub
		}

		@Override
		public String getElementAt(int index) {
			Shop shop = ShopEditor.shops.get(index);
			if(shop == null)
				return "Null Shop";
			return shop.getId()+" - "+shop.getName();
		}

		@Override
		public int getSize() {
			// TODO Auto-generated method stub
			return ShopEditor.shops.size();
		}

		@Override
		public void removeListDataListener(ListDataListener l) {
			// TODO Auto-generated method stub
		}
		
	}
	
	public static Shop getCurrentShop() {
		int index = list.getSelectedIndex();
		Shop s = shops.get(index);
		return s;
	}
	
	private class ShopTable extends AbstractTableModel {

			@Override
			public int getColumnCount() {
				return 3;
			}

			@Override
			public int getRowCount() {
				Shop s = getCurrentShop();
				if(s == null || s.getItems() == null)
					return 0;
				return s == null ? 0 : s.getItems().size();
			}

			@Override
			public Object getValueAt(final int row, final int column) {
				Shop s = getCurrentShop();
				if(s == null)
					return null;
				Item item = s.getItems().get(row);
				if(column == 1){
					return item.getId();
				} else if(column == 2) {
					return item.getAmount();
				} else if(column == 0) 
					return item.getDefinition().getName();
				return null;
			}
			
			@Override
			public void setValueAt(Object aValue, int rowIndex, int column) {
				Shop s = getCurrentShop();
				if(s == null)
					return;
				int value = Integer.parseInt((String)aValue);
				Item item = s.getItems().get(rowIndex);
				if(column == 1){
					item.setId(value);
				} else if(column == 2) {
					item.setAmount(value);
				}
				ShopManager.save(shops);
			}
			
			@Override
			public String getColumnName(final int column) {
				switch (column) {
				case 0:
					return "Name";
				case 1:
					return "ID";
				case 2:
					return "Amount";
				}
				return null;
			}
			
			@Override
			public boolean isCellEditable(int row, int cell) {
				if(cell == 0)
					return false;
				return true;
			}
	    	
	    }
	   

	/**
	 * Create the frame.
	 */
	public ShopEditor() {
		setTitle("Vendetta Shop Editor");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 383);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JScrollPane scrollPane_1 = new JScrollPane();
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JLabel lblId = new JLabel("ID:");
		
		JLabel lblAmount = new JLabel("Amount:");
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		
		JButton btnAddItem = new JButton("Add Item");
		btnAddItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Shop current = getCurrentShop();
				if(current == null)
					return;
				int item = Integer.parseInt(textField.getText());
				int amount = Integer.parseInt(textField_1.getText());
				current.getItems().add(new Item(item, amount));
				ShopManager.save(shops);
				textField.setText("");
				textField_1.setText("");
				table.revalidate();
			}	
		});
		
		JButton btnDelSelected = new JButton("Del Selected");
		
		btnDelSelected.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Shop current = getCurrentShop();
				if(current == null)
					return;
				int row = table.getSelectedRow();
				if(row == -1)
					return;
				current.getItems().remove(row);
				ShopManager.save(shops);
				table.revalidate();
			}
		});
		
		JButton btnNewShop = new JButton("New Shop");
		btnNewShop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String name = JOptionPane.showInputDialog ( 
						   null, "Enter Shop Name:");
				int index = ShopEditor.getNextIndex();
				shops.put(index, new Shop(name, index));
				ShopManager.save(shops);
				list.setModel(new ShopListModel());
				table.revalidate();
			}
		});
		JButton btnDelShop = new JButton("Empty Shop");
		btnDelShop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Shop current = getCurrentShop();
				if(current == null)
					return;
				int delete = JOptionPane.showConfirmDialog(null, "Confirm Empty of Shop: "+current.getName()+"?", "Confirm Empty", JOptionPane.YES_NO_OPTION);
				if(delete == 0) {
					current.getItems().clear();
					table.revalidate();
					ShopManager.save(shops);
				}
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 378, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblAmount)
								.addComponent(lblId))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(textField)
								.addComponent(textField_1))
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnAddItem, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnDelSelected, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnDelShop, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnNewShop, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
					.addContainerGap(9, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 325, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblId)
								.addComponent(btnAddItem)
								.addComponent(btnNewShop))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblAmount)
								.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnDelSelected)
								.addComponent(btnDelShop))))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		table = new JTable();
		table.setModel(new ShopTable());
		scrollPane_1.setViewportView(table);
		
		list = new JList<String>();
		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				table.revalidate();
			}
		});
		list.setModel(new ShopListModel());
		scrollPane.setViewportView(list);
		contentPane.setLayout(gl_contentPane);
	}
}
