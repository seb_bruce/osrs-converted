package com.runecore.tool.autospawn;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

public class NPCAutoSpawnTool extends JFrame {
	
	public static List<NPCAutoSpawn> spawns;

	private JPanel contentPane;
	private JTextField xField;
	private JTextField yField;
	private JTextField zField;
	private JTextField idField;
	private JTextField dirField;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		spawns = NPCAutoSpawn.load();
		if(spawns == null)
			spawns = new ArrayList<NPCAutoSpawn>();
		System.err.println("Loaded "+spawns.size()+" autospawns from file");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					JFrame.setDefaultLookAndFeelDecorated(true);
					NPCAutoSpawnTool frame = new NPCAutoSpawnTool();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
    private class AutospawnTable extends AbstractTableModel {

		@Override
		public int getColumnCount() {
			return 5;
		}

		@Override
		public int getRowCount() {
			return spawns.size();
		}

		@Override
		public Object getValueAt(final int row, final int column) {
			NPCAutoSpawn data = spawns.get(row);
			if(column == 0){
				return data.getId();
			} else if(column == 1) {
				return data.getX();
			} else if(column == 2) {
				return data.getY();
			} else if(column == 3) {
				return data.getZ();
			} else if(column == 4) {
				return data.getDirection();
			}
			return null;
		}
		
		@Override
		public void setValueAt(Object aValue, int rowIndex, int column) {
			int value = Integer.parseInt((String)aValue);
			NPCAutoSpawn data = spawns.get(rowIndex);
			if(column == 0){
				data.setId(value);
			} else if(column == 1) {
				data.setX(value);
			} else if(column == 2) {
				data.setY(value);
			} else if(column == 3) {
				data.setZ(value);
			} else if(column == 4) {
				data.setDirection(value);
			}
			NPCAutoSpawn.save(spawns);
		}
		
		@Override
		public String getColumnName(final int column) {
			switch (column) {
			case 0:
				return "ID";
			case 1:
				return "Loc X";
			case 2:
				return "Loc Y";
			case 3:
				return "Loc Z";
			case 4:
				return "Dir";
			}
			return null;
		}
		
		@Override
		public boolean isCellEditable(int row, int cell) {
			return true;
		}
    	
    }

	/**
	 * Create the frame.
	 */
	public NPCAutoSpawnTool() {
		setTitle("Vendetta NPC AutoSpawner");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 515, 329);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		
		xField = new JTextField();
		xField.setHorizontalAlignment(SwingConstants.CENTER);
		xField.setText("X");
		xField.setColumns(10);
		
		yField = new JTextField();
		yField.setHorizontalAlignment(SwingConstants.CENTER);
		yField.setText("Y");
		yField.setColumns(10);
		
		zField = new JTextField();
		zField.setHorizontalAlignment(SwingConstants.CENTER);
		zField.setText("Z");
		zField.setColumns(10);
		
		idField = new JTextField();
		idField.setText("ID");
		idField.setHorizontalAlignment(SwingConstants.CENTER);
		idField.setColumns(10);
		
		dirField = new JTextField();
		dirField.setText("DIR");
		dirField.setHorizontalAlignment(SwingConstants.CENTER);
		dirField.setColumns(10);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int x = Integer.parseInt(xField.getText());
				int y = Integer.parseInt(yField.getText());
				int z = Integer.parseInt(zField.getText());
				int id = Integer.parseInt(idField.getText());
				int dir = Integer.parseInt(dirField.getText());
				NPCAutoSpawn spawn = new NPCAutoSpawn(x, y, z, id, dir);
				spawns.add(spawn);
				table.revalidate();
				NPCAutoSpawn.save(spawns);
				System.err.println("Saved "+spawns.size()+" to json file");
				xField.setText("");
				yField.setText("");
				zField.setText("");
				idField.setText("");
				dirField.setText("");
			}
		});
		
		JButton btnDeleteSel = new JButton("Delete Sel.");
		btnDeleteSel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int row = table.getSelectedRow();
				if(row == -1)
					return;
				spawns.remove(row);
				table.revalidate();
				NPCAutoSpawn.save(spawns);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
					.addGap(5))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(xField, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(yField, GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE)
					.addGap(2)
					.addComponent(zField, GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(idField, GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(dirField, GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnAdd, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnDeleteSel)
					.addGap(18))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(xField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(yField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(zField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(idField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(dirField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnAdd)
						.addComponent(btnDeleteSel))
					.addContainerGap())
		);
		
		table = new JTable();
		table.setModel(new AutospawnTable());
		scrollPane.setViewportView(table);
		contentPane.setLayout(gl_contentPane);
	}
}
