package com.runecore.tool.autospawn;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class NPCAutoSpawn {
	
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	public static List<NPCAutoSpawn> load() {
		List<NPCAutoSpawn> autospawns = null;
		String json = null;
		try {
			File file = new File("./data/autospawns.json");
			if(!file.exists())
				return null;
			FileReader reader = new FileReader(file);
			char[] chars = new char[(int) file.length()];
			reader.read(chars);
			json = new String(chars);
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		autospawns = gson.fromJson(json, new TypeToken<List<NPCAutoSpawn>>(){}.getType());
		return autospawns;
	}
	
	public static void save(List<NPCAutoSpawn> spawns) {
		try {
			FileWriter fw = new FileWriter("./data/autospawns.json");
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(gson.toJson(spawns));
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private int x, y, z, id, direction;
	
	public NPCAutoSpawn(int x, int y, int z, int id, int direction) {
		this.setX(x);
		this.setY(y);
		this.setZ(z);
		this.setId(id);
		this.setDirection(direction);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	public int getId() {
		return id;
	}

	public int getDirection() {
		return direction;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

}
