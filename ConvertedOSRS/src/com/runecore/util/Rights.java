package com.runecore.util;

public enum Rights {
	
	NORMAL,
	MODERATOR,
	ADMINISTRATOR;

}
