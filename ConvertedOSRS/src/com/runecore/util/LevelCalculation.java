package com.runecore.util;

import com.runecore.env.model.player.Player;

public class LevelCalculation {
	
	/**
	 * Where Xps[n] is the minimum experience for level n.
	 * e.g. Minimum xp for level 10 is Xps[10].
	 */
	public static int[] Xps = { -1, 0, 83, 174, 276, 388, 512, 650, 801, 969, 1154,
		1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470, 5018,
		5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363, 14833,
		16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648, 37224,
		41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014, 91721,
		101333, 111945, 123660, 136594, 150872, 166636, 184040, 203254,
		224466, 247886, 273742, 302288, 333804, 368599, 407015, 449428,
		496254, 547953, 605032, 668051, 737627, 814445, 899257, 992895,
		1096278, 1210421, 1336443, 1475581, 1629200, 1798808, 1986068,
		2192818, 2421087, 2673114, 2951373, 3258594, 3597792, 3972294,
		4385776, 4842295, 5346332, 5902831, 6517253, 7195629, 7944614,
		8771558, 9684577, 10692629, 11805606, 13034431, };
	
	/**
	 * Gets a level by experience.
	 * @param skill The skill id.
	 * @return The level.
	 */
	public static int getLevelForExperience(double xp) {
		int exp = (int)xp;
		for(int i = 99; i >= 0; i--){
			if(exp >= Xps[i]){
				return i;
			}
		}
		return 1;
	}
	
	public static int getXPForLevel(int level) {
		int points = 0;
		int output = 0;
		for (int lvl = 1; lvl <= level; lvl++) {
			points += Math.floor((double) lvl + 300.0
					* Math.pow(2.0, (double) lvl / 7.0));
			if (lvl >= level) {
				return output;
			}
			output = (int) Math.floor(points / 4);
		}
		return 0;
	}
	
	/**
	 * Gets a level by experience.
	 * @param skill The skill id.
	 * @return The level.
	 */
	public static int getLevelForExperience(int skill, Player p) {
		if(skill > 23 - 1)
			return 1;
		int exp = (int)p.getSkills().getXp()[skill+1];
		for(int i = 99; i >= 0; i--){
			if(exp >= Xps[i]){
				return i;
			}
		}
		return 1;
	}

}